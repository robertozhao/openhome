//
//  UnderLineButton.m
//  ScoreApprove
//
//  Created by Yang on 15/3/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "UnderLineButton.h"
#import "Macro.h"
#import "TextResource.h"
#define SingleW   15.0
#define padding   5.0

#define LineViewTag 9999999

@implementation UnderLineButton



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)awakeFromNib{
    
    
}

-(void)didMoveToSuperview{

    if (self.superview) {
        [self addLine];
    }else{
        [lineView removeFromSuperview];
    }
}


-(void)addLine{

    lineView = [[UIView alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y+self.frame.size.height, self.frame.size.width, 3)];
    lineView.tag = LineViewTag +self.tag;
    
    
    float lastW=self.frame.size.width;
    float cur_x=0;
    do {
        CGRect fram;
//        if (lastW < SingleW) {
            fram = CGRectMake(cur_x, 0, lastW, 1);
            cur_x += (lastW + padding);
            
            lastW -= (lastW + padding);
//        }else{
//            fram = CGRectMake(cur_x, 0, SingleW, 3);
//            cur_x += (SingleW + padding);
//            
//            lastW -= (SingleW + padding);
//        }
        
        UIView * line = [self line:fram];
        [lineView addSubview:line];
        


    } while (cur_x < self.frame.size.width);
    
//    CGRect fram_ = view.frame;
//    fram_.size.width = cur_x;
//    view.frame = fram_;
  
    [self.superview addSubview:lineView];
}

-(UIView *)line:(CGRect)fram{
    UIView* view = [[UIView alloc] initWithFrame:fram];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    UIColor *topColor = RGBCOLOR(200,200,200);
    UIColor *bottomColor = RGBCOLOR(250,250,250);
    gradient.colors = [NSArray arrayWithObjects:(id)[topColor CGColor], (id)[bottomColor CGColor], nil];
    [view.layer insertSublayer:gradient atIndex:0];

    view.backgroundColor = [UIColor redColor];
    return view;
}


-(void)judgeW:(NSString *)title{
    
//    NSString * title = [self titleForState:0];

    NSDictionary *attributes = @{NSFontAttributeName:self.titleLabel.font};
    CGRect rect_titleLb;
    
    if (self.tag == 7652) { //  phone num in lead detail
        if (!title || [title isEqualToString:@""]) {
            title = PhoneNumPlaceHoldeInLeadDetail;
            [self setTitleColor:[UIColor lightGrayColor] forState:0];
        }else{
            [self setTitleColor:[UIColor blackColor] forState:0];
        }
    }
    
    rect_titleLb  = [title boundingRectWithSize:CGSizeMake(MAXFLOAT,self.frame.size.height)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes
                                             context:nil];
    
    rect_titleLb.size.height = self.frame.size.height;
    if (self.tag == 98987) {
        if (rect_titleLb.size.width > 388) {
            rect_titleLb = [title boundingRectWithSize:CGSizeMake(388,MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
        }
    }else if (self.tag == 9){

        if (rect_titleLb.size.width > 745) {
            rect_titleLb = [title boundingRectWithSize:CGSizeMake(745,MAXFLOAT)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
        }
    }else if (self.tag  == 1){
        if (rect_titleLb.size.width > 620) {
            rect_titleLb.size.width  = 620;
        }
    }
    
    CGRect fram = self.frame;

    if (self.titleLabel.textAlignment == 2 ) {
        fram.origin.x += (self.frame.size.width-rect_titleLb.size.width);
    }
    
    //  button in leaddetail
    if (self.tag == 7654 || self.tag == 7653 || self.tag == 7652 || self.tag == 98987) {
        if (self.tag == 7654 || self.tag == 7653 || self.tag == 7652 ) {  // width has most value
            if (rect_titleLb.size.width > 640) {
                rect_titleLb.size.width = 640;
            }
        }
        fram.origin.x += ((self.frame.size.width-rect_titleLb.size.width)/2.0);
    }
    fram.size.height = rect_titleLb.size.height;
    fram.size.width = rect_titleLb.size.width;
    self.frame  = fram;
    
    [self setTitle:title forState:0];
    
    UIView * view = [self.superview viewWithTag:LineViewTag+self.tag];
    [view removeFromSuperview];
    
    [self addLine];
}

@end
