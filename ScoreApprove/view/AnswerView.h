//
//  AnswerView.h
//  ScoreApprove
//
//  Created by Yang on 15/4/25.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question.h"
#import "AnswerView.h"
#import "MMUTextFile.h"
#import "MAnswer.h"

#define ButtonTfBaseTag  999

#define AddressTfBaseTag 200

#define ButtonTypeBaseTag 300

#define RadioTypeBaseTag  400

#define ContactTypeBaseTag 500

NS_ENUM(int, QuestionType){

    QuestionTypeAdd,
    QuestionTypeEdit,
    QuestionTypeAppear
};

@protocol  AnswerViewDelegate <NSObject>
@optional

-(void)autoNextQuestion;
-(void)addOrEditItem:(MAnswer *)answer;

-(void)remarkSelfFrmeWithMMutextfile;
-(void)selectHasReply; // user to hide select require quetion has no answer
@end

@interface AnswerView : UIView<UITableViewDataSource,UITableViewDelegate,MMUTextFileDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    // select
    UITableView * select_tb;
    NSMutableArray * selectSource;
    NSMutableArray * selectSource_temp;
    
    //  buttons

    NSMutableArray * buttonTitles;
    UIScrollView * contentSV;

    //  select appear
    
    int indexSelect;
    
    //checkbox

    BOOL  isSelected;
    
    //password
    
    //address
    
    // text
    
    MMUTextFile * commenTextFile;
    
    BOOL isNewOption;
}

@property (nonatomic, strong) UIDatePicker * datePicker;

//  show first_name  last_name  email

@property (nonatomic) BOOL isContactInfo;


//button
// radio
@property (nonatomic , strong) UIButton * buttonSelect;

@property (nonatomic , strong) UIPickerView * pickerView;

@property (nonatomic , strong) UITextField * cur_tf;
@property (nonatomic , weak) id<AnswerViewDelegate>delegate;
@property (nonatomic) int questionType;
@property (nonatomic , strong) Question * q;

- (instancetype)initWithFrame:(CGRect)frame;
-(void)initSubView:(Question *)q;
-(void)initSubViewWithoutSource:(Question *)q;
- (NSMutableArray *)returnSelectSource;
- (void)setSelectSourceTemp:(NSMutableArray *)selectSource_;

-(void)editDone;


-(void)makeTfAlert:(int)tfTag alertStr:(NSString *)alertStr;

-(void)refreshTb;
-(void)refreshTbWithNew:(NSString *)newItem;
-(void)giveUpRefrsh;
@end

