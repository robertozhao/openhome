//
//  SyncImageView.m
//  ScoreApprove
//
//  Created by Yang on 15/5/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "SyncImageView.h"
#import "LocalFileManager.h"

@implementation SyncImageView

-(void)setPic:(Picture *)pic{

    self.image = [UIImage imageNamed:_placeHold];
    
    _pic = pic;
    
    NSString * dataPath = self.isBig==0?pic.localPath_small:pic.localPath;
    NSData * data = [NSData dataWithContentsOfFile:dataPath];
    UIImage * img = [UIImage imageWithData:data];
    if (img) {
        self.image = img;
    }else{

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downLoadFinish) name:[NSString stringWithFormat:@"%@_finish",pic.localPath] object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PictureDownLoad" object:pic];
    }
    
}


-(void)didMoveToSuperview{

    if (!self.superview) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}


-(void)downLoadFinish{
    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.isBig==0?_pic.localPath_small:_pic.localPath]];
    if (img) {
        self.image = img;
    }else{
        // download fail
    }
}

@end
