
//  QuestionNaireView.h
//  ScoreApprove
//
//  Created by Yang on 15/4/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnswerView.h"
#import "QuestionCellVerify.h"
#import "List.h"
#import "QuestionToolTipViewController.h"

@protocol  QuestionNaireViewDelegate <NSObject>

-(void)exitQuestionNaire:(Lead *)lead;
-(void)pageNum:(NSString *)str;
-(void)toolTipIsHid:(BOOL)ishide;
-(void)emailVerify:(NSString *)email;
//-(void)navigationBarBackIsAppear:(BOOL)isAppear;
-(void)navigationBarNextIsAppear:(BOOL)isAppear;
-(void)navigationBarPreviousIsAppear:(BOOL)isAppear;
-(void)exitButtonClicked;
@end

@interface QuestionNaireView : UIView<UIAlertViewDelegate,AnswerViewDelegate>{

    CGRect contentOriginRect;
    NSArray * questionArray;
    UIScrollView * contentV;
    UIView * processBtView;
    UIView * processView;
    UIView * processLb;
    UILabel * questionLb;
    UILabel * questionSubLb;
    UILabel * questionTitleLb;
    
    float width_perQ;
    int index_curQ;
    
    UIButton * backBt;
    UIButton * nextBt;
    
    QuestionCellVerify * qCellVerify;
    UILabel * selectAlertInfo;  // show when select type require question not reply
    BOOL emailHadVerify;
}

// question temp answer
@property (nonatomic ,strong) NSString * coustomerIncome;


@property (nonatomic ,weak) id<QuestionNaireViewDelegate>delegate;
@property (nonatomic , strong) AnswerView * answerView;
@property (nonatomic , strong) List * list;

- (instancetype)initWithFrame:(CGRect)frame;


-(Question *)curQuestion;
-(void)cleanQcells;

-(void)previousQuestion;
-(void)nextQuestion;
// question is first name ,  verify email is ok
-(void)showEmailInvaild:(NSString *)str;
-(void)emailVerfyOk;
@end
