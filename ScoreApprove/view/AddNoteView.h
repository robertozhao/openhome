//
//  AddNoteView.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macro.h"
#import "Lead.h"
#import "BaseViewController.h"

@protocol AddNoteViewDelegate <NSObject>

@optional
-(void)addNoteOK;


@end

@interface AddNoteView : BaseViewController{

    UIView * bkView;
    
    UIView * contentV;
    UIView * navView;
    UITextView * tv;
}

@property (nonatomic , weak) id<AddNoteViewDelegate>delegate;
@property (nonatomic , strong) Lead * lead;
@property (nonatomic, assign) int viewType;

@end
