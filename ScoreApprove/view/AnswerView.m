
#import "AnswerView.h"
#import "Answer.h"
#import "Macro.h"
#import "TextResource.h"
#import "CoredataManager.h"
#import "UUITextFile.h"
#import "QuestionCell.h"
#import "MMUTextFile.h"
#import "DataManage.h"


#define ButtonTfBaseTag  999

#define AddressTfBaseTag 200

#define ButtonTypeBaseTag 300

#define RadioTypeBaseTag  400

#define ContactTypeBaseTag 500

#define CellH  44

@implementation AnswerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        indexSelect = -1;
    }
    selectSource_temp = [[NSMutableArray alloc] init];
    return self;
}

-(void)makeTfAlert:(int)tfTag alertStr:(NSString *)alertStr{
    MMUTextFile * tf ;
    if (contentSV) {
        tf = (MMUTextFile *)[contentSV viewWithTag:tfTag];
    }else{
        tf = (MMUTextFile *)[self viewWithTag:tfTag];
    }
    [tf setTfState:TFStateAlert andText:alertStr];
}

-(void)initSubView:(Question *)q{
    isNewOption = NO;
//    selectSource_temp = [[NSMutableArray alloc] init];
    
    self.q = q;
    
    if (_isContactInfo) {
        [self showSpecilViewWithContactInfo];
        return;
    }
    
    NSString * type;
    if (_questionType == QuestionTypeAppear) {
        type = q.answer_type;
    }else{
        type = q.answerType_temp;
    }
    
    switch ([type intValue]) {
        case QUESTION_ANSER_TYPE_SELECT:{
            [self subViewForSelect];
            break;
        }
        case QUESTION_ANSER_TYPE_RADIO:{
            [self subViewForRadio];
            break;
        }
        case QUESTION_ANSER_TYPE_CHECKBOX:{
            [self subViewForCheckbox];
            break;
        }
        case QUESTION_ANSER_TYPE_BUTTON:{
            [self subViewForButton];
            break;
        }
        case QUESTION_ANSER_TYPE_PASSWORD:{
            [self subViewForPassword];
            break;
        }
        case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
        case QUESTION_ANSER_TYPE_ADDRESS:{
            [self subViewForAddress];
            break;
        }
        case QUESTION_ANSER_TYPE_DOB:
        case QUESTION_ANSER_TYPE_DOB_JOIN:{
        
            [self subViewDDOB];
            break;
        }
        case QUESTION_ANSER_TYPE_TEXT:
        case QUESTION_ANSER_TYPE_MONEY:
        case QUESTION_ANSER_TYPE_PHONENUMBER:
        case QUESTION_ANSER_TYPE_EMAIL:
        case QUESTION_ANSER_TYPE_EMAIL_VERIFY:

        case QUESTION_ANSER_TYPE_SSN:
        case QUESTION_ANSER_TYPE_FULL_NAME:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:

        case QUESTION_ANSER_TYPE_SSN_JOIN:{

            [self subViewForText];
            break;
        }
        case QUESTION_ANSER_TYPE_HIDDEN:{

            break;
        }
        default:
            break;
    }
}

- (NSMutableArray *)returnSelectSource {
    return selectSource;
}

- (void)setSelectSourceTemp:(NSMutableArray *)selectSource_ {
    selectSource_temp = [[NSMutableArray alloc] init];
    selectSource_temp = selectSource_;
}

-(void)showSpecilViewWithContactInfo{
    
    __block  float cur_y = 0;
    
    [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        QuestionCell * cell = (QuestionCell *)obj;
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 25)];
        lb.text = cell.question_appear;
        [self addSubview:lb];
        
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        lb.font = font;
        lb.textColor = RGBCOLOR(146, 146, 146);
        
        cur_y += (lb.frame.size.height +5);

        MMUTextFile * tf = [[MMUTextFile alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 45)];
        tf.font = font;
        tf.textColor = RGBCOLOR(146, 146, 146);
        tf.layer.borderColor = RGBCOLOR(146, 146, 146).CGColor;
        tf.layer.cornerRadius = 5;
        tf.tag = ContactTypeBaseTag+idx;
        tf.delegate = self;
        
        [self addSubview:tf];
        cur_y += (tf.frame.size.height + 10);
        
        if ([lb.text isEqualToString:@"Email"]) {
            // set keyboard modle
            tf.keyboardType = UIKeyboardTypeEmailAddress;
        }
    }];
 
    CGRect fram = self.frame;
    fram.size.height = cur_y;
    self.frame = fram;
    
    
    // apear
    
    __block BOOL isHasInfo = NO;
    
    [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        QuestionCell * cell = (QuestionCell *)obj;
        if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
            isHasInfo = YES;
        }
    }];
    
    if (isHasInfo) {
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        
        UITextField * firstName = (UITextField *)[self viewWithTag:ContactTypeBaseTag];
        UITextField * lastName =(UITextField *)[self viewWithTag:ContactTypeBaseTag+1];
        UITextField * email =(UITextField *)[self viewWithTag:ContactTypeBaseTag+2];
        
        firstName.font = font;
        lastName.font = font;
        email.font = font;
        
        firstName.textColor = RGBCOLOR(146, 146, 146);
        lastName.textColor = RGBCOLOR(146, 146, 146);
        email.textColor = RGBCOLOR(146, 146, 146);
        
        NSArray * array = @[firstName, lastName, email];
        [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            QuestionCell * qCell =  (QuestionCell *)obj;
            UITextField * tf = array[idx];
            tf.text = qCell.question_answer;
        }];
    }
}

-(void)subViewForSelect{
    
    selectSource = [NSMutableArray array];
    
    if ([selectSource_temp count] > 0) {
        selectSource = selectSource_temp;
    }
    
    if(_questionType == QuestionTypeEdit || _questionType == QuestionTypeAppear){
        
        if ([selectSource_temp count] <= 0) {
            NSArray * answers = [_q.answer allObjects];
            answers =  [answers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                Answer * a = (Answer *)obj1;
                Answer * a_ = (Answer *)obj2;
                if ([a.order intValue] > [a_.order intValue]) {
                    return NSOrderedDescending;
                }else{
                    return NSOrderedAscending;
                }
            }];
            if (_questionType == QuestionTypeAppear) {
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Answer * a = (Answer *)obj;
                    [selectSource addObject:a.answer_option?a.answer_option:@""];
                }];
            }else{
                
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Answer * a = (Answer *)obj;
                    MAnswer * ma = [[MAnswer alloc] init];
                    ma.answer_id = a.answer_id;
                    ma.answer_option = a.answer_option;
                    ma.answer_value = a.answer_value;
                    ma.extra = a.extra;
                    ma.order = a.order;
                    ma.question_id = a.question_id;
                    ma.question = a.question;
                    ma.toDele = a.toDele;
                    
                    [selectSource addObject:ma];
                }];
            }
        }
    }

    if (_questionType == QuestionTypeAdd || _questionType == QuestionTypeEdit) {
    
        UIView * subview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        subview.backgroundColor = [UIColor whiteColor];
        [self addSubview:subview];
        
        CGRect  fram;
        if (![_q.user_id isEqualToString:@"0"]) {
            UIButton * bt = [UIButton buttonWithType:0];
            bt.frame = CGRectMake(0, 0, 150, 34);
            [bt setTitle:AddQuestionAnserChooseBtton forState:0];
            [bt setTitleColor:[UIColor whiteColor] forState:0];
            [bt.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0f]];
            
            [bt setBackgroundImage:[UIImage imageNamed:@"blue-btn.png"] forState:UIControlStateNormal];
            
            bt.layer.cornerRadius = 3;
            [bt addTarget:self action:@selector(addNewChooseForQ) forControlEvents:UIControlEventTouchUpInside];
            [subview addSubview:bt];
            
            fram = CGRectMake(2, 80, subview.frame.size.width-10, subview.frame.size.height-80);
        }else{
            fram = CGRectMake(2, 0, subview.frame.size.width-10, subview.frame.size.height);
        }
        select_tb = [[UITableView alloc] initWithFrame:fram ];
        select_tb.delegate  = self;
        select_tb.dataSource = self;
        select_tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        select_tb.allowsSelectionDuringEditing = YES;
        [subview addSubview:select_tb];
        
        if (![_q.user_id isEqualToString:@"0"]) {
            select_tb.editing = YES;
        } else {
            select_tb.allowsSelection = NO;
        }
    }else if (_questionType == QuestionTypeAppear){

        float h = selectSource.count>8?CellH*8.5:CellH*selectSource.count;
        

        select_tb = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, h)];
        select_tb.delegate  = self;
        select_tb.separatorStyle = UITableViewCellSeparatorStyleNone;

        select_tb.dataSource = self;
        [self addSubview:select_tb];
        
        CGRect fram = self.frame;
        fram.size.height = h;
        self.frame = fram;
        
        // apear
        NSArray * array = self.q.qCells;
        
        QuestionCell * cell = (QuestionCell *)array[0];
        if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
            
            NSArray * answers = [_q.answer allObjects];
            answers =  [answers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                Answer * a = (Answer *)obj1;
                Answer * a_ = (Answer *)obj2;
                if ([a.order intValue] > [a_.order intValue]) {
                    return NSOrderedDescending;
                }else{
                    return NSOrderedAscending;
                }
            }];
            
            __block NSUInteger index = -1;
            
            [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Answer * a = (Answer *)obj;
                if ([a.answer_option isEqualToString:cell.question_answer]) {
                    index = idx;
                    *stop = YES;
                }
            }];
            if (index != -1) {
                indexSelect = (int)index;
                
                UITableViewCell * cell = [select_tb cellForRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0]];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
    }
}

- (void)refreshTb {
    if (isNewOption) {
        [selectSource removeObjectAtIndex:indexSelect];
        isNewOption = NO;
    }
    [select_tb reloadData];
}

-(void)refreshTbWithNew:(NSString *)newItem{

    if (indexSelect != -1) {
        
        if (newItem) {
            MAnswer * answer = selectSource[indexSelect];
            answer.answer_option = newItem;
            answer.answer_value = newItem;
        }
    }
    [select_tb reloadData];
}

-(void)giveUpRefrsh{

    MAnswer * a = selectSource[indexSelect];
    if (!a.answer_option) {
        //  new  add answer
        [selectSource removeObjectAtIndex:indexSelect];
    }
    [select_tb reloadData];
}

-(void)addNewChooseForQ{
    isNewOption = YES;
    NSString * num = [[NSUserDefaults standardUserDefaults] objectForKey:AnswerID];
    if (!num) {
        num = [NSString stringWithFormat:@"%d",AnswerBaseId];
    }
    int n = (int)[num integerValue];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",(n-1)] forKey:AnswerID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MAnswer * a = [[MAnswer alloc] init];
    a.answer_id = num;
    a.answer_option = nil;
    a.answer_value = nil;
    a.question_id = _q.question_id;
    a.question = _q;
    a.order = [NSString stringWithFormat:@"%lu",(unsigned long)selectSource.count];

    [selectSource addObject:a];

    indexSelect = (int)selectSource.count-1;
    
    [self.delegate addOrEditItem:a];
}


-(void)editDone{
    
    [self.cur_tf resignFirstResponder];
    
    if (_questionType == QuestionTypeAdd || _questionType == QuestionTypeEdit) {
        
        [self.cur_tf resignFirstResponder];

        
        switch ([_q.answerType_temp intValue]) {
            case QUESTION_ANSER_TYPE_SELECT:{
                
                NSSet * as = _q.answer;
                [_q removeAnswer:as];
                
                [selectSource enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    MAnswer * ma = (MAnswer *)obj;
                    
                    Answer * a;
                    NSArray * answers = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",ma.answer_id]];
                    if (answers && answers.count != 0) {
                        a = answers[0];
                    }else{
                        a = (Answer *)[[CoredataManager manager] newObjectFromEntity:@"Answer"];
                        NSString * extraStr = [[NSUserDefaults standardUserDefaults] objectForKey:ExtraID];
                        if (!extraStr) {
                            extraStr = @"0";
                        }
                        
                        
                        a.extra = extraStr;
                        
                        NSString * newExtra = [NSString stringWithFormat:@"%d",([extraStr intValue]+1)];
                        [[NSUserDefaults standardUserDefaults] setValue:newExtra forKey:ExtraID];
                        [[NSUserDefaults standardUserDefaults] synchronize];

                    }

                    a.answer_id =ma.answer_id;
                    a.answer_option = ma.answer_option;
                    a.answer_value = ma.answer_value;
                    a.question_id = _q.question_id;
                    a.question = _q;
                    a.toDele = nil;
                    a.order = [NSString stringWithFormat:@"%lu",(unsigned long)idx];

                    [_q addAnswerObject:a];
                }];
                
                
                NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"question_id='%@' and toDele=1",_q.question_id]];
                [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Answer * a = (Answer *)obj;
                    if (a.extra) {   //  has not sync to server
                        [[CoredataManager manager] delete:a];
                    }
                }];
                
                break;
            }
            case QUESTION_ANSER_TYPE_BUTTON:{
                
                NSSet * as = _q.answer;
                [_q removeAnswer:as];
                
                for (int i=0;i<selectSource.count;i++) {
             
                    MAnswer *ma = (MAnswer *)selectSource[i];
                    Answer * a;
                    NSArray * answers = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",ma.answer_id]];
                    if (answers && answers.count != 0) {
                        a = answers[0];
                    }else{
                        a = (Answer *)[[CoredataManager manager] newObjectFromEntity:@"Answer"];
                        NSString * extraStr = [[NSUserDefaults standardUserDefaults] objectForKey:ExtraID];
                        if (!extraStr) {
                            extraStr = @"0";
                        }
                        a.extra = extraStr;
                        
                        NSString * newExtra = [NSString stringWithFormat:@"%d",([extraStr intValue]+1)];
                        [[NSUserDefaults standardUserDefaults] setValue:newExtra forKey:ExtraID];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    a.answer_id = ma.answer_id;
                    a.answer_option = ma.answer_option;
                    a.answer_value = ma.answer_value;
                    a.question_id = _q.question_id;
                    a.question = _q;
                    a.toDele = nil;
                    a.order = [NSString stringWithFormat:@"%lu",i];
                    [_q addAnswerObject:a];
                }
                break;
            }
            case QUESTION_ANSER_TYPE_TEXT:{

            }
        }
        [[CoredataManager manager] saveContext];
    }else{
    
        if (_isContactInfo) {
            
            UITextField * firstName = (UITextField *)[self viewWithTag:ContactTypeBaseTag];
            UITextField * lastName =(UITextField *)[self viewWithTag:ContactTypeBaseTag+1];
            UITextField * email =(UITextField *)[self viewWithTag:ContactTypeBaseTag+2];
            
            NSArray * array = @[firstName, lastName, email];
            [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                QuestionCell * qCell =  (QuestionCell *)obj;
                UITextField * tf = array[idx];
                qCell.question_answer = tf.text;
            }];
            return;
        }
        switch ([_q.answer_type intValue]) {
            case QUESTION_ANSER_TYPE_SELECT:{

                QuestionCell * qCell =  self.q.qCells[0];
                if (indexSelect != -1) {
                    qCell.question_answer = selectSource[indexSelect];
                    [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        Answer * a = (Answer *)obj;
                        if ([a.answer_option isEqualToString:qCell.question_answer]) {
                            qCell.question_answerID = a.answer_id;
                            *stop = YES;
                        }
                    }];
                }
                break;
            }
            case QUESTION_ANSER_TYPE_RADIO:{
                QuestionCell * qCell =  self.q.qCells[0];
                if (_buttonSelect) {
                    qCell.question_answer = buttonTitles[_buttonSelect.tag-RadioTypeBaseTag];
                    [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        Answer * a = (Answer *)obj;
                        if ([a.answer_option isEqualToString:qCell.question_answer]) {
                            qCell.question_answerID = a.answer_id;
                            *stop = YES;
                        }
                    }];
                }else{
                    qCell.question_answer = nil;
                }
                break;
            }
            case QUESTION_ANSER_TYPE_CHECKBOX:{
                QuestionCell * qCell =  self.q.qCells[0];
                if (isSelected) {
                    qCell.question_answer = @"Yes";
                }else{
                    qCell.question_answer = @"No";
                }
                break;
            }
            case QUESTION_ANSER_TYPE_BUTTON:{
                if (_buttonSelect) {
                    QuestionCell * qCell =  self.q.qCells[0];
                    qCell.question_answer = selectSource[_buttonSelect.tag-ButtonTypeBaseTag];
                    [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        Answer * a = (Answer *)obj;
                        if ([a.answer_option isEqualToString:qCell.question_answer]) {
                            qCell.question_answerID = a.answer_id;
                            *stop = YES;
                        }
                    }];
                }
                break;
            }
            case QUESTION_ANSER_TYPE_PASSWORD:{
                QuestionCell * qCell =  self.q.qCells[0];
                qCell.question_answer = commenTextFile.text;
                break;
            }
            case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
            case QUESTION_ANSER_TYPE_ADDRESS:{

                UITextField * address = (UITextField *)[self viewWithTag:AddressTfBaseTag];
                UITextField * zipCode =(UITextField *)[self viewWithTag:AddressTfBaseTag+1];
                UITextField * state =(UITextField *)[self viewWithTag:AddressTfBaseTag+2];
                UITextField * city =(UITextField *)[self viewWithTag:AddressTfBaseTag+3];
                
                NSArray * array = @[address, zipCode, state, city];
                [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    QuestionCell * qCell =  (QuestionCell *)obj;
                    UITextField * tf = array[idx];
                    qCell.question_answer = tf.text;
                }];
                
                break;
            }
            case QUESTION_ANSER_TYPE_TEXT:
            case QUESTION_ANSER_TYPE_MONEY:
            case QUESTION_ANSER_TYPE_PHONENUMBER:
            case QUESTION_ANSER_TYPE_EMAIL:
            case QUESTION_ANSER_TYPE_EMAIL_VERIFY:
            case QUESTION_ANSER_TYPE_DOB:
            case QUESTION_ANSER_TYPE_SSN:
            case QUESTION_ANSER_TYPE_FULL_NAME:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:
            case QUESTION_ANSER_TYPE_DOB_JOIN:
            case QUESTION_ANSER_TYPE_SSN_JOIN:{
                NSArray * array = self.q.qCells;
                QuestionCell * qCell =  array[0];
                qCell.question_answer = commenTextFile.text;
                break;
            }
            case QUESTION_ANSER_TYPE_HIDDEN:{
                
                break;
            }
            default:
                break;
        }
    }
}

//

-(void)subViewForText{
    
    selectSource = [[NSMutableArray alloc] init];
    if ([selectSource_temp count] > 0) {
        selectSource = selectSource_temp;
    }
    
    if (self.questionType == QuestionTypeAppear) {
    
        commenTextFile = [[MMUTextFile alloc] initWithFrame:CGRectMake(15, 10, self.frame.size.width-30, 50)];
        commenTextFile.layer.borderColor = RGBCOLOR(146, 146, 146).CGColor;
        commenTextFile.layer.borderWidth = 1;
        commenTextFile.layer.cornerRadius = 5;
        commenTextFile.delegate = self;
        commenTextFile.tag = 888;
        [self addSubview:commenTextFile];
        
        if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_MONEY) {
            UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, commenTextFile.frame.size.height)];
            lb.text = @"  $";
            commenTextFile.leftView = lb;
            commenTextFile.leftViewMode = UITextFieldViewModeAlways;
            commenTextFile.keyboardType = UIKeyboardTypeNumberPad;
            
        }else if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_VERIFY || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_JOIN || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY )  {
            commenTextFile.keyboardType = UIKeyboardTypeNumberPad;
        }else if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_EMAIL || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_EMAIL_VERIFY) {
            commenTextFile.keyboardType = UIKeyboardTypeEmailAddress;
        }else if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_SSN_JOIN || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_SSN){
            commenTextFile.keyboardType = UIKeyboardTypeNumberPad;
        }
        
        CGRect fram = self.frame;
        fram.size.height = 70;
        self.frame = fram;
        
        // apear  answer
        
        QuestionCell * cell = self.q.qCells[0];
        if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
            commenTextFile.text = cell.question_answer;
            if (cell.warmingText) {
                [commenTextFile setTfState:TFStateWarming andText:cell.warmingText];
            }
        }
    } else {
        return;
    }
}

-(void)subViewForRadio{

    contentSV = [[UIScrollView alloc] initWithFrame:self.bounds];
    [self addSubview:contentSV];
    
    NSArray * answers = [_q.answer allObjects];
    answers = [answers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Answer * a = (Answer *)obj1;
        Answer * a_ = (Answer *)obj2;
        if ([a.order intValue] > [a_.order intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    
    NSMutableArray *temp = [NSMutableArray array];
    [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Answer * a = (Answer *)obj;
        [temp addObject:a.answer_option];
    }];
    
    buttonTitles = temp;
    float padding = 5;

    __block float cur_y = padding;
    
    [buttonTitles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * title = (NSString *)obj;
        
        CGRect fram = CGRectMake(30, cur_y, self.frame.size.width - 30-10, 50);
        
        UIButton * bt = [UIButton buttonWithType:0];
        [bt addTarget:self action:@selector(radioButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        bt.frame = fram;
        bt.tag = RadioTypeBaseTag+idx;
        bt.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 450);
        [bt setImage:[UIImage imageNamed:@"single_un"] forState:0];
        [bt setImage:[UIImage imageNamed:@"single_sel"] forState:UIControlStateSelected];
        [contentSV addSubview:bt];
        
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(90, cur_y, self.frame.size.width - 10 - 90, 50)];
        lb.text = title;
        lb.font = [UIFont systemFontOfSize:17];
        lb.numberOfLines = 0;
        lb.adjustsFontSizeToFitWidth = YES;
        lb.minimumScaleFactor = 13;
        [contentSV addSubview:lb];
        
        cur_y += (50 + padding);
    }];
    
    float h;
    if (cur_y> 500) {
        h = 500;
        contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, cur_y);
    }else{
        h = cur_y;
    }
    
    CGRect fram = contentSV.frame;
    fram.size.height = h;
    contentSV.frame = fram;
    
    CGRect fram_ = self.frame;
    fram_.size.height = contentSV.frame.size.height;
    self.frame = fram_;
    
    // apear
    
    QuestionCell * cell = (QuestionCell *)_q.qCells[0];
    if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
        __block int index = -1;
        [buttonTitles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * title = (NSString *)obj;
            if ([title isEqualToString:cell.question_answer]) {
                index = (int)idx;
            }
        }];
        if (index != -1) {
            UIButton * bt = (UIButton *)[contentSV viewWithTag:RadioTypeBaseTag+index];
            bt.selected = YES;
            self.buttonSelect = bt;
        }
    }
}

-(void)radioButtonAction:(UIButton *)bt{

    if (_buttonSelect == bt) {
        bt.selected = NO;
        _buttonSelect = nil;
        return;
    }
    
    if (_buttonSelect) {
        _buttonSelect.selected= NO;
    }
    
    bt.selected = YES;
    
    self.buttonSelect = bt;
}

//
-(void)subViewForCheckbox{
    
    CGRect fram = CGRectMake(15, 0, self.frame.size.width - 15-10, 50);
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt addTarget:self action:@selector(checkboxBtAction:) forControlEvents:UIControlEventTouchUpInside];
    bt.frame = fram;
    bt.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 450);
    [bt setImage:[UIImage imageNamed:@"select_u"] forState:0];
    [bt setImage:[UIImage imageNamed:@"select_sel"] forState:UIControlStateSelected];
    [self addSubview:bt];


    CGRect fram_ = self.frame;
    fram_.size.height = 50;
    self.frame = fram_;
    
    // appear
    
    QuestionCell * cell = (QuestionCell *)_q.qCells[0];
    if (cell.question_answer && ![cell.question_answer isEqualToString:@"Yes"]) {
        bt.selected = YES;
        isSelected = YES;
    }
}

-(void)checkboxBtAction:(UIButton *)bt{
    bt.selected = !bt.selected;
    isSelected = bt.selected;
}

-(void)subViewForButton{
    
    selectSource = [NSMutableArray array];
    
    if ([selectSource_temp count] > 0) {
        selectSource = selectSource_temp;
    }
    
    if(_questionType == QuestionTypeEdit || _questionType == QuestionTypeAppear){
        
        if ([selectSource_temp count] <= 0) {
            NSArray * answers = [_q.answer allObjects];
            answers =  [answers sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                Answer * a = (Answer *)obj1;
                Answer * a_ = (Answer *)obj2;
                if ([a.order intValue] > [a_.order intValue]) {
                    return NSOrderedDescending;
                }else{
                    return NSOrderedAscending;
                }
            }];
            if (_questionType == QuestionTypeAppear) {
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Answer * a = (Answer *)obj;
                    [selectSource addObject:a.answer_option];
                }];
            }else{
                
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Answer * a = (Answer *)obj;
                    MAnswer * ma = [[MAnswer alloc] init];
                    ma.answer_id = a.answer_id;
                    ma.answer_option = a.answer_option;
                    ma.answer_value = a.answer_value;
                    ma.extra = a.extra;
                    ma.order = a.order;
                    ma.question_id = a.question_id;
                    ma.question = a.question;
                    ma.toDele = a.toDele;
                    
                    [selectSource addObject:ma];
                }];
            }
        }
    }

    if (_questionType == QuestionTypeAdd || _questionType == QuestionTypeEdit) {

        UIView * subview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        subview.backgroundColor = [UIColor whiteColor];
        [self addSubview:subview];
        
        CGRect  fram;
        if (![_q.user_id isEqualToString:@"0"]) {
            UIButton * bt = [UIButton buttonWithType:0];
            bt.frame = CGRectMake(0, 0, 150, 34);
            [bt setTitle:AddQuestionAnserChooseBtton forState:0];
            [bt setTitleColor:[UIColor whiteColor] forState:0];
            [bt.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0f]];
            
            [bt setBackgroundImage:[UIImage imageNamed:@"blue-btn.png"] forState:UIControlStateNormal];
            
            bt.layer.cornerRadius = 3;
            [bt addTarget:self action:@selector(addNewChooseForQ) forControlEvents:UIControlEventTouchUpInside];
            [subview addSubview:bt];
            
            fram = CGRectMake(2, 80, subview.frame.size.width-10, subview.frame.size.height-80);
        }else{
            fram = CGRectMake(0, 0, subview.frame.size.width-10, subview.frame.size.height);
        }
        select_tb = [[UITableView alloc] initWithFrame:fram ];
        select_tb.delegate  = self;
        select_tb.dataSource = self;
        select_tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        select_tb.allowsSelectionDuringEditing = YES;
        [subview addSubview:select_tb];
        
        if (![_q.user_id isEqualToString:@"0"]) {
            select_tb.editing = YES;
        } else {
            select_tb.allowsSelection = NO;
        }
        
    }else if (_questionType == QuestionTypeAppear){
        
        contentSV = [[UIScrollView alloc] initWithFrame:self.bounds];
        
        [self addSubview:contentSV];
        [self initBts];
    }
}

-(void)initBts{
    
        float padding = 10;
//        float padding = 0;
        __block float cur_x = padding;
        __block float cur_y = padding;
        float btW = 100.0;
        float btH = 32.0;
        
        int num = (self.frame.size.width - padding)/(btW+padding);
        
        [selectSource enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * title = (NSString *)obj;
            
            CGRect fram = CGRectMake(cur_x, cur_y, btW, btH);
           
            UIButton * bt = [UIButton buttonWithType:0];
            [bt addTarget:self action:@selector(buttonTypeSelect:) forControlEvents:UIControlEventTouchUpInside];
            bt.frame = fram;
            bt.tag = ButtonTypeBaseTag+idx;
            [bt setTitle:title forState:0];
            bt.backgroundColor = [UIColor whiteColor];
            [bt setTitleColor:RGBCOLOR(60, 141, 254) forState:0];
            bt.layer.borderColor = RGBCOLOR(60, 141, 254).CGColor;
            bt.layer.borderWidth = 1;
            [contentSV addSubview:bt];
            
            if ((idx+1) % num ==0) {
                cur_x = padding;
                cur_y += (btH + padding);
            }else{
                cur_x += (btW+padding);
            }
        }];
        
        if (selectSource.count % num != 0) {
            cur_y +=(btH + padding);
        }
        
        float h;
        if (cur_y> 500) {
            h = 500;
            contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, cur_y);
        }else{
            h = cur_y;
        }
        
        CGRect fram = contentSV.frame;
        fram.size.height = h;
        contentSV.frame = fram;
        CGRect fram_ = self.frame;
        fram_.size.height = contentSV.frame.size.height;
        self.frame = fram_;
    
        // apear
    
    QuestionCell * cell = self.q.qCells[0];
    if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {

        __block int index = -1;
        [selectSource enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * title = (NSString *)obj;
            if ([cell.question_answer isEqualToString:title]) {
                index = (int)idx;
            }
        }];
        if (index != -1) {
            
            UIButton * bt = (UIButton *)[contentSV viewWithTag:ButtonTypeBaseTag+index];
            bt.selected = YES;
            bt.backgroundColor = RGBCOLOR(60, 141, 254);
            bt.titleLabel.textColor = [UIColor whiteColor];
            [bt setTitleColor:[UIColor whiteColor] forState:0];
            self.buttonSelect = bt;
        }
    }
}

-(void)buttonTypeSelect:(UIButton *)bt{

    if (_buttonSelect) {
        _buttonSelect.selected = NO;
        bt.backgroundColor = [UIColor whiteColor];
        [bt setTitleColor:[UIColor blackColor] forState:0];
    }
    
    bt.selected = YES;
    bt.backgroundColor = RGBCOLOR(252, 147, 0);
    [bt setTitleColor:[UIColor whiteColor] forState:0];
    
    self.buttonSelect = bt;

    [self.delegate autoNextQuestion];
}

-(void)subViewForAddress{

    __block  float cur_y = 0;
    
    [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    
        QuestionCell * cell = (QuestionCell *)obj;
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 30)];
        lb.text = cell.question_appear;
        [self addSubview:lb];
        
        cur_y += (lb.frame.size.height +10);
        
        if ([lb.text isEqualToString:@"State"]) {
            
            self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 400)];
            self.pickerView.delegate = self;
            self.pickerView.dataSource = self;
            
            MMUTextFile * tf = [[MMUTextFile alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 45)];
//            tf.text =@"AK - Alaska";
            tf.inputView = self.pickerView;
            tf.delegate = self;
            tf.tag = AddressTfBaseTag+idx;
            [self addSubview:tf];
            
            UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
            [topView setBarStyle:UIBarStyleBlack];
            
            UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            
            UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyBoard)];
            
            NSArray * buttonsArray = [NSArray arrayWithObjects:btnSpace,doneButton,nil];
            
            [topView setItems:buttonsArray];  
            [tf setInputAccessoryView:topView];
            
            // default
            
//            cell.question_answer = @"AK - Alaska";
            cur_y += (tf.frame.size.height + 10);
            
        }else{
            MMUTextFile * tf = [[MMUTextFile alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 45)];
            tf.tag = AddressTfBaseTag+idx;
            
            tf.delegate = self;
            [self addSubview:tf];
            cur_y += (tf.frame.size.height + 10);
            
            if ([lb.text isEqualToString:@"Zip Code"]) {
                // set keyboard modle
                tf.keyboardType = UIKeyboardTypeNumberPad;
            }
        }
    }];
    
    CGRect fram = self.frame;
    fram.size.height = cur_y;
    self.frame = fram;
    
    // apear
    
    UITextField * address = (UITextField *)[self viewWithTag:AddressTfBaseTag];
    UITextField * zipCode =(UITextField *)[self viewWithTag:AddressTfBaseTag+1];
    UITextField * state =(UITextField *)[self viewWithTag:AddressTfBaseTag+2];
    UITextField * city =(UITextField *)[self viewWithTag:AddressTfBaseTag+3];
    
    NSArray * array = @[address, zipCode, state, city];
    [_q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        QuestionCell * cell = (QuestionCell *)obj;
        if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
            UITextField * tf = (UITextField *)array[idx];
            tf.text = cell.question_answer;
        }
    }];
}

-(void)dismissKeyBoard{
    
    if (commenTextFile) {
        [commenTextFile resignFirstResponder];
    }
    
    UITextField * tf = (UITextField *)[self viewWithTag:AddressTfBaseTag+2];
    [tf resignFirstResponder];
}

-(void)subViewForPassword{

    __block float cur_y = 0;
    
    [self.q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        QuestionCell * cell = (QuestionCell *)obj;
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 30)];
        lb.text = cell.question_appear;
        [self addSubview:lb];
        
        cur_y += (lb.frame.size.height +10);
        MMUTextFile * tf = [[MMUTextFile alloc] initWithFrame:CGRectMake(10, cur_y, self.frame.size.width - 20, 45)];
        tf.tag = idx;
        tf.delegate = self;
        tf.secureTextEntry = YES;
        [self addSubview:tf];
        
        cur_y += (tf.frame.size.height + 10);
    }];
    CGRect fram = self.frame;
    fram.size.height = cur_y;
    self.frame = fram;
}

-(void)subViewDDOB{

    __block float cur_y = 0;
    
    commenTextFile = [[MMUTextFile alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 20, 45)];
    commenTextFile.delegate = self;
    commenTextFile.tag = 888;
    [self addSubview:commenTextFile];
    
    
    NSCalendar*calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * comps =[calendar components:NSCalendarUnitYear  fromDate:[NSDate date]];
    
    NSInteger year = [comps year];
    
    int max = (int)year-17;
    NSDate * maxDate = [Utility dateFromDateStr:[NSString stringWithFormat:@"%d-12-31 00:00:00",max] withFormat:@"YYYY-MM-dd HH-mm-ss"];
    
    int min = (int)year-100;
    NSDate * minDate = [Utility dateFromDateStr:[NSString stringWithFormat:@"%d-01-01 00:00:00",min] withFormat:@"YYYY-MM-dd HH-mm-ss"];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 400)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.minimumDate = minDate;
    self.datePicker.maximumDate = maxDate;
    [_datePicker addTarget:self action:@selector(dobValueChoose) forControlEvents:UIControlEventValueChanged];
    
    cur_y += (commenTextFile.frame.size.height + 10);
    
    UIToolbar * topView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    [topView setBarStyle:UIBarStyleBlack];
    
    UIBarButtonItem * btnSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyBoard)];
    
    NSArray * buttonsArray = [NSArray arrayWithObjects:btnSpace,doneButton,nil];
    
    [topView setItems:buttonsArray];
    
    [commenTextFile setInputAccessoryView:topView];
    commenTextFile.inputView = self.datePicker;
    
    CGRect fram = self.frame;
    fram.size.height = cur_y;
    self.frame = fram;
}

-(void)dobValueChoose{

    NSDate * date = _datePicker.date;
    NSString * str = [Utility timeStringFromFormat:@"MM/dd/YYYY" withDate:date];
    commenTextFile.text = str;
}

#pragma mark -- UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    NSString * path = [[NSBundle mainBundle] pathForResource:@"AddressState" ofType:@"plist"];
    NSArray * array = [NSArray arrayWithContentsOfFile:path];
    return array.count;

}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString * path = [[NSBundle mainBundle] pathForResource:@"AddressState" ofType:@"plist"];
    NSArray * array = [NSArray arrayWithContentsOfFile:path];
    NSDictionary * dic = array[row];
    
    return dic[@"appear"];
}

#pragma mark -- UIPickerViewDelegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    
    if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_ADDRESS) {
        
        NSArray * cells = _q.qCells;

      __block  QuestionCell * cur_cell;
        [cells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            QuestionCell * cell = (QuestionCell *)obj;
            if ([cell.question_appear isEqualToString:@"State"]) {
                cur_cell = cell;
                * stop = YES;
            }
        }];
        
        if (!cur_cell) {
            return;
        }
        
        NSString * path = [[NSBundle mainBundle] pathForResource:@"AddressState" ofType:@"plist"];
        NSArray * array = [NSArray arrayWithContentsOfFile:path];
            NSDictionary * dic = array[row];
        UITextField * tf = (UITextField *)[self viewWithTag:AddressTfBaseTag+2];
//        UITextField * tf = (UITextField *)[self viewWithTag:[cells indexOfObject:cur_cell]];
        tf.text = dic[@"appear"];
        
        cur_cell.question_answer = array[row];
    }
}



#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == select_tb) {
        return selectSource.count;
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentify = @"cellIdentify";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
    }
    
    if (indexSelect == indexPath.row) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.preferredMaxLayoutWidth = 218;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    
    NSString *str = [[NSString alloc] init];
    if (_questionType == QuestionTypeAppear) {
        str = selectSource[indexPath.row];
    }else{
        MAnswer * answer = selectSource[indexPath.row];
        str = answer.answer_option;
    }
    cell.textLabel.text = str;
    
    UIImageView *imgBg = [cell viewWithTag:400];
    CGRect newBgFrame = CGRectMake(0, 3, self.frame.size.width, [self calculateCellHeight:str] + 24);
    if (imgBg == nil) {
        imgBg = [[UIImageView alloc] initWithFrame:newBgFrame];
        [imgBg setImage:[UIImage imageNamed:@"photo-list"]];
        [imgBg setTag:400];
        [cell addSubview:imgBg];
        [cell sendSubviewToBack:imgBg];
    } else {
        imgBg.frame = newBgFrame;
    }
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSString *str = [[NSString alloc] init];
    if (_questionType == QuestionTypeAppear) {
        str = selectSource[indexPath.row];
    }else{
        MAnswer * answer = selectSource[indexPath.row];
        str = answer.answer_option;
    }
    
    return [self calculateCellHeight:str] + 30;
}

- (float) calculateCellHeight:(NSString *)str {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15.0f]};
    
    CGRect rect_titleLb = [str boundingRectWithSize:CGSizeMake(295, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributes
                                            context:nil];
    return rect_titleLb.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{

    MAnswer * a = selectSource[sourceIndexPath.row];
    [selectSource removeObjectAtIndex:sourceIndexPath.row];
    [selectSource insertObject:a atIndex:destinationIndexPath.row];

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        MAnswer * a = selectSource[indexPath.row];
        

        NSArray * answers = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",a.answer_id]];
        if (answers && answers.count != 0) {
            //  the answer is old,so need mark in coredata
            Answer * a_ = answers[0];
            a_.toDele = @"1";
            [[CoredataManager manager] saveContext];
        }
        [selectSource removeObject:a];
        [select_tb reloadData];
    }
}


#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    isNewOption = NO;
    if (_questionType != QuestionTypeAppear) {
        
        MAnswer * answer = selectSource[indexPath.row];
        
        if(self.delegate && [self.delegate respondsToSelector:@selector(addOrEditItem:)]){
            indexSelect = (int)indexPath.row;
            [self.delegate addOrEditItem:answer];
        }
    }else{
        if (indexSelect == indexPath.row) {
            UITableViewCell * cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexSelect inSection:0]];
            cell.accessoryType =  UITableViewCellAccessoryNone;
            indexSelect = -1;
        }else{
            if (indexSelect != -1) {
                UITableViewCell * cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0]];
                cell.accessoryType =  UITableViewCellAccessoryNone;
            }
            UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType =  UITableViewCellAccessoryCheckmark;
            
            indexSelect = (int)indexPath.row;
        }
        
        [self.delegate selectHasReply];
    }
}


#pragma mark -- MMUTextFileDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_MONEY) {
        
        NSString * str_ = @"0123456789";
        if (([str_ rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""])) {
            return NO;
        }
        NSString * str = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSString * temp;
        if ([string isEqualToString:@""]) {
            temp = [str substringToIndex:str.length-1];
        }else{
            temp = [NSString stringWithFormat:@"%@a",str];
        }
        NSString * valid = [NSString string];
        int j=0;
        for (int i = (int)temp.length-1; i>-1; i--) {
            
            if (j%3==0 && i!=temp.length-1 && j!=0) {
                valid = [NSString stringWithFormat:@",%@",valid];
            }
            NSString *s = [temp substringWithRange:NSMakeRange(i, 1)];
            valid = [NSString stringWithFormat:@"%@%@",s,valid];
            j++;
        }
        if ([string isEqualToString:@""]) {
            NSString * str =[textField.text substringFromIndex:textField.text.length-1];
            valid = [NSString stringWithFormat:@"%@%@",valid,str];
        }else{
            NSString *str =[valid substringToIndex:valid.length-1];
            valid = [NSMutableString stringWithString:str];
        }
        textField.text = valid;
        
    }else if([_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_VERIFY || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_JOIN || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY){
        NSString * str = @"0123456789";
        if ([str rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }else{
            
            if (textField.text.length == 13 && ![string isEqualToString:@""]) {
                return NO;
            }
            
            if (textField.text.length == 3 && ![string isEqualToString:@""]) {
                NSString * str = textField.text;
                textField.text = [NSString stringWithFormat:@"(%@)",str];
            }
            
            if (textField.text.length == 8 && ![string isEqualToString:@""]) {
                NSString * str = textField.text;
                textField.text = [NSString stringWithFormat:@"%@-",str];
            }
            if (textField.text.length == 5 && [string isEqualToString:@""]) {
                NSString * str = textField.text;
                textField.text = [str substringFromIndex:1];
            }
        }
    }else if ([_q.answer_type intValue] == QUESTION_ANSER_TYPE_SSN || [_q.answer_type intValue] == QUESTION_ANSER_TYPE_SSN_JOIN){
        if (textField.text.length == 9) {
            return NO;
        }
        NSString * str = @"0123456789";
        if ([str rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }
    }else if (textField.tag == AddressTfBaseTag+1 ){
    
        NSString * str = @"0123456789";
        if ([str rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(MMUTextFile *)textField{
    self.cur_tf = textField;
    if(textField.tfState == TFStateAlert){
        textField.tfState = TFStateNomal;
    }
    // revert self fram begin edit.if show textfile alert.
        // current only money has length alert
        if (CGRectGetHeight(textField.alertInfo.frame) > AlertLableOriginHeight) {
            [textField revertFram];           
            [self reSizeSupFram];
        }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag > ButtonTfBaseTag ||  textField.tag == ButtonTfBaseTag){
        NSString * str = textField.text;
        if (str && ![str isEqualToString:@""]) {
            [selectSource replaceObjectAtIndex:textField.tag - ButtonTfBaseTag withObject:str];
        }else{
            [selectSource removeObjectAtIndex:textField.tag - ButtonTfBaseTag];
        }
    }
}

-(void)reSizeSupFram{
 
    CGRect fram = self.frame;
    fram.size.height = CGRectGetMaxY(commenTextFile.alertInfo.frame);
    self.frame = fram;
    [self.delegate remarkSelfFrmeWithMMutextfile];
}
@end
