//
//  QuestionEditView.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/16.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macro.h"

@interface QuestionEditView : UIView<UITableViewDataSource,UITableViewDelegate>{
    UITableView * myTableView;
    NSArray * source;
}

@end
