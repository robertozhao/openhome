//
//  PopView.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/2.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"
#import "Lead.h"
NS_ENUM(int, PopViewType){


    PopViewTypeEditListAddress,
    PopViewTypePropertyUrl,
    PopViewTypeAreaEdit,
    PopViewTypePriceEdit,
    PopViewTypeDescription,
    PopViewTypeEmailSignature,
};

@protocol PopViewDelegate <NSObject>
@optional

-(void)creatNewListing:(NSDictionary *)info;


-(void)newAddress:(NSString *)address;
-(void)newPrice:(NSString *)price;
-(void)newDesc:(NSString *)desc;
-(void)newSize:(NSString *)size;

-(void)newLeadName:(NSString *)name;
-(void)newLeadEmail:(NSString *)email;
-(void)newLeadPhone:(NSString *)phone;

-(void)newEmailSignture:(NSString *)signture;

@end

@interface PopView : UIView<UITextFieldDelegate>{

    UIControl * bkControl;
    
    UIView * navView;
    UIView * contentV;
    
    //  address edit
    UITextField * addressEditTf;

//  property url
    UITextField * propertyTf;
    
    // price edit
    UITextField * priceTf;
    
    // description edit
    UITextView* descTV;

    //  area edit
    
    UITextField * areaTf;
    UISegmentedControl *areaUnitChooseSegment;
    
    // email signture
    
    UITextView * signtureTV;
}

@property (nonatomic , weak) id<PopViewDelegate>delegate;
@property (nonatomic, strong) List * list;
@property (nonatomic, strong) Lead * lead;
@property (nonatomic) enum PopViewType  viewType;

- (instancetype)initWithFrame:(CGRect)frame;


@end
