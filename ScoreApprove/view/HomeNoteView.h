//
//  HomeNoteView.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/9.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeNoteViewDelegate <NSObject>

-(void)endOpenHome;
-(void)addNote;

@end

@interface HomeNoteView : UIView{

    IBOutlet UIButton *visitorBt;

    IBOutlet UIButton *endBt;
    
}

@property (nonatomic , strong) id<HomeNoteViewDelegate>delegate;

@end
