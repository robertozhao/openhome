//
//  HomeNoteView.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/9.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "HomeNoteView.h"
#import "TextResource.h"

@implementation HomeNoteView



-(void)awakeFromNib{

    self.layer.cornerRadius = 4;
    visitorBt.layer.cornerRadius = 4;
    endBt.layer.cornerRadius = 4;
    
    
    [endBt setTitle:EndVisitorBtTitle forState:0];
    [visitorBt setTitle:AddVisitorNoteBtTitle forState:0];
}

- (IBAction)endOpenHome:(id)sender {
    [self.delegate endOpenHome];
}

- (IBAction)addVisitorNotes:(id)sender {
    [self.delegate addNote];
}

@end
