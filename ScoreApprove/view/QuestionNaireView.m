//
//  QuestionNaireView.m
//  ScoreApprove
//
//  Created by Yang on 15/4/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "QuestionNaireView.h"
#import "CoredataManager.h"
#import "Question.h"
#import "Answer.h"
#import "QuestionCell.h"
#import "Macro.h"
#import "MMUTextFile.h"
#import "LeadQuestion.h"
#import "Lead.h"


#define CustomerAnnalIncomeLower  30000
#define AlertTagForLeave  8989
#define DebtPercentageErrorLevel  0.083
#define DebtPercentageWarmLevel  0.016



@implementation QuestionNaireView

- (void) enableKeyboarderListener {
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleKeyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleKeyboardWillHide:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];


}

- (void) disableKeyboarderListener {

    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIKeyboardWillShowNotification
                                                  object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIKeyboardWillHideNotification
                                                  object: nil];
}



- (void) handleKeyboardWillShow: (NSNotification *) notify {

    float  keyboardHeight = [[notify userInfo][@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
    float animationDuration = [[notify userInfo][@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];

    if ([_answerView.cur_tf.inputView isKindOfClass:[UIPickerView class]]) {
        keyboardHeight = 251;
    }
    
    [UIView animateWithDuration:animationDuration animations:^{
        CGRect fram = contentV.frame;
        fram.size.height = 768-keyboardHeight;
        contentV.frame = fram;
    } completion:nil];
}


- (void) handleKeyboardWillHide: (NSNotification *) notify {

    contentV.frame = contentOriginRect;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self enableKeyboarderListener];
        
        index_curQ = -1;
        
        [self dataInit];
        [self viewInit];
    }
    return self;
}
-(void)didMoveToSuperview{
    
    if (!self.superview) {
        [self disableKeyboarderListener];
    }else{
        [self nextQuestion];
    }
}

-(Question *)curQuestion{

    if (index_curQ !=-1) {
        Question * q = questionArray[index_curQ];
        return q;
    }else{
        return nil;
    }
}

-(void)cleanQcells{
    [questionArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Question * q = (Question *)obj;
        q.qCells = nil;
    }];
}

// question is first name ,  verify email is ok
-(void)showEmailInvaild:(NSString *)str{

    [_answerView makeTfAlert:ContactTypeBaseTag+2 alertStr:str];
}

-(void)emailVerfyOk{
    emailHadVerify = YES;
    [self nextQuestion];
}

-(void)dataInit{

    NSMutableArray * temp = [NSMutableArray array];
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:@"user_id==0 and is_active=='1'" ];
    [temp addObjectsFromArray:array];
    
    NSArray * array_ = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"user_id=='%@' and is_active=='1'",[GlobalInstance instance].user_id]];
    [temp addObjectsFromArray:array_];
    
    
    __block Question * question_lastName;
    __block Question * question_email;
    
    [temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
         Question * q = (Question *)obj;
        if ([q.question isEqualToString:@"Last Name"]) {
            question_lastName = q;
        }else if ([q.question isEqualToString:@"Email"]){
            question_email = q;
        }
    }];
    
    if (question_lastName) {
        [temp removeObject:question_lastName];
    }
    if (question_email) {
        [temp removeObject:question_email];
    }
    
    NSArray * temp_ = [temp sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Question * q = (Question *)obj1;
        Question * q_ = (Question *)obj2;
        if ([q.sortNum intValue] > [q_.sortNum intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    questionArray = temp_;
    
    [questionArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Question * q = (Question *)obj;
        
        switch ([q.answer_type intValue]) {
            case QUESTION_ANSER_TYPE_SELECT:
            case QUESTION_ANSER_TYPE_RADIO:
            case QUESTION_ANSER_TYPE_CHECKBOX:
            case QUESTION_ANSER_TYPE_BUTTON:
            case QUESTION_ANSER_TYPE_TEXT:
            case QUESTION_ANSER_TYPE_MONEY:
            case QUESTION_ANSER_TYPE_PHONENUMBER:
            case QUESTION_ANSER_TYPE_EMAIL:
            case QUESTION_ANSER_TYPE_EMAIL_VERIFY:
            case QUESTION_ANSER_TYPE_DOB:
            case QUESTION_ANSER_TYPE_SSN:
            case QUESTION_ANSER_TYPE_PHONENUMBER_VERIFY:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:
            case QUESTION_ANSER_TYPE_DOB_JOIN:
            case QUESTION_ANSER_TYPE_SSN_JOIN:
            case QUESTION_ANSER_TYPE_PASSWORD:
            case QUESTION_ANSER_TYPE_HIDDEN:{

                QuestionCell * qC = [[QuestionCell alloc] init];
                qC.question_appear = q.question;
                q.qCells = @[qC];
                break;
            }
            case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
            case QUESTION_ANSER_TYPE_ADDRESS:{

                NSArray * qTitles = @[@"Address",@"Zip Code",@"State",@"City"];
                NSMutableArray * temp = [NSMutableArray array];
                [qTitles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSString * title = (NSString *)obj;
                    QuestionCell * qC = [[QuestionCell alloc] init];
                    qC.question_appear = title;
                    qC.question_answer = nil;
                    [temp addObject:qC];
                }];
                q.qCells = temp;
                break;
            }
                
            case QUESTION_ANSER_TYPE_FULL_NAME:{
                NSArray * qTitles = @[@"First Name",@"Last Name"];
                NSMutableArray * temp = [NSMutableArray array];
                [qTitles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSString * title = (NSString *)obj;
                    QuestionCell * qC = [[QuestionCell alloc] init];
                    qC.question_appear = title;
                    [temp addObject:qC];
                }];
                q.qCells = temp;
                break;
            }
            default:
                break;
        }
        
        if ([q.question isEqualToString:@"First Name"]) {
            q.qCells = nil;
            NSArray * qTitles = @[@"First Name",@"Last Name",@"Email"];
            NSMutableArray * cellTemp = [NSMutableArray array];
            [qTitles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString * title = (NSString *)obj;
                QuestionCell * qC = [[QuestionCell alloc] init];
                qC.question_appear = title;
                [cellTemp addObject:qC];
            }];
            q.qCells = cellTemp;
        }
    }];
}

-(void)viewInit{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
    
    contentV = [[UIScrollView alloc] initWithFrame:self.bounds];
    contentV.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentV];
    
    questionLb = [[UILabel alloc] initWithFrame:CGRectMake(55, 20, contentV.frame.size.width - 100, 25)];
    questionLb.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    questionLb.textColor = [UIColor blackColor];
    questionLb.text = @"Please Sign In.";
    
    questionSubLb = [[UILabel alloc] initWithFrame:CGRectMake(55, questionLb.frame.origin.y + questionLb.frame.size.height, contentV.frame.size.width - 100, 25)];
    questionSubLb.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    questionSubLb.textColor = RGBCOLOR(24, 149, 214);
    questionSubLb.text = @"* Required";
    
    [contentV addSubview:questionLb];
//    [contentV addSubview:questionSubLb];

//    processView = [[UIView alloc] initWithFrame:CGRectMake(10, 20, contentV.frame.size.width - 20, 30)];
//    processView.backgroundColor = RGBCOLOR(230, 230, 230);
//    [contentV addSubview:processView];
//    
//    width_perQ = processView.frame.size.width / (float)questionArray.count;
//    
//    processLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, processView.frame.size.height)];
//    processLb.backgroundColor = RGBCOLOR(0 ,150, 68);
//    [processView addSubview:processLb];
    
    questionTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(55, questionLb.frame.origin.y + questionLb.frame.size.height, contentV.frame.size.width - 110, 45)];
    questionTitleLb.font = font;
    questionTitleLb.backgroundColor = [UIColor whiteColor];
    questionTitleLb.numberOfLines = 0;
    [contentV addSubview:questionTitleLb];
    
    processBtView = [[UIView alloc] initWithFrame:CGRectMake(0, contentV.frame.size.height - 82, contentV.frame.size.width, 50)];
    processBtView.backgroundColor = [UIColor clearColor];
    [contentV addSubview:processBtView];
    
    backBt = [UIButton buttonWithType:0];
    [backBt setTitle:@"Exit" forState:0];
    backBt.frame = CGRectMake(60, 0, 154, 35);
    backBt.alpha = 0.9;
    backBt.layer.cornerRadius = 4;
//    [backBt setBackgroundColor:RGBCOLOR(230, 230, 230)];
    [backBt.titleLabel setFont:font];
    [backBt setBackgroundImage:[UIImage imageNamed:@"blue-btn"] forState:UIControlStateNormal];
    [backBt setTitleColor:[UIColor whiteColor] forState:0];
    [backBt addTarget:self action:@selector(exitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [processBtView addSubview:backBt];
//    backBt.hidden = YES;
    
    nextBt = [UIButton buttonWithType:0];
    [nextBt setTitle:@"Next" forState:0];
    nextBt.layer.cornerRadius = 4;
//    [nextBt setBackgroundColor:RGBCOLOR(0 ,150, 68)];
    [nextBt.titleLabel setFont:font];
    [nextBt setBackgroundImage:[UIImage imageNamed:@"begin-btn.png"] forState:UIControlStateNormal];
    nextBt.frame = CGRectMake((contentV.frame.size.width - backBt.frame.size.width) - 60, 0, 154, 35);
    [nextBt setTitleColor:[UIColor whiteColor] forState:0];
    [nextBt addTarget:self action:@selector(nextQuestion) forControlEvents:UIControlEventTouchUpInside];
    [processBtView addSubview:nextBt];
}

- (void)exitButtonClicked {
    [self.delegate exitButtonClicked];
}

-(void)previousQuestion{

    [self.delegate toolTipIsHid:YES];

    if (index_curQ == 0) {
        return;
    }
    
    index_curQ --;
    NSString * bt_title = [nextBt titleForState:0];
    if ([bt_title isEqualToString:@"Done"]) {
        [nextBt setTitle:@"Next" forState:0];
    }
    
    //  is the first question , hide back button
    
    if (index_curQ != 0) {
        if (backBt.hidden) {
            CGRect fram = backBt.frame;
            fram.origin.x = contentV.frame.size.width - fram.size.width - 60;
            nextBt.frame = fram;
            
            backBt.hidden = NO;
        }
    }else{
//        backBt.hidden = YES;
//        
//        //  move nextbt to the left opzation
//        CGRect fram = nextBt.frame;
//        fram.origin.x = (contentV.frame.size.width - fram.size.width) / 2;
//        nextBt.frame = fram;
    }
    NSString * str =[NSString stringWithFormat:@"Question %d of %lu",index_curQ+1,(unsigned long)questionArray.count];
    [self.delegate pageNum:str];

    
    Question * q = questionArray[index_curQ];
    
    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON) {
//        nextBt.hidden = YES;
    }else{
        nextBt.hidden = NO;
    }
    
    CGRect fram = processLb.frame;
    fram.size.width -= width_perQ;
    processLb.frame = fram;
    
    // title
    
    NSDictionary *attributes = @{NSFontAttributeName:questionTitleLb.font};
    CGRect rect_titleLb = [q.question boundingRectWithSize:CGSizeMake(questionTitleLb.frame.size.width, MAXFLOAT)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:attributes
                                                   context:nil];
    if (rect_titleLb.size.height >45) {
        CGRect fram_ = questionTitleLb.frame;
        fram_.size.height = rect_titleLb.size.height;
        questionTitleLb.frame = fram_;
    }
    
    questionTitleLb.text = q.question;
    
    __block float cur_y = questionTitleLb.frame.size.height + questionTitleLb.frame.origin.y + 10;
    
    if (_answerView) {
        [_answerView editDone];
        [_answerView removeFromSuperview];
        _answerView = nil;
    }
    
    BOOL isContactQ;
    CGRect fram_answer;
    if ([q.question isEqualToString:@"First Name"]) {
        emailHadVerify = NO;
        isContactQ = YES;
        cur_y = questionLb.frame.size.height+questionLb.frame.origin.y+10;
        fram_answer = CGRectMake(0, cur_y, contentV.frame.size.width, 0);
    }else{
        isContactQ = NO;
        fram_answer = CGRectMake(0, cur_y, contentV.frame.size.width, 0);
    }
    
    self.answerView = [[AnswerView alloc] initWithFrame:CGRectMake(50, cur_y, contentV.frame.size.width - 100, 0)];
    _answerView.delegate = self;
    _answerView.questionType = QuestionTypeAppear;
    
    if (isContactQ) {
        _answerView.isContactInfo = YES;
    }else{
        _answerView.isContactInfo = NO;
    }
    [_answerView initSubView:q];
    [contentV addSubview:_answerView];
    
    cur_y += _answerView.frame.size.height +20;
    
    CGRect fram_pro =  processBtView.frame;
    fram_pro.origin.y = cur_y;
    processBtView.frame = fram_pro;
    
    cur_y += processBtView.frame.size.height;
    
    contentV.contentSize  = CGSizeMake(contentV.frame.size.width, cur_y);
    contentOriginRect = contentV.frame;
    
    if (q.tool_tip && ![q.tool_tip isEqualToString:@""]) {
        [self.delegate toolTipIsHid:NO];
    }
    // is the last questio or button type questio, hide button on navigation bar need hide
//    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON || index_curQ == questionArray.count -1) {
    if (index_curQ == questionArray.count -1) {
        [self.delegate navigationBarNextIsAppear:NO];
    }else{
        [self.delegate navigationBarNextIsAppear:YES];
    }
//    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON || index_curQ == 0) {
    if (index_curQ == 0) {
        [self.delegate navigationBarPreviousIsAppear:NO];
    }else{
        [self.delegate navigationBarPreviousIsAppear:YES];
    }
}

-(void)nextQuestion{
    
    [_answerView editDone];
    if (index_curQ > -1) {
        if (![self qVerify]) {
            return;
        }
    }
    // done
    NSString  * title = [nextBt titleForState:0];
    if ([title isEqualToString:@"Done"]) {
        
        Lead * lead = (Lead *)[[CoredataManager manager] newObjectFromEntity:@"Lead"];
        lead.list = _list;
        lead.agent_id = [GlobalInstance instance].user_id;
        lead.hadChange = @"no";
        lead.created = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
        
        NSString * extraStr = [[NSUserDefaults standardUserDefaults] objectForKey:ExtraID];
        if (!extraStr) {
            extraStr = @"0";
        }
        lead.extra = extraStr;
        
        NSString * newExtra = [NSString stringWithFormat:@"%d",([extraStr intValue]+1)];
        [[NSUserDefaults standardUserDefaults] setValue:newExtra forKey:ExtraID];

        NSString * num = [[NSUserDefaults standardUserDefaults] objectForKey:LeadID];
        if (!num) {
            num = [NSString stringWithFormat:@"%d",LeadBaseId];
        }
        lead.lead_id = num;
        int n = (int)[num integerValue];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",(n-1)] forKey:LeadID];
        [[NSUserDefaults standardUserDefaults] synchronize];

        __block int leadQIndex = 0;
        
        [questionArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Question * q = (Question *)obj;
            if ([q.question isEqualToString:@"First Name"]) {
                [q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    QuestionCell * cell = (QuestionCell *)obj;
                    if (idx == 0) {
                        lead.first_name = cell.question_answer;
                    }else if (idx == 1){
                        lead.last_name = cell.question_answer;
                    }else if (idx == 2){
                        lead.email = cell.question_answer;
                    }
                }];
            }else if ([q.question isEqualToString:@"Phone"]){
                QuestionCell * cell = (QuestionCell *)q.qCells[0];
                lead.phone = cell.question_answer;
            }else if ([q.question isEqualToString:@"What is your current address?"]){
            
                [q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    QuestionCell * cell = (QuestionCell *)obj;
                    switch (idx) {
                        case 0:{
                            lead.address = cell.question_answer;
                            break;
                        }
                        case 1:{
                            lead.zipCode  = cell.question_answer;
                            break;
                        }
                        case 2:{

                            lead.state  = cell.question_answer;
                            break;
                        }
                        case 3:{
                            lead.city  = cell.question_answer;
                            break;
                        }
                        default:
                            break;

                    }
                }];
            }else if([q.question isEqualToString:@"What is your date of birth?"]){
                QuestionCell * cell = q.qCells[0];
                lead.brithday = cell.question_answer;
            }else if ([q.question_id integerValue] == 16 || [q.question_id integerValue] == 17){
             // dob and address
                return ;
            }else{
                if ([q.is_saved_answer intValue] == 0 && [q.user_id intValue] == 0) {
                    return;
                }
                __block BOOL isValid = YES;
                
                [q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    QuestionCell * cell = (QuestionCell *)obj;
                    if (!cell.question_answer || [cell.question_answer isEqualToString:@""]) {
                        isValid = NO;
                        * stop = YES;
                    }
                }];
                if (isValid) {
                    [q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        QuestionCell * cell = (QuestionCell *)obj;
                        LeadQuestion * lq =(LeadQuestion *)[[CoredataManager manager] newObjectFromEntity:@"LeadQuestion"];
                        lq.answer = cell.question_answer;
                        lq.answerValue = cell.question_answer;
                        lq.answerId = cell.question_answerID;
                        lq.id_ = [NSString stringWithFormat:@"%d",leadQIndex];
                        lq.questionId = q.question_id;
                        lq.title = cell.question_appear;
                        lq.sortNum = lq.id_;
                        lq.type = @"2";
    
                        [lead addLeadQObject:lq];
                        
                        leadQIndex++;
                    }];
                    [[CoredataManager manager] saveContext];
                }
            }
        }];
        [_list addLeadObject:lead];
        [[CoredataManager manager] saveContext];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadShowLead" object:nil];
        
        [self.delegate exitQuestionNaire:lead];
        return;
    }
    // next
    //  to next
    if (index_curQ > -1) {
        if (![self qVerify]) {
            return;
        }
        Question * q_ = questionArray[index_curQ];
        if ([q_.question isEqualToString:@"First Name"]) {
            
            if (!emailHadVerify) {
                QuestionCell * cell = q_.qCells[2];
                NSString * email = cell.question_answer;
                [self.delegate emailVerify:email];
                return;
            }
        }
    }
    //  emai verify
    [self.delegate toolTipIsHid:YES];
    
    if (index_curQ == questionArray.count-1) {
        return;
    }
    index_curQ ++;
    if (index_curQ == questionArray.count -1) {
        [nextBt setTitle:@"Done" forState:0];
    }else{
        nextBt.hidden = NO;
    }
    //  is the first question , hide back button
    if (index_curQ != 0) {
        if (backBt.hidden) {
            CGRect fram = backBt.frame;
            fram.origin.x = contentV.frame.size.width - fram.size.width - 60;
            nextBt.frame = fram;
            
            backBt.hidden = NO;
        }
    }
    CGRect fram = processLb.frame;
    fram.size.width += width_perQ;
    processLb.frame = fram;
    
    NSString * str =[NSString stringWithFormat:@"Question %d of %lu",index_curQ+1,(unsigned long)questionArray.count];
    [self.delegate pageNum:str];
    
    Question * q = questionArray[index_curQ];
    // title
    NSDictionary *attributes = @{NSFontAttributeName:questionTitleLb.font};
    CGRect rect_titleLb = [q.question boundingRectWithSize:CGSizeMake(questionTitleLb.frame.size.width, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributes
                                            context:nil];
    if (rect_titleLb.size.height >45) {
        CGRect fram_ = questionTitleLb.frame;
        fram_.size.height = rect_titleLb.size.height;
        questionTitleLb.frame = fram_;
    }
    questionTitleLb.text = q.question;
    
    __block float cur_y = questionTitleLb.frame.size.height + questionTitleLb.frame.origin.y + 10;
    
    if (_answerView) {
        [_answerView editDone];
        [_answerView removeFromSuperview];
        _answerView = nil;
    }
    
    BOOL isContactQ;
    CGRect fram_answer;
    if ([q.question isEqualToString:@"First Name"]) {
        emailHadVerify = NO;
        isContactQ = YES;
        cur_y = questionLb.frame.size.height+questionLb.frame.origin.y+10;
        fram_answer = CGRectMake(0, cur_y, contentV.frame.size.width, 0);
    }else{
        isContactQ = NO;
        fram_answer = CGRectMake(0, cur_y, contentV.frame.size.width, 0);
    }
    
    self.answerView = [[AnswerView alloc] initWithFrame:CGRectMake(50, cur_y, contentV.frame.size.width -100, 0)];
    _answerView.delegate = self;
    _answerView.questionType = QuestionTypeAppear;
    
    if (isContactQ) { // is question First Name
        _answerView.isContactInfo = YES;
    }else{
        _answerView.isContactInfo = NO;
    }
    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON) {
//        nextBt.hidden = YES;
    }else{
        nextBt.hidden = NO;
    }
    [_answerView initSubView:q];
    [contentV addSubview:_answerView];
    cur_y += _answerView.frame.size.height +20;
    CGRect fram_pro =  processBtView.frame;
    fram_pro.origin.y = cur_y;
    processBtView.frame = fram_pro;
    cur_y += processBtView.frame.size.height;
    contentV.contentSize  = CGSizeMake(contentV.frame.size.width, cur_y);
    contentOriginRect = contentV.frame;
    if (q.tool_tip && ![q.tool_tip isEqualToString:@""]) {
        [self.delegate toolTipIsHid:NO];
    }
    // is the last questio or button type questio, hide button on navigation bar need hide
//    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON || index_curQ == questionArray.count -1) {
    if (index_curQ == questionArray.count -1) {
        [self.delegate navigationBarNextIsAppear:NO];
    }else{
        [self.delegate navigationBarNextIsAppear:YES];
    }
    
//    if ([q.answer_type intValue] == QUESTION_ANSER_TYPE_BUTTON || index_curQ == 0) {
    if (index_curQ == 0) {
        [self.delegate navigationBarPreviousIsAppear:NO];
    }else{
        [self.delegate navigationBarPreviousIsAppear:YES];
    }
}


-(BOOL)qVerify{
    
    Question * _q = questionArray[index_curQ];
    if ([_q.question isEqualToString:@"First Name"]) {
        
        __block BOOL isOk = YES;
        [_q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            QuestionCell * cell = (QuestionCell *)obj;
            
            if (!cell.question_answer || [cell.question_answer isEqualToString:@""]) {
                isOk = NO;
                [_answerView makeTfAlert:ContactTypeBaseTag+(int)idx alertStr:@"Please Input"];
                *stop = YES;
            }else if (idx == 2){
                if (![Utility isLegalEmailAddress:cell.question_answer]) {
                    isOk = NO;
                    [_answerView makeTfAlert:ContactTypeBaseTag+(int)idx alertStr:EmailFormatWrong];
                    *stop = YES;
                }
            }
        }];
        if (isOk) {
            if (![self configVerfig:_q]) {
                isOk = NO;
            }
        }
        if (!isOk) {
            return isOk;
        }
    }
    __block BOOL isOk = YES;
    //  is require
    
    if ([_q.is_required integerValue] == 1) {
        switch ([_q.answer_type intValue]) {
            case QUESTION_ANSER_TYPE_SELECT:
            {
                QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                if (!cell.question_answer || [cell.question_answer isEqualToString:@""]) {

                    if (!selectAlertInfo) {
                        CGRect fram = CGRectMake(questionTitleLb.frame.origin.x, questionTitleLb.frame.size.height+questionTitleLb.frame.origin.y-5, questionTitleLb.frame.size.width, 15);
                        selectAlertInfo = [[UILabel alloc] initWithFrame:fram];
                        selectAlertInfo.textColor = [UIColor redColor];
                        selectAlertInfo.text =RequireAlert;
                        selectAlertInfo.backgroundColor = [UIColor clearColor];
                        selectAlertInfo.font = [UIFont systemFontOfSize:14];
                        [contentV addSubview:selectAlertInfo];
                    }
                    selectAlertInfo.hidden = NO;
                    isOk = NO;
                }
                break;
            }
            case QUESTION_ANSER_TYPE_PHONENUMBER:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
            case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:
            case QUESTION_ANSER_TYPE_EMAIL:
            case QUESTION_ANSER_TYPE_EMAIL_VERIFY:
            case QUESTION_ANSER_TYPE_SSN:
            case QUESTION_ANSER_TYPE_SSN_JOIN:
            case QUESTION_ANSER_TYPE_TEXT:
            case QUESTION_ANSER_TYPE_DOB:
            case QUESTION_ANSER_TYPE_DOB_JOIN:
            case QUESTION_ANSER_TYPE_PASSWORD:
            {
                QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                if (!cell.question_answer || [cell.question_answer isEqualToString:@""]) {
                    [_answerView makeTfAlert:888 alertStr:RequireAlert];
                    isOk = NO;
                }
                break;
            }
            case QUESTION_ANSER_TYPE_MONEY:{
                
                // monthly income
                // annual income
                if ([_q.question isEqualToString:@"Annual Income"]){
                    QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                    if (cell.question_answer) {
                        NSLog(@"%@",cell.question_answer);
                        NSLog(@"%@",cell.question_appear);
                    }
                }else if ([_q.question isEqualToString:@"Monthly debts"]){
                
                }
                break;
            }
            case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
            case QUESTION_ANSER_TYPE_ADDRESS:{
                MMUTextFile * address = (MMUTextFile *)[self viewWithTag:AddressTfBaseTag];
                MMUTextFile * zipCode =(MMUTextFile *)[self viewWithTag:AddressTfBaseTag+1];
                MMUTextFile * state =(MMUTextFile *)[self viewWithTag:AddressTfBaseTag+2];
                MMUTextFile * city =(MMUTextFile *)[self viewWithTag:AddressTfBaseTag+3];
                
                if (!address.text || [address.text isEqualToString:@""]) {
                    address.alertInfo.text = RequireAlert;
                    address.tfState = TFStateAlert;
                    isOk = NO;
                }
                if (!zipCode.text || [zipCode.text isEqualToString:@""]) {
                    zipCode.alertInfo.text = RequireAlert;
                    zipCode.tfState = TFStateAlert;
                    isOk = NO;
                }
                if (!state.text || [state.text isEqualToString:@""]) {
                    state.alertInfo.text = RequireAlert;
                    state.tfState = TFStateAlert;
                        isOk = NO;
                }
                if (!city.text || [city.text isEqualToString:@""]) {
                    city.alertInfo.text = RequireAlert;
                    city.tfState = TFStateAlert;
                    isOk = NO;
                }
                
                break;
            }
            case QUESTION_ANSER_TYPE_RADIO:
            case QUESTION_ANSER_TYPE_CHECKBOX:
            case QUESTION_ANSER_TYPE_BUTTON:
            case QUESTION_ANSER_TYPE_FULL_NAME:{
                break;
            }
            default:
                break;
        }
    
    }
    if (!isOk) {
        return isOk;
    }
    
    // cofig

    if (![self configVerfig:_q]) {
        return NO;
    }
    
    //  is right form
    
    switch ([_q.answer_type intValue]) {
        case QUESTION_ANSER_TYPE_SELECT:
            
        case QUESTION_ANSER_TYPE_RADIO:
        case QUESTION_ANSER_TYPE_CHECKBOX:
        case QUESTION_ANSER_TYPE_BUTTON:
        case QUESTION_ANSER_TYPE_PASSWORD:
        {
            break;
        }
        case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
        case QUESTION_ANSER_TYPE_ADDRESS:{
            
            UITextField * address = (UITextField *)[self viewWithTag:AddressTfBaseTag];
            UITextField * zipCode =(UITextField *)[self viewWithTag:AddressTfBaseTag+1];
            UITextField * state =(UITextField *)[self viewWithTag:AddressTfBaseTag+2];
            UITextField * city =(UITextField *)[self viewWithTag:AddressTfBaseTag+3];
            
            if([address.text isEqualToString:@""] && [zipCode.text isEqualToString:@""] && [state.text isEqualToString:@""] && [city.text isEqualToString:@""]){
                // this questio is not answer
                break;
            }
            [_q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                QuestionCell * qCell =  (QuestionCell *)obj;
                if ([qCell.question_answer isEqualToString:@""]) {
                    [_answerView makeTfAlert:AddressTfBaseTag+(int)idx alertStr:@"Please input"];
                    isOk = NO;
                    * stop = YES;
                }
            }];
            break;
        }
        case QUESTION_ANSER_TYPE_TEXT:
        case QUESTION_ANSER_TYPE_MONEY:{
            if ([_q.question isEqualToString:@"Annual Income"]){
                QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                if (cell.question_answer && cell.question_answer.length != 0) {
                    NSString * money = [cell.question_answer stringByReplacingOccurrencesOfString:@"," withString:@""];
                    self.coustomerIncome = [NSString stringWithFormat:@"%f",[money floatValue]];
                    if ([money intValue] < CustomerAnnalIncomeLower) {
                        isOk = NO;
                        [_answerView makeTfAlert:888 alertStr:CustomerIncomeNotEnoughForCertificate];
                    }
                }
            }else if ([_q.question isEqualToString:@"Monthly debts"]){
                if (_coustomerIncome) {
                    QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                    cell.warmingText = nil;
                    if (cell.question_answer && cell.question_answer.length != 0) {
                        NSString * debtsMoney = [cell.question_answer stringByReplacingOccurrencesOfString:@"," withString:@""];

                        float debtPercentage = [debtsMoney floatValue] / [_coustomerIncome floatValue];
                        
                        if (debtPercentage > DebtPercentageErrorLevel) {
                            isOk = NO;
                            [_answerView makeTfAlert:888 alertStr:TextDebtsError];
                        }else if (debtPercentage > DebtPercentageWarmLevel){
                            [_answerView makeTfAlert:888 alertStr:TextDebtsWarm];
                            cell.warmingText = TextDebtsWarm;
                        }
                    }
                }
            }
            break;
        }
        case QUESTION_ANSER_TYPE_DOB:
        case QUESTION_ANSER_TYPE_DOB_JOIN:
        case QUESTION_ANSER_TYPE_FULL_NAME:{
            break;
        }
        case QUESTION_ANSER_TYPE_SSN:
        case QUESTION_ANSER_TYPE_SSN_JOIN:{
            QuestionCell * qcell = _q.qCells[0];
            if ((!qcell.question_answer || [qcell.question_answer isEqualToString:@""]) && ([_q.is_required intValue]!=1)) {
                break;
            }
            if (qcell.question_answer.length != 9) {
                isOk = NO;
                [_answerView makeTfAlert:888 alertStr:TextInputFormatNotCorrect];
            }
            break;
        }
        case QUESTION_ANSER_TYPE_EMAIL:
        case QUESTION_ANSER_TYPE_EMAIL_VERIFY:{
            QuestionCell * cell = (QuestionCell *)_q.qCells[0];
            if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
                if (![Utility isLegalEmailAddress:cell.question_answer]) {
                    isOk = NO;
                    [_answerView makeTfAlert:888 alertStr:EmailFormatWrong];
                }
            }
            break;
        }
        case QUESTION_ANSER_TYPE_PHONENUMBER:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:{
            
            QuestionCell * cell = (QuestionCell *)_q.qCells[0];
            if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
                if (cell.question_answer.length != 13) {
                    isOk = NO;
                    [_answerView makeTfAlert:888 alertStr:AlertMsgForPhoneDigit];
                }
            }
            break;
        }
        case QUESTION_ANSER_TYPE_HIDDEN:{
            break;
        }
        default:
            break;
    }
    return isOk;
}


-(BOOL)configVerfig:(Question *)_q{
    
    __block BOOL isOk = YES;
    if (_q.config && _q.config.length !=0) {
        
        if ([_q.answer_type intValue]  == QUESTION_ANSER_TYPE_TEXT) {
            
            NSData * jsonData = [_q.config dataUsingEncoding: NSUTF8StringEncoding];
            NSError * error=nil;
            NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
            
            if ([_q.question isEqualToString:@"First Name"]) {
                __block NSString * alertStr;
                [_q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if (idx == 2) {
                        return ;
                    }
                    QuestionCell * cell = (QuestionCell *)obj;
                    NSString * answer = cell.question_answer;
                    
                    [[parsedData allKeys] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSString * key = (NSString *)obj;
                        
                        if ([key isEqualToString:@"rule"]) {
                            NSString * value = parsedData[key];
                            
                            NSArray * alertArray = @[@"alpha-numeric characters, underscores or dashes only please",
                                                     @"natural number only please",
                                                     @"number and comma only please",
                                                     @"alpha, underscores, space and apostrophe(') only please"];
                            
                            NSArray * regularArray = @[@"^([a-zA-Z0-9]||-)+",@"^([0-9])+",@"^([0-9]|,\\.)+",@"^([a-zA-Z]|\\s|'|_)+"];
                            NSArray * array = @[@"validation_alpha_dash",@"validationis_natural",@"validation_number",@"validation_name"];
                            

                            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                NSString * str = (NSString *)obj;
                                if ([str isEqualToString:value]) {
                                    
                                    NSString * regular = regularArray[idx];
                                    
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regular];
                                    
                                    if (![predicate evaluateWithObject:answer]) {
                                        isOk = NO;
                                        alertStr = alertArray[idx];
                                    }
                                    *stop = YES;
                                }
                            }];
                        }else if ([key isEqualToString:@"min_len"]){
                            NSString * len = parsedData[@"min_len"];
                            if (answer.length < [len intValue]) {
                                alertStr = [NSString stringWithFormat:@"you should input more than %@",len];
                                isOk = NO;
                            }
                        }else if ([key isEqualToString:@"max_len"]){
                            NSString * len = parsedData[@"max_len"];
                            if (answer.length > [len intValue]) {
                                alertStr = [NSString stringWithFormat:@"you should not input more than %@",len];
                                isOk = NO;
                            }
                        }
                        if (!isOk) {
                            *stop = YES;
                        }
                    }];
                    if (!isOk) {
                        
                        int n = ContactTypeBaseTag+(int)idx;
                        [_answerView makeTfAlert:n alertStr:alertStr];
                        *stop = YES;
                    }
                }];
            }else{
                QuestionCell * cell = (QuestionCell *)_q.qCells[0];
                NSString * answer = cell.question_answer;
                
                if ((answer && answer.length != 0) || [_q.is_required intValue] ==1) {
                    [[parsedData allKeys] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSString * key = (NSString *)obj;
                        
                        if ([key isEqualToString:@"rule"]) {
                            NSString * value = parsedData[key];
                            
                            NSArray * alertArray = @[@"alpha-numeric characters, underscores or dashes only please",
                                                     @"natural number only please",
                                                     @"number and comma only please",
                                                     @"alpha, underscores, space and apostrophe(') only please"];
                            
                            NSArray * regularArray = @[@"^([a-zA-Z0-9]||-)+",@"^([0-9])+",@"^([0-9]|,\\.)+",@"^([a-zA-Z]|\\s|'|_)+"];
                            NSArray * array = @[@"validation_alpha_dash",@"validationis_natural",@"validation_number",@"validation_name"];
                            
                            __block NSString * alert;
                            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                NSString * str = (NSString *)obj;
                                if ([str isEqualToString:value]) {
                                    
                                    NSString * regular = regularArray[idx];
                                    
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regular];
                                    
                                    if (![predicate evaluateWithObject:answer]) {
                                        isOk = NO;
                                        alert = alertArray[idx];
                                    }
                                    *stop = YES;
                                }
                            }];
                            [_answerView makeTfAlert:888 alertStr:alert];
                        }else if ([key isEqualToString:@"min_len"]){
                            NSString * len = parsedData[@"min_len"];
                            if (answer.length < [len intValue]) {
                                [_answerView makeTfAlert:888 alertStr:[NSString stringWithFormat:@"you should input more than %@",len]];
                                isOk = NO;
                            }
                        }else if ([key isEqualToString:@"max_len"]){
                            NSString * len = parsedData[@"max_len"];
                            if (answer.length > [len intValue]) {
                                [_answerView makeTfAlert:888 alertStr:[NSString stringWithFormat:@"you should not input more than %@",len]];
                                isOk = NO;
                            }
                        }
                        if (!isOk) {
                            *stop = YES;
                        }
                    }];

                }
            }
        }
    }
    return isOk;
}

#pragma mark -- AnswerViewDelegate

-(void)autoNextQuestion{

    [self nextQuestion];
}

-(void)selectHasReply{
    if (!selectAlertInfo.hidden) {
        selectAlertInfo.hidden = YES;
    }
}
-(void)remarkSelfFrmeWithMMutextfile{
    CGRect  frame =  processBtView.frame;
    frame.origin.y = CGRectGetMaxY(_answerView.frame) + 20;
    processBtView.frame = frame;
}

@end
