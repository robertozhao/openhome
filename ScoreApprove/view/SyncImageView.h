//
//  SyncImageView.h
//  ScoreApprove
//
//  Created by Yang on 15/5/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Picture.h"

@interface SyncImageView : UIImageView


@property (nonatomic, strong) Picture * pic;
@property  int  isBig;  // 0:small  1:big
@property (nonatomic, strong) NSString * placeHold;
@end
