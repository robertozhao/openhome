//
//  QuestionEditView.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/16.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "QuestionEditView.h"
#import "UUITextFile.h"
#import "TextResource.h"

@implementation QuestionEditView



-(void)initSubView{

    float contentVW = 600;
    UIView * navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentVW, 44)];
    navView.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:navView];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = ManageQuestion;
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * add = [UIButton buttonWithType:0];
    [add setTitle:Add forState:UIControlStateNormal];
    [add addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [add setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    add.frame = CGRectMake(navView.frame.size.width - 90, 0, 70, navView.frame.size.height);
    [navView addSubview:add];
    
    myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, navView.frame.size.height, self.frame.size.width, self.frame.size.height - navView.frame.size.height) style:0];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self addSubview:myTableView];

}




-(void)add{

}

-(void)done{

}



#pragma mark --  UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return source.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentify = @"cellIdentify";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
#pragma mark -- UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
