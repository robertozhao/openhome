//
//  PopView.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/2.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "PopView.h"
#import "Macro.h"
#import "UUITextFile.h"
#import "GlobalInstance.h"
#import "Utility.h"
#import "TextResource.h"
#import "DataManage.h"

@implementation PopView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

-(void)setViewType:(enum PopViewType)viewType{

    bkControl = [[UIControl alloc] initWithFrame:self.bounds];
    bkControl.backgroundColor = [UIColor blackColor];
    bkControl.alpha = 0.6;
    [self addSubview:bkControl];
    
    UIView *cancelView = [[UIView alloc] initWithFrame:self.frame];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancel)];
    [cancelView addGestureRecognizer:recognizer];
    [self addSubview:cancelView];
    
    switch (viewType) {

        case PopViewTypeEditListAddress:{
            [self creatSubViewForAddressEdit];
            break;
        }
        case PopViewTypePriceEdit:{
            [self creatSubViewForPriceEdit];
            break;
        }
        case PopViewTypeAreaEdit:{
            [self creatSubViewForAreaEdit];
            break;
        }
        case PopViewTypeDescription:{
            [self creatSubViewForDescriptionEdit];
            break;
        }
        case PopViewTypeEmailSignature:{
            [self creatSubViewForEmailSignture];
        }
        default:
            break;
    }
}

-(void)cancel{

    [self removeFromSuperview];
}


//  address edit
-(void)creatSubViewForAddressEdit{
    
    float contentVW = 550;
    contentV = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-contentVW)/2.0, 40, contentVW, 500)];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 6;
    contentV.clipsToBounds = YES;
    [self addSubview:contentV];
    

    navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [contentV addSubview:navView];
    
    UILabel * lb_line = [[UILabel alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, navView.frame.size.width, 1)];

    lb_line.backgroundColor = RGBCOLOR(210, 210, 210);
    [navView addSubview:lb_line];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = Address;
    lb.textColor = [UIColor blackColor];
    lb.font = [UIFont systemFontOfSize:19];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleCancel forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [done addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    done.frame = CGRectMake(navView.frame.size.width - 90, 0, 70, navView.frame.size.height);
    [navView addSubview:done];

 
    
    addressEditTf = [[UITextField alloc] initWithFrame:CGRectMake(30,navView.frame.size.height + navView.frame.origin.y + 40, contentVW-2*30, 80)];
    UILabel * lb_ = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 90, 70)];
    lb_.text = [NSString stringWithFormat:@"  %@",Address];
    lb_.font = [UIFont systemFontOfSize:19];
    lb_.backgroundColor = [UIColor clearColor];
    addressEditTf.leftViewMode = UITextFieldViewModeAlways;
    addressEditTf.leftView = lb_;
    addressEditTf.borderStyle = UITextBorderStyleLine;
    addressEditTf.layer.cornerRadius = 8;
    addressEditTf.layer.borderWidth = 1;
    addressEditTf.layer.borderColor = RGBCOLOR(220, 220, 220).CGColor;
    addressEditTf.text = _list.address;
    addressEditTf.clearButtonMode = UITextFieldViewModeAlways;
    addressEditTf.font = [UIFont systemFontOfSize:25];
    [contentV addSubview:addressEditTf];
    
    
    [addressEditTf becomeFirstResponder];
}


-(void)done{
    if ([addressEditTf.text isEqualToString:@""]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:AlertMsgForNoAddress delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    
    [self.delegate newAddress:addressEditTf.text];
    
    [self cancel];
}

//  property url



-(void)add{

}

//  price edit

-(void)creatSubViewForPriceEdit{

    
    float contentVW = 550;
    contentV = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-contentVW)/2.0, 40, contentVW, 500)];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 6;
    contentV.clipsToBounds = YES;
    [self addSubview:contentV];
    
    
    navView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, contentVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [contentV addSubview:navView];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = ButtonTitlePrice;
    lb.font = [UIFont systemFontOfSize:19];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleCancel forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [done addTarget:self action:@selector(priceDone) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    done.frame = CGRectMake(navView.frame.size.width - 80, 0, 70, navView.frame.size.height);
    [navView addSubview:done];
    
    
    UILabel * lb_line = [[UILabel alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, navView.frame.size.width, 1)];
    lb_line.backgroundColor = RGBCOLOR(210, 210, 210);
    [navView addSubview:lb_line];
    
 
    
    float padding = 20;
    priceTf = [[UITextField alloc] initWithFrame:CGRectMake(padding, navView.frame.size.height + navView.frame.origin.y + 2* padding, contentVW-2*padding, 80)];
    UILabel * lb_ = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 80, 80)];
    lb_.text = [NSString stringWithFormat:@"    %@",ButtonTitlePrice];
    lb_.font = [UIFont systemFontOfSize:19];
    lb_.backgroundColor = [UIColor clearColor];
    priceTf.leftViewMode = UITextFieldViewModeAlways;
    
    priceTf.delegate = self;
    priceTf.leftView = lb_;
    priceTf.keyboardType = UIKeyboardTypeNumberPad;
    priceTf.borderStyle = UITextBorderStyleLine;
    priceTf.layer.cornerRadius = 4;
    priceTf.layer.borderWidth = 1;
    priceTf.layer.borderColor = RGBCOLOR(220, 220, 220).CGColor;
    priceTf.text = [_list.price isEqualToString:@"0"]?@"":_list.price;
    priceTf.clearButtonMode = UITextFieldViewModeAlways;
    priceTf.font = [UIFont systemFontOfSize:25];
    [contentV addSubview:priceTf];
    
    
    [priceTf becomeFirstResponder];

}


-(void)priceDone{
    [self.delegate newPrice:priceTf.text];
    [self cancel];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"tableViewReload" object:nil];
}


// description edit
//  price edit

-(void)creatSubViewForDescriptionEdit{
    
    float contentVW = 550;
    contentV = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-contentVW)/2.0, 20, contentVW, 340)];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 6;
    contentV.clipsToBounds = YES;
    [self addSubview:contentV];
    
    navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [contentV addSubview:navView];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = Description;
    lb.font = [UIFont systemFontOfSize:19];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleCancel forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [done addTarget:self action:@selector(descriptionDone) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    done.frame = CGRectMake(navView.frame.size.width - 80, 0, 70, navView.frame.size.height);
    [navView addSubview:done];
    
    UILabel * lb_line = [[UILabel alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, navView.frame.size.width, 1)];
    lb_line.backgroundColor = RGBCOLOR(210, 210, 210);

    [navView addSubview:lb_line];
    
    
    float padding = 20;
    descTV = [[UITextView alloc] initWithFrame:CGRectMake(padding, navView.frame.size.height + navView.frame.origin.y+padding, contentVW-2*padding, 250)];
    descTV.layer.cornerRadius = 4;
    descTV.layer.borderWidth = 1;
    descTV.font = [UIFont systemFontOfSize:19];

    descTV.text = _list.desc;

    descTV.layer.borderColor =RGBCOLOR(220, 220, 220).CGColor;
    [contentV addSubview:descTV];
    
    [descTV becomeFirstResponder];
}


-(void)descriptionDone{
    [self.delegate newDesc:descTV.text];
    [self cancel];
}


// area edit

-(void)creatSubViewForAreaEdit{
    
    float contentVW = 550;
    contentV = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-contentVW)/2.0, 20, contentVW, 500)];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 6;
    contentV.clipsToBounds = YES;
    [self addSubview:contentV];
    
    navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [contentV addSubview:navView];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = Size;
    lb.font = [UIFont systemFontOfSize:19];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleCancel forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [done addTarget:self action:@selector(areaDone) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    done.frame = CGRectMake(navView.frame.size.width - 80, 0, 70, navView.frame.size.height);
    [navView addSubview:done];
    
    
    UILabel * lb_line = [[UILabel alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, navView.frame.size.width, 1)];
    lb_line.backgroundColor = RGBCOLOR(210, 210, 210);
    
    [navView addSubview:lb_line];
    
    
    
    float padding = 20;
    areaTf = [[UITextField alloc] initWithFrame:CGRectMake(padding, navView.frame.size.height + navView.frame.origin.y+2*padding, contentVW-2*padding, 80)];
    UILabel * lb_ = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 80)];
    lb_.text = [NSString stringWithFormat:@"    %@",Size];
    lb_.font = [UIFont systemFontOfSize:19];
    lb_.backgroundColor = [UIColor clearColor];
    areaTf.leftViewMode = UITextFieldViewModeAlways;
    
    
    areaTf.leftView = lb_;
    areaTf.keyboardType = UIKeyboardTypeNumberPad;
    areaTf.borderStyle = UITextBorderStyleLine;
    areaTf.layer.cornerRadius = 4;
    areaTf.layer.borderWidth = 1;
    areaTf.layer.borderColor = RGBCOLOR(220, 220, 220).CGColor;
    areaTf.text = [_list.size isEqualToString:@"0"]?@"":_list.size;
    areaTf.delegate = self;
    areaTf.font = [UIFont systemFontOfSize:22];
    areaTf.clearButtonMode = UITextFieldViewModeAlways;
    [contentV addSubview:areaTf];
    
    [areaTf becomeFirstResponder];
    
    areaUnitChooseSegment = [[UISegmentedControl alloc] initWithItems:@[Sqft,Sqmters]];
    areaUnitChooseSegment.frame = CGRectMake(20, CGRectGetMaxY(areaTf.frame)+30, contentVW-2*padding, 50);
    areaUnitChooseSegment.tintColor = RGBCOLOR(252, 144, 0);
    if (!_list.sizeUnit || [_list.sizeUnit isEqualToString:Sqft]) {
        [areaUnitChooseSegment setSelectedSegmentIndex:0];
    }else{
        [areaUnitChooseSegment setSelectedSegmentIndex:1];
    }
    UIFont *font = [UIFont boldSystemFontOfSize:19.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
                                                           forKey:NSFontAttributeName];
    [areaUnitChooseSegment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    [contentV addSubview:areaUnitChooseSegment];
}
-(void)areaDone{
    if (areaUnitChooseSegment.selectedSegmentIndex == 0) {
        _list.sizeUnit = Sqft;
    }else{
        _list.sizeUnit = Sqmters;
    }
    [self.delegate newSize:areaTf.text];
    [self cancel];
}
-(void)creatSubViewForEmailSignture{

    float contentVW = 550;
    contentV = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-contentVW)/2.0, 20, contentVW, 340)];
    contentV.backgroundColor = [UIColor whiteColor];
    contentV.layer.cornerRadius = 6;
    contentV.clipsToBounds = YES;
    [self addSubview:contentV];
    
    navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [contentV addSubview:navView];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = @"Email  signture";
    lb.font = [UIFont systemFontOfSize:19];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:ButtonTitleCancel forState:UIControlStateNormal];
    [bt addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(0, 0, 70, navView.frame.size.height);
    [navView addSubview:bt];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleSave forState:UIControlStateNormal];
    [done addTarget:self action:@selector(signtureDone) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    done.frame = CGRectMake(navView.frame.size.width - 80, 0, 70, navView.frame.size.height);
    [navView addSubview:done];
    
    UILabel * lb_line = [[UILabel alloc] initWithFrame:CGRectMake(0, navView.frame.size.height-1, navView.frame.size.width, 1)];
    lb_line.backgroundColor = RGBCOLOR(210, 210, 210);
    
    [navView addSubview:lb_line];
    
    float padding = 20;
    signtureTV = [[UITextView alloc] initWithFrame:CGRectMake(padding, navView.frame.size.height + navView.frame.origin.y+padding, contentVW-2*padding, 250)];
    signtureTV.layer.cornerRadius = 4;
    signtureTV.layer.borderWidth = 1;
    signtureTV.font = [UIFont systemFontOfSize:19];
    
    
    NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];

    if (signtureDic) {
        
        NSString * str = signtureDic[@"signature"];
        
        if (str) {
         NSString * signture =  [str stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            signtureTV.text = signture;
        }
    }
    
    signtureTV.layer.borderColor =RGBCOLOR(220, 220, 220).CGColor;
    [contentV addSubview:signtureTV];
    
    [signtureTV becomeFirstResponder];
}

-(void)signtureDone{
    
    if ([signtureTV.text isEqualToString:@""]) {
        [self cancel];
        return;
    }
    
    NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    
    NSString * str = [signtureTV.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    NSDictionary *dic = @{@"signature":str,@"isHadNew":@"1",@"isAutoSend":signtureDic[@"isAutoSend"]};
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[DataManage instance] syncEmailSignToSV];
    
//    [self.delegate newEmailSignture:signtureTV.text];
    [self cancel];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == priceTf || textField == areaTf){

        NSString * str_ = @"0123456789";
        if (([str_ rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""])) {
            return NO;
        }
        NSString * str = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSString * temp;
        if ([string isEqualToString:@""]) {
            temp = [str substringToIndex:str.length-1];
        }else{
            temp = [NSString stringWithFormat:@"%@a",str];
        }
        NSString * valid = [NSString string];
        int j=0;
        for (int i = (int)temp.length-1; i>-1; i--) {
            
            if (j%3==0 && i!=temp.length-1 && j!=0) {
                valid = [NSString stringWithFormat:@",%@",valid];
            }
            NSString *s = [temp substringWithRange:NSMakeRange(i, 1)];
            valid = [NSString stringWithFormat:@"%@%@",s,valid];
            j++;
        }
        if ([string isEqualToString:@""]) {
            NSString * str =[textField.text substringFromIndex:textField.text.length-1];
            valid = [NSString stringWithFormat:@"%@%@",valid,str];
        }else{
            NSString *str =[valid substringToIndex:valid.length-1];
            valid = [NSMutableString stringWithString:str];
        }
        textField.text = valid;
    }
    return YES;
}



@end
