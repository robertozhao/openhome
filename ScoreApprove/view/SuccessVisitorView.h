//
//  SuccessVisitorView.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/21.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessVisitorView : UIView{

}
- (instancetype)initWithFrame:(CGRect)frame;
-(void)show:(UIView *)supView;

@end
