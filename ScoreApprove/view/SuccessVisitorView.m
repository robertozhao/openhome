//
//  SuccessVisitorView.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/21.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "SuccessVisitorView.h"
#import "TextResource.h"
@implementation SuccessVisitorView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubView];
    }
    return self;
}


-(void)initSubView{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4;
    
    UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0,self.frame.size.height - 130, self.frame.size.width, 50)];
    lb.textAlignment = 1;
    lb.text = AddLeadSuccessAlert;
    lb.font = [UIFont systemFontOfSize:30];
    [self  addSubview:lb];
    
    UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 181, 180)];
    imgV.center = CGPointMake(self.center.x, self.center.y-40);
    imgV.image = [UIImage imageNamed:@"checkmark.png"];
    [self addSubview:imgV];
}

-(void)show:(UIView *)supView{

    self.alpha = 0;
    self.center = CGPointMake(supView.center.x, supView.center.y-90);
    [supView addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            [self performSelector:@selector(hide) withObject:nil afterDelay:0.8];
        }
    }];
}


-(void)hide{

    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}

@end
