//
//  AddNoteView.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "AddNoteView.h"
#import "TextResource.h"
#import "Lead_Note.h"
#import "CoredataManager.h"

#define btnWidth  150
#define btnHeight  35

@implementation AddNoteView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(587, 320);
    [self initSubView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    tv.text = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [tv becomeFirstResponder];
}

-(void)initSubView{
        
    self.view.backgroundColor = [UIColor whiteColor];
    
    navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 587, 50)];
    navView.backgroundColor = PopViewNavColor;
    [self.view addSubview:navView];
    
    UIImageView *headerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, navView.frame.size.width, 50)];
    headerImage.contentMode = UIViewContentModeScaleToFill;
    [headerImage setImage:[UIImage imageNamed:@"notice-header.png"]];
    [navView addSubview:headerImage];
    //    [headerImage sendSubviewToBack:headerImage];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:navView.bounds];
    if (self.viewType == 0) {
        lb.text = @"Add Notes";
    } else {
        lb.text = @"Add Visitor Notes";
    }
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    tv = [[UITextView alloc] initWithFrame:CGRectMake(50, navView.frame.size.height + 25 , 490,166)];
    tv.layer.borderColor = RGBCOLOR(234, 234, 234).CGColor;
    tv.layer.borderWidth = 1;
    tv.layer.cornerRadius = 9;
    tv.font = [UIFont systemFontOfSize:17];
    [self.view addSubview:tv];
    
    float btnYPos = tv.frame.origin.y + tv.frame.size.height + 20;
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f];
    
    UIButton * done = [UIButton buttonWithType:0];
    if (self.viewType == 0) {
        [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    } else {
        [done setTitle:ButtonTitleContinue forState:UIControlStateNormal];
    }
    
    [done addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:[UIColor whiteColor] forState:0];
    [done setBackgroundImage:[UIImage imageNamed:@"begin-btn"] forState:UIControlStateNormal];
    [done.titleLabel setFont:font];
    done.frame = CGRectMake(navView.frame.size.width - 130 - btnWidth, btnYPos, btnWidth, btnHeight);
    [self.view addSubview:done];
    
    UIButton * cancel = [UIButton buttonWithType:0];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [cancel setTitleColor:[UIColor whiteColor] forState:0];
    [cancel setBackgroundImage:[UIImage imageNamed:@"blue-btn"] forState:UIControlStateNormal];
    [cancel.titleLabel setFont:font];
    cancel.frame = CGRectMake(130, btnYPos, btnWidth, btnHeight);
    [self.view addSubview:cancel];
}



-(void)cancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)done{
    if ([tv.text isEqualToString:@""]) {
        [self cancel];
        return;
    }
    
    Lead_Note * note = (Lead_Note *)[[CoredataManager manager] newObjectFromEntity:@"Lead_Note"];
    note.lead_id = _lead.lead_id;
    note.note = tv.text;
    note.lead = _lead;
    note.creat = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    
    NSString * leadTempId = [[NSUserDefaults standardUserDefaults] objectForKey:LeadNoteID];
    if (!leadTempId) {
        leadTempId = [NSString stringWithFormat:@"%d",LeadNoteBaseId];
    }
    
    note.extra = leadTempId;
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",([leadTempId intValue]-1)] forKey:LeadNoteID];
    
    [_lead addNotesObject:note];
    
    [[CoredataManager manager] saveContext];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(addNoteOK)]){
        [self.delegate addNoteOK];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
