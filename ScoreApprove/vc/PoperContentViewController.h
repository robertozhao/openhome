//
//  PoperContentViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/7.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Question.h"

@protocol PoperContentViewControllerDelegate <NSObject>
@optional
-(void)tbItemChoosed:(NSString *)text;
-(void)choosedBeds:(NSString *)bed baths:(NSString *)bath;
-(void)doneButtonPressed;
@end
NS_ENUM(int, TBtype){
    TBtypeListType =0,    //   list 类型
    TBtypePhotoChoose,   //  图片相关
    TBtypeMoneyUnit,     //  钱 单位
    TBtypeSchedule,     //  日历
    TBtypeFitment,     //  家具
    TBtypeAuction,     //  listState
    TBtypeSetting,    //  setting
    TBtypeListShare,    //  setting
    TBtypeQuestionAnswerType, // answer type
    TBtypeBed,
    TBtypeBaths
};

@interface PoperContentViewController : BaseViewController


@property (nonatomic,strong) List * list;
@property (nonatomic) int tbType;
@property (nonatomic, weak) id<PoperContentViewControllerDelegate>delegate;


-(NSInteger)tbType:(int)type;

@end
