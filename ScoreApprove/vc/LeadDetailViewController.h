//
//  LeadDetailViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "DetailBaseViewController.h"
#import "Lead.h"


@interface LeadDetailViewController : DetailBaseViewController

@property (nonatomic , strong) Lead * lead;

@end
