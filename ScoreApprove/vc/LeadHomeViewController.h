//
//  LeadHomeViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailBaseViewController.h"



@interface LeadHomeViewController : DetailBaseViewController

@property int soureCate;

@property (nonatomic , strong) List * menuFilterList;
@property (nonatomic , strong) List * curFilterList;

@end
