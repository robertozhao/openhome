//
//  EmailEditViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "EmailEditViewController.h"
#import <AddressBook/AddressBook.h>

@interface EmailEditViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    UITableView * myTableView;
    NSArray * source;
}

@end

@implementation EmailEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
//    source = @[@"Add Lead to Contacts"];
    source = @[@"Email signature",@"Automatic thank you emails"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
    myTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:0];
    myTableView.dataSource = self;
    myTableView.delegate  = self;
    myTableView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:myTableView];
}



-(void)syncLeadChange:(UISwitch *)sh{
    
//    if (sh.on) {
//        
//        CFErrorRef error = NULL;
//        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
//        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
//            if (granted) {
//                //agree use for address list
//                [DataManage instance].isSyncLeadToAddressList = sh.on;
//            }else{
//                
//                dispatch_async(dispatch_get_main_queue(), ^{ // 2
//                    sh.on = NO;
//                    
//                    id objc = [[NSUserDefaults standardUserDefaults] objectForKey:SyncLeadOnAddressList];
//                    if(objc){
//                        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Unable to add contacts" message:@"Please go into Privacy Settings on your iPad and click on Contacts.Make sure to flip the switch on for ScoreApprove" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        
//                        [alert show];
//                    }
//                    [DataManage instance].isSyncLeadToAddressList = sh.on;
//                });
//
//            }
//        });
//        
//
//    }else{
//        [DataManage instance].isSyncLeadToAddressList = sh.on;
//    }
}

-(void)shiftSh:(UISwitch *)sh{

    NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    NSMutableDictionary * temp = [NSMutableDictionary dictionaryWithObject:sh.on?@"1":@"0" forKey:@"isAutoSend"];
    

    [temp setObject:@"1" forKey:@"isHadNew"];

    if (dic[@"signture"]) {
        [temp setObject:dic[@"signture"] forKey:@"signture"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[DataManage instance] syncEmailSignToSV];
}

#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return source.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        NSString * cellIdentify = @"cellIdentify_text";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        NSString * str = source[indexPath.row];
        cell.textLabel.text = str;
        return cell;
    }else{
        NSString * cellIdentify = @"cellIdentify_switch";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            
            UISwitch * sh = [[UISwitch alloc] initWithFrame:CGRectMake(tableView.frame.size.width - 60,5, 50, 40)];
            sh.tag = 500;
            [sh addTarget:self action:@selector(shiftSh:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:sh];
        }
        
        NSString * str = source[indexPath.row];
        if ([str isEqualToString:str]) {
            UISwitch * sh = (UISwitch *)[cell viewWithTag:500];
            
            NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
            if (!signtureDic) {

                signtureDic = @{@"isAutoSend":@"0"};
                [[NSUserDefaults standardUserDefaults] setObject:signtureDic forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            sh.on = [signtureDic[@"isAutoSend"] intValue] ==0 ?NO:YES;
        }
        cell.textLabel.text = str;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 44;
}

#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"EditEmailSignature" object:nil];
    }
}




@end
