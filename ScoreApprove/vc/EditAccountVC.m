//
//  EditAccountVC.m
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "EditAccountVC.h"

@interface EditAccountVC (){

    IBOutlet UILabel *titleLb;
    IBOutlet UILabel *dgLb;
    IBOutlet UITextField *dgTxt;
    IBOutlet UIView *txtContainer;
}

@end

@implementation EditAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.preferredContentSize = CGSizeMake(444, 265);

    if (self.dgType == 0) {
        [titleLb setText:@"Notice"];
        dgLb.hidden = NO;
        txtContainer.hidden = YES;
    } else if (self.dgType == 1) {
        [titleLb setText:@"Add Option"];
        dgLb.hidden = YES;
        txtContainer.hidden = NO;
        
        dgTxt.placeholder = @"Option";
        dgTxt.text = self.answerTitle;  
        [dgTxt becomeFirstResponder];
        
        txtContainer.layer.cornerRadius = 3;
        txtContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
        txtContainer.layer.borderWidth = 1;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
}

- (void)setWindowType:(int)dgType {
    self.dgType = dgType;

}

- (IBAction)onCancelButton:(id)sender {
    [self.delegate cancelButtonClicked];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onContinueButton:(id)sender {
    if (self.dgType == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://agents.scoreapprove.com/leads/dashboard/profile"]];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else if (self.dgType == 1) {
        if ([dgTxt.text isEqualToString:@""]) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please add option title." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        [self.delegate continueButtonClicked:dgTxt.text];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
