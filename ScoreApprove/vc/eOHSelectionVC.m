//
//  LeadFilterViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "eOHSelectionVC.h"

@interface eOHSelectionVC (){
    
}

@property (nonatomic, strong) NSArray *selectionButtons;

@end

@implementation eOHSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.viewType == 0) {
        self.selectionButtons = @[Home,Townhouse,Condo,Coop,SingleFamily,LotLand,TIC,Loft];
    } else if (self.viewType == 1) {
        self.selectionButtons = @[ForSale,InContract,Sold,ShortSale,Rental,Rented,Auction];
    }
    
    [self initContentView];
}

- (void)initContentView {
    [self setPreferredContentSize:CGSizeMake(190, 40 * [self.selectionButtons count])];
    
    for (int i = 0; i < [self.selectionButtons count]; i++) {
        UIButton *itemButton = [[UIButton alloc] initWithFrame:CGRectMake(32, 20 + i * 40, 190, 40)];
        [itemButton setImage:[UIImage imageNamed:@"icon_radio"] forState:UIControlStateNormal];
        [itemButton setImage:[UIImage imageNamed:@"icon_radio_selected"] forState:UIControlStateSelected];
        NSString *labelTxt = [self.selectionButtons objectAtIndex:i];
        NSString *buttonTitle = [NSString stringWithFormat:@"   %@", labelTxt];
        [itemButton setTitle:buttonTitle forState:UIControlStateNormal];
        [itemButton setTitleColor:RGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
        [itemButton setTitleColor:RGBCOLOR(60, 141, 254) forState:UIControlStateSelected];
        [itemButton setTag:(1010 + i)];
        itemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.view addSubview:itemButton];
        
        [itemButton addTarget:self action:@selector(onItemButton:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([labelTxt isEqualToString:self.selectedText]) {
            [itemButton setSelected:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)onItemButton:(UIButton *) sender{
    UIButton *buttonWithTag = [self.view viewWithTag:(1010 + self.selectedIndex)];
    [buttonWithTag setSelected:NO];
    [sender setSelected:YES];
    int tag = (int)sender.tag - 1010;
    self.selectedIndex = tag;
    NSDictionary *userInfo = @{ @"text":[sender.currentTitle substringFromIndex:3], @"viewType" : [NSString stringWithFormat:@"%d", self.viewType] };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"eOHSelectiontTapped" object:nil userInfo: userInfo];
}

@end
