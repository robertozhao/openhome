//
//  QuestionCheckViewController.m
//  ScoreApprove
//
//  Created by Yang on 15/4/24.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "QuestionCheckViewController.h"
#import "PoperContentViewController.h"
#import "AnswerView.h"
#import "Answer.h"
#import "MAnswer.h"
#import "AlertVC.h"
#import "EditAccountVC.h"
#define AnswerItemEditAlertTag  100

@interface QuestionCheckViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PoperContentViewControllerDelegate,AnswerViewDelegate,UIAlertViewDelegate, EditAccountVCDelegate, DataManageDelegate>{

    IBOutlet UIScrollView *contentSV;
    IBOutlet UITextField *questionTf;
    IBOutlet UIButton *typeBt;
    
    IBOutlet UIView *answerContentV;
    BOOL isQFromSV;
    IBOutlet UILabel *typeLbForSvQ;
    IBOutlet UISwitch *requireSwitch;
    IBOutlet UILabel *questionTitleLb;
    
    NSMutableArray * answerItems;
    NSDictionary * answerTypeDic;
    IBOutlet UILabel *questionBottomLine;
    
    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    
    BOOL isEditted;
}

@property (nonatomic , strong) UITextField * cur_tf;
@property (nonatomic , strong)     AnswerView * answerView;
@property (nonatomic, strong)  PoperContentViewController * popContentVC;
@property (nonatomic, strong) UIPopoverController * popCtr;

@end

@implementation QuestionCheckViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"Question Detail";
    isNeedBack = YES;
    
    typeBt.layer.borderColor = RGBCOLOR(200, 200, 200).CGColor;
    typeBt.layer.borderWidth = 1.5;
    typeBt.layer.cornerRadius = 5;
    
    questionTf.layer.cornerRadius = 5;
    
    if (_questionType == 1) {
        if ([_q.user_id isEqualToString:@"0"]) {
            isQFromSV = YES;
        }else{
            isQFromSV = NO;
            UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, questionTf.frame.size.height)];
            questionTf.leftView = view;
            questionTf.leftViewMode = UITextFieldViewModeAlways;
        }
    }else{
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, questionTf.frame.size.height)];
        questionTf.leftView = view;
        questionTf.leftViewMode = UITextFieldViewModeAlways;
        isQFromSV = NO;
    }

    if (isQFromSV && ([_q.answer_type intValue] != QUESTION_ANSER_TYPE_SELECT &&  [_q.answer_type intValue] != QUESTION_ANSER_TYPE_BUTTON)) {
        if ([_q.is_required intValue] == 1) {
            requireSwitch.on = YES;
        }else{
            requireSwitch.on = NO;
        }
    }else{
        
        if ([_q.is_required intValue] == 1) {
            requireSwitch.on = YES;
        }else{
            requireSwitch.on = NO;
        }
    }
    
    
    //TODO:
    answerTypeDic = @{@"1":@"Select",
                      @"2":@"Radio",
                      @"3":@"Check Box",
                      @"4":@"Buttons",
                      @"5":@"Information: Question with only information",
                      @"20":@"Text",
                      @"21":@"Money",
                      @"22":@"Phone Number",
                      @"23":@"Phone Number",
                      @"24":@"Email",
                      @"25":@"Email",
                      @"26":@"Address",
                      @"27":@"Address",
                      @"28":@"DOB",
                      @"29":@"SSN",
                      @"30":@"Email with Xverify",
                      @"31":@"Phone Number",
                      @"32":@"Phone Number",
                      @"33":@"DOB",
                      @"34":@"SSN",
                      @"35":@"Password",
                      @"36":@"Hidden Question: this question will be show as a hidden form field" };
    if (isQFromSV) {
        questionTf.hidden = YES;
        typeBt.hidden = YES;
        typeLbForSvQ.text =answerTypeDic[_q.answer_type];
        questionTitleLb.text = _q.question;
    }else{
        questionBottomLine.hidden = YES;
        questionTf.layer.borderWidth = 1;
        questionTf.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        [typeBt setTitle:answerTypeDic[_q.answer_type] forState:0];
        questionTf.clearButtonMode = UITextFieldViewModeWhileEditing;
        
        questionTf.text = _q.question;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelButtonClicked) name:@"ModalViewDismissed" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
    
    isEditted = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [questionTf resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
//    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(done)];
//    self.navigationItem.rightBarButtonItem = item;
    
//    self.navigationItem.leftItemsSupplementBackButton = YES;
    
    if (isQFromSV && ([_q.answer_type intValue] != QUESTION_ANSER_TYPE_SELECT &&  [_q.answer_type intValue] != QUESTION_ANSER_TYPE_BUTTON)) {
        return;
    }
    contentOriginFram = contentSV.frame;
    if (_questionType == 0) {
        
        NSString * num = [[NSUserDefaults standardUserDefaults] objectForKey:QuestionID];
        if (!num) {
            num = [NSString stringWithFormat:@"%d",QuestionBaseId];
        }
        self.q = (Question *)[[CoredataManager manager] newObjectFromEntity:@"Question"];
        self.q.answer_type = @"1";
        self.q.sortNum = _sortNum;
        self.q.question_id = num;
        self.q.is_required = @"external";
        self.q.is_active = @"0";
        self.q.user_id = [GlobalInstance instance].user_id;
        self.q.tool_tip = @"";

        [typeBt setTitle:@"Select" forState:0];    
    }
    
    self.q.answerType_temp = self.q.answer_type;
    
    self.answerView = [[AnswerView alloc] initWithFrame:answerContentV.frame];
    _answerView.questionType = _questionType;
    _answerView.delegate = self;
    
    [_answerView initSubView:_q];
    [contentSV addSubview:_answerView];
}

- (void)viewWillDisappear:(BOOL)animated {
    if (self.questionType == 0 && isEditted == NO) {
        [self back];
    }
}

- (IBAction)isQuestionRequest:(id)sender {
    [self dismissKeyboard];
}

- (IBAction)onButtonSave:(id)sender {
    [self dismissKeyboard];
    [self done];
}


-(void)done{
    
    isEditted = YES;
    
    _q.is_required = requireSwitch.on?@"1":@"0";
    
    if (isQFromSV) {
        [[CoredataManager manager] saveContext];
        [self.delegate questionEditDone];
//        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    [_cur_tf resignFirstResponder];
    [_answerView editDone];
    
    
    if ([questionTf.text isEqualToString:@""]) {
        [self alertShow:@"Please input question description"];
        return;
    }else {
        
       __block BOOL isOK = YES;
        switch ([_q.answerType_temp intValue]) {
            case QUESTION_ANSER_TYPE_SELECT:{
                if ([_q.answer allObjects].count <2) {
                    [self showAddOptionAlert];
                    isOK = NO;
                }else{
                    [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        Answer * a = (Answer *)obj;
                        if ([a.answer_option isEqualToString:@""]) {
                            [self alertShow:@"Has answer no option"];
                            isOK = NO;
                            *stop = YES;
                        }
                    }];
                }
                break;
            }
            case QUESTION_ANSER_TYPE_BUTTON:{
                if ([_q.answer allObjects].count <2) {
                    [self showAddOptionAlert];
                    isOK = NO;
                }else{
                    [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        Answer * a = (Answer *)obj;
                        if ([a.answer_option isEqualToString:@""]) {
                            [self alertShow:@"Has answer no option"];
                            isOK = NO;
                            *stop = YES;
                        }
                    }];
                }
                break;
            }
            case QUESTION_ANSER_TYPE_TEXT:{

                break;
            }
        default:
                break;
        }
        if (!isOK) {
            return;
        }
    }
        [self showLoadingViewWithText:@""];
    _q.answer_type = _q.answerType_temp;
    _q.question = questionTf.text;
    if ([_q.answer_type isEqualToString:@"20"]) {
        // QUESTION_ANSER_TYPE_TEXT
        [[_q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Answer * answer = (Answer *)obj;
            answer.toDele = @"1";
        }];
        NSSet * as =_q.answer;
        [_q removeAnswer:as];
    }
    [[CoredataManager manager] saveContext];
    NSString * extraStr = [[NSUserDefaults standardUserDefaults] objectForKey:ExtraID];
    if (!extraStr) {
        extraStr = @"0";
    }
    self.q.extra = extraStr;
    
    NSString * newExtra = [NSString stringWithFormat:@"%d",([extraStr intValue]+1)];
    [[NSUserDefaults standardUserDefaults] setValue:newExtra forKey:ExtraID];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if (_questionType == 0) {
        NSString * num = [[NSUserDefaults standardUserDefaults] objectForKey:QuestionID];
        if (!num) {
            num = [NSString stringWithFormat:@"%d",QuestionBaseId];
        }
        NSInteger n = [num integerValue];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",(n-1)] forKey:QuestionID];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[DataManage instance] submitQuestionToSV:@[_q]];
   
    [self.delegate questionEditDone];
    [self hideLoadingView];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAddOptionAlert {
    AlertVC * vc = [[AlertVC alloc] initWithNibName:@"AlertVC" bundle:nil];
    [vc setTitle:AddQuestionAnserChooseTitle];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.navigationBarHidden = YES;
    
    //This will be the size you want
    
    [self.splitViewController presentViewController:nav animated:YES completion:nil];
}

-(void)back{
    
    if (_questionType == 0) {
        [[CoredataManager manager] deleteObject:self.q];
    }
    [[self.q.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Answer * answer = (Answer *)obj;
        answer.toDele = @"0";
    }];
    [[CoredataManager manager] saveContext];
    
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)questionTypeChoose:(id)sender {
    [self dismissKeyboard];
    
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    float h =  [_popContentVC tbType:TBtypeQuestionAnswerType];
    
    UIButton * bt = (UIButton *)sender;
    _popCtr.popoverContentSize = CGSizeMake(bt.frame.size.width, h);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    _popCtr.backgroundColor = [UIColor whiteColor];
}


-(void)keyboardWillHide:(NSNotification *)notify{

    contentSV.frame = contentOriginFram;
}

-(void)keyboardWillShow:(NSNotification *)notify{

    CGRect fram = contentSV.frame;
    fram.size.height = contentOriginFram.size.height -250;
    contentSV.frame = fram;
}

#pragma mark --  UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return answerItems.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentify = @"cellIdentify";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
#pragma mark -- UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark --  UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.cur_tf = textField;
}
#pragma mark -- PoperContentViewControllerDelegate
-(void)tbItemChoosed:(NSString *)text{
    
    [_popCtr dismissPopoverAnimated:YES];
    NSString * str = [typeBt titleForState:0];
    if (![str isEqualToString:text]) {
        NSDictionary * dic = @{@"Select":@"1",@"Buttons":@"4",@"Text":@"20"};
        _q.answerType_temp = dic[text];
        [typeBt setTitle:text forState:0];
        NSMutableArray * selectSource = [self.answerView returnSelectSource];
        if (self.answerView) {
            [self.answerView removeFromSuperview];
            self.answerView = nil;
        }
        self.answerView = [[AnswerView alloc] initWithFrame:answerContentV.frame];
        _answerView.questionType = _questionType;
        [_answerView setSelectSourceTemp: selectSource];
        _answerView.delegate = self;
        [_answerView initSubView:self.q];
        [contentSV addSubview:_answerView];
    }
}
#pragma mark -- AnswerViewDelegate
-(void)addOrEditItem:(MAnswer *)item{
    [questionTf resignFirstResponder];
    EditAccountVC * vc = [[EditAccountVC alloc] initWithNibName:@"EditAccountVC" bundle:nil];
    vc.dgType = 1;
    vc.delegate = self;
    vc.answerTitle = item.answer_option;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.navigationBarHidden = YES;
    
    //This will be the size you want
    
    [self.splitViewController presentViewController:nav animated:YES completion:nil];
    
//    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:nil delegate:self cancelButtonTitle:ButtonTitleCancel otherButtonTitles:ButtonTitleDone, nil];
//    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
//    alert.tag = AnswerItemEditAlertTag;
//    UITextField * tf = [alert textFieldAtIndex:0];
//    tf.placeholder = @"Option";
//    tf.text = item.answer_option;
//    [alert show];
}

- (void)cancelButtonClicked {
    [_answerView refreshTb];
}

- (void)continueButtonClicked:(NSString *)text {
    [_answerView refreshTbWithNew:text];
}

#pragma mark -- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == AnswerItemEditAlertTag) {
        if (buttonIndex == 1) {
            UITextField * tf = [alertView textFieldAtIndex:0];
            [_answerView refreshTbWithNew:tf.text];
        }else{
            [_answerView giveUpRefrsh];
        }
    }
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}
@end
