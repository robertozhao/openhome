//
//  SourceHomeViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailBaseViewController.h"


@interface SourceHomeViewController : DetailBaseViewController

@property int soureCate;
@property (nonatomic , strong) NSString * specilListShow;
@end
