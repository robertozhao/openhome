//
//  MasterViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class DetailViewController;

@interface MasterViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{

    IBOutlet UIButton *notifyBt;
    
    IBOutlet UIButton *editBt;
    IBOutlet UIButton *setBt;
    IBOutlet UITableView *myTableView;
    IBOutlet UITableView *subTableView;
}

@property (strong, nonatomic) DetailViewController *detailViewController;

-(void)updataTbListItem;
@end

