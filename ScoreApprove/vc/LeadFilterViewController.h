//
//  LeadFilterViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol LeadFilterViewControllerDelegate <NSObject>

-(void)listChoosed:(List *)list;    

@end

@interface LeadFilterViewController : BaseViewController

@property (nonatomic , weak) id<LeadFilterViewControllerDelegate>delegate;

@property (nonatomic , strong) List * menuList;
@property (nonatomic , strong) List * curList;

@end
