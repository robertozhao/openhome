//
//  LeadHomeViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LeadHomeViewController.h"
#import "Lead.h"
#import "LeadDetailViewController.h"
#import "LeadFilterViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface LeadHomeViewController ()<UITableViewDataSource,UITableViewDelegate,LeadFilterViewControllerDelegate,MFMailComposeViewControllerDelegate,UISearchBarDelegate, DataManageDelegate>{

        NSDateFormatter * dateFormat;
    
    IBOutlet UIButton *btnFilter;
    
    IBOutlet UILabel *leadsLabel;
    IBOutlet UITableView *myTableView;
    IBOutlet UIView *tbHeaderView;
    IBOutlet UIView *tbHeaderTitleView;
    
    IBOutletCollection(UIButton) NSArray *headTitleBtArray;

    IBOutlet UISearchBar *searchBar;

    IBOutlet UIButton *firNameBt;
    IBOutlet UIButton *lastNameBt;
    IBOutlet UIButton *createdBt;
    IBOutlet UIButton *listAddBt;
    
    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    
}
@property (nonatomic , strong) NSString * searchText;
@property (nonatomic , strong) NSString * orderPara;
@property (nonatomic , strong) NSString * orderState;
@property (nonatomic , strong) UIButton * cur_headBt;

@property (nonatomic , strong)    NSArray * sourceList;

@property (nonatomic, strong) UIPopoverController * popCtr;
@property (nonatomic, strong)  LeadFilterViewController * popContentVC;

@end

@implementation LeadHomeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [headTitleBtArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton * bt = (UIButton *)obj;
//        bt.layer.borderColor = RGBCOLOR(210, 210, 210).CGColor;
//        bt.layer.borderWidth = 0.5;
//        bt.backgroundColor = RGBCOLOR(229, 229, 229);
    }];
    self.cur_headBt = createdBt;
    self.orderState = @"descend";
    self.orderPara = @"created";
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeStyle:NSDateFormatterLongStyle];
    [dateFormat setDateFormat:@"EEE MMM dd YYYY   hh:mm aaa"];
    NSLocale * twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormat.locale = twelveHourLocale;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leadItemChoosedNotification:) name:@"LeadItemSelected" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([defaults boolForKey:@"OK"] == true) {
//        [defaults setObject:_curFilterList.address forKey:@"Founded"];
//        [defaults synchronize];
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"setRow1" object:nil];
//        [defaults setBool:false forKey:@"OK"];
//        [defaults synchronize];
//    }
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableSource) name:@"reloadLeadTb" object:nil];

    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    self.navigationItem.leftBarButtonItem = item;
    item.tintColor = [UIColor clearColor];
    
    [self getLeadSource];
}


-(void)reloadTableSource{
    [self getLeadSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)filterAction:(id)sender {
    self.popContentVC = [[LeadFilterViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popContentVC.curList = self.curFilterList;
    self.popContentVC.menuList = self.menuFilterList;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:self.popContentVC];
    nav.navigationBar.hidden = YES;
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    self.popCtr.backgroundColor = RGBCOLOR(240, 240, 240);
    
    _popCtr.popoverContentSize = CGSizeMake(230, 148);
    
    [_popCtr presentPopoverFromRect:btnFilter.bounds inView:btnFilter permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    return;
}

- (IBAction)tbHeaderAction:(id)sender {
    
    UIButton * bt = (UIButton *)sender;
    
    if (_cur_headBt == bt) {
        if ([_orderState isEqualToString:@"ascend"]) {
            self.orderState = @"descend";
            [bt setImage:[UIImage imageNamed:@"arrow_up"] forState:0];
        }else{
            self.orderState = @"ascend";
            [bt setImage:[UIImage imageNamed:@"arrow_down"] forState:0];
        }
    }else{
        [self.cur_headBt setImage:[UIImage imageNamed:@"arrows"] forState:0];
        self.cur_headBt = bt;
        self.orderState = @"descend";
        [bt setImage:[UIImage imageNamed:@"arrow_up"] forState:0];
        
        if (bt == firNameBt) {
            self.orderPara = @"first_name";
            
        }else if (bt == lastNameBt){
            self.orderPara = @"last_name";
            
        }else if (bt == createdBt){
            self.orderPara = @"created";
        }
    }
    [self getLeadSource];
}

- (void)filter{
    
    self.popContentVC = [[LeadFilterViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popContentVC.curList = self.curFilterList;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:self.popContentVC];
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    
    _popCtr.popoverContentSize = CGSizeMake(260, 500);
    
    [_popCtr presentPopoverFromBarButtonItem:self.navigationItem.leftBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

//- (void)export {
//    // new csv file
//    
//    NSString * path = [[DataManage instance] pathForLeadCSVFile:_curFilterList];
//    // pop email
//    
//    [_popCtr dismissPopoverAnimated:NO];
//    
//    if (![MFMailComposeViewController canSendMail]) {
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:EmailCanNotUse message:EmailCanNotUseMsg delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
//        [alert show];
//        return;
//    }
//    if (path) {
//        [self displayMailPicker:path];
//    }else{
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:CSVFileCreatFial delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
//        [alert show];
//    }
//}

//-(void)displayMailPicker:(NSString *)path{
//    
//    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
//    mailPicker.mailComposeDelegate = self;
//    [mailPicker setSubject: LeadExportSubject];
//    
//    NSArray *toRecipients = [NSArray arrayWithObject:[GlobalInstance instance].username];
//    [mailPicker setToRecipients: toRecipients];
//    NSData * data = [NSData dataWithContentsOfFile:path];
//    [mailPicker addAttachmentData:data mimeType:@"csv" fileName:LeadExportCSVFileName];
//    [self presentViewController:mailPicker animated:YES completion:^{
//        
//    }];
//}


-(NSArray *)sortArray:(NSArray *)source{

    source =  [source sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Lead * lead = (Lead *)obj1;
        Lead * lead_ = (Lead *)obj2;
        NSString * value1 = [lead performSelector:NSSelectorFromString(_orderPara)];
        NSString * value2 = [lead_ performSelector:NSSelectorFromString(_orderPara)];
        
        NSComparisonResult result =  [value1 compare:value2];
        
        if ([_orderState isEqualToString:@"ascend"]) {
            if (result == NSOrderedAscending) {
                return NSOrderedAscending;
            }else{
                return NSOrderedDescending;
            }
        }else{
            if (result == NSOrderedAscending) {
                return NSOrderedDescending;
            }else{
                return NSOrderedAscending;
            }
        }
    }];
    return source;
}

-(void)getLeadSource{
    [self setFilterLabel];
       dispatch_async([DataManage instance].dataQueue, ^{        
        
        NSArray * array = [[CoredataManager manager] nodesFromEntity:@"Lead" orderBy:_orderPara orderState:_orderState withPredicate:[NSString stringWithFormat:@"agent_id='%@'",[GlobalInstance instance].user_id]];
        self.title = LeadListTitleAll;
        self.sourceList = array;

        if (_sourceList.count == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self performSelector:@selector(hideViews) withObject:nil afterDelay:1];
            });
        }else{
            if (_curFilterList) {
                if ([[_curFilterList.lead allObjects] count] == 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self performSelector:@selector(hideViews) withObject:nil afterDelay:1];
                    });
                    return;
                }
                self.sourceList = [_curFilterList.lead allObjects];
                self.sourceList =  [self sortArray:_sourceList];
            }
            
            //do a delay
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [myTableView reloadData];
            });
        }
    });
}

- (void)hideViews {
    myTableView.hidden = YES;
    tbHeaderView.hidden = YES;
}

-(void)getLeadSourceFroSearch:(NSString *)_keyword{
    
    dispatch_async([DataManage instance].dataQueue, ^{
        NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Lead"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(first_name contains[c] %@) OR(last_name contains[c] %@) OR (list.address contains[c] %@)", _keyword, _keyword, _keyword];
        NSArray * curArray = [array filteredArrayUsingPredicate: predicate];
        
        self.title = LeadListTitleAll;
        self.sourceList = curArray;
        if (_curFilterList) {
            self.sourceList = [[_curFilterList.lead allObjects] filteredArrayUsingPredicate:predicate];
            self.sourceList =  [self sortArray:_sourceList];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [myTableView reloadData];
        });
    });
}


-(UILabel *)lb:(CGRect)fram tag:(int)tag backColor:(UIColor *)bkColor{
    UILabel * lb = [[UILabel alloc] initWithFrame:fram];
    lb.tag = tag;
    lb.font = [UIFont systemFontOfSize:17];
    lb.textAlignment = 1;
    if (bkColor) {
        lb.backgroundColor = bkColor;
    }
    
    return lb;
}

#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sourceList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentify = @"cellIdentify";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setBackgroundColor:RGBCOLOR(239, 239, 244)];
        
        [cell addSubview:[self lb:CGRectMake(0, 35, tableView.frame.size.width, 1) tag:0 backColor:[UIColor whiteColor]]];
        [cell addSubview:[self lb:CGRectMake(0, 0, 140, 36) tag:1000 backColor:nil]];
//        [cell addSubview:[self lb:CGRectMake(132, 0, 0.5, 44) tag:0 backColor:RGBCOLOR(240, 240, 240)]];
        
        [cell addSubview:[self lb:CGRectMake(140, 0, 140, 36) tag:1001 backColor:nil]];
//        [cell addSubview:[self lb:CGRectMake(315, 0, 0.5, 44) tag:0 backColor:RGBCOLOR(240, 240, 240)]];
        
//        [cell addSubview:[self lb:CGRectMake(240, 0, 200, 36) tag:1002 backColor:nil]];
//        [cell addSubview:[self lb:CGRectMake(567, 0, 0.5, 44) tag:0 backColor:RGBCOLOR(240, 240, 240)]];
        
        [cell addSubview:[self lb:CGRectMake(280, 0, 348, 36) tag:1003 backColor:nil]];
    }
    Lead * lead = self.sourceList[indexPath.row];

    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    
    UILabel * lb_firName = (UILabel *)[cell viewWithTag:1000];
    lb_firName.text = lead.first_name;
    lb_firName.font = font;
    lb_firName.textColor = RGBCOLOR(102, 102, 102);
    
    
    UILabel * lb_lastName = (UILabel *)[cell viewWithTag:1001];
    lb_lastName.text = lead.last_name;
    lb_lastName.font = font;
    lb_lastName.textColor = RGBCOLOR(102, 102, 102);
    
    UILabel * lb_listAddress = (UILabel *)[cell viewWithTag:1002];

    if (lead.list) {
        lb_listAddress.text = lead.list.address;
    }else{
        lb_listAddress.text = @"";
    }
    lb_listAddress.font = font;
    lb_listAddress.textColor = RGBCOLOR(102, 102, 102);
    
    UILabel * lb_creat = (UILabel *)[cell viewWithTag:1003];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: [lead.created doubleValue]];
    NSString *timeStr = [dateFormat stringFromDate: date];

    lb_creat.text = timeStr;
    lb_creat.font = font;
    lb_creat.textColor = RGBCOLOR(102, 102, 102);

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36;
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//
//    if (_curFilterList) {
//        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
//        lb.text =  [NSString stringWithFormat:@"   Leads for \"%@\"",_curFilterList.address];
//        lb.backgroundColor = RGBCOLOR(28, 28, 28);
//        lb.font = [UIFont systemFontOfSize:18];
//        lb.textColor = [UIColor whiteColor];
//        return lb;
//    }else{
//        return  nil;
//    }
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//
//    if (_curFilterList) {
//        return 40;
//    }else{
//        return 0;
//    }
//}

#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Lead * lead = self.sourceList[indexPath.row];
    LeadDetailViewController * vc = [[LeadDetailViewController alloc] initWithNibName:@"LeadDetailViewController" bundle:nil];
    vc.lead = lead;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:true forKey:@"OK"];
    [defaults synchronize];

    [defaults setObject:lead.list.address forKey:@"tempoSave1"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"setRow1" object:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- LeadFilterViewControllerDelegate

-(void)listChoosed:(List *)list{
    myTableView.hidden = NO;
    tbHeaderView.hidden = NO;
    
    [self.popCtr dismissPopoverAnimated:YES];
    self.curFilterList = list;
    [self setFilterLabel];
    
    if (self.searchText && ![self.searchText isEqualToString:@""]) {
        [self getLeadSourceFroSearch:_searchText];
    }else{
        [self getLeadSource];
    }
}

- (void)leadItemChoosedNotification:(NSNotification *)notification {
    [self listChoosed:(List *)notification.object];
}

- (void)setFilterLabel {
    [leadsLabel setText:_curFilterList.address];
}

#pragma mark -- MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -- UISearchBarDelegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar_{
    searchBar.showsCancelButton = YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    self.searchText = searchText;
    if ([searchText isEqualToString:@""]) {
        [self getLeadSource];
    }else{
        [self getLeadSourceFroSearch:searchText];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar_{

    searchBar.text = @"";
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
    [self getLeadSource];
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}

@end
