//
//  LoginViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginManage.h"
#import "GlobalInstance.h"

@interface LoginViewController ()<UITextFieldDelegate,LoginManageDelegate,UIAlertViewDelegate>{

    IBOutlet UIScrollView *registeContentSV;
    CGRect contentSVOrigin;
    
    IBOutlet UIButton *autoLoginBt;
    
    LoginManage * manage;
    
    UIView * bkView;
    
    IBOutlet UIView *bigV;
    IBOutlet UIButton *loginBt;
    IBOutlet UIView *loginV;
    IBOutlet UIButton *creatBt;
    
    IBOutlet UUITextFile *emailTf;
    IBOutlet UUITextFile *passwordTf;
    IBOutlet UIButton *signInBt;
    IBOutlet UIButton *closeBt;
    IBOutlet UIView *signUpView;
    
    
    IBOutletCollection(UUITextFile) NSArray *tfArray;

    IBOutlet UUITextFile *zuojiTf;
    IBOutlet UUITextFile *retypePwd;
    IBOutlet UUITextFile *userNameTf;
    IBOutlet UUITextFile *companyTf;
    IBOutlet UUITextFile *phoneNumTf;
    IBOutlet UUITextFile *lastNameTf;
    IBOutlet UUITextFile *firstNameTf;

    IBOutlet UUITextFile *emailTf_;
    IBOutlet UUITextFile *pwdTf_;
    IBOutlet UIButton *closeBt_;
    
    IBOutlet UIButton *signUpBt_;
    
    
    BOOL isAutoLogin;
}
@property (nonatomic , strong) UITextField * curTf;
@property (nonatomic , strong) UIView * curPopView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    manage = [[LoginManage alloc] init];
    manage.delegate = self;
    
    loginV.layer.cornerRadius = 5;
    signInBt.layer.cornerRadius = 3;
    closeBt.layer.cornerRadius = 3;
    
    emailTf.layer.cornerRadius = 3;
    emailTf.layer.borderColor = [UIColor lightGrayColor].CGColor;
    emailTf.layer.borderWidth = 1;
    
    passwordTf.layer.cornerRadius = 3;
    passwordTf.layer.borderColor = [UIColor lightGrayColor].CGColor;
    passwordTf.layer.borderWidth = 1;
    
    signUpView.layer.cornerRadius = 5;
    
    [tfArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView * view = (UIView *)obj;
        view.layer.cornerRadius = 3;
        view.layer.borderColor = [UIColor lightGrayColor].CGColor;
        view.layer.borderWidth = 1;
        
    }];

    closeBt_.layer.cornerRadius = 3;
    
    loginBt.layer.cornerRadius = 3;
//    loginBt.layer.borderColor = RGBCOLOR(60, 60, 60).CGColor;
//    loginBt.layer.borderWidth = 1;
    
    creatBt.layer.cornerRadius = 3;
    
    
    if([GlobalInstance instance].isLoginAuto){
        autoLoginBt.selected = YES;
    }else{
        autoLoginBt.selected = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    registeContentSV.contentSize = CGSizeMake(registeContentSV.frame.size.width, registeContentSV.frame.size.height);
    
    contentSVOrigin = registeContentSV.frame;
    if (!bkView) {
        bkView = [[UIView alloc] initWithFrame:self.view.bounds];
        bkView.backgroundColor = [UIColor blackColor];
        bkView.alpha = 0.8;
    }
    if ([GlobalInstance instance].isLoginAuto) {
        autoLoginBt.selected = YES;
    }
}


-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];

    if (_fromLogout) {
        return;
    }
    
    if ([GlobalInstance instance].isLoginAuto) {
        NSDictionary * info = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLastLoginInfo];
        if(info){
            if ([DataManage instance].netWorkIsOK) {
                isAutoLogin = YES;
                NSDictionary * dic = @{@"username":info[@"userName"],@"password":info[@"password"]};
//                [self showLoadingViewWithText:PopUpLoadingViewText];
                [manage login:dic];
            }else{
                isAutoLogin = YES;
                [GlobalInstance instance].username = info[@"userName"];
                [GlobalInstance instance].password = info[@"password"];
                [GlobalInstance instance].user_id = info[@"user_id"];
                [GlobalInstance instance].loginType = LoginTypeLocally;
                [DataManage instance].islogined = YES;
                
                [self hideSelf];
            }
        }else{
            isAutoLogin = NO;
        }
    }else{
        isAutoLogin = NO;
    }
}

-(void)hideSelf{
    
    [DataManage instance].islogined = YES;
    [self.delegate loginFinish];
}

-(void)keyboardWillShow:(NSNotification *)notify{

    CGRect fram = registeContentSV.frame;
    fram.size.height = contentSVOrigin.size.height - 320;
    registeContentSV.frame = fram;
}

-(void)keyboardWillHide:(NSNotification *)notify{

    registeContentSV.frame = contentSVOrigin;
}

- (IBAction)loginAutoShift:(id)sender {
    UIButton * bt = (UIButton *)sender;
    bt.selected = !bt.selected;
    
    [GlobalInstance instance].isLoginAuto = bt.selected;
    if (bt.selected) {
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:KeyForLoginAuto];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KeyForLoginAuto];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// login
- (IBAction)forgetPwd:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:AlertTitleNotice message:AlertMsgForLeavingApp delegate:self cancelButtonTitle:ButtonTitleCancel otherButtonTitles:ButtonTitleContinue, nil];
    [alert show];
}

- (IBAction)close:(id)sender {

    [self.curTf resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        loginV.alpha = 0;
        bkView.alpha = 0;
    } completion:^(BOOL finished) {
        [bkView removeFromSuperview];
        bkView.alpha = 0.8;
        [self.view bringSubviewToFront:bigV];
    }];
}

- (IBAction)signIn:(id)sender {
    
    [self.curTf resignFirstResponder];
    

    if ([emailTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputEmail];
        return;
    }
    if ([passwordTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputPwd];
        return;
    }
    if (passwordTf.text.length <6 ) {
        [self alertShow:AlertMsgForPwdDigits];
        return;
    }
//    [self showLoadingViewWithText:PopUpLoadingViewText];
    
    if ([GlobalInstance instance].netWorkStatue) {
        
        NSDictionary * dic = @{@"username":emailTf.text,@"password":passwordTf.text};
        [manage login:dic];
        
    }else{
        
        NSArray * array = [[NSUserDefaults standardUserDefaults] objectForKey:UserInLocalKey];
        if (!array || array.count == 0) {
//            [self hideLoadingView];
            [self simplePopUpWithTitle:@"No Network" Msg:nil];
            return;
        }
        
        NSDictionary * dic = @{@"user":emailTf.text,@"pwd":passwordTf.text};
        
        if ([[DataManage instance] localLogin:dic]) {
            [GlobalInstance instance].username = emailTf.text;
            [GlobalInstance instance].password = passwordTf.text;
            NSDictionary * loginInfo = @{@"userName":emailTf.text,@"password":passwordTf.text};
            [[NSUserDefaults standardUserDefaults] setObject:loginInfo forKey:KeyForLastLoginInfo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self hideSelf];
        }else{
            [self simplePopUpWithTitle:OffLineLoginInFail Msg:nil];
        }
//        [self hideLoadingView];
    }
}

// sign up

- (IBAction)ternsofservice:(id)sender {

}
- (IBAction)privacyPolicy:(id)sender {

}

- (IBAction)closeForSignUp:(id)sender {
    
    [self.curTf resignFirstResponder];
    [UIView animateWithDuration:0.2 animations:^{
        signUpView.alpha = 0;
        bkView.alpha = 0;
    } completion:^(BOOL finished) {
            [bkView removeFromSuperview];
        bkView.alpha=0.8;
        [self.view bringSubviewToFront:bigV];
    }];
}


- (IBAction)signUp:(id)sender {
    
    [self.curTf resignFirstResponder];
    if ([firstNameTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputFirstName];
        return;
    }
    
    
    if ([lastNameTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputLastName];
        return;
    }
    
    if ([phoneNumTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputPhone];
        return;
    }

    if (phoneNumTf.text.length < 13) {
        [self alertShow:AlertMsgForPhoneDigit];
        return;
    }
    
    
    if ([emailTf_.text isEqualToString:@""]) {
        [self alertShow:AlertForInputEmail];
        return;
    }
    
    if (![Utility isLegalEmailAddress:emailTf_.text]) {
        [self alertShow:EmailFormatWrong];
        return;
    }
    
    if ([userNameTf.text isEqualToString:@""]) {
        [self alertShow:AlertForInputUserName];
        return;
    }
    
    if ([pwdTf_.text isEqualToString:@""]) {
        [self alertShow:AlertForInputPwd];
        return;
    }
    
    if (pwdTf_.text.length <6 ) {
        [self alertShow:AlertMsgForPwdDigits];
        return;
    }
    
    if ([retypePwd.text isEqualToString:@""]) {
        [self alertShow:AlertForInputRePwd];
        return;
    }
    if (![pwdTf_.text isEqualToString:retypePwd.text]) {
        [self alertShow:AlertForInputTwoPwdWrong];
        return;
    }
    if ([GlobalInstance instance].netWorkStatue) {
//        [self showLoadingViewWithText:PopUpLoadingViewText];
        
        NSString * phone = [[GlobalInstance instance] phoneNumWithOutFormat:phoneNumTf.text];
     
        NSDictionary * dic = @{@"first_name":firstNameTf.text,@"last_name":lastNameTf.text,@"company":companyTf.text,@"phone_ext":zuojiTf.text,@"phonenumber":phone,@"email":emailTf_.text,@"username":userNameTf.text,@"password":pwdTf_.text};
        
//        NSDictionary * dic = @{@"user_info":info};
        [manage toRegister:dic];
    }else{
        [self simplePopUpWithTitle:AlertForNetWorkOff Msg:nil];
    }
}

//  big view

- (IBAction)toLogin:(id)sender {
    //login email auto input
    
     NSDictionary * loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLastLoginInfo];
    if (loginInfo) {
        emailTf.text = loginInfo[@"userName"];
    }else{
        emailTf.text = @"";
    }
    passwordTf.text = @"";
    [self.view addSubview:bkView];
    CGRect fram = loginV.frame;
    fram.origin.x = (self.view.frame.size.width-loginV.frame.size.width)/2.0;
    fram.origin.y = 105;
    loginV.frame = fram;
    
    loginV.alpha =0;
    [self.view bringSubviewToFront:loginV];
    [UIView animateWithDuration:0.2 animations:^{
        loginV.alpha = 1;
    } completion:^(BOOL finished) {
    }];
}

- (IBAction)toCreatCccount:(id)sender {
    emailTf_.text = @"";

    passwordTf.text = @"";
    [self.view addSubview:bkView];
    CGRect fram = signUpView.frame;
    fram.origin.x = (self.view.frame.size.width-signUpView.frame.size.width)/2.0;
    fram.origin.y = 105;
    signUpView.frame = fram;
    
    signUpView.alpha =0;
    [self.view bringSubviewToFront:signUpView];
    [UIView animateWithDuration:0.2 animations:^{
        signUpView.alpha = 1;
    } completion:^(BOOL finished) {
    }];
}


#pragma mark -- UITextFieldDelegate


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if (textField == phoneNumTf) {
        NSString * str = @"0123456789";
        if ([str rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }else{
            
            if (phoneNumTf.text.length == 13 && ![string isEqualToString:@""]) {
                return NO;
            }
        
            if (phoneNumTf.text.length == 3 && ![string isEqualToString:@""]) {
                NSString * str = phoneNumTf.text;
                phoneNumTf.text = [NSString stringWithFormat:@"(%@)",str];
            }
            
            if (phoneNumTf.text.length == 8 && ![string isEqualToString:@""]) {
                NSString * str = phoneNumTf.text;
                phoneNumTf.text = [NSString stringWithFormat:@"%@-",str];
            }
            if (phoneNumTf.text.length == 5 && [string isEqualToString:@""]) {
                NSString * str = phoneNumTf.text;
                phoneNumTf.text = [str substringFromIndex:1];
            }
        }
    }
    if (textField == zuojiTf) {
        NSString * str = @"0123456789";
        if ([str rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.curTf = textField;
    textField.layer.borderColor=[[UIColor colorWithRed:0.235 green:0.553 blue:0.996 alpha:1] CGColor];
    textField.textColor = [UIColor colorWithRed:0.235 green:0.553 blue:0.996 alpha:1];
    textField.layer.borderWidth = 2;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    textField.layer.borderColor=[[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] CGColor];
    textField.textColor = [UIColor blackColor];
    textField.layer.borderWidth = 2;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if (textField.tag > 899 && textField.tag < 908) {
        UITextField * tf = (UITextField *)[registeContentSV viewWithTag:(textField.tag+1)];
        [tf becomeFirstResponder];
    }else if (textField.tag == 908){
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark -- LoginManageDelegate

-(void)loginFinish:(NSDictionary *)info errorMsg:(NSString *)str{
    
//    [self hideLoadingView];
    
    NSDictionary * userInfo = info[@"userInfo"];
    
    if (userInfo) {

        [DataManage instance].islogined = YES;
        if (!isAutoLogin) {
            
            NSDictionary *dic = @{@"user":emailTf.text,@"pwd":passwordTf.text,@"user_id":userInfo[@"user_id"]};
            [[DataManage instance] addNewUserToLocal:dic];
            [GlobalInstance instance].loginType = LoginTypeServer;
            [GlobalInstance instance].username = emailTf.text;
            [GlobalInstance instance].password = passwordTf.text;
            [GlobalInstance instance].user_id = userInfo[@"user_id"];
            
            NSDictionary * loginInfo = @{@"userName":emailTf.text,@"password":passwordTf.text,@"user_id":userInfo[@"user_id"]};
            [[NSUserDefaults standardUserDefaults] setObject:loginInfo forKey:KeyForLastLoginInfo];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else{
            NSDictionary * info_ = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLastLoginInfo];
            [GlobalInstance instance].loginType = LoginTypeServer;
            [GlobalInstance instance].username = info_[@"userName"];
            [GlobalInstance instance].password = info_[@"password"];
            [GlobalInstance instance].user_id = info_[@"user_id"];
        }
        
        NSDictionary * reference = info[@"preference"];
        if (reference) {
            NSMutableDictionary * temp = [NSMutableDictionary dictionary];
            if (reference[@"signature"]) {
                [temp setObject:reference[@"signature"] forKey:@"signature"];
            }
            if (reference[@"thankyou_email"]) {
                [temp setObject:reference[@"thankyou_email"] forKey:@"isAutoSend"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:temp forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        //  sync lead for login
        
        [self hideSelf];
        [[DataManage instance] dataSync];
        
    }else{
        [self alertShow:str];
    }
}

-(void)registerFinish:(NSString *)user_id errorMsg:(NSString *)str{

//    [self hideLoadingView];
    
    if (user_id) {
        [DataManage instance].islogined = YES;
        NSDictionary *dic = @{@"user":emailTf.text,@"pwd":passwordTf.text,@"user_id":user_id};
        [[DataManage instance] addNewUserToLocal:dic];
        [GlobalInstance instance].loginType = LoginTypeServer;
        [GlobalInstance instance].username = emailTf_.text;
        [GlobalInstance instance].password = pwdTf_.text;
        [GlobalInstance instance].user_id = user_id;
        
        NSDictionary * loginInfo = @{@"userName":emailTf_.text,@"password":pwdTf_.text,@"user_id":user_id};
        [[NSUserDefaults standardUserDefaults] setObject:loginInfo forKey:KeyForLastLoginInfo];
        [[NSUserDefaults standardUserDefaults] synchronize];
                [self hideSelf];
        //  sync lead for login
        

        
        NSDictionary * emailSignatureInfo = @{@"isHadNew":@"1",@"isAutoSend":@"1"};
        [[NSUserDefaults standardUserDefaults] setObject:emailSignatureInfo forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[DataManage instance] dataSync];
        
    }else{
        
        [self alertShow:str];
    }

}

#pragma mark -- UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://agents.scoreapprove.com/auth/forgot_password"]];
    }
}

@end
