//
//  QuestionToolTipViewController.h
//  ScoreApprove
//
//  Created by Yang on 15/5/1.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface QuestionToolTipViewController : BaseViewController

@property (nonatomic , strong) NSString * toolTip;

-(void)loadHtml:(NSString *)string;

@end
