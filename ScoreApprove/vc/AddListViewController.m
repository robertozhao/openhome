//
//  AddListViewController.m
//  ScoreApprove
//
//  Created by Yang on 15/5/6.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "AddListViewController.h"
#import "UUITextFile.h"
#import "DataManage.h"

@interface AddListViewController ()<UITextFieldDelegate>{
    UUITextFile * addressTf;
    UUITextFile * zipCodeTf;
}

@end

@implementation AddListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NewList;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = item;
    [self initSubView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [addressTf becomeFirstResponder];
}

-(void)cancel{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)initSubView{
    
    float padding = 30;
    addressTf = [[UUITextFile alloc] initWithFrame:CGRectMake(20, 70, self.view.frame.size.width-40, 55)];
    addressTf.placeholder =AddListTfPlaceHold ;
    addressTf.borderStyle = UITextBorderStyleNone;
    addressTf.layer.cornerRadius = 4;
    addressTf.layer.borderWidth = 1;
    addressTf.font = [UIFont systemFontOfSize:20];
    addressTf.layer.borderColor = PopViewNavColor.CGColor;
    [self.view addSubview:addressTf];
    
    zipCodeTf = [[UUITextFile alloc] initWithFrame:CGRectMake(20, padding + addressTf.frame.size.height+ addressTf.frame.origin.y, self.view.frame.size.width-40, 55)];
    
    zipCodeTf.placeholder = AddListCodePlaceHold;
    zipCodeTf.borderStyle = UITextBorderStyleNone;
    zipCodeTf.layer.cornerRadius = 4;
    zipCodeTf.layer.borderWidth = 1;
    zipCodeTf.delegate = self;
    zipCodeTf.font = [UIFont systemFontOfSize:20];
    zipCodeTf.keyboardType = UIKeyboardTypeNumberPad;
    zipCodeTf.layer.borderColor = PopViewNavColor.CGColor;
    zipCodeTf.returnKeyType = UIReturnKeyDone;
    [self.view addSubview:zipCodeTf];
    
    UIButton * sure_bt = [UIButton buttonWithType:0];
    [sure_bt setTitleColor:[UIColor whiteColor] forState:0];
    [sure_bt addTarget:self action:@selector(creat) forControlEvents:UIControlEventTouchUpInside];
    [sure_bt setBackgroundColor:OriginColor];
    [sure_bt setBackgroundImage:[UIImage imageNamed:@"info-bar.png"] forState:UIControlStateHighlighted];
    sure_bt.layer.cornerRadius = 3;
    [sure_bt setTitle:ButtonTitleAddList forState:0];
    sure_bt.frame = CGRectMake(20, padding+zipCodeTf.frame.size.height+zipCodeTf.frame.origin.y, self.view.frame.size.width-40, 50);
    [self.view addSubview:sure_bt];
}


-(void)creat{
    if ([addressTf.text isEqualToString:@""] || [zipCodeTf.text isEqualToString:@""]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:AlertMsgForNotCodeForAddlist delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    NSDictionary * dic = @{@"address":addressTf.text,@"zipcode":zipCodeTf.text};
//    [self.delegate creatNewListing:dic];
     [[DataManage instance] addNewList:dic];
    [self cancel];
}

#pragma mark -- UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == zipCodeTf) {
        
        NSString * str_ = @"0123456789";
        if ([str_ rangeOfString:string].location == NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }else if(textField.text.length == 5 && ! [string isEqualToString:@""]){
            return NO;
        }else{
        
            return YES;
        }
    }
    return YES;
}



@end
