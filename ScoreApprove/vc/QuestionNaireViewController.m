//
//  QuestionNaireViewController.m
//  ScoreApprove
//
//  Created by Yang on 15/5/9.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "QuestionNaireViewController.h"
#import "QuestionNaireView.h"
#import "Question.h"
#import "AFAppDotNetAPIClient.h"
#import "SyncManage.h"
#import "Lead.h"

@interface QuestionNaireViewController ()<QuestionNaireViewDelegate,SyncManageDelegate>{
    QuestionNaireView * naireView;
    UIButton * toolTipBt;
    SyncManage * manage;
}

@property (nonatomic, strong) UIPopoverController * popCtr;

@end

@implementation QuestionNaireViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    toolTipBt = [UIButton buttonWithType:0];
    [toolTipBt setImage:[UIImage imageNamed:@"toolTip.png"] forState:0];
    toolTipBt.frame = CGRectMake(self.navigationController.navigationBar.frame.size.width -60,0, 60, self.navigationController.navigationBar.frame.size.height);
    [toolTipBt addTarget:self action:@selector(showToolTip) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addSubview:toolTipBt];
    toolTipBt.hidden = NO;
    
    naireView = [[QuestionNaireView alloc] initWithFrame:self.view.bounds];
    naireView.backgroundColor = [UIColor clearColor];
    naireView.delegate = self;
    naireView.list = _list;
    [self.view addSubview:naireView];
 
//    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Exit" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//    self.navigationItem.leftBarButtonItem = backItem;
    [self navigationBarNextIsAppear:YES];
    [self navigationBarPreviousIsAppear:NO];
}

-(void)showToolTip{

    Question * q = [naireView curQuestion];
    
    QuestionToolTipViewController * vc = [[QuestionToolTipViewController alloc] init];
    [vc loadHtml:q.tool_tip];
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:vc];
    _popCtr.popoverContentSize = CGSizeMake(400, 500);
    [_popCtr presentPopoverFromRect:toolTipBt.frame inView:toolTipBt.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)back{
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"Cancel registration?" delegate:self cancelButtonTitle:ButtonTitleNo otherButtonTitles:ButtonTitleYes, nil];
    [alert show];
}

- (void)previousQuestion {
    [naireView previousQuestion];
}

-(void)nextQuestion{
    [naireView nextQuestion];
}

#pragma mark -- QuestionNaireViewDelegate

-(void)exitQuestionNaire:(Lead *)lead{

    [self.delegate questionNaireFinish:lead];
}

-(void)pageNum:(NSString *)str{
    self.title = str;
}

-(void)emailVerify:(NSString *)email{
    if (!manage) {
        manage = [[SyncManage alloc] init];
        manage.delegate = self;
    }
    [self showLoadingViewWithText:VerfiyEmail];
    if ([AFAppDotNetAPIClient sharedClient].reachabilityManager.networkReachabilityStatus == AFNetworkReachabilityStatusUnknown ||
        [AFAppDotNetAPIClient sharedClient].reachabilityManager.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
        
        __block BOOL isOk = YES;
        NSArray * leads = [[CoredataManager manager] allNodeForEntity:@"Lead"];
        [leads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            Lead * lead = (Lead *)obj;
            if ([lead.email isEqualToString:email]) {
                isOk = NO;
                *stop = YES;
            }
        }];
        [self hideLoadingView];
        if (isOk) {
            [self verifyEmailFinsih:YES msg:nil];
        }else{
            [self verifyEmailFinsih:NO msg:@"Email has been taken please choose another one"];
        }
    }else{
        [manage verifyEmail:email];
    }
}

- (void)navigationBarPreviousIsAppear:(BOOL)isAppear{
    if (isAppear && toolTipBt.hidden) {
        
        if (!self.navigationItem.leftBarButtonItem) {
            UIBarButtonItem *previousItem = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStylePlain target:self action:@selector(previousQuestion)];
            self.navigationItem.leftBarButtonItem = previousItem;
        }
    }else{
        self.navigationItem.leftBarButtonItem = nil;
    }
}

- (void)navigationBarNextIsAppear:(BOOL)isAppear{
    if (isAppear && toolTipBt.hidden) {
        
        if (!self.navigationItem.rightBarButtonItem) {
            UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextQuestion)];
            self.navigationItem.rightBarButtonItem = nextItem;
        }
    }else{
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)toolTipIsHid:(BOOL)ishide{
    if (ishide) {
        toolTipBt.hidden = YES;
        if (!self.navigationItem.rightBarButtonItem) {
            UIBarButtonItem *nextItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextQuestion)];
            self.navigationItem.rightBarButtonItem = nextItem;
        }
    }else{
        toolTipBt.hidden = NO;
        if (self.navigationItem.rightBarButtonItem) {
            self.navigationItem.rightBarButtonItem = nil;
        }
    }
}

- (void)exitButtonClicked {
    [self back];
}

#pragma mark --  SyncManageDelegate
-(void)verifyEmailFinsih:(BOOL)isVerify msg:(NSString *)msg{
    [self hideLoadingView];
    if (isVerify) {
        [naireView emailVerfyOk];
    }else{
        [naireView showEmailInvaild:msg];
        
    }
}

#pragma mark -- UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        //  give up this visitor
        [naireView cleanQcells];
        [self.delegate questionNaireFinish:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
