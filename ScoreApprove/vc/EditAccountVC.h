//
//  EditAccountVC.h
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol EditAccountVCDelegate <NSObject>
@optional
-(void)continueButtonClicked:(NSString *)text;
-(void)cancelButtonClicked;
@end

@interface EditAccountVC : BaseViewController

@property (nonatomic, assign) int dgType;   // 0 : edit account 1 : add option
@property (nonatomic, strong) NSString *answerTitle;
@property (nonatomic, weak) id<EditAccountVCDelegate>delegate;

@end
