//
//  QuestionToolTipViewController.m
//  ScoreApprove
//
//  Created by Yang on 15/5/1.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "QuestionToolTipViewController.h"

@interface QuestionToolTipViewController (){

    UIWebView * webView;
}

@end

@implementation QuestionToolTipViewController

- (void)viewDidLoad {

    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void)loadHtml:(NSString *)string{
    if(!webView){
        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 400, 500)];
        webView.scrollView.scrollEnabled = YES;
        webView.backgroundColor = [UIColor whiteColor];
        webView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:webView];
    }
    [webView loadHTMLString:string baseURL:nil];
}





@end
