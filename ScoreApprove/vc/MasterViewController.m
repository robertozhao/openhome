//
//  MasterViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "SourceInfoViewController.h"
#import "PopView.h"
#import "PoperContentViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "QuestionCheckViewController.h"

#import "LeadQuestion.h"
#import "EditAccountVC.h"
#import "ContactUsViewController.h"
#import "M13BadgeView.h"
#import "AddListViewController.h"
#import "DataManage.h"

#import "MasterTVCell.h"
#import "SubMenuTVCell.h"
#import "MenuItem.h"
#import "EmailTVCell.h"

#import "Answer.h"


@interface MasterViewController ()<PopViewDelegate,PoperContentViewControllerDelegate,LoginViewControllerDelegate,UIAlertViewDelegate,DataManageDelegate,PopViewDelegate,AddListViewControllerDelegate, QuestionCheckViewControllerDelegate>{
    
    IBOutlet UIView *syncView;
    IBOutlet UILabel *syncLb;

    IBOutlet UIActivityIndicatorView *sync;
    
    dispatch_queue_t dataQueue;

    UIView * bkView;
    PopView * addSourceView;
    NSArray * source;
    
    int numPostToService;
    int numGetFromService;
    
    BOOL isAppearFirst;
    BOOL isSyncFirst;
    
    int detailCurType;
    int subTVEditMode;      // 0 : disabled 1 : enabled
    
    int selectedListFilterIndex;
    
    NSIndexPath *selectedSubCell;
    
    BOOL isNewQuestion;
}

@property (nonatomic , strong) PopView * popView;

@property (nonatomic , strong) NSString * leadNum;
@property (nonatomic , strong) NSMutableDictionary * listNumDic;
@property (nonatomic , strong) NSMutableArray * listItemArray;
@property (nonatomic , strong) NSMutableArray * menuItemArray;

@property (nonatomic , strong)  NSIndexPath * cellSelected;
@property (nonatomic , strong)  NSArray * curUpdataLead;

@property NSMutableArray *objects;
@property (nonatomic, strong) UIPopoverController * popCtr;
@property (nonatomic, strong)  PoperContentViewController * popContentVC;

@property (nonatomic , strong)    NSArray * sourceList;

@property (nonatomic , strong)     NSMutableArray * activeQArray;
@property (nonatomic , strong)    NSMutableArray * inactiveQArray;

@property int myListings;
@property int myQuestions;
@property int counting;

@end

@implementation MasterViewController

+ (void) sharedInstance {
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    dataQueue =  dispatch_queue_create("dataQueue", NULL);

//    self.view.backgroundColor = RGBCOLOR(28, 28, 28);
    
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    
    setBt.layer.cornerRadius = 6;
    setBt.layer.borderColor = RGBCOLOR(66, 66, 66).CGColor;
    setBt.layer.borderWidth = 2;
    
    notifyBt.layer.cornerRadius = 4;
    notifyBt.layer.borderColor = RGBCOLOR(66, 66, 66).CGColor;
    notifyBt.layer.borderWidth = 2;
    
    isAppearFirst = YES;
    isSyncFirst = YES;
    myTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    myTableView.separatorColor = [UIColor blackColor];
    
    // use for sync
    [DataManage instance].delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    sync.color = [UIColor whiteColor];
    self.navigationController.navigationBar.hidden = YES;
    self.leadNum = 0;
    
    // show list detail with schedule
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showListDetail) name:@"NotificationForList" object:nil];

    // show lead with special list  at list detial page  trigger
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLeadForList:) name:@"NotificationForLeadShowList" object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popViewForEditEmailSignture) name:@"EditEmailSignature" object:nil];

    // reload tableView appear after add new list
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updataTbListItem) name:@"addNewlistFinish" object:nil];
    
     //  list sync finish post
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updataTbListItem) name:@"syncListFinish" object:nil];

    // reload sub table view
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subTableRefresh) name:@"tableViewReload" object:nil];
    
    // add lead after question naire
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updataTbListItem) name:@"reloadShowLead" object:nil];

    // auto select a row for listing
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setRow) name:@"setRow" object:nil];
    
    // auto select a row for lead
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setRow1) name:@"setRow1" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewSource) name:@"AddNewSource" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showListingContentByFilter:) name:@"ListFilterButtonClicked" object:nil];
    
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.listItemArray = [NSMutableArray arrayWithArray:@[MasterTbListAll,MasterTbListForSale,MasterTbListForInContract,MasterTbListForSold]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subTableRefresh) name:@"PictureDownloadFinish" object:nil];
    
    self.menuItemArray = [[NSMutableArray alloc] init];
    MenuItem *item = [[MenuItem alloc] init];
    
    item.title = MasterTBList;
    item.image = @"icon_house";
    item.selected_image = @"icon_house_select";
    item.item_id = 0;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBLeads;
    item.image = @"icon_leads";
    item.selected_image = @"icon_leads_select";
    item.item_id = 1;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBQuestions;
    item.image = @"icon_questions";
    item.selected_image = @"icon_questions_select";
    item.item_id = 2;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBAccount;
    item.image = @"icon_account";
    item.selected_image = @"icon_account_select";
    item.item_id = 3;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBEmail;
    item.image = @"icon_email";
    item.selected_image = @"icon_email_select";
    item.item_id = 4;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBSync;
    item.image = @"icon_sync";
    item.selected_image = @"icon_sync_select";
    item.item_id = 5;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBHelp;
    item.image = @"icon_help";
    item.selected_image = @"icon_help_select";
    item.item_id = 6;
    [self.menuItemArray addObject:item];
    
    item = [[MenuItem alloc] init];
    item.title = MasterTBLogout;
    item.image = @"icon_logout";
    item.selected_image = @"icon_logout_selected";
    item.item_id = 7;
    [self.menuItemArray addObject:item];
    
    [self initEditButton];
    editBt.hidden = NO;
    isNewQuestion = NO;
    
    self.myListings = 1;
    self.myQuestions = 0;
    
    myTableView.delaysContentTouches = NO;
    subTableView.delaysContentTouches = NO;
    
    self.counting = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:false forKey:@"OK"];
    [defaults synchronize];
//    [self tableView:myTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    

    if (![DataManage instance].islogined) {

        LoginViewController * vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        vc.delegate = self;
        vc.fromLogout = NO;
        AppDelegate * appdelegate =  [UIApplication sharedApplication].delegate;
        appdelegate.window.rootViewController = vc;
        
    }else{
        if (isAppearFirst) {
            if (detailCurType == SourceShowAll || detailCurType > LeadShowTypeAll || detailCurType == 0) {
                detailCurType  = SourceShowAll;
                // set default type
                [self.detailViewController setDetailType:SourceShowAll];
                
            }else{
                
                [self.detailViewController setDetailType:detailCurType];
            }
        }
        [self initSubList];
    }
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (isAppearFirst && [DataManage instance].islogined) {
        [self tableView:myTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        //  set default cell select
        self.cellSelected = [NSIndexPath indexPathForItem:0 inSection:0];
        MasterTVCell * cell = (MasterTVCell *)[myTableView cellForRowAtIndexPath:_cellSelected];
        
        [cell setSelected:YES animated:NO];
        
        isAppearFirst = NO;

    }
}

#pragma mark - new listing
#pragma mark

- (void) initSubList {
//    [self showLoadingViewWithText:@"loading..."];
    
//    if (detailCurType != EmailSignature && (detailCurType != QuestionManager || detailCurType != SourceShowAll)) {
        if (detailCurType != EmailSignature && (detailCurType != QuestionManager || detailCurType != SourceShowAll)) {

        if (selectedListFilterIndex == 0) {
            self.sourceList = [DataManage instance].allListing;
        } else {
            self.sourceList = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"sellState=%@ and user_id='%@'",[NSString stringWithFormat:@"%d", selectedListFilterIndex],[GlobalInstance instance].user_id]];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setInteger:selectedListFilterIndex forKey:@"selectedListFilterIndex"];
            [defaults synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshSourceHomeView" object: nil];
        }
        [self soureSort];
    }
    
    if (detailCurType == QuestionManager) {
        subTableView.editing = YES;
        subTableView.allowsSelectionDuringEditing = YES;
    } else if (detailCurType == SourceShowAll || detailCurType == SourceInfo) {
        subTableView.editing = YES;
        subTableView.allowsSelectionDuringEditing = YES;
    } else {
        subTableView.editing = NO;
        subTableView.allowsSelectionDuringEditing = NO;
    }
    
    [self subTableRefresh];
//    [self hideLoadingView];
}

- (void)subTableRefresh {
    [subTableView reloadData];
}

-(void)soureSort{
    self.sourceList = [self.sourceList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        List * list = (List *)obj1;
        List * list_ = (List *)obj2;
        
        NSDate *date1 = [Utility dateFromDateStr:list.creatDate withFormat:@"YYYY-MM-dd HH-mm-ss"];
        NSDate *date2 = [Utility dateFromDateStr:list_.creatDate withFormat:@"YYYY-MM-dd HH-mm-ss"];
        
        double timer1 = [Utility timeintervalForDate:date1];
        double timer2 = [Utility timeintervalForDate:date2];
        
        if ( timer1> timer2) {
            return NSOrderedAscending;
        }else{
            return NSOrderedDescending;
        }
    }];
}

#pragma mark -- notification method

// show list detail with schedule

-(void)showListDetail{

    NSIndexPath * index = _cellSelected;
//    if (index.row == 0 && index.section ==0) {
//        return;
//    }else{
        UITableViewCell * cell_ = [myTableView cellForRowAtIndexPath:self.cellSelected];
        [cell_ setSelected:NO animated:NO];
        
        UITableViewCell * cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0 ]];
        [cell setSelected:YES animated:NO];
        self.cellSelected = [NSIndexPath indexPathForItem:0 inSection:0 ];
//    }
}

// case 1: show lead with special list  at list detial page  trigger

-(void)showLeadForList:(NSNotification *)nofi{
    UITableViewCell * cell_ = [myTableView cellForRowAtIndexPath:self.cellSelected];
    [cell_ setSelected:NO animated:NO];

    NSIndexPath *selectedCell = [NSIndexPath indexPathForItem:1 inSection:0];
    UITableViewCell * cell = [myTableView cellForRowAtIndexPath:selectedCell];
    self.cellSelected = selectedCell;
    [cell setSelected:YES animated:NO];
    
    List * list = (List*)[nofi object];
    self.detailViewController.filterList =list;
    detailCurType =LeadShowTypeAll;
    [self.detailViewController setDetailType:LeadShowTypeAll];
    
    editBt.hidden = YES;
}

-(void)popViewForEditEmailSignture{
    
    [self.splitViewController dismissViewControllerAnimated:NO completion:nil];
    
    self.popView = [[PopView alloc] initWithFrame:self.splitViewController.view.bounds];
    self.popView.delegate = self;
    [self.popView setViewType:PopViewTypeEmailSignature];
    [self.splitViewController.view addSubview:self.popView];
}

// case 1: reload tableView appear after add new list
// case 2: list sync finish
// case 3: add lead after question naire

-(void)updataTbListItem{
    
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"user_id='%@'",[GlobalInstance instance].user_id]];
    
    NSDictionary * dic = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};
    
    NSArray * allItem = @[MasterTbListAll,MasterTbListForSale,MasterTbListForInContract,MasterTbListForSold,ShortSale,Rental,Rented,Auction];
    NSMutableArray * temp = [NSMutableArray arrayWithArray:allItem];
    
    NSMutableDictionary * itemDic = [NSMutableDictionary dictionaryWithDictionary:@{ShortSale:@"no",Rental:@"no",Rented:@"no",Auction:@"no"}];
    NSMutableDictionary * itemNumDic =[NSMutableDictionary dictionaryWithDictionary:@{MasterTbListAll:[NSString stringWithFormat:@"%lu",(unsigned long)array.count],MasterTbListForSale:@"0",MasterTbListForInContract:@"0",MasterTbListForSold:@"0",ShortSale:@"0",Rental:@"0",Rented:@"0",Auction:@"0"}]
    ;
    
    NSArray * keys = @[ShortSale,Rental,Rented,Auction];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        List * list = (List *)obj;
        NSString * cate_string = dic[list.sellState];
        NSString * num_str = itemNumDic[cate_string];
        int n= [num_str intValue]+1;
        [itemNumDic setObject:[NSString stringWithFormat:@"%d",n] forKey:cate_string];
        
        [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * cate_key = (NSString *)obj;
            if ([cate_key isEqualToString:cate_string]) {
                [itemDic setObject:@"yes" forKey:cate_key];
                *stop = YES;
            }
        }];
    }];
    
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * cate_key = (NSString *)obj;
        if ([itemDic[cate_key] isEqualToString:@"no"]) {
            __block NSUInteger index = -1;
            [temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString  * cate = (NSString *)obj;
                if ([cate isEqualToString:cate_key]) {
                    index = idx;
                    *stop = YES;
                }
            }];
            if (index!=-1) {
                [temp removeObjectAtIndex:index];
            }
        }
    }];
    self.listNumDic = itemNumDic;
    self.listItemArray = temp;
    
//    CGRect fram = myTableView.frame;
//    fram.size.height = (self.listItemArray.count+3)*44;
//    myTableView.frame = fram;
    
    NSArray * allLead = [[CoredataManager manager] allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"agent_id='%@'",[GlobalInstance instance].user_id]];
    self.leadNum = [NSString stringWithFormat:@"%lu",(unsigned long)allLead.count];
//    [myTableView reloadData];
    
    if (detailCurType != QuestionManager) {
        [self initSubList];
    }
    
//    if (_detailViewController.detailType == SourceShowAll && isSyncFirst) {
//        _detailViewController.detailType = SourceShowAll;
//        detailCurType = SourceShowAll;
//        isSyncFirst = NO;
//    }
}

#pragma mark -- private method

- (IBAction)setting:(id)sender {
 
    UIButton * bt = (UIButton *)sender;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:self.popContentVC];
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    
    float h =  [_popContentVC tbType:TBtypeSetting];
    _popCtr.popoverContentSize = CGSizeMake(330, h+44);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(void)addNewSource{
    
    AddListViewController * vc = [[AddListViewController alloc] initWithNibName:@"AddListViewController" bundle:nil];
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle =UIModalPresentationFormSheet;
    [self.splitViewController presentViewController:nav animated:YES completion:nil];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showDetail"]) {

        NSIndexPath *indexPath = [myTableView indexPathForSelectedRow];
        
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        self.detailViewController = controller;

        if (indexPath.section == 0) {

            NSString * cateChoosed = _listItemArray[indexPath.row];
            // MasterTbListAll,MasterTbListForSale,MasterTbListForInContract,MasterTbListForSold,ShortSale,Rental,Rented,Auction
            
            if ([cateChoosed isEqualToString:MasterTbListAll]) {
                detailCurType = SourceShowAll;
            }else if ([cateChoosed isEqualToString:MasterTbListForSale]){
                detailCurType = SourceShowForSale;
            }else if ([cateChoosed isEqualToString:MasterTbListForInContract]){
                detailCurType = SourceShowInContract;
            }else if ([cateChoosed isEqualToString:MasterTbListForSold]){
                detailCurType = SourceShowSold;
            }else if ([cateChoosed isEqualToString:ShortSale]){
                detailCurType = SourceShowShortSale;
            }else if ([cateChoosed isEqualToString:Rental]){
                detailCurType = SourceShowRental;
            }else if ([cateChoosed isEqualToString:Rented]){
                detailCurType = SourceShowRented;
            }else if ([cateChoosed isEqualToString:Auction]){
                detailCurType = SourceShowAuction;
            }
//            [controller setDetailType:detailCurType];
            [_detailViewController setDetailType:detailCurType];
        }else{
            if (indexPath.row == 0) {
//                [controller setDetailType:LeadShowTypeAll];
                [_detailViewController setDetailType:LeadShowTypeAll];
                detailCurType = LeadShowTypeAll;
            }
        }
    }
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender  {
    
    UITableViewCell * cell = (UITableViewCell *)sender;
    NSIndexPath * indexPath = [myTableView indexPathForCell:cell];
    
    if (indexPath.row == _cellSelected.row) {
        return NO;
    }else{
        return YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == subTableView && detailCurType == QuestionManager) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == myTableView) {
        return _menuItemArray.count;
    } else if (tableView == subTableView) {
        if (detailCurType == SourceShowAll || detailCurType == SourceInfo) {
            return [self.sourceList count] + 1;
        } else if (detailCurType == EmailSignature) {
            return 2;
        } else if (detailCurType == QuestionManager) {
            if (section == 0) {
                return _activeQArray.count;
            }else{
                return _inactiveQArray.count + 1;
            }
        } else {
            return [self.sourceList count];
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == myTableView) {
        MasterTVCell *cell = (MasterTVCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        MenuItem *item = [self.menuItemArray objectAtIndex:indexPath.row];
        [cell.mTitle setText:item.title];
        cell.mImage = item.image;
        cell.mSelectedImage = item.selected_image;
        [cell setTag:item.item_id];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        M13BadgeView * badgeView = (M13BadgeView *)[cell viewWithTag:555];
        if (!badgeView) {
            badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
            badgeView.hidesWhenZero = YES;
            badgeView.tag = 555;
            badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentMiddle;
            [cell addSubview:badgeView];
        }
        /*
         if (indexPath.section == 0) {
         NSString * cate = _listItemArray[indexPath.row];
         NSString * num = _listNumDic[cate];
         if ([num intValue] != 0) {
         badgeView.text = [NSString stringWithFormat:@"%@",_listNumDic[cate]];
         badgeView.hidden = NO;
         }else{
         badgeView.text = @"0";
         badgeView.hidden = YES;
         }
         cell.textLabel.text =cate;
         }else if (indexPath.section == 1){
         if ([_leadNum  intValue] != 0) {
         badgeView.text = [NSString stringWithFormat:@"%@",_leadNum];
         badgeView.hidden = NO;
         }else{
         badgeView.hidden = YES;
         }
         cell.textLabel.text = MasterTbLeadAll;
         }
         */
        CGRect fram = badgeView.frame;
        fram.origin.x = 210-badgeView.frame.size.width;
        badgeView.frame = fram;
        
        
//        if(indexPath.row == _cellSelected.row){
//            [cell setSelected:YES animated:NO];
//        }else{
//            [cell setSelected:NO animated:NO];
//        }
        return cell;
    } else if (tableView == subTableView) {
        if (detailCurType == EmailSignature) {
            EmailTVCell *cell = (EmailTVCell *)[tableView dequeueReusableCellWithIdentifier:@"EmailTVCell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if (indexPath.row == 0) {
                cell.cellTitle.text = @"Email Signature";
                cell.cellSwitch.hidden = YES;
                cell.cellBg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
            } else {
                cell.cellTitle.text = @"Auto Thank You Email";
                cell.imgArrow.hidden = YES;
                
                cell.cellSwitch.hidden = NO;
                cell.cellSwitch.tag = 500;
                
                NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
                if (!signtureDic) {
                    
                    signtureDic = @{@"isAutoSend":@"0"};
                    [[NSUserDefaults standardUserDefaults] setObject:signtureDic forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                cell.cellSwitch.on = [signtureDic[@"isAutoSend"] intValue] ==0 ?NO:YES;
                [cell.cellSwitch addTarget:self action:@selector(shiftSh:) forControlEvents:UIControlEventValueChanged];
            }
            
            return cell;
        } else if (detailCurType == QuestionManager) {
            if (indexPath.section == 1 && indexPath.row >= [_inactiveQArray count]) {
                NSString *cellIdentify = @"CreateQButtonCell";
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];
                UIButton *createQButton= [[UIButton alloc] initWithFrame:CGRectMake(8, 5, 272, 37)];
                [createQButton addTarget:self action:@selector(addQuestion) forControlEvents:UIControlEventTouchUpInside];
                createQButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0];
                [createQButton setBackgroundImage:[UIImage imageNamed:@"begin-btn.png"] forState:UIControlStateNormal];
                [createQButton setTitle:@"Create New" forState:UIControlStateNormal];
                createQButton.titleLabel.textColor = [UIColor whiteColor];
                
                [cell addSubview:createQButton];
                
                return cell;
            } else {
                Question * q;
                if (indexPath.section == 0) {
                    q = _activeQArray[indexPath.row];
                }else{
                    q = _inactiveQArray[indexPath.row];
                }
                
                NSString * cellIdentify = [NSString stringWithFormat:@"cellIndefire%@",q.question_id];
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
                
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor clearColor];
                UIImageView *origianlBg = [cell viewWithTag:5001];
                [origianlBg removeFromSuperview];
                
                UIImageView *imgBg = [[UIImageView alloc] initWithFrame:CGRectMake(8, 5, 272, 37)];
                [imgBg setImage:[UIImage imageNamed:@"sub_cell_bg"]];
//                [imgBg setHighlightedImage:[UIImage imageNamed:@"sub_cell_bg_selected"]];
                [imgBg setTag:5001];
                [cell addSubview:imgBg];
                [cell sendSubviewToBack:imgBg];
                
                UIView * view = [cell viewWithTag:500];
                
                int qID = [q.question_id intValue];
                if (qID == 6 || qID == 7 || qID == 8) {
                    if (!view) {
                        UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_lock.png"]];
                        imgV.frame = CGRectMake(tableView.frame.size.width-40, 10, 25, 25);
                        imgV.tag = 500;
                        [cell addSubview:imgV];
                    }
                }else{
                    if (view) {
                        [view removeFromSuperview];
                    }
                    if (subTVEditMode == 0) {
                        UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_right.png"]];
                        imgV.frame = CGRectMake(tableView.frame.size.width-17, 16, 8, 12);
                        imgV.tag = 500;
                        [cell addSubview:imgV];
                        
                    }
                }
                cell.textLabel.text = q.question;
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0];
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                if (indexPath == selectedSubCell) {
                    QuestionCheckViewController * vc =  [[QuestionCheckViewController alloc] initWithNibName:@"QuestionCheckViewController" bundle:nil];
                    vc.q = q;
                    vc.questionType = 1;
                    vc.delegate = self;
                    
                    [_detailViewController.navigationController pushViewController:vc animated:NO];
                    
                    [imgBg setImage:[UIImage imageNamed:@"sub_cell_bg_selected"]];
                    UIImageView *imgArrow = [cell viewWithTag:500];
                    imgArrow.hidden = YES;
                    
                    if (isNewQuestion) {
                        [subTableView scrollToRowAtIndexPath:indexPath
                                            atScrollPosition:UITableViewScrollPositionTop
                                                    animated:YES];
                    }
                }
                    
                return cell;
            }
        } else if (detailCurType == SourceShowAll) {
            if (indexPath.row < [self.sourceList count]) {
                SubMenuTVCell *cell = (SubMenuTVCell *)[tableView dequeueReusableCellWithIdentifier:@"SubMenuCell" forIndexPath:indexPath];
                
                cell.backgroundColor = [UIColor clearColor];
                List * list = _sourceList[indexPath.row];
                cell.mImg.isBig = 0;
                cell.mImg.image = [UIImage imageNamed:@"no-photos"];
                cell.mImg.placeHold = @"no-photos";
                cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg.png"];
                
                NSArray * array = [[list.picture allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    Picture * pic1 = (Picture *)obj1;
                    Picture * pic2 = (Picture *)obj2;
                    if ([pic1.order intValue] > [pic2.order intValue]) {
                        return NSOrderedDescending;
                    }else{
                        return NSOrderedAscending;
                    }
                }];
                if(array && array.count !=0){
                    Picture * pic = array[0];
                    cell.mImg.pic = pic;
                }
                
                [cell.mTitle setText:list.address];
                
                if ([list.price isEqual:[NSNull null]]) {
                    [cell.mPrice setText:[NSString stringWithFormat:@""]];
                } else {
                    [cell.mPrice setText:[NSString stringWithFormat:@"$%@", list.price]];
                    if([[cell.mPrice text] isEqualToString:@"$(null)"]) {
                        [cell.mPrice setText:[NSString stringWithFormat:@""]];
                    }
                }
                
                NSDictionary * dic = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};
                [cell.mStates setText:dic[list.sellState]];
                cell.mBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)[list.lead allObjects].count];
                
                if (indexPath == selectedSubCell) {
                    cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
                }
                
                if (subTVEditMode == 0) {
                    cell.sub_cell_bg.frame = CGRectOffset(cell.sub_cell_bg.frame, 0, 0);
                    NSLog(@"0");
                } else {
                    cell.sub_cell_bg.frame = CGRectOffset(cell.sub_cell_bg.frame, 0, 0);
                }
                
                return cell;
            } else {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreateButtonCell" forIndexPath:indexPath];
                
                cell.backgroundColor = [UIColor clearColor];
                UIButton *createButton=(UIButton *)[cell viewWithTag:111];
                [createButton addTarget:self action:@selector(addNewSource) forControlEvents:UIControlEventTouchUpInside];
                
                return cell;
            }
            
        } else {
            if (indexPath.row < [self.sourceList count]) {
                SubMenuTVCell *cell = (SubMenuTVCell *)[tableView dequeueReusableCellWithIdentifier:@"SubMenuCell" forIndexPath:indexPath];
                
                cell.backgroundColor = [UIColor clearColor];
                List * list = _sourceList[indexPath.row];
                cell.mImg.isBig = 0;
                cell.mImg.image = [UIImage imageNamed:@"no-photos"];
                cell.mImg.placeHold = @"no-photos";
                cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg.png"];
                
                NSArray * array = [[list.picture allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    Picture * pic1 = (Picture *)obj1;
                    Picture * pic2 = (Picture *)obj2;
                    if ([pic1.order intValue] > [pic2.order intValue]) {
                        return NSOrderedDescending;
                    }else{
                        return NSOrderedAscending;
                    }
                }];
                if(array && array.count !=0){
                    Picture * pic = array[0];
                    cell.mImg.pic = pic;
                }
                
                [cell.mTitle setText:list.address];
//                if ([list.price isEqualToString:@"0"]) {
//                    [cell.mPrice setText:[NSString stringWithFormat:@"$0"]];
//                } else {
//                    [cell.mPrice setText:[NSString stringWithFormat:@"$%@", list.price]];
//                }
                
                if ([list.price isEqual:[NSNull null]]) {
                    [cell.mPrice setText:[NSString stringWithFormat:@""]];
                } else {
                    [cell.mPrice setText:[NSString stringWithFormat:@"$%@", list.price]];
                    if([[cell.mPrice text] isEqualToString:@"$(null)"]) {
                        [cell.mPrice setText:[NSString stringWithFormat:@""]];
                    }
                }
                
                NSDictionary * dic = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};
                [cell.mStates setText:dic[list.sellState]];
                cell.mBadge.text = [NSString stringWithFormat:@"%lu",(unsigned long)[list.lead allObjects].count];
                
                if (indexPath == selectedSubCell) {
                    cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
                }
                
                return cell;
            } else {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CreateButtonCell" forIndexPath:indexPath];
                
                cell.backgroundColor = [UIColor clearColor];
                UIButton *createButton=(UIButton *)[cell viewWithTag:111];
                [createButton addTarget:self action:@selector(addNewSource) forControlEvents:UIControlEventTouchUpInside];
                
                return cell;
            }
        }
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == myTableView) {
        return 79;
    } else if (tableView == subTableView) {
        if (detailCurType == QuestionManager) {
            return 44;
        }
        if (indexPath.row >= [_sourceList count] || detailCurType == EmailSignature) {
            return 42;
        }
        return 84;
    }
    return 0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (subTVEditMode == 0) {                 // For QuestionManager only
        return NO;
    }
    return YES;
}

//*
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if (tableView == subTableView && detailCurType == QuestionManager) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
        lb.backgroundColor = RGBCOLOR(50, 50, 50);
        lb.textColor = [UIColor whiteColor];
        if (section == 0) {
            lb.text =[NSString stringWithFormat:@"           %@",Active];
        }else{
            lb.text =[NSString stringWithFormat:@"           %@",InActive];
        }
        return lb;
    }
    return nil;
}
// */

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == subTableView && detailCurType == QuestionManager) {
        return 44;
    }
    return 0;
}

- (void)syncAction {
    if (![GlobalInstance instance].netWorkStatue) {
        [self alertShow:AlertForNetWorkOff];
        return;
    }
    
    numGetFromService = 0;
    numPostToService = 0;
    
    //  submit  lead
    [DataManage instance].delegate = self;
    [[DataManage instance] dataSync];
}

-(void) setRow {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSIndexPath *defaultIndexPath = [NSIndexPath indexPathForRow:[defaults integerForKey:@"tempoSave"] inSection:0];
    [self tableView:subTableView didSelectRowAtIndexPath:defaultIndexPath];
}

-(void) setRow1 {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [defaults objectForKey:@"tempoSave1"];
    for (int i = 0; i < [subTableView numberOfRowsInSection:0]; i++) {
        SubMenuTVCell *cell = [subTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];

//        Lead* lead = self.sourceList[i];
        
//        NSString *leadAgentID = lead.user_id;
        
        if([str isEqualToString:cell.mTitle.text] == true) {
            cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
            selectedSubCell = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
//    NSIndexPath *defaultInde = [NSIndexPath indexPathForRow:[defaults integerForKey:@"tempoSave1"] inSection:0];
//    [self tableView:subTableView didSelectRowAtIndexPath:defaultIndexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == myTableView) {
        MasterTVCell * cell = [myTableView cellForRowAtIndexPath:indexPath];
        
        if (cell.tag != 5 && cell.tag != 6) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            MasterTVCell * cell_ = [myTableView cellForRowAtIndexPath:self.cellSelected];
            [cell_ setSelected:NO animated:NO];
            
            [cell setSelected:YES animated:NO];
            self.cellSelected = indexPath;
            editBt.hidden = YES;
        } else if(cell.tag == 7) {
            MasterTVCell * cell_ = [myTableView cellForRowAtIndexPath:self.cellSelected];
            [cell_ setSelected:NO animated:NO];
            [cell setSelected:NO animated:NO];
        } else {
            [cell setSelected:NO animated:NO];
        }
        
        switch(cell.tag) {
            case 0: {   // MasterTBList
                selectedSubCell = nil;
                [self initEditButton];
                editBt.hidden = NO;
                detailCurType  = SourceShowAll;
                // set default type
                [self.detailViewController setDetailType:SourceShowAll];
                [self initSubList];
                self.myListings = 1;
                self.myQuestions = 0;
            }
                break;
            case 1: {   // MasterTBLeads
                selectedSubCell = nil;
                [self initEditButton];
                self.detailViewController.filterList = nil;
                [_detailViewController setDetailType:LeadShowTypeAll];
                detailCurType = LeadShowTypeAll;
                [self initSubList];
            }
                break;
            case 2: {   // MasterTBQuestions
                isNewQuestion = NO;
                selectedSubCell = nil;
                editBt.hidden = NO;
                [self initEditButton];
                detailCurType = QuestionManager;
                [self getAllQuestion];
                [self initSubList];
                self.myListings = 0;
                self.myQuestions = 1;
            }
                break;
            case 3: {   // MasterTBAccount
                [self initEditButton];
                detailCurType = AccountManage;
                [_detailViewController setDetailType:AccountManage];
            }
                break;  
            case 4: {   // MasterTBEmail
                selectedSubCell = nil;
                [_detailViewController setDetailType:EmailSignature];
                detailCurType = EmailSignature;
                [self initSubList];
            }
                break;
            case 5: {   // MasterTBSync
                [self initEditButton];
                [self syncAction];
            }
                break;
            case 6: {   // MasterTBHelp
                [self initEditButton];
                ContactUsViewController * vc = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                
                nav.modalPresentationStyle = UIModalPresentationFormSheet;
                nav.navigationBarHidden = YES;
                
                [self.splitViewController presentViewController:nav animated:YES completion:nil];
                [self syncAction];
            }
                break;
            case 7: {   // MasterTBLogout
                [GlobalInstance instance].loginType =  LoginTypeNone;
                [DataManage instance].islogined = NO;
                
                LoginViewController * vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
                vc.delegate = self;
                vc.fromLogout = YES;
                AppDelegate * appdelegate =  [UIApplication sharedApplication].delegate;
                appdelegate.window.rootViewController = vc;
                [cell setSelected:NO animated:NO];
                [self syncAction];
            }
                break;
            default:
                break;
        }
    } else if (tableView == subTableView) {
        
        if (detailCurType == LeadShowTypeAll) {
            if ([_sourceList count] > 0) {
                List * list = _sourceList[indexPath.row];
                
                self.detailViewController.filterList = list;
                [_detailViewController setDetailType:LeadShowTypeAll];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"LeadItemSelected" object:list];
                
                if (selectedSubCell != nil) {
                    SubMenuTVCell *cell_ = (SubMenuTVCell *) [tableView cellForRowAtIndexPath:selectedSubCell];
                    cell_.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg"];
                }
                
                SubMenuTVCell *cell = (SubMenuTVCell *) [tableView cellForRowAtIndexPath:indexPath];
                cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
                selectedSubCell = indexPath;
            }
        } else if (detailCurType == QuestionManager) {
            isNewQuestion = NO;
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            Question * q_;
            
            if (indexPath.section == 0) {
                q_ = _activeQArray[indexPath.row];
            }else{
                if (indexPath.row >= [self.inactiveQArray count]) {
                    return;
                }
                q_ = _inactiveQArray[indexPath.row];
            }
            
            int qID = [q_.question_id intValue];
            if (qID == 6 || qID == 7 || qID == 8) {
                return;
            }
            
            QuestionCheckViewController * vc =  [[QuestionCheckViewController alloc] initWithNibName:@"QuestionCheckViewController" bundle:nil];
            vc.q = q_;
            vc.questionType = 1;
            vc.delegate = self;
            
            [_detailViewController.navigationController pushViewController:vc animated:NO];
            
            if (selectedSubCell != nil) {
                UITableViewCell *cell_ = (UITableViewCell *) [tableView cellForRowAtIndexPath:selectedSubCell];
                UIImageView *imgBg_ = [cell_ viewWithTag:5001];
                [imgBg_ setImage:[UIImage imageNamed:@"sub_cell_bg"]];
                UIImageView *imgArrow_ = [cell_ viewWithTag:500];
                imgArrow_.hidden = NO;
            }
            
            UITableViewCell *cell = (UITableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *imgBg = [cell viewWithTag:5001];
            [imgBg setImage:[UIImage imageNamed:@"sub_cell_bg_selected"]];
            UIImageView *imgArrow = [cell viewWithTag:500];
            imgArrow.hidden = YES;
            selectedSubCell = indexPath;
            
        } else if (detailCurType == EmailSignature) {
            
//        } else if (detailCurType == SourceShowAll) {
//            NSLog(@"-------------");
        } else {
            if ([_sourceList count] > 0) {
                if (indexPath.row >= [self.sourceList count]) {
                    return;
                }
                List * list = _sourceList[indexPath.row];
                _detailViewController.selectedList = list;
                
                [_detailViewController setDetailType:SourceInfo];
                detailCurType = SourceInfo;
                
                if (selectedSubCell != nil) {
                    SubMenuTVCell *cell_ = (SubMenuTVCell *) [tableView cellForRowAtIndexPath:selectedSubCell];
                    cell_.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg"];
                }
                
                SubMenuTVCell *cell = (SubMenuTVCell *) [tableView cellForRowAtIndexPath:indexPath];
                cell.sub_cell_bg.image = [UIImage imageNamed:@"sub_cell_bg_selected"];
                selectedSubCell = indexPath;
            }
        }
    }
}

#pragma mark - Question Manager tableview delegate
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == subTableView && detailCurType == QuestionManager) {
        
        if ((indexPath.section == 1 && indexPath.row >= [_inactiveQArray count]) || subTVEditMode == 0) {
            return NO;
        }
        
        Question * q ;
        if (indexPath.section ==0) {
//            return NO;
            q = _activeQArray[indexPath.row];
        }else{
            q = _inactiveQArray[indexPath.row];
        }
        NSInteger n = [q.question_id integerValue];
        if (n==6 || n==7 || n==8) {
            return NO;
        }else{
            return YES;
        }
    }
    
    return NO;
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DFLog(@"%@",indexPath);
    if(self.myQuestions == 1) {
        Question * q_;
        
        if (indexPath.section == 0) {
            q_ = _activeQArray[indexPath.row];
        }else{
            q_ = _inactiveQArray[indexPath.row];
        }
        
        [[CoredataManager manager] deleteObject:q_];
        [[q_.answer allObjects] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Answer * answer = (Answer *)obj;
            answer.toDele = @"0";
        }];
        [[CoredataManager manager] saveContext];
        
        selectedSubCell = nil;
        [self getAllQuestion];
        [self initSubList];
        
    } else {
        //[yourArray removeObjectAtIndex:indexPath.row];
        List* list = self.sourceList[indexPath.row];
        [[CoredataManager manager] deleteObject:list];
        
        NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:_sourceList];
        [mutableArray removeObjectAtIndex:indexPath.row];
        
        _sourceList = [NSArray arrayWithArray: mutableArray];
        [self tableView:myTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

        [subTableView reloadData];
    }
//    [self addQuestion];
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    if ((sourceIndexPath.section == 1 && sourceIndexPath.row >= [_inactiveQArray count]) ||  (destinationIndexPath.section == 1 && destinationIndexPath.row > [_inactiveQArray count]) ||
        (destinationIndexPath.section == 1 && destinationIndexPath.row == [_inactiveQArray count] && [_inactiveQArray count] != 0) ) {
        [self initSubList];
        return;
    }
    
    Question * q ;
    if (sourceIndexPath.section ==0) {
        q = _activeQArray[sourceIndexPath.row];
    }else{
        q = _inactiveQArray[sourceIndexPath.row];
    }
    
    if (destinationIndexPath.section ==0) {
        Question * q_;
        if (_activeQArray.count-1 < destinationIndexPath.row) {
            q_ = nil;
        }else{
            q_= _activeQArray[destinationIndexPath.row];
        }
        
        if (sourceIndexPath.section ==0) {
            [_activeQArray removeObject:q];
        }else{
            [_inactiveQArray removeObject:q];
        }
        q.is_active = @"1";
        [_activeQArray insertObject:q atIndex:destinationIndexPath.row];
        [_activeQArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Question * q = (Question *)obj;
            q.sortNum = [NSString stringWithFormat:@"%ld",(unsigned long)idx];
        }];
        
    }else{
        if (sourceIndexPath.section ==0) {
            [_activeQArray removeObject:q];
        }else{
            [_inactiveQArray removeObject:q];
        }
        q.is_active = @"0";
        [_inactiveQArray insertObject:q atIndex:destinationIndexPath.row];
    }
    [[CoredataManager manager] saveContext];
    [self initSubList];
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == subTableView && detailCurType == QuestionManager) {
        if ((indexPath.section == 1 && indexPath.row >= [_inactiveQArray count]) || subTVEditMode == 0) {
            return UITableViewCellEditingStyleNone;
        }
        Question * q ;
        if (indexPath.section ==0) {
//            return UITableViewCellEditingStyleNone;
            q = _activeQArray[indexPath.row];
        }else{
            q = _inactiveQArray[indexPath.row];
        }
        
        NSInteger n = [q.question_id integerValue];
        if (n==6 || n==7 || n==8) {
            return UITableViewCellEditingStyleNone;
        }
        
        if ([q.is_required integerValue] == 1) {
            return UITableViewCellEditingStyleNone;
        }else{
            return UITableViewCellEditingStyleDelete;
        }
    }
    
    if (tableView == subTableView && (detailCurType == SourceShowAll || detailCurType == SourceInfo)) {
        if( indexPath.row == [self.sourceList count]) {
            return UITableViewCellEditingStyleNone;
        }
        return UITableViewCellEditingStyleDelete;
    }
    
    return     UITableViewCellEditingStyleNone;
}

#pragma mark -- PopViewDelegate

-(void)newEmailSignture:(NSString *)signture{
    
//    if ([signture isEqualToString:@""]) {
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
//    }else{
//        [[NSUserDefaults standardUserDefaults] setObject:signture forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
//    }
//    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Quesetion Methods
-(void)getAllQuestion{
    
    NSMutableArray * temp = [NSMutableArray array];
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:@"user_id==0" ];
    [temp addObjectsFromArray:array];
    
    NSArray * array_ = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"user_id=='%@'",[GlobalInstance instance].user_id]];
    [temp addObjectsFromArray:array_];
    
    NSMutableArray * active_temp = [NSMutableArray array];
    self.inactiveQArray = [NSMutableArray array];
    
    [temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Question *q = (Question *)obj;
        if ([q.is_active isEqualToString:@"1"]) {
            [active_temp addObject:q];
        }else{
            [_inactiveQArray addObject:q];
        }
    }];
    
    NSArray * a = [active_temp sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Question * q = (Question *)obj1;
        Question * q_ = (Question *)obj2;
        if ([q.sortNum intValue] > [q_.sortNum intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    self.activeQArray = [NSMutableArray arrayWithArray:a];
    
    if (selectedSubCell == nil) {
        if ([_activeQArray count] > 3) {
            selectedSubCell = [NSIndexPath indexPathForItem:3 inSection:0];
        } else {
            selectedSubCell = [NSIndexPath indexPathForItem:0 inSection:1];
        }
    }
}

-(void)addQuestion{
    isNewQuestion = YES;
    
    QuestionCheckViewController * vc = [[QuestionCheckViewController alloc] initWithNibName:@"QuestionCheckViewController" bundle:nil];
    vc.delegate = self;
    vc.questionType = 0;
    vc.sortNum = [NSString stringWithFormat:@"%u",(unsigned int)(_activeQArray.count + _inactiveQArray.count)];
    
    [_detailViewController.navigationController pushViewController:vc animated:NO];
}

#pragma mark -- QuestionCheckViewControllerDelegate

-(void)questionEditDone{
    
    [[CoredataManager manager] saveContext];
    [self getAllQuestion];
    
    if (isNewQuestion) {
        selectedSubCell = [NSIndexPath indexPathForItem:[self.inactiveQArray count] - 1 inSection:1];
        
        [self initSubList];
    } else {
        [self animateSelectedSubCell];
    }
}

- (void)animateSelectedSubCell {
    UITableViewCell *cell = [subTableView cellForRowAtIndexPath:selectedSubCell];
    
    UIImageView *imgBg = [cell viewWithTag:5001];
    [imgBg setImage:[UIImage imageNamed:@"sub_cell_bg"]];
    UIImageView *imgArrow = [cell viewWithTag:500];
    imgArrow.hidden = NO;
    
    [self performSelector:@selector(becomeActiveAnimation) withObject:nil afterDelay:0.0];
}
- (void)becomeActiveAnimation {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    UITableViewCell *cell = [subTableView cellForRowAtIndexPath:selectedSubCell];
    UIImageView *imgBg = [cell viewWithTag:5001];
    [imgBg setImage:[UIImage imageNamed:@"sub_cell_bg_selected"]];
    [imgBg.layer addAnimation:transition forKey:nil];
    UIImageView *imgArrow = [cell viewWithTag:500];
    imgArrow.hidden = YES;
    [imgArrow.layer addAnimation:transition forKey: nil];
}

#pragma mark -- PoperContentViewControllerDelegate

-(void)tbItemChoosed:(NSString *)text{
    [self.splitViewController dismissViewControllerAnimated:NO completion:nil];
    if ([text isEqualToString:SettingManageQuestion]) {
        [self.popCtr dismissPopoverAnimated:NO];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"showQuestionPageSheet" object:nil];
    }else if ([text isEqualToString:SettingLogout]){
    
        [GlobalInstance instance].loginType =  LoginTypeNone;
        [DataManage instance].islogined = NO;
        
        LoginViewController * vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        vc.delegate = self;
        vc.fromLogout = YES;
        AppDelegate * appdelegate =  [UIApplication sharedApplication].delegate;
        appdelegate.window.rootViewController = vc;   
    }else if ([text isEqualToString:SettingEditAccount]){
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:AlertTitleNotice message:AlertMsgForLeavingApp delegate:self cancelButtonTitle:ButtonTitleCancel otherButtonTitles:ButtonTitleContinue, nil];
        alert.tag = 9898;
        [alert show];

    }else if ([text isEqualToString:SettingGetHelp]){
     
        ContactUsViewController * vc = [[ContactUsViewController alloc] initWithNibName:@"ContactUsViewController" bundle:nil];
        UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
        
        nav.modalPresentationStyle = UIModalPresentationFormSheet;
        nav.modalPresentationStyle = UIModalTransitionStyleCrossDissolve;
        
        //This will be the size you want
        
        [self.splitViewController presentViewController:nav animated:YES completion:nil];
    }
    else if ([text rangeOfString:SettingSync].location != NSNotFound){
        
        if (![GlobalInstance instance].netWorkStatue) {
            [self alertShow:AlertForNetWorkOff];
            return;
        }
        
        numGetFromService = 0;
        numPostToService = 0;
        
        //  submit  lead
        [DataManage instance].delegate = self;
        [[DataManage instance] dataSync];
    }
}

#pragma mark -- LoginViewControllerDelegate

-(void)loginFinish{
    
    AppDelegate * appdelegate =  [UIApplication sharedApplication].delegate;
    appdelegate.window.rootViewController = appdelegate.splitViewController;

    [self updataTbListItem];
    isSyncFirst = YES;
    detailCurType = SourceShowAll;
    
     [self.detailViewController setDetailType:SourceShowAll];
    
    self.cellSelected = [NSIndexPath indexPathForItem:0 inSection:0];
    UITableViewCell * cell = [myTableView cellForRowAtIndexPath:_cellSelected];
    [cell setSelected:YES animated:NO];
}



#pragma mark -- DataManageDelegate

-(void)beginToSync:(NSString *)text{
    syncLb.text = text;
    syncView.hidden = NO;
    [sync startAnimating];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BeginToSync" object:nil];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FinishSync" object:nil];
}

#pragma mark - Email Signature Switch
-(void)shiftSh:(UISwitch *)sh{
    
    NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    NSMutableDictionary * temp = [NSMutableDictionary dictionaryWithObject:sh.on?@"1":@"0" forKey:@"isAutoSend"];
    
    
    [temp setObject:@"1" forKey:@"isHadNew"];
    
    if (dic[@"signture"]) {
        [temp setObject:dic[@"signture"] forKey:@"signture"];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[DataManage instance] syncEmailSignToSV];
}

- (IBAction)onEditButton:(id)sender {
    if (subTVEditMode == 0) {
        subTVEditMode = 1;
        [editBt setTitle:@"Done" forState:UIControlStateNormal];
    } else {
        [self initEditButton];
    }
    
    [self initSubList];
}

- (void)initEditButton {
    subTVEditMode = 0;
    [editBt setTitle:@"Edit" forState:UIControlStateNormal];
}

#pragma mark - List detail page post
- (void) showListingContentByFilter:(NSNotification *)notification {
    NSDictionary *userInfo = (NSDictionary *)notification.userInfo;
    selectedListFilterIndex = [[userInfo objectForKey:@"selectedButtonIndex"] intValue];
    [DataManage instance].listFilterButtonIndex = selectedListFilterIndex;
    
    [self initSubList];
    [self tableView:myTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

@end
