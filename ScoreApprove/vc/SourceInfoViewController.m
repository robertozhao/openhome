//
//  SourceInfoViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "SourceInfoViewController.h"
#import "PoperContentViewController.h"
#import "PhotoEditeCtr.h"
#import "PopView.h"
#import "OpenHomeViewController.h"
#import "ScheduleViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
//#import "AMPFBUtility.h"
#import "ELCImagePickerHeader.h"
#import "ELCImagePickerController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "MasterViewController.h"
#import "Picture.h"
#import "PictureManage.h"
#import "AppDelegate.h"
#import "UIImage+fixOrientation.h"
#import "SyncImageView.h"

#import "ListFilterVC.h"
#import "eOHSelectionVC.h"

#import "M13BadgeView.h"

//AMPFBUtilityDelegate
@interface SourceInfoViewController ()<PoperContentViewControllerDelegate,PopViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,PhotoEditeCtrDelegate,UIPopoverControllerDelegate,MFMailComposeViewControllerDelegate,ELCImagePickerControllerDelegate,ELCImagePickerControllerDelegate,OpenHomeViewControllerDelegate,PictureManageDeleaget, ScheduelViewControllerDelegate, UITextFieldDelegate>{

    IBOutlet UIScrollView *contentSV;
    
    IBOutlet UIButton *leadBt;
    IBOutlet UIView *subContentV;

    IBOutlet UIButton *openHomeBt;
    
    IBOutlet UIButton *scheduleBt;

    IBOutlet UIScrollView *imgContentSV;

    IBOutlet UIButton *photoEditBt;
    IBOutlet UIButton *addPhotoBt;

    IBOutlet UnderLineButton *addressBt;
    IBOutlet UnderLineButton *sellStateBt;
    IBOutlet UnderLineButton *priceBt;
    
    IBOutlet UnderLineButton *listCateBt;
    IBOutlet UnderLineButton *fitmentBt;
    IBOutlet UnderLineButton *bedsBt;
    IBOutlet UnderLineButton *bathsBt;
    IBOutlet UnderLineButton *areaBt;
    IBOutlet UILabel *areaLbl;
    IBOutlet UnderLineButton *descBt;
    IBOutlet UILabel *descLb;
    IBOutlet UITextView *descTxt;
    IBOutlet UILabel *bathsLabel;
    
    IBOutlet UILabel *schdeLb;
//    AMPFBUtility * fbUtilty;
    
    PictureManage * picManage;
 
    BOOL viewWillDisappearWithBack;
    
    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    
    IBOutlet UIButton *btnAdd;
    IBOutlet UIButton *btnFilter;
    IBOutlet UITextField *editAddressTf;
    
    NSDateFormatter * dateFormat;
}

@property (nonatomic, strong) PopView * popView;
@property (nonatomic, strong) UIButton * actionBt;
@property (nonatomic, strong) UIPopoverController * popCtr;
@property (nonatomic, strong)  PoperContentViewController * popContentVC;
@property (nonatomic, strong)  ScheduleViewController * scheduleVC;
@property (nonatomic, strong)  PhotoEditeCtr * photoEditCtr;

@property (nonatomic, strong) M13BadgeView * leadBadge;

@property (nonatomic, assign) int selectedFilterButton;

@end

@implementation SourceInfoViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeStyle:NSDateFormatterLongStyle];
    [dateFormat setDateFormat:@"hh:mm aaa\nEEE MMM dd"];
    NSLocale * twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormat.locale = twelveHourLocale;
    
    CGRect frame = self.view.frame;
    self.view.frame = CGRectMake(0, -100, frame.size.width, frame.size.height);
    

    viewWillDisappearWithBack = YES;
    
    self.title = _list.address;
    self.view.backgroundColor = [UIColor whiteColor];
    openHomeBt.layer.cornerRadius = 5;
    [openHomeBt setTitle:ListInfoViewBeginOpenHouse forState:0];
    
    photoEditBt.alpha = 0.8;
    scheduleBt.alpha = 0.8;
    photoEditBt.layer.cornerRadius = 5;
    scheduleBt.layer.cornerRadius = 5;
    photoEditBt.backgroundColor= [UIColor whiteColor];
    photoEditBt.backgroundColor = [UIColor whiteColor];
    schdeLb.hidden = YES;

    if (_list.schedule) {
        NSString * str = [dateFormat stringFromDate:_list.schedule];
        schdeLb.hidden = NO;
        schdeLb.text = str;
    }
    
    if (_list.picture && [_list.picture allObjects].count > 0) {

        [self addListImageToSV];
        [addPhotoBt removeFromSuperview];
    }

//    [addressBt judgeW:_list.address];
    [addressBt setTitle:_list.address forState:0];
    [editAddressTf setText:_list.address];
    
    NSDictionary * dic = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};

    NSString * cate = dic[_list.sellState];
//    [sellStateBt judgeW:cate];
    [sellStateBt setTitle:cate forState:0];
    if ([cate isEqualToString:Sold]) {

        openHomeBt.hidden = YES;
    }else{
        openHomeBt.hidden = NO;
    }
    
    if (_list.price) {
//        [priceBt judgeW:_list.price];
        [priceBt setTitle:_list.price forState:0];
    }else{
//        [priceBt judgeW:@"000"];
        [priceBt setTitle:@"" forState:0];
    }
    
//    [listCateBt judgeW:_list.listCate];
    [listCateBt setTitle:_list.listCate forState:0];
//    NSString * beds = [_list.beds isEqualToString:@"Studio"]?@"Studio":[NSString stringWithFormat:@"%@ Beds",_list.beds];
//    [fitmentBt judgeW:[NSString stringWithFormat:@"%@/%@",beds,[NSString stringWithFormat:@"%@ Baths",_list.baths]]];
//    [fitmentBt setTitle:[NSString stringWithFormat:@"%@/%@",beds,[NSString stringWithFormat:@"%@ Baths",_list.baths]] forState:0];
    [bedsBt setTitle:_list.beds forState:0];
    [bathsBt setTitle:_list.baths forState:0];

    if (_list.size) {
//        [areaBt judgeW:title];
        [areaBt setTitle:_list.size forState:0];
        [areaLbl setText:_list.sizeUnit];
    }else{
//        [areaBt judgeW:[NSString stringWithFormat:@"0 %@",Sqft]];
        [areaBt setTitle:@"0" forState:0];
    }

    if (_list.desc && ![_list.desc isEqualToString:@""]) {
        NSString * str = _list.desc;
        descTxt.backgroundColor = [UIColor whiteColor];
        descTxt.text = _list.desc;
        /*
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:17]};
        CGRect rect_titleLb = [str boundingRectWithSize:CGSizeMake(descLb.frame.size.width, MAXFLOAT)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:attributes
                                                 context:nil];
        
        if (rect_titleLb.size.height > 30) {
            CGRect fram = descLb.frame;
            fram.size.height = rect_titleLb.size.height;
            descLb.frame = fram;
            
            CGRect fram_bt = descBt.frame;
            fram_bt.size.height = rect_titleLb.size.height;
            descBt.frame = fram_bt;
        }
        descLb.text = str;
        [descBt judgeW:str];
         */
    }else{
        descTxt.backgroundColor = [UIColor clearColor];
        descTxt.text = @"";
        descLb.text = ListDesctiptionPlaceHold;
//        [descBt judgeW:ListDesctiptionPlaceHold];
    }
//    contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, descBt.frame.size.height+descBt.frame.origin.y+10);
    [descBt setTitle:@"" forState:0];
    
    UITapGestureRecognizer *tapDesc = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(descEdit:)];
    [descTxt addGestureRecognizer:tapDesc];
    
    // Add Badge View to Leads Button
    _leadBadge = [[M13BadgeView alloc] init];
    
    _leadBadge.hidesWhenZero = YES;
    _leadBadge.textColor = [UIColor whiteColor];
    _leadBadge.badgeBackgroundColor = [UIColor colorWithRed:246.0/255.0f green:146.0/255.0f blue:30.0/255.0f alpha:1.0f];
    _leadBadge.tag = 555;
    _leadBadge.verticalAlignment = M13BadgeViewVerticalAlignmentTop;
    _leadBadge.horizontalAlignment = M13BadgeViewHorizontalAlignmentRight;
    _leadBadge.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10.0f];
    [_leadBadge setText:@"0"];

    [leadBt addSubview:_leadBadge];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eOHTapped:) name:@"eOHSelectiontTapped" object:nil];
    
    self.selectedFilterButton = [DataManage instance].listFilterButtonIndex;
    if (self.selectedFilterButton == nil) {
        self.selectedFilterButton = 0;
    }
    
    editAddressTf.hidden = YES;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissEditAddress)];
    
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tapImg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editPhotoAction)];
    [imgContentSV addGestureRecognizer:tapImg];
    
    //add tap gesture to labels
    bathsLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(onTapLabel)];
    [bathsLabel addGestureRecognizer:tapGesture];
    [priceBt.titleLabel setAdjustsFontSizeToFitWidth:YES];
}

- (void) onTapLabel {
    [self dismissEditAddress];

    self.actionBt = bathsBt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    self.popCtr.backgroundColor = [UIColor whiteColor];
    
    float h =  [_popContentVC tbType:TBtypeBaths];
    _popCtr.popoverContentSize = CGSizeMake(190, h);
    [_popCtr presentPopoverFromRect:bathsBt.bounds inView:bathsBt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void) editPhotoAction {
    self.photoEditCtr = [[PhotoEditeCtr alloc] init];
    self.photoEditCtr.list = _list;
    self.photoEditCtr.delegate = self;
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:self.photoEditCtr];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.navigationBarHidden = YES;
    
    [self.splitViewController presentViewController:nav animated:YES completion:nil];
}

#pragma mark - Address textfield method
- (void)dismissEditAddress {
    [editAddressTf resignFirstResponder];
    [self newAddress:editAddressTf.text];
    editAddressTf.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    CGRect frame = self.view.frame;
//    self.view.frame = CGRectMake(0, -1000, frame.size.width, frame.size.height);
//    NSLog(@"Frame=%f", self.view.frame.origin.y);
    self.navigationItem.leftBarButtonItem = nil;
//    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
//    self.navigationItem.leftBarButtonItem.tintColor = OriginColor;
    
    if ([_list.lead allObjects].count != 0) {
        [_leadBadge setText:[NSString stringWithFormat:@"%lu", (unsigned long)[_list.lead allObjects].count]];
//        [leadBt setTitle:[NSString stringWithFormat:@"Listing Lead (%lu)",(unsigned long)[_list.lead allObjects].count] forState:0];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    // when camer show, viewWillDisappearWithBack will be no
    if ([_list.needSync isEqualToString:@"1"] && viewWillDisappearWithBack) {
        [[DataManage instance] submitListToSv:@[_list]];
        [[CoredataManager manager] saveContext];
    }
}

-(void)addListImageToSV{

    NSArray * array = [imgContentSV subviews];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView * view = (UIView*)obj;
        [view removeFromSuperview];
    }];
    float padding =0;
    NSArray * imgs = [_list.picture allObjects];
    imgs = [imgs sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Picture * pic1 = (Picture *)obj1;
        Picture * pic2 = (Picture *)obj2;
        if ([pic1.order intValue] > [pic2.order intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    [imgs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * pic = (Picture *)obj;
        SyncImageView * imgV = [[SyncImageView alloc] initWithFrame:CGRectMake(padding+imgContentSV.frame.size.width*idx, 0, imgContentSV.frame.size.width-2* padding, imgContentSV.frame.size.height)];
        imgV.contentMode = UIViewContentModeScaleAspectFill;
        imgV.placeHold = @"image_load";
        imgV.isBig = 1;
        imgV.pic = pic;

        [imgContentSV addSubview:imgV];
    }];
    imgContentSV.contentSize = CGSizeMake(imgs.count*(imgContentSV.frame.size.width+padding)-2*padding, imgContentSV.frame.size.height);
}


- (IBAction)showLead:(id)sender {
    [self dismissEditAddress];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationForLeadShowList" object:_list];
    
}


- (IBAction)shareFb:(id)sender {
    
//    if (!fbUtilty) {
//        fbUtilty = [[AMPFBUtility alloc] init];
//        fbUtilty.delegate  = self;
//    }
//    
////    NSString * imgPath = [[DataManage instance] listFirImgPath:_list];
//    
//    NSDictionary * dic = @{@"name":[NSString stringWithFormat:@"ScroeApprove for %@",_list.address],@"description":_list.address,@"link":@"https://agents.scoreapprove.com",@"caption":@""};
//    [fbUtilty shareURL:dic];
}

-(void)shareAction:(UIBarButtonItem *)item{

    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    
    float h =  [_popContentVC tbType:TBtypeListShare];
    
    _popCtr.popoverContentSize = CGSizeMake(260, h);
    [_popCtr presentPopoverFromBarButtonItem:item permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

-(void)twitterShare{
    DFLog(@"twitter share");
}

-(void)emailShare{
    [_popCtr dismissPopoverAnimated:NO];

    if (![MFMailComposeViewController canSendMail]) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:EmailCanNotUse message:EmailCanNotUseMsg delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
//    [self displayMailPicker];
}

/*
-(void)displayMailPicker{

    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    
    [mailPicker setSubject: [NSString stringWithFormat:@"About %@",_list.address]];
    
    if (_list.photoName && _list.photoName.length > 0) {
        [addPhotoBt removeFromSuperview];
        UIImage * img = [[DataManage instance] listFirstImg:_list];

        NSData *imageData = UIImagePNGRepresentation(img);
        [mailPicker addAttachmentData: imageData mimeType: @"" fileName: @"list_icon.png"];
    }

    NSString * addInfo = [NSString stringWithFormat:@"I thought you would be interested in %@",_list.address];
    
    NSString * beds = [_list.beds isEqualToString:@"Studio"]?@"Studio":[NSString stringWithFormat:@"%@ bedrooms",_list.beds];
    
   NSString * emalText = [NSString stringWithFormat:@"%@<br><br>-%@<br>-%@ bathrooms<br>-%@ %@<br>-%@ %@<br><br>%@<br>",addInfo,beds,_list.baths,_list.priceUnit,_list.price,_list.size,_list.sizeUnit,_list.desc];
    NSString *emailBody = [NSString stringWithFormat:@"<font color='black'>%@</font>",emalText];

    [mailPicker setMessageBody:emailBody isHTML:YES];
    [self presentViewController:mailPicker animated:YES completion:^{
        
    }];
}

*/

- (IBAction)addPhoto:(id)sender {
    [self dismissEditAddress];
    
    UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:ButtonTitleDismiss destructiveButtonTitle:nil otherButtonTitles:ActionTitleCamera, ActionTitlePhotoLibrary, nil];
    [sheet showInView:self.view];
}


- (IBAction)addressEdit:(id)sender {

    editAddressTf.hidden = NO;
    [editAddressTf becomeFirstResponder];
    
//    self.popView = [[PopView alloc] initWithFrame:self.splitViewController.view.bounds];
//    self.popView.delegate = self;
//    self.popView.list = self.list;
//    [self.popView setViewType:PopViewTypeEditListAddress];
//
//    [self.splitViewController.view addSubview:self.popView];
}

- (IBAction)auction:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    
    eOHSelectionVC *vc = [[eOHSelectionVC alloc] init];
    
    NSDictionary * dic = @{ForSale:@"0",InContract:@"1",Sold:@"2",ShortSale:@"3",Rental:@"4",Rented:@"5",Auction:@"6"};
    NSString *selectedTitle = sellStateBt.currentTitle;
    
    vc.selectedIndex = [[dic objectForKey:selectedTitle] intValue];
    vc.selectedText = selectedTitle;
    vc.viewType = 1;
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    self.popCtr.backgroundColor = RGBCOLOR(240, 240, 240);
    
    [_popCtr presentPopoverFromRect:self.actionBt.bounds inView:self.actionBt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    return;
}

- (IBAction)photoEdit:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    self.popCtr.backgroundColor = [UIColor whiteColor];
    
    float h =  [_popContentVC tbType:TBtypePhotoChoose];
    _popCtr.popoverContentSize = CGSizeMake(260, h);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (IBAction)schedule:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    self.scheduleVC = [[ScheduleViewController alloc] init];
    self.scheduleVC.list = _list;
    self.scheduleVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.scheduleVC];
    self.popCtr.backgroundColor = [UIColor whiteColor];
    _popCtr.popoverContentSize = CGSizeMake(280, 318);
    self.popCtr.delegate = self;
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}


- (IBAction)priceEdit:(id)sender {
    [self dismissEditAddress];
    
    self.popView = [[PopView alloc] initWithFrame:self.splitViewController.view.bounds];
    self.popView.delegate = self;
    self.popView.list = _list;
    [self.popView setViewType:PopViewTypePriceEdit];
    
    [self.splitViewController.view addSubview:self.popView];
}

- (IBAction)cateChoose:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    
    eOHSelectionVC *vc = [[eOHSelectionVC alloc] init];
    
    NSDictionary * dic = @{Home:@"0",Townhouse:@"1",Condo:@"2",Coop:@"3",SingleFamily:@"4",LotLand:@"5",TIC:@"6",Loft:@"7"};
    NSString *selectedTitle = listCateBt.currentTitle;
    
    vc.selectedIndex = [[dic objectForKey:selectedTitle] intValue];
    vc.selectedText = selectedTitle;
    vc.viewType = 0;
    
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    self.popCtr.backgroundColor = RGBCOLOR(240, 240, 240);
    
    [_popCtr presentPopoverFromRect:self.actionBt.bounds inView:self.actionBt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    return;
}

- (IBAction)onBtnBeds:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    self.popCtr.backgroundColor = [UIColor whiteColor];
    
    float h =  [_popContentVC tbType:TBtypeBed];
    _popCtr.popoverContentSize = CGSizeMake(190, h);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (IBAction)onBtnBaths:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
    self.actionBt = bt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    self.popCtr.backgroundColor = [UIColor whiteColor];
    
    float h =  [_popContentVC tbType:TBtypeBaths];
    _popCtr.popoverContentSize = CGSizeMake(190, h);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (IBAction)fitment:(id)sender {
    [self dismissEditAddress];
    
    UIButton * bt = (UIButton *)sender;
        self.actionBt = bt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.list = _list;
     self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    
    float h =  [_popContentVC tbType:TBtypeFitment];
    _popCtr.popoverContentSize = CGSizeMake(330, h);
    [_popCtr presentPopoverFromRect:bt.bounds inView:bt permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}
- (IBAction)descEdit:(id)sender {
    [self dismissEditAddress];
    
    self.popView = [[PopView alloc] initWithFrame:self.splitViewController.view.bounds];
    self.popView.delegate = self;
    self.popView.list = _list;
    [self.popView setViewType:PopViewTypeDescription];
    
    [self.splitViewController.view addSubview:self.popView];
}
- (IBAction)areaEdit:(id)sender {
    [self dismissEditAddress];
    
    self.popView = [[PopView alloc] initWithFrame:self.splitViewController.view.bounds];
    self.popView.delegate = self;
    self.popView.list = _list;
    [self.popView setViewType:PopViewTypeAreaEdit];
    
    [self.splitViewController.view addSubview:self.popView];
}
- (IBAction)openHome:(id)sender {
    [self dismissEditAddress];
    OpenHomeViewController * vc = [[OpenHomeViewController alloc] initWithNibName:@"OpenHomeViewController" bundle:nil];
    vc.list  = _list;
    vc.delegate = self;
    [self.splitViewController addChildViewController:vc];
    [self.splitViewController.view addSubview:vc.view];
}

#pragma mark -- PoperContentViewControllerDelegate

-(void)eOHTapped:(NSNotification *) notification {
     NSDictionary *dict = [notification userInfo];
    NSString *text = [dict valueForKey:@"text"];
    int viewType = [[dict objectForKey:@"viewType"] intValue];
    
    if(viewType == 0) {
        _list.needSync = @"1";
        self.list.listCate = text;
        //        [listCateBt judgeW:text];
        [listCateBt setTitle:text forState:0];
    } else if (viewType == 1) {
        _list.needSync = @"1";
        NSDictionary * dic = @{ForSale:@"1",InContract:@"2",Sold:@"3",ShortSale:@"4",Rental:@"5",Rented:@"6",Auction:@"7"};
        self.list.sellState = dic[text];
        //        [sellStateBt judgeW:text];
        [sellStateBt setTitle:text forState:0];
        if ([text isEqualToString:Sold]){
            openHomeBt.hidden = YES;
        }else{
            openHomeBt.hidden = NO;
        }
        NSArray * array = self.splitViewController.viewControllers;
        UINavigationController * nav = array[0];
        [[CoredataManager manager] saveContext];
        MasterViewController * vc = (MasterViewController *)nav.topViewController;
        [vc updataTbListItem];
    }
}

-(void)tbItemChoosed:(NSString *)text{
    if (self.popContentVC.tbType == TBtypeListType) {
        _list.needSync = @"1";
        self.list.listCate = text;
//        [listCateBt judgeW:text];
        [listCateBt setTitle:text forState:0];
    }else if (self.popContentVC.tbType == TBtypeAuction){
        _list.needSync = @"1";
        NSDictionary * dic = @{ForSale:@"1",InContract:@"2",Sold:@"3",ShortSale:@"4",Rental:@"5",Rented:@"6",Auction:@"7"};
        self.list.sellState = dic[text];
//        [sellStateBt judgeW:text];
        [sellStateBt setTitle:text forState:0];
        if ([text isEqualToString:Sold]){
            openHomeBt.hidden = YES;
        }else{
            openHomeBt.hidden = NO;
        }
        NSArray * array = self.splitViewController.viewControllers;
        UINavigationController * nav = array[0];
        [[CoredataManager manager] saveContext];
        MasterViewController * vc = (MasterViewController *)nav.topViewController;
        [vc updataTbListItem];
    }else if (self.popContentVC.tbType == TBtypeListShare){
        if ([text isEqualToString:@"Twitter"]) {
            [self twitterShare];
        }else if ([text isEqualToString:@"Email"]){
            [self emailShare];
        }
    } else if (self.popContentVC.tbType == TBtypeBed) {
        self.list.beds = text;
        [bedsBt setTitle:text forState: 0];
    } else if (self.popContentVC.tbType == TBtypeBaths) {
        self.list.baths = text;
        [bathsBt setTitle:text forState: 0];
    }
    
    [_popCtr dismissPopoverAnimated:YES];

    if (_actionBt == photoEditBt){
//        [self.actionBt setTitle:nil forState:0];
        if ([text isEqualToString:ActionTitlePhotoLibrary]) {
            [self performSelector:@selector(photoOrCamer:) withObject:@(UIImagePickerControllerSourceTypePhotoLibrary) afterDelay:0.0];
                [_popCtr dismissPopoverAnimated:YES];
        }else if([text isEqualToString:ActionTitleCamera]){
            [self performSelector:@selector(photoOrCamer:) withObject:@(UIImagePickerControllerSourceTypeCamera) afterDelay:0.0];
                [_popCtr dismissPopoverAnimated:YES];
        }else{
            //  edit photo
            [_popCtr dismissPopoverAnimated:NO];
            [self editPhotoAction];
            
//            self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.photoEditCtr];
//            _popCtr.popoverContentSize = CGSizeMake(260, self.photoEditCtr.h);
//            
//            [self presentViewController:self.photoEditCtr animated:NO completion:nil];
//            [_popCtr presentPopoverFromRect:photoEditBt.bounds inView:photoEditBt permittedArrowDirections:UIPopoverArrowDirectionDown animated:NO];
        }
    }
    [[CoredataManager manager] saveContext];
}


-(void)choosedBeds:(NSString *)bed baths:(NSString *)bath{

    NSString * beds = [bed isEqualToString:@"Studio"]?@"Studio":[NSString stringWithFormat:@"%@ Beds",bed];
    
    NSString * title =[NSString stringWithFormat:@"%@ / %@",beds,[NSString stringWithFormat:@"%@ Baths",bath]];
    
    self.list.beds = bed;
    self.list.baths = bath;
    [[CoredataManager manager] saveContext];
//    [fitmentBt judgeW:title];
    [fitmentBt setTitle:title forState:0];
}

- (void)doneButtonPressed {
    [_popCtr dismissPopoverAnimated:YES];
    [self popoverControllerDidDismissPopover:_popCtr];
}

#pragma mark -- PopViewDelegate


-(void)newAddress:(NSString *)address{
    
    _list.needSync = @"1";
    self.list.address  = address;
    [[CoredataManager manager] saveContext];
//    [addressBt judgeW:address];
    [addressBt setTitle:address forState:0];
}

-(void)newPrice:(NSString *)price{
    
    NSString * price_;
    if (price && ![price isEqualToString:@""]) {
        
        _list.needSync = @"1";
        self.list.price  = price;
        price_ = price;
        [[CoredataManager manager] saveContext];
    }else{
        self.list.price = nil;
        price_ = @"";
    }
//    [priceBt judgeW:price_];
    [priceBt setTitle:price_ forState:0];
}

-(void)newDesc:(NSString *)desc{

    NSString * desc_;
    if (desc && ![desc isEqualToString:@""]) {
        _list.needSync = @"1";
        self.list.desc  = desc;
        [[CoredataManager manager] saveContext];
        
        NSString * str = _list.desc;
        
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:17]};
        CGRect rect_titleLb = [str boundingRectWithSize:CGSizeMake(descLb.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attributes
                                                context:nil];
        if (rect_titleLb.size.height > 30) {
            CGRect fram = descLb.frame;
            fram.size.height = rect_titleLb.size.height;
            descLb.frame = fram;
            
//            CGRect fram_bt = descBt.frame;
//            fram_bt.size.height = rect_titleLb.size.height;
//            descBt.frame = fram_bt;
        }
        descLb.text = str;
        desc_ = desc;
        descTxt.text = desc;
    }else{
        self.list.desc = nil;
        if (descLb.frame.size.height > 30) {
            CGRect fram = descLb.frame;
            fram.size.height = 30;
            descLb.frame = fram;
            
//            CGRect fram_bt = descBt.frame;
//            fram_bt.size.height = 30;
//            descBt.frame = fram_bt;
        }
        descLb.text = ListDesctiptionPlaceHold;
        desc_ =  ListDesctiptionPlaceHold;
    }
//    [descBt judgeW:desc_];
    [descBt setTitle:@"KKK" forState:0];
//    contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, descBt.frame.size.height+descBt.frame.origin.y+10);
}

-(void)newSize:(NSString *)size{
    
    NSString * size_;
    if (size && ![size isEqualToString:@""]) {
        _list.needSync = @"1";
        self.list.size  = size;
        [[CoredataManager manager] saveContext];
        size_ = size;
    }else{
        self.list.size  = nil;
        size = @"000";
    }
    
//    [areaBt judgeW:title];
    [areaBt setTitle:_list.size forState:0];
    [areaLbl setText:_list.sizeUnit];
}

-(void)photoOrCamer:(NSNumber *)type{
 
    viewWillDisappearWithBack = NO;
    UIImagePickerControllerSourceType sourceType=[type integerValue];
    
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIAlertView * alert  = [[UIAlertView alloc] initWithTitle:CameraCanNotUse message:nil delegate:nil
                                                    cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    
    if (sourceType != UIImagePickerControllerSourceTypeCamera) {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        
        elcPicker.maximumImagesCount = 100; //Set the maximum number of images to select to 100
        elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
        elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
        elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
        elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
        
        elcPicker.imagePickerDelegate = self;
        elcPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:elcPicker animated:YES completion:nil];

    }else{
        AppDelegate * appDelegate = [UIApplication sharedApplication].delegate;
        appDelegate.isShowPortrait = YES;
        UIImagePickerController * picker = [[UIImagePickerController alloc]init];
        picker.delegate = self;
        picker.allowsEditing=NO;
        picker.sourceType=sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


#pragma mark -- UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    AppDelegate * appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isShowPortrait = NO;
    
    UIImage * image= [info objectForKey:UIImagePickerControllerOriginalImage];
    
    Picture * pic = [[DataManage instance] creatNewPicObj:_list image:image];
    if (!picManage) {
        picManage = [[PictureManage alloc] init];
        picManage.delegate = self;
    }
    [picManage newPictureToSync:pic];
    _list.needSync = @"1";
    
    if ([_list.picture allObjects].count != 0) {
        [self addListImageToSV];
        [addPhotoBt removeFromSuperview];
    }

    [self dismissViewControllerAnimated:YES completion:nil];
    viewWillDisappearWithBack = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    AppDelegate * appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isShowPortrait = NO;
    viewWillDisappearWithBack = YES;
}

#pragma mark -- UIActionSheetDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        [self performSelector:@selector(photoOrCamer:) withObject:@(UIImagePickerControllerSourceTypeCamera) afterDelay:0.0];
    }else if(buttonIndex == 1){
        
        [self performSelector:@selector(photoOrCamer:) withObject:@(UIImagePickerControllerSourceTypePhotoLibrary) afterDelay:0.0];
    }
}

#pragma mark -- PhotoEditeCtr

-(void)newOrderHad{

    if (_list.photoName.length != 0) {
        [self addListImageToSV];
    }else{
        [subContentV addSubview:addPhotoBt];
    }
}

-(void)photoToAdd{
    
    [self.photoEditCtr dismissViewControllerAnimated:YES completion:nil];
    
    self.actionBt = photoEditBt;
    self.popContentVC = [[PoperContentViewController alloc] init];
    self.popContentVC.delegate = self;
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:self.popContentVC];
    
    float h =  [_popContentVC tbType:TBtypePhotoChoose];
    _popCtr.popoverContentSize = CGSizeMake(260, h);
    [_popCtr presentPopoverFromRect:photoEditBt.bounds inView:photoEditBt permittedArrowDirections:UIPopoverArrowDirectionDown animated:NO];
}

-(void)photoEditDone{
    
    [self addListImageToSV];
    _list.needSync = @"1";
    
    if ([_list.picture allObjects].count == 0) {
        [subContentV addSubview:addPhotoBt];
    }
//    [self.popCtr dismissPopoverAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{

    if (_actionBt == scheduleBt){
        if (!_list.schedule) {
            schdeLb.hidden = YES;
//            [scheduleBt setTitleColor:[UIColor clearColor] forState:0];
        }else{
            NSString * str = [dateFormat stringFromDate:_list.schedule];
            schdeLb.hidden = NO;
            schdeLb.text = str;
        }
    }
}


#pragma mark -- MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- AMPFBUtilityDelegate

//- (void) finishPicturePost: (PostResult) result withInfo: (NSString *) info{
//}

#pragma mark -- ELCImagePickerControllerDelegate
- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (!picManage) {
        picManage = [[PictureManage alloc] init];
        picManage.delegate = self;
    }
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){

                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
              
                Picture * pic = [[DataManage instance] creatNewPicObj:_list image:image];
                [picManage newPictureToSync:pic];

                [picker dismissViewControllerAnimated:YES completion:nil];
                _list.needSync = @"1";
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else {
            NSLog(@"Uknown asset type");
        }
    }
    if ([_list.picture allObjects].count != 0) {
        [self addListImageToSV];
        [addPhotoBt removeFromSuperview];
    }
    viewWillDisappearWithBack = YES;
}
- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    viewWillDisappearWithBack = YES;
}
#pragma mark -- OpenHomeViewControllerDelegate
-(void)creatLeadSuccess{

    if ([_list.lead allObjects].count !=0) {
        if (leadBt.hidden) {
            leadBt.hidden = NO;
        }
        [leadBt setTitle:[NSString stringWithFormat:@"Listing Lead (%lu)",(unsigned long)[_list.lead allObjects].count] forState:0];
    }
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}

- (IBAction)onButtonAdd:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddNewSource" object:nil];
}

- (IBAction)onButtonFilter:(id)sender {
    ListFilterVC *vc = [[ListFilterVC alloc] init];
    vc.selectedIndex = self.selectedFilterButton;
//    self.popContentVC.delegate = self;
//    self.popContentVC.se = self.selectedFilterButton;
//    self.popContentVC.selectedIndex = self.selectedFilterButton;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    self.popCtr.backgroundColor = RGBCOLOR(240, 240, 240);
    
    _popCtr.popoverContentSize = CGSizeMake(230, 335);
    
    [_popCtr presentPopoverFromRect:btnFilter.bounds inView:btnFilter permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    return;
}

@end
