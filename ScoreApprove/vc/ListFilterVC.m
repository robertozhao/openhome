//
//  LeadFilterViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "ListFilterVC.h"

@interface ListFilterVC (){
    
}

@property (nonatomic, strong) NSDictionary *filterButtons;

@end

@implementation ListFilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.filterButtons = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};
    
    [self initContentView];
}

- (void)initContentView {
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 5, 70, 35)];
    headerLabel.text = @"FILTERS";
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    headerLabel.textColor = RGBCOLOR(146, 146, 146);
    [self.view addSubview:headerLabel];
    
    UIView *headerUnderline = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 111, 3)];
    headerUnderline.backgroundColor = RGBCOLOR(246, 146, 30);
    [self.view addSubview:headerUnderline];
    
    for (int i = 0; i <= [[self.filterButtons allKeys] count]; i++) {
        UIButton *itemButton = [[UIButton alloc] initWithFrame:CGRectMake(50, 50 + i * 40, 180, 40)];
        [itemButton setImage:[UIImage imageNamed:@"icon_radio"] forState:UIControlStateNormal];
        [itemButton setImage:[UIImage imageNamed:@"icon_radio_selected"] forState:UIControlStateSelected];
        NSString *buttonTitle;
        if (i == 0) {
            buttonTitle = @"   All";
        } else {
            buttonTitle = [NSString stringWithFormat:@"   %@", [self.filterButtons objectForKey:[NSString stringWithFormat:@"%d", i]]];
        }
        [itemButton setTitle:buttonTitle forState:UIControlStateNormal];
        [itemButton setTitleColor:RGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
        [itemButton setTitleColor:RGBCOLOR(60, 141, 254) forState:UIControlStateSelected];
        [itemButton setTag:(1010 + i)];
        itemButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.view addSubview:itemButton];
        
        [itemButton addTarget:self action:@selector(onItemButton:) forControlEvents:UIControlEventTouchUpInside];
//        if (self.selectedIndex == i) {
        if ([[DataManage instance] listFilterButtonIndex] == i) {
            [itemButton setSelected:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)onItemButton:(UIButton *) sender{
    UIButton *buttonWithTag = [self.view viewWithTag:(1010 + self.selectedIndex)];
    [buttonWithTag setSelected:NO];
    [sender setSelected:YES];
    int tag = (int)sender.tag - 1010;
    self.selectedIndex = tag;
    NSDictionary *userInfo = @{ @"selectedButtonIndex":[NSNumber numberWithInt:tag] };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ListFilterButtonClicked" object:nil userInfo: userInfo];
}

@end
