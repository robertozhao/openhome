//
//  LeadFilterViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface eOHSelectionVC : BaseViewController

@property (nonatomic, strong) NSString *selectedText;
@property (nonatomic, assign) int selectedIndex;
@property (nonatomic, assign) int viewType;         // 0 : auction, 1 : category

@end
