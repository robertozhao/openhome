//
//  ScheduleViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "ScheduleViewController.h"

@interface ScheduleViewController (){
    UILabel * start_dateLb;
//    UILabel * end_dateLb;
//    UILabel * start_timeLb;
//    UILabel * end_timeLb;
    
    UIDatePicker * myPicker;
    UISwitch * sh;
    
    UIView * view_start;
    
    NSDateFormatter * dateFormat;
    
    UIButton * btnDone;
}

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeStyle:NSDateFormatterLongStyle];
    [dateFormat setDateFormat:@"EEE MMM dd YYYY   hh:mm aaa"];
    NSLocale * twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormat.locale = twelveHourLocale;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initSubView];
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
}


-(void)initSubView{

    UIView * contentV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 450)];
    contentV.layer.cornerRadius = 3;
    contentV.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentV];
    
    UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(30, 25, 160, 20)];
    lb.text = ScheduleScoreApproce;
    lb.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    lb.backgroundColor = [UIColor clearColor];
    [contentV addSubview:lb];
    
    sh = [[UISwitch alloc] initWithFrame:CGRectMake(contentV.frame.size.width-65, 20, 20, 15)];
    [sh addTarget:self action:@selector(openShift) forControlEvents:UIControlEventValueChanged];
    [contentV addSubview:sh];
    
    sh.on = _list.schedule?YES:NO;

     view_start = [[UIView alloc] initWithFrame:CGRectMake(0, 68, contentV.frame.size.width, 32)];
    view_start.backgroundColor = RGBCOLOR(79 ,152, 254);
    [contentV addSubview:view_start];

    start_dateLb = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, contentV.frame.size.width-10, view_start.frame.size.height)];
    start_dateLb.textAlignment = 1;
    start_dateLb.textColor = [UIColor whiteColor];
    start_dateLb.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    start_dateLb.backgroundColor = [UIColor clearColor];
    [view_start addSubview:start_dateLb];
    
    float y= view_start.frame.size.height+view_start.frame.origin.y + 10;
    myPicker= [[UIDatePicker alloc] initWithFrame:CGRectMake(0, y, 280, 160)];
    myPicker.backgroundColor = [UIColor whiteColor];
    [myPicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    myPicker.minimumDate = [NSDate date];
//    myPicker.timeZone = [NSTimeZone defaultTimeZone];
    myPicker.datePickerMode = UIDatePickerModeDateAndTime;
    [myPicker addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    if (_list.schedule) {
        myPicker.date = _list.schedule;
        NSString * str = [dateFormat stringFromDate:myPicker.date];
        start_dateLb.text = str;
    }
    [contentV addSubview:myPicker];
    
    btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnDone addTarget:self
               action:@selector(dismissPopover)
     forControlEvents:UIControlEventTouchUpInside];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:RGBCOLOR(5, 126, 241) forState:UIControlStateNormal];
    btnDone.frame = CGRectMake(0, myPicker.frame.origin.y + myPicker.frame.size.height + 5, contentV.frame.size.width, 36);
    [contentV addSubview:btnDone];
}
-(void)openShift{

    if (sh.on) {
        [[DataManage instance] addLocalNofi:myPicker.date withList:_list];
        self.list.schedule = myPicker.date;
    }else{
        [[DataManage instance] removeLocalNofi:_list];
        self.list.schedule = nil;
    }
}

-(void)valueChange:(UIDatePicker *)picker{
    
    NSDate *date = [picker date];
 
    NSString * str = [dateFormat stringFromDate:date];
    start_dateLb.text = str;

    if (sh.on &&  [picker.date timeIntervalSinceNow] > 0) {
        [[DataManage instance] addLocalNofi:myPicker.date withList:_list];
        _list.schedule = date;
    }
}

-(void)dismissPopover {
    [self.delegate doneButtonPressed];
}

@end
