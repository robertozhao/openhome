//
//  QuestionCheckViewController.h
//  ScoreApprove
//
//  Created by Yang on 15/4/24.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Question.h"



@protocol QuestionCheckViewControllerDelegate <NSObject>

-(void)questionEditDone;

@end

@interface QuestionCheckViewController : BaseViewController

@property (nonatomic , weak) id<QuestionCheckViewControllerDelegate>delegate;


@property (nonatomic , strong) NSString * sortNum;
@property (nonatomic , strong) Question * q;
@property (nonatomic) int questionType;  // 0: add  1: edit
@end
