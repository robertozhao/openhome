//
//  ContactUsViewController.m
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController (){

    IBOutlet UILabel *phoneLb;
    
    IBOutlet UILabel *explanWhyToContactlB;
    IBOutlet UILabel *callUsTitleLb;
    IBOutlet UILabel *timeLb;
    IBOutlet UILabel *mailUsTitle;
    IBOutlet UILabel *mailLb;
}

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.preferredContentSize = CGSizeMake(444, 468);
    self.title = ContactUsTitle;
    
    explanWhyToContactlB.text = ExplanWhyToContactUs;
    callUsTitleLb.text = CallUs;
    timeLb.text = ContactUsTime;
    mailUsTitle.text = MailUs;
    mailLb.text = EmailUsMail;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = nil;
    self.view.layer.cornerRadius = 0;
}

- (IBAction)done:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)callPhone:(id)sender {
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",ContactUsPhone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

- (IBAction)sendMail:(id)sender {
    // Email Subject
    NSString *emailTitle = @"eOpenHome Customer Support";
    // Email Content
    NSString *messageBody = @"";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:EmailUsMail];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
