//
//  LeadFilterViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ListFilterVC : BaseViewController

@property (nonatomic, assign) int selectedIndex;

@end
