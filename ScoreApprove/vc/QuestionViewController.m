//
//  QuestionViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/18.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "QuestionViewController.h"
#import "QuestionCheckViewController.h"
#import "Question.h"

@interface QuestionViewController ()<UITableViewDataSource,UITableViewDelegate,QuestionCheckViewControllerDelegate>{

    UITableView * myTableView;


}

@property (nonatomic , strong)     NSMutableArray * activeArray;
@property (nonatomic , strong)    NSMutableArray * inactiveArray;

@end

@implementation QuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title  = ManageQuestion;
    
    
    [self getAllQuestion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem * item_l = [[UIBarButtonItem alloc] initWithTitle:ButtonTitleDone style:UIBarButtonItemStylePlain target:self action:@selector(done)];
    self.navigationItem.leftBarButtonItem = item_l;
    
    UIBarButtonItem * item_r = [[UIBarButtonItem alloc] initWithTitle:Add style:UIBarButtonItemStylePlain target:self action:@selector(addQ)];
    self.navigationItem.rightBarButtonItem = item_r;

    if(!myTableView){
        myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        myTableView.dataSource = self;
        myTableView.delegate = self;
        myTableView.editing = YES;
        myTableView.allowsSelectionDuringEditing = YES;
        [self.view addSubview:myTableView];
    }
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
}

-(void)done{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)getAllQuestion{
    
    NSMutableArray * temp = [NSMutableArray array];
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:@"user_id==0" ];
    [temp addObjectsFromArray:array];
    
    NSArray * array_ = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"user_id=='%@'",[GlobalInstance instance].user_id]];
    [temp addObjectsFromArray:array_];
    
    NSMutableArray * active_temp = [NSMutableArray array];
    self.inactiveArray = [NSMutableArray array];
    
    [temp enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Question *q = (Question *)obj;
        if ([q.is_active isEqualToString:@"1"]) {
            [active_temp addObject:q];
        }else{
            [_inactiveArray addObject:q];
        }
    }];
    
    NSArray * a = [active_temp sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Question * q = (Question *)obj1;
        Question * q_ = (Question *)obj2;
        if ([q.sortNum intValue] > [q_.sortNum intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    self.activeArray = [NSMutableArray arrayWithArray:a];
    
}


-(void)addQ{

    QuestionCheckViewController * vc = [[QuestionCheckViewController alloc] initWithNibName:@"QuestionCheckViewController" bundle:nil];
    vc.delegate = self;
    vc.questionType = 0;
    vc.sortNum = [NSString stringWithFormat:@"%u",(unsigned int)(_activeArray.count + _inactiveArray.count)];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return _activeArray.count;
    }else{
        return _inactiveArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    Question * q;
    if (indexPath.section == 0) {
        q = _activeArray[indexPath.row];
    }else{
        q = _inactiveArray[indexPath.row];
    }

    NSString * cellIdentify = [NSString stringWithFormat:@"cellIndefire%@",q.question_id];
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
         cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
    }
    UIView * view = [cell viewWithTag:500];
    
    int qID = [q.question_id intValue];
    if (qID == 6 || qID == 7 || qID == 8) {
        if (!view) {
            UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_lock.png"]];
            imgV.frame = CGRectMake(tableView.frame.size.width-40, 10, 25, 25);
            imgV.tag = 500;
            [cell addSubview:imgV];
        }
    }else{
        if (view) {
            [view removeFromSuperview];
        }
    }
    cell.textLabel.text = q.question;
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(UIView * )tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    lb.backgroundColor = RGBCOLOR(50, 50, 50);
    lb.textColor = [UIColor whiteColor];
    if (section == 0) {
        lb.text =[NSString stringWithFormat:@"           %@",Active];
    }else{
        lb.text =[NSString stringWithFormat:@"           %@",InActive];
    }
    return lb;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 44;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Question * q ;
    if (indexPath.section ==0) {
        q = _activeArray[indexPath.row];
    }else{
        q = _inactiveArray[indexPath.row];
    }
    NSInteger n = [q.question_id integerValue];
    if (n==6 || n==7 || n==8) {
        return NO;
    }else{
        return YES;
    }
    

    return YES;
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{

    DFLog(@"%@",indexPath);
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
  
    Question * q ;
    if (sourceIndexPath.section ==0) {
        q = _activeArray[sourceIndexPath.row];
    }else{
        q = _inactiveArray[sourceIndexPath.row];
    }
    
    if (destinationIndexPath.section ==0) {
        Question * q_;
        if (_activeArray.count-1 < destinationIndexPath.row) {
            q_ = nil;
        }else{
            q_= _activeArray[destinationIndexPath.row];
        }

        if (sourceIndexPath.section ==0) {
            [_activeArray removeObject:q];
        }else{
            [_inactiveArray removeObject:q];
        }
        q.is_active = @"1";
        [_activeArray insertObject:q atIndex:destinationIndexPath.row];
        [_activeArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Question * q = (Question *)obj;
            q.sortNum = [NSString stringWithFormat:@"%ld",(unsigned long)idx];
        }];

    }else{
        if (sourceIndexPath.section ==0) {
            [_activeArray removeObject:q];
        }else{
            [_inactiveArray removeObject:q];
        }
        q.is_active = @"0";
        [_inactiveArray insertObject:q atIndex:destinationIndexPath.row];
    }
    [[CoredataManager manager] saveContext];
    [myTableView reloadData];
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

//    return     UITableViewCellEditingStyleNone;
    Question * q ;
    if (indexPath.section ==0) {
        q = _activeArray[indexPath.row];
    }else{
        q = _inactiveArray[indexPath.row];
    }
    if ([q.is_required integerValue] == 1) {
            return UITableViewCellEditingStyleNone;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Question * q_;
    
    if (indexPath.section == 0) {
        q_ = _activeArray[indexPath.row];
    }else{
        q_ = _inactiveArray[indexPath.row];
    }

    int qID = [q_.question_id intValue];
    if (qID == 6 || qID == 7 || qID == 8) {
        return;
    }
    
    QuestionCheckViewController * vc =  [[QuestionCheckViewController alloc] initWithNibName:@"QuestionCheckViewController" bundle:nil];
    vc.q = q_;
    vc.questionType = 1;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- QuestionCheckViewControllerDelegate

-(void)questionEditDone{
    
    [[CoredataManager manager] saveContext];
    [self getAllQuestion];
    [myTableView reloadData];
}

@end
