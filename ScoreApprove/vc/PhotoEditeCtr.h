//
//  PhotoEditeCtr.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/16.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macro.h"
#import "List.h"
#import "DataManage.h"
#import "BaseViewController.h"

@protocol PhotoEditeCtrDelegate <NSObject>

-(void)newOrderHad;
-(void)photoToAdd;
-(void)photoEditDone;

@end

@interface PhotoEditeCtr : BaseViewController<UITableViewDelegate,UITableViewDataSource>{

    NSMutableArray * source;
    UITableView * myTableView;
}

@property (nonatomic , weak) id<PhotoEditeCtrDelegate>delegate;
@property (nonatomic , strong) List * list;
@property (nonatomic)  float h;

@end
