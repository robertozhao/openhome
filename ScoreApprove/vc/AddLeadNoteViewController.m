//
//  AddLeadNoteViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "AddLeadNoteViewController.h"

@interface AddLeadNoteViewController ()
{


    UITextView * tv;
}

@end

@implementation AddLeadNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = Note;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    UIBarButtonItem * item_l = [[UIBarButtonItem alloc] initWithTitle:ButtonTitleCancel style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = item_l;
    
    UIBarButtonItem * item_r = [[UIBarButtonItem alloc] initWithTitle:ButtonTitleDone style:UIBarButtonItemStylePlain target:self action:@selector(done)];
    self.navigationItem.rightBarButtonItem = item_r;

    
    tv = [[UITextView alloc] initWithFrame:CGRectMake(15, 15, self.view.frame.size.width-30, self.view.frame.size.height-30)];
    tv.layer.borderColor = RGBCOLOR(234, 234, 234).CGColor;
    tv.layer.borderWidth = 1;
    tv.layer.cornerRadius = 9;
    [self.view addSubview:tv];
}


-(void)cancel{
    
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

-(void)done{
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
