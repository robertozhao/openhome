//
//  OpenHomeViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/9.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"
#import "BaseViewController.h"

@protocol OpenHomeViewControllerDelegate <NSObject>

-(void)creatLeadSuccess;

@end

@interface OpenHomeViewController : BaseViewController

@property (nonatomic, strong) List * list;

@property (nonatomic, weak) id<OpenHomeViewControllerDelegate>delegate;

@end
