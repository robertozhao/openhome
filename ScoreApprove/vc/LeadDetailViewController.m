//
//  LeadDetailViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LeadDetailViewController.h"
#import "List.h"
#import "LeadQuestion.h"
#import "AddLeadNoteViewController.h"
#import "AddNoteView.h"
#import "UnderLineButton.h"
#import "PopView.h"
#import "Lead_Note.h"


@interface LeadDetailViewController ()<AddNoteViewDelegate,PopViewDelegate, DataManageDelegate>{

    IBOutlet UIButton *emailBt;
    IBOutlet UIButton *phoneBt;
    AddNoteView * addNoteView;
    
    IBOutlet UIScrollView *contentSV;
  
    IBOutlet UILabel *nameLb;
    
    IBOutlet UILabel *firstNameLb;
    IBOutlet UILabel *lastNameLb;
    
    IBOutlet UILabel *addressLb;
    IBOutlet UILabel *zipCodeLb;
    
    IBOutlet UILabel *cityLb;
    IBOutlet UILabel *stateLb;
    IBOutlet UILabel *emailLb;
    
    IBOutlet UILabel *brithLb;

    
    
    IBOutlet UIView *questionV;
    
    IBOutlet UIView *questionTitleV;

    IBOutlet UIButton *addNoteBt;


    
    IBOutlet UIView *noteTitleView;
    
    NSString * notestr;
    
    BOOL isHadNewNote;
    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    
}

@property (nonatomic , strong) PopView * popView;

@end

@implementation LeadDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    isHadNewNote = NO;
    
    addNoteBt.layer.borderWidth = 1;
    addNoteBt.layer.borderColor = RGBCOLOR(230, 230, 230).CGColor;

    
    if (_lead.list) {
        UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithTitle:@"Listing" style:UIBarButtonItemStylePlain target:self action:@selector(toListDetail)];
        self.navigationItem.rightBarButtonItem = item;
    }
    
    self.title =[NSString stringWithFormat:@"%@ %@",_lead.first_name,_lead.last_name];
    
    [emailBt setTitle: _lead.email forState:0];
    
    NSString * phone = [[GlobalInstance instance] phoneNumWithFormat:_lead.phone];
    [phoneBt setTitle:phone forState:0];
    
    firstNameLb.text = _lead.first_name;
    lastNameLb.text = _lead.last_name;
    addressLb.text = _lead.address;
    zipCodeLb.text = _lead.zipCode;
    cityLb.text = _lead.city;
    stateLb.text = _lead.state;
    brithLb.text = _lead.brithday;
    emailLb.text = _lead.email;

    if (_lead.leadQ && [_lead.leadQ allObjects].count !=0) {
        [self initAnswerQ];
    }else{
        questionTitleV.hidden = YES;
    }
    
    NSArray * notes = [_lead.notes allObjects];
    if (notes && notes.count != 0 ) {
        noteTitleView.hidden = NO;
        [self initNote];
    }
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.leftBarButtonItem = self.navigationItem.backBarButtonItem;
}


-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    [[DataManage instance] syncNoteToSever:nil];
}

-(void)toListDetail{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationForList" object:_lead.list.id_];
}

-(void)initNote{
    NSArray * array = [[_lead.notes allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Lead_Note * note = (Lead_Note *)obj1;
        Lead_Note * note_ = (Lead_Note *)obj2;
        if ([note.creat floatValue] > [note_.creat floatValue]) {
            return NSOrderedAscending;
        }else{
            return NSOrderedDescending;
        }
    }];
    // reomve note view before refresh
    
    UIView * view = [self.view viewWithTag:990];
    if (view) {
        [view removeFromSuperview];
    }
    UIView * noteContentV = [[UIView alloc] initWithFrame:CGRectMake(27, noteTitleView.frame.origin.y + noteTitleView.frame.size.height+9, 574, 0)];
    noteContentV.tag = 990;
    [contentSV addSubview:noteContentV];
    __block float cur_y_left = 0;
    __block float cur_y_right = 0;
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Lead_Note * note = (Lead_Note *)obj;
        
//        float y = (idx%2==0?cur_y_left:cur_y_right);
//        float x = (idx%2==0?0:(questionV.frame.size.width/2.0+5));
        
//        UIView * itemV  = [[UIView alloc] initWithFrame:CGRectMake(x, y, questionV.frame.size.width/2.0-5, 0)];
        UIView * itemV  = [[UIView alloc] initWithFrame:CGRectMake(0, cur_y_left, questionV.frame.size.width, 0)];
        itemV.backgroundColor = RGBCOLOR(239, 239, 244);
        UILabel * flagLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 3, 0)];
        flagLb.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        flagLb.backgroundColor = RGBCOLOR([self random], [self random], [self random]);
        flagLb.backgroundColor = RGBCOLOR(246, 146, 30);
        [itemV addSubview:flagLb];
        UILabel * timeLb = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 200, 20)];
        
        timeLb.text = [Utility timeStringFromFormat:@"EEE MMM d, YYYY hh:mm a" withTI:[note.creat doubleValue]];
        timeLb.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
        timeLb.textColor = RGBCOLOR(102, 102, 102);
        [itemV addSubview:timeLb];
        UILabel * noteLb = [[UILabel alloc] initWithFrame:CGRectMake(15, 40, itemV.frame.size.width-10, 0)];
        noteLb.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
        noteLb.textColor = RGBCOLOR(102, 102, 102);
        noteLb.numberOfLines = 0;
        noteLb.backgroundColor= [UIColor clearColor];
        [itemV addSubview:noteLb];
        
        NSDictionary *attributes = @{NSFontAttributeName:noteLb.font};
        CGRect rect_titleLb = [note.note boundingRectWithSize:CGSizeMake(noteLb.frame.size.width, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:attributes
                                                     context:nil];
        
        CGRect fram = noteLb.frame;
        fram.size.height = rect_titleLb.size.height;
        noteLb.frame = fram;
       
        noteLb.text = note.note;

        CGRect fram_itemV = itemV.frame;
        fram_itemV.size.height = noteLb.frame.size.height+noteLb.frame.origin.y+15;
        itemV.frame = fram_itemV;
        
//        if (idx %2 == 0) {
            cur_y_left += (itemV.frame.size.height+12);
//        }else{
//            cur_y_right += (itemV.frame.size.height+12);
//        }
        [noteContentV addSubview:itemV];
    }];
    
    
    CGRect fram = noteContentV.frame;
    fram.size.height = cur_y_right>cur_y_left?cur_y_right:cur_y_left;
    noteContentV.frame = fram;
    
    contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, noteContentV.frame.size.height+noteContentV.frame.origin.y+10);
    
}

- (IBAction)sendEmail:(id)sender {
    [self toEmail:_lead.email];
}

-(IBAction)sendCall:(id)sender{
    [self toPhone:_lead.phone];
}


-(int)random{

    int x = arc4random() % 255;
    return x;
}

-(void)initAnswerQ{

   __block float cur_y_left = 0;
   __block float cur_y_right = 0;
    
  NSArray *  array = [[self.lead.leadQ allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        LeadQuestion * lq = (LeadQuestion *)obj1;
        LeadQuestion * lq_ = (LeadQuestion *)obj2;
        if ([lq.sortNum intValue]> [lq_.sortNum intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        LeadQuestion * lq = (LeadQuestion *)obj;
        
        if ([lq.id_ intValue]<0) {
            return ;
        }
        float y = (idx%2==0?cur_y_left:cur_y_right);
        float x = (idx%2==0?0:(questionV.frame.size.width/2.0+5));
        
        UIView * itemV  = [[UIView alloc] initWithFrame:CGRectMake(x, y, questionV.frame.size.width/2.0-5, 0)];
        UILabel * flagLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1.5, 0)];
        flagLb.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        flagLb.backgroundColor = RGBCOLOR([self random], [self random], [self random]);
        [itemV addSubview:flagLb];
        
        itemV.backgroundColor = RGBCOLOR(244, 251, 251);
        [questionV addSubview:itemV];
        
        
        UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake(9, 9, 20, 20)];
        imgV.image = [UIImage imageNamed:@"wenhao.png"];
        [itemV addSubview:imgV];
        
        float title_w = itemV.frame.size.width - (imgV.frame.size.width + imgV.frame.origin.x) - 10;
        float title_x = imgV.frame.size.width + imgV.frame.origin.x+5;
        
        UILabel * qTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(title_x, 9, title_w, 0)];
        qTitleLb.font = [UIFont boldSystemFontOfSize:15];
        qTitleLb.numberOfLines = 0;
        qTitleLb.backgroundColor = [UIColor clearColor];
        
        qTitleLb.text = lq.title;
        [itemV addSubview:qTitleLb];
        
        NSDictionary *attributes = @{NSFontAttributeName:qTitleLb.font};
        CGRect rect_titleLb = [lq.title boundingRectWithSize:CGSizeMake(qTitleLb.frame.size.width, MAXFLOAT)
                                                options:NSStringDrawingUsesLineFragmentOrigin
                                             attributes:attributes
                                                context:nil];
        
        if (rect_titleLb.size.height > 0) {
            CGRect fram = qTitleLb.frame;
            fram.size.height = rect_titleLb.size.height;
            qTitleLb.frame = fram;
        }
        
        UILabel * answerLb = [[UILabel alloc] initWithFrame:CGRectMake(15, qTitleLb.frame.size.height + qTitleLb.frame.origin.y+10, itemV.frame.size.width-10, 0)];
        answerLb.font = [UIFont systemFontOfSize:15];
        answerLb.numberOfLines = 0;
        answerLb.backgroundColor= [UIColor clearColor];
        [itemV addSubview:answerLb];
        
        NSDictionary *attributes_answer = @{NSFontAttributeName:answerLb.font};
        CGRect rect_answerLb = [lq.answer boundingRectWithSize:CGSizeMake(answerLb.frame.size.width, MAXFLOAT)
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:attributes_answer
                                                     context:nil];
        answerLb.text = lq.answer;
        if (rect_answerLb.size.height > 0) {
            CGRect fram_answer = answerLb.frame;
            fram_answer.size.height = rect_answerLb.size.height;
            answerLb.frame = fram_answer;
        }

        CGRect fram_itemV = itemV.frame;
        fram_itemV.size.height = answerLb.frame.size.height+answerLb.frame.origin.y+10;
        itemV.frame = fram_itemV;
        
        if (idx %2 == 0) {
            cur_y_left += (itemV.frame.size.height+12);
        }else{
            cur_y_right += (itemV.frame.size.height+12);
        }
    }];
    
    CGRect fram = questionV.frame;
    fram.size.height = cur_y_right>cur_y_left?cur_y_right:cur_y_left;
    questionV.frame = fram;
    
    
    CGRect fram_note =  noteTitleView.frame;
    fram_note.origin.y = questionV.frame.size.height + questionV.frame.origin.y+10;
    noteTitleView.frame = fram_note;
    
    contentSV.contentSize = CGSizeMake(contentSV.frame.size.width, noteTitleView.frame.size.height + noteTitleView.frame.origin.y+10);
}

- (IBAction)addNewNote:(id)sender {

    if (!addNoteView) {
        addNoteView = [[AddNoteView alloc] init];
        addNoteView.delegate = self;
        addNoteView.lead = _lead;
        addNoteView.viewType = 0;
    }
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:addNoteView];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.navigationBarHidden = YES;
    
    [self presentViewController:nav animated:YES completion:nil];
    dispatch_async(dispatch_get_main_queue(), ^{});

//    [self.view addSubview:addNoteView];
}

#pragma mark -- AddNoteViewDelegate


-(void)addNoteOK{

    isHadNewNote = YES;
// refresh note appear
    [self initNote];
  
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}

@end
