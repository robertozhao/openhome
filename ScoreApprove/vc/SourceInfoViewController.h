//
//  SourceInfoViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailBaseViewController.h"
#import "UnderLineButton.h"

@interface SourceInfoViewController : DetailBaseViewController

@property (nonatomic, strong) List * list;

@end
