//
//  BaseViewController.h
//  vernon_admin
//
//  Created by Score Approve on 6/30/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Utility.h"
#import "MBProgressHUD.h"
#import "Macro.h"

#import "DataManage.h"
#import "List.h"
#import "CoredataManager.h"
#import "GlobalInstance.h"
#import "TextResource.h"

NS_ENUM(NSUInteger, SubViewType){
    
    SourceShowAll=1000,
    SourceShowForSale=1001,
    SourceShowInContract,
    SourceShowSold,
    SourceShowAddNewlist,
    SourceShowShortSale,
    SourceShowRental,
    SourceShowRented,
    SourceShowAuction,
    SourceInfo,
    EmailSignature,
    QuestionManager,
    AccountManage,
    LeadShowTypeAll,
};


@interface BaseViewController : UIViewController <MBProgressHUDDelegate>{
    
    BOOL keyboardShown;
    CGFloat keyboardHeight;
    CGFloat animationDuration;
    
    CGRect  contentOriginFram;
    
    //pop up
    MBProgressHUD *loadingView;
    UIAlertView *loadingPopUpView;
    BOOL loadingViewShowing;
    UIView *loadingParentView;
    
    BOOL isNeedBack;
}


- (void) enableKeyboarderListener;
- (void) disableKeyboarderListener;

- (void) keyboardWillHide: (NSNotification *) notify;
- (void) keyboardWillShow: (NSNotification *) notify;
//pop up
-(void)simplePopUpWithTitle : (NSString *)title Msg : (NSString *)msg;
-(void)simplePopUpWithTitle : (NSString *)title Msg : (NSString *)msg withTag: (NSInteger) tag cancelTitle: (NSString *) cancelText confirmTitle: (NSString *) confirmText;

-(void)showLoadingViewWithText : (NSString *)text;
-(void)showLoadingViewWithText : (NSString *)text withDetailText : (NSString *)sText;
- (void) showLoadingViewWithText: (NSString *) text withDetailText: (NSString *) sText autoHide: (NSTimeInterval) interval;
- (void) showCheckMarkPopView: (NSString *) text hideAfterDelay: (NSTimeInterval) interval;
-(void)hideLoadingView;

- (void)showLoadingPopUp: (NSString *) text;
-(void)hideLoadingPopUp;

-(void)alertShow:(NSString *)alert;

-(void)back;

-(void)toEmail:(NSString *)email;
-(void)toPhone:(NSString *)phone;
@end
