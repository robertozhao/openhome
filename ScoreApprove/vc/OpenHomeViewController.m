//
//  OpenHomeViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/9.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "OpenHomeViewController.h"
#import "HomeNoteView.h"
#import "SuccessVisitorView.h"

#import "Lead.h"
#import "AddNoteView.h"
#import "QuestionToolTipViewController.h"
#import "QuestionNaireViewController.h"
#import "Picture.h"

@interface OpenHomeViewController ()<HomeNoteViewDelegate,AddNoteViewDelegate,QuestionNaireViewControllerDelegate>{

    AddNoteView * addNoteView;
    
    IBOutlet UIView *addressContentV;
    
    IBOutlet UIImageView *imgFir;
    IBOutlet UIImageView *imgSec;
    
    UIControl * bkControl;
    
    
    IBOutlet UIImageView *gradient_layer;
    IBOutlet UILabel *addressLb;
    IBOutlet UILabel *saleStatesLb;
    IBOutlet UIButton *homeBt;
    IBOutlet UIButton *toBeginBt;
    IBOutlet UIButton *touchRange1;
    IBOutlet UIButton *touchRange2;
    IBOutlet UIButton *touchRange3;
    IBOutlet UIButton *touchRange4;
    
    HomeNoteView * noteView;
    
    NSArray * imgArray;
    int imgShowIndex;
    
    NSTimer * timer;
    
}

@property (nonatomic, strong) SuccessVisitorView * successView;
@property (nonatomic, strong) UIPopoverController * popCtr;

@property (nonatomic , strong)    Lead * curLead;

@end

@implementation OpenHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSpecilList:) name:@"NotificationForList" object:nil];
    
    NSDictionary *attributes = @{NSFontAttributeName:addressLb.font};
    CGRect rect_titleLb = [_list.address boundingRectWithSize:CGSizeMake(MAXFLOAT, addressLb.frame.size.height)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:attributes
                                            context:nil];
    NSDictionary * dic = @{@"1":ForSale,@"2":InContract,@"3":Sold,@"4":ShortSale,@"5":Rental,@"6":Rented,@"7":Auction};
    
    addressLb.text = _list.address;
    saleStatesLb.text = dic[_list.sellState];
    
    addressContentV.layer.cornerRadius = 8;
    toBeginBt.layer.cornerRadius = 8;
    
//    CGRect fram_addresslb = addressLb.frame;
//    if (rect_titleLb.size.width > 878) {
//        fram_addresslb.size.width = 878;
//    }else{
//        fram_addresslb.size.width = rect_titleLb.size.width;
//    }
//    addressLb.frame = fram_addresslb;
    
    CGRect fram_addlbContentC = addressContentV.frame;
    fram_addlbContentC.size.width = addressLb.frame.origin.x + addressLb.frame.size.width;
    addressContentV.frame = fram_addlbContentC;
    
    if ([_list.picture allObjects].count  != 0)  {
        imgArray = [_list.picture allObjects];
    }else{
//        imgFir.image = [UIImage imageNamed:@"home-bg.jpg"];
        imgSec.image = [UIImage imageNamed:@"home-bg.jpg"];
    }
    
    if (imgArray && imgArray.count != 0) {
        imgShowIndex = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(changeImgShow) userInfo:nil repeats:YES];
        [timer fire];
    }
    
    //add tap gesture recoginzer to touch range
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [touchRange addGestureRecognizer:singleFingerTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//- (void)handleSingleTap:(UITapGestureRecognizer*) recognizer {
//    
//}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

// user for list schedlu
-(void)showSpecilList:(NSNotification *)notify{
//    NSString * listId = (NSString *)[notify object];
//    if ([_list.id_ isEqualToString:listId]) {
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
//    }
}
- (IBAction)onBtnTapRange1:(id)sender {
    QuestionNaireViewController * vc = [[QuestionNaireViewController alloc] init];
    vc.delegate = self;
    vc.list = _list;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.preferredContentSize = CGSizeMake(445, 405);
    [self presentViewController:nav animated:YES completion:nil];
}
- (IBAction)onBtnTapRange2:(id)sender {
    QuestionNaireViewController * vc = [[QuestionNaireViewController alloc] init];
    vc.delegate = self;
    vc.list = _list;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.preferredContentSize = CGSizeMake(445, 405);
    [self presentViewController:nav animated:YES completion:nil];
    
}
- (IBAction)onBtnTapRange3:(id)sender {
    QuestionNaireViewController * vc = [[QuestionNaireViewController alloc] init];
    vc.delegate = self;
    vc.list = _list;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.preferredContentSize = CGSizeMake(445, 405);
    [self presentViewController:nav animated:YES completion:nil];
    
}
- (IBAction)onBtnTapRange4:(id)sender {
    QuestionNaireViewController * vc = [[QuestionNaireViewController alloc] init];
    vc.delegate = self;
    vc.list = _list;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.preferredContentSize = CGSizeMake(445, 405);
    [self presentViewController:nav animated:YES completion:nil];
    
}


- (IBAction)homeAction:(id)sender {
    
    if(_curLead){
        [self endOpenHome];
        return;
    }
    if (!bkControl) {
        bkControl = [[UIControl alloc] initWithFrame:self.view.bounds];
        [bkControl addTarget:self action:@selector(hideNoteView) forControlEvents:UIControlEventTouchUpInside];
        bkControl.backgroundColor = [UIColor blackColor];
        bkControl.alpha = 0.8;
        noteView = [[[NSBundle mainBundle] loadNibNamed:@"HomeNoteView" owner:self options:nil] objectAtIndex:0];
        noteView.delegate = self;
        
        CGRect fram = noteView.frame;
        fram.origin.x = (self.view.frame.size.width-noteView.frame.size.width)/2.0;
        fram.origin.y = 242;
        noteView.frame = fram;
    }
    [self.view addSubview:bkControl];
    [self.view addSubview:noteView];
}

- (IBAction)beginQ:(id)sender {
    QuestionNaireViewController * vc = [[QuestionNaireViewController alloc] init];
    vc.delegate = self;
    vc.list = _list;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
//    nav.preferredContentSize = CGRectMake(290, 168, 445, 512);
    nav.preferredContentSize = CGSizeMake(445, 405);
    [self presentViewController:nav animated:YES completion:nil];
    
//    [self hidePartForQ];
}

-(void)hideNoteView{
    [noteView removeFromSuperview];
    [bkControl removeFromSuperview];
}

-(void)hidePartForQ{
    gradient_layer.hidden = YES;
    addressContentV.hidden = YES;
    addressLb.hidden = YES;
    saleStatesLb.hidden = YES;
    homeBt.hidden = YES;
    toBeginBt.hidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(void)showPartAfterQ{
    gradient_layer.hidden = NO;
    addressContentV.hidden = NO;
    addressLb.hidden = NO;
    saleStatesLb.hidden = NO;
    homeBt.hidden = NO;
    toBeginBt.hidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)changeImgShow{

    if (imgShowIndex< imgArray.count) {
        Picture * pic = (Picture *)imgArray[imgShowIndex];
        
        UIImage * img = [UIImage imageWithContentsOfFile:pic.localPath];
        if (imgFir.alpha ==0) {
            imgFir.image = img;
            [UIView animateWithDuration:0.5 animations:^{
                imgFir.alpha = 1;
            } completion:^(BOOL finished) {
                
            }];
        }else{
            imgSec.image = img;
            [UIView animateWithDuration:0.5 animations:^{
                imgFir.alpha = 0;
            } completion:^(BOOL finished) {
                
            }];
        }
    }
    imgShowIndex ++;
    if (imgShowIndex == imgArray.count) {
        imgShowIndex = 0;
    }
}

-(void)showSuccessView{
    
    self.successView = [[SuccessVisitorView alloc] initWithFrame:CGRectMake(0, 0, 600, 500)];
    [self.successView show:self.view];
}

#pragma mark -- HomeNoteViewDelegate

-(void)endOpenHome{
    
    if (_curLead) {
        [[DataManage instance] syncLeadToService:@[_curLead]];
    }
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

-(void)addNote{
    
    [noteView removeFromSuperview];
    [bkControl removeFromSuperview];
    if (!addNoteView) {
        addNoteView = [[AddNoteView alloc] init];
        addNoteView.delegate = self;
        addNoteView.lead = _curLead;
        addNoteView.viewType = 1;
    }
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:addNoteView];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.navigationBarHidden = YES;
    
    [self.splitViewController presentViewController:nav animated:YES completion:nil];
//    [self.view addSubview:addNoteView];
}

#pragma mark -- QuestionNaireViewControllerDelegate

-(void)questionNaireFinish:(Lead *)lead{
    
    [self dismissViewControllerAnimated:YES completion:nil];

    if (lead) {
        
        self.curLead = lead;
       // [self showSuccessView];
        
        [self.delegate creatLeadSuccess];
        [self showCheckMarkPopView:LeadCreatSuccess hideAfterDelay:3];
    }
    
    [self showPartAfterQ];
}






@end
