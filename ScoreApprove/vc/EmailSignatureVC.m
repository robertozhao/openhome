//
//  LeadDetailViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/19.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "EmailSignatureVC.h"

@interface EmailSignatureVC ()<DataManageDelegate>{

    IBOutlet UIScrollView *contentSV;
    IBOutlet UITextView *emailTxt;
    
    IBOutlet UIView *syncView;
    
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
}
@end

@implementation EmailSignatureVC

- (void)viewDidLoad {
    [super viewDidLoad];

    emailTxt.layer.borderColor = RGBCOLOR(234, 234, 234).CGColor;
    emailTxt.layer.borderWidth = 1;
    emailTxt.layer.cornerRadius = 9;
    emailTxt.textContainerInset = UIEdgeInsetsMake(20, 20, 20, 20);
    
    NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    
    if (signtureDic) {
        
        NSString * str = signtureDic[@"signature"];
        
        if (str) {
            NSString * signture =  [str stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            emailTxt.text = signture;
        }
    }
    
    [DataManage instance].delegate = self;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [emailTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onBtnSave:(id)sender {
    [self dismissKeyboard];
    if ([emailTxt.text isEqualToString:@""]) {
        return;
    }
    
    NSDictionary * signtureDic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    
    NSString * str = [emailTxt.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    NSDictionary *dic = @{@"signature":str,@"isHadNew":@"1",@"isAutoSend":signtureDic[@"isAutoSend"]};
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[DataManage instance] syncEmailSignToSV];
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}

@end
