//
//  QuestionViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/18.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol  QuestionViewControllerDelegate <NSObject>
@optional
-(void)addQuestion:(NSString *)orderNum;

@end

@interface QuestionViewController : BaseViewController

@property (nonatomic , strong) id<QuestionViewControllerDelegate>delegate;

@end
