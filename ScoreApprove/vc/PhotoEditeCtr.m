//
//  PhotoEditeCtr.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/16.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "PhotoEditeCtr.h"
#import "Picture.h"

#define CellH 150
#define SelfVW  445
#define btnWidth  150
#define btnHeight  35
#define photoBoxWidth 125

@interface PhotoEditeCtr (){

    NSMutableArray * toDeletListArray;
}

@end

@implementation PhotoEditeCtr

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.preferredContentSize = CGSizeMake(445, 509);
    
    [self initContentV];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(void)setList:(List *)list_{

    _list = list_;

    NSArray * array = [_list.picture allObjects];
    source = [NSMutableArray arrayWithArray:array];
    float h = 44+ CellH *( array.count>3?3:array.count);
    self.h = h;
    [myTableView reloadData];
}

-(void)initContentV{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView * navView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SelfVW, 50)];
    navView.backgroundColor = PopViewNavColor;
    [self.view addSubview:navView];
    
    UIImageView *headerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, navView.frame.size.width, 50)];
    headerImage.contentMode = UIViewContentModeScaleToFill;
    [headerImage setImage:[UIImage imageNamed:@"notice-header.png"]];
    [navView addSubview:headerImage];
//    [headerImage sendSubviewToBack:headerImage];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:navView.bounds];
    lb.text = [NSString stringWithFormat:@"%u %@", (unsigned int)[_list.picture count], TextPhotoEditTitle];
    lb.textColor = [UIColor blackColor];
    lb.textAlignment = 1;
    [navView addSubview:lb];
    
    UIButton * bt = [UIButton buttonWithType:0];
    [bt setTitle:PhotoEditAdd forState:UIControlStateNormal];
    bt.titleLabel.font = [UIFont systemFontOfSize:35];
    [bt addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [bt setTitleColor:RGBCOLOR(0, 122, 255) forState:0];
    bt.frame = CGRectMake(navView.frame.size.width - 50, 0, 50, navView.frame.size.height);
    [navView addSubview:bt];
    
    NSArray * temp = [[_list.picture allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Picture * pic1 = (Picture *)obj1;
        Picture * pic2 = (Picture *)obj2;
        if ([pic1.order intValue] > [pic2.order intValue]) {
            return NSOrderedDescending;
        }else{
            return NSOrderedAscending;
        }
    }];
    source = [NSMutableArray arrayWithArray:temp];

    
    myTableView = [[UITableView alloc] initWithFrame:CGRectMake(15, navView.frame.size.height + 5, SelfVW - 30, CellH * 2.5) style:0];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview:myTableView];
    myTableView.editing = YES;
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    float btnYPos = myTableView.frame.origin.y + myTableView.frame.size.height + 10;
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f];
    
    UIButton * done = [UIButton buttonWithType:0];
    [done setTitle:ButtonTitleDone forState:UIControlStateNormal];
    [done addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    [done setTitleColor:[UIColor whiteColor] forState:0];
    [done setBackgroundImage:[UIImage imageNamed:@"begin-btn"] forState:UIControlStateNormal];
    [done.titleLabel setFont:font];
    done.frame = CGRectMake(navView.frame.size.width - 60 - btnWidth, btnYPos, btnWidth, btnHeight);
    [self.view addSubview:done];
    
    UIButton * cancel = [UIButton buttonWithType:0];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [cancel setTitleColor:[UIColor whiteColor] forState:0];
    [cancel setBackgroundImage:[UIImage imageNamed:@"blue-btn"] forState:UIControlStateNormal];
    [cancel.titleLabel setFont:font];
    cancel.frame = CGRectMake(60, btnYPos, btnWidth, btnHeight);
    [self.view addSubview:cancel];
}

-(void)add{
    [self.delegate photoToAdd];
}

-(void)done{
    
    [source enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * pic = (Picture *)obj;
        pic.order = [NSString stringWithFormat:@"%lu",(unsigned long)idx];
        pic.extra = @"-1";
    }];
    
    [toDeletListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * pic = (Picture *)obj;
        pic.toDele = @"1";
        [_list removePictureObject:pic];
    }];
    
    [[CoredataManager manager] saveContext];
    
    [self.delegate photoEditDone];
//    [[DataManage instance] creatPicture];
}

-(void)cancel {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return source.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentify = @"cellIdentify";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        UIImageView *bgCell = [[UIImageView alloc] initWithFrame:CGRectMake(0, 3.5, self.view.frame.size.width - 30, CellH - 7)];
        [bgCell setImage:[UIImage imageNamed:@"photo-list"]];
        [cell addSubview:bgCell];
        
        UIImageView * bgImgV = [[UIImageView alloc] initWithFrame:CGRectMake(141, 12.5, photoBoxWidth, photoBoxWidth)];
        [bgImgV setImage:[UIImage imageNamed:@"photo-box"]];
        [cell addSubview:bgImgV];
        UIImageView * imgV = [[UIImageView alloc] initWithFrame:CGRectMake(141 + 4, 12.5 + 4, photoBoxWidth - 8, photoBoxWidth - 8)];
        imgV.tag = 400;
        [cell addSubview:imgV];
    }
    
    Picture * pic = (Picture *)source[indexPath.row];
    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfFile:pic.localPath]];
    UIImageView * imgV = (UIImageView *)[cell viewWithTag:400];
    imgV.image = img;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CellH;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
    NSUInteger fromRow = [sourceIndexPath row];
    NSUInteger toRow = [destinationIndexPath row];
    
    id object = [source objectAtIndex:fromRow];
    [source removeObjectAtIndex:fromRow];
    [source insertObject:object atIndex:toRow];
    
    
    NSString * str = [source componentsJoinedByString:@","];
    _list.photoName = str;
    
//    [self.delegate newOrderHad];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
//            [source removeObjectAtIndex:indexPath.row];
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            
//            NSString * str = [source componentsJoinedByString:@","];
//            _list.photoName = str;
//            
//            [self.delegate newOrderHad];

        
        if (!toDeletListArray) {
            toDeletListArray = [NSMutableArray array];
        }
        Picture * pic = source[indexPath.row];
        [toDeletListArray addObject:pic];
    
        [source removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        NSString * str = [source componentsJoinedByString:@","];
        _list.photoName = str;
        
//        [self.delegate newOrderHad];
        
    }
}

#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




@end
