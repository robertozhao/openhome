//
//  DetailViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "DetailViewController.h"
#import "SourceHomeViewController.h"
#import "SourceInfoViewController.h"
#import "EmailSignatureVC.h"
#import "LeadHomeViewController.h"
#import "AccountVC.h"


@interface DetailViewController (){

}

@property (nonatomic , strong)    SourceHomeViewController * sourceHomeCtr;
@property (nonatomic , strong) NSString * specilListId;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item


-(void)setDetailType:(int)detailType_{
    if (_detailType != detailType_) {
        _detailType = detailType_;
    }
    [self loadSubVC];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadSubVC];
    [self addedFuncForLeads];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)addedFuncForLeads {
    
}

-(void)loadSubVC {

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSpecilList:) name:@"NotificationForList" object:nil];
    
    //  add observer code put in viewdidload and view willappear method,not valid.so at here
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    if (_detailType ==  SourceShowAll || _detailType ==  SourceShowForSale || _detailType ==  SourceShowInContract|| _detailType ==  SourceShowSold || _detailType ==  SourceShowShortSale || _detailType ==  SourceShowRental || _detailType ==  SourceShowRented || _detailType ==  SourceShowAuction || _detailType ==  SourceShowAddNewlist ) {
        

        if (!_sourceHomeCtr) {
            self.sourceHomeCtr = [[SourceHomeViewController alloc] initWithNibName:@"SourceHomeViewController" bundle:nil];
        self.sourceHomeCtr.soureCate = _detailType;
            [self.navigationController pushViewController:self.sourceHomeCtr animated:NO];
        }else{
            self.sourceHomeCtr.soureCate = _detailType;
            [self.navigationController pushViewController:self.sourceHomeCtr animated:NO];
        }
        
        self.sourceHomeCtr.specilListShow = _specilListId;
        
    }else if(_detailType == LeadShowTypeAll ){

        LeadHomeViewController * vc = [[LeadHomeViewController alloc] initWithNibName:@"LeadHomeViewController" bundle:nil];
        vc.soureCate = _detailType;
        vc.curFilterList = _filterList;
        vc.menuFilterList = _filterList;
        [self.navigationController pushViewController:vc animated:NO];
    }else if(_detailType == SourceInfo) {
        
        SourceInfoViewController * vc = [[SourceInfoViewController alloc] initWithNibName:@"SourceInfoViewController" bundle:nil];
        vc.list  = self.selectedList;
        
        [self.navigationController pushViewController:vc animated:NO];
    } else if (_detailType == EmailSignature) {
        EmailSignatureVC * vc = [[EmailSignatureVC alloc] initWithNibName:@"EmailSignatureVC" bundle:nil];
        
        [self.navigationController pushViewController:vc animated:NO];
    } else if (_detailType == AccountManage) {
        AccountVC * vc = [[AccountVC alloc] initWithNibName:@"AccountVC" bundle:nil];
        
        [self.navigationController pushViewController:vc animated:NO];
    }
    self.specilListId = nil;
}


-(void)showSpecilList:(NSNotification *)notify{
    
    NSString * listId = (NSString *)[notify object];
    self.specilListId = listId;
    if(_detailType == SourceShowAll){

        self.sourceHomeCtr.specilListShow = _specilListId;
        self.sourceHomeCtr.soureCate = _detailType;
        
    }else{
        self.detailType = SourceShowAll;
        self.sourceHomeCtr.soureCate = _detailType;
//        [self loadSubVC];
    }
    [self.splitViewController dismissViewControllerAnimated:YES completion:nil];
}



@end
