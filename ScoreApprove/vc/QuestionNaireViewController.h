//
//  QuestionNaireViewController.h
//  ScoreApprove
//
//  Created by Yang on 15/5/9.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "List.h"

@protocol QuestionNaireViewControllerDelegate <NSObject>

-(void)questionNaireFinish:(Lead *)lead;

@end

@interface QuestionNaireViewController : BaseViewController

@property (nonatomic , strong) List * list;

@property (nonatomic , weak) id<QuestionNaireViewControllerDelegate>delegate;

@end
