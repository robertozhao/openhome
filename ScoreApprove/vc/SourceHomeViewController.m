//
//  SourceHomeViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "SourceHomeViewController.h"
#import "SourceInfoViewController.h"
#import "QuestionViewController.h"
#import "Picture.h"
#import "SyncImageView.h"
#import "MasterViewController.h"
#import "ListFilterVC.h"

#define CellPhotoItemBaseTag  400
#define photoItemViewAddressLbTag  300
#define photoItemViewPersonLbTag  301
#define photoItemViewphotoNumLbTag  302
#define photoItemViewImageViewTag  303


@interface SourceHomeViewController ()<UITableViewDataSource,UITableViewDelegate>{

    UIBarButtonItem * photoAppearItem;
    UIBarButtonItem * listAppearItem;
    
    UITableView *photoTableView;
    IBOutlet UITableView *myTableView;
    
    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    IBOutlet UIButton *filterButton;
}

@property (nonatomic , strong)    NSArray * sourceList;
@property (nonatomic, assign) int selectedFilterButton;
@property (nonatomic, strong) UIPopoverController * popCtr;

@end

@implementation SourceHomeViewController
@synthesize soureCate = _soureCate;

- (void)viewDidLoad {

    [super viewDidLoad];
    
    
    myTableView.hidden = YES;
    photoTableView.hidden = YES;
//    [self soureSort];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
    self.selectedFilterButton = [DataManage instance].listFilterButtonIndex;
    if (self.selectedFilterButton == nil) {
        self.selectedFilterButton = 0;
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableView) name:@"RefreshSourceHomeView" object:nil];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) refreshTableView {
    [myTableView reloadData];
    [photoTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];


    //  list sync finish

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tableRefresh) name:@"PictureDownloadFinish" object:nil];
    
    if([GlobalInstance instance].listHomeAppearType == 1){
        [self listAppear];
    }else{
        [self photoAppear];
    }
    [self setNavItem];
    [self getSource];
}

-(int)soureCate{

    return _soureCate;
}

-(void)setSoureCate:(int)soureCate{

    _soureCate = soureCate;
    
    [self getSource];
}

-(void)getSource{
//    [self showLoadingViewWithText:@"loading..."];
    
    //    dispatch_async([DataManage instance].dataQueue, ^{
    //do a delay
    NSString * stateValue = nil;
//    if (_soureCate == SourceShowAll) {
//        self.title = ListListTitleAll;
//        self.sourceList = [DataManage instance].allListing;
//    }else if (_soureCate == SourceShowForSale){
//        stateValue = @"1";
//        self.title = ListListTitleForSale;
//    }else if (_soureCate == SourceShowInContract){
//        self.title = ListListTitleForInContract;
//        stateValue = @"2";
//    }else if (_soureCate == SourceShowSold){
//        self.title = ListListTitleForSold;
//        stateValue = @"3";
//    }else if (_soureCate == SourceShowShortSale){
//        self.title = ListListTitleForSold;
//        stateValue = @"4";
//    }else if (_soureCate == SourceShowRental){
//        self.title = ListListTitleForSold;
//        stateValue = @"5";
//    }else if (_soureCate == SourceShowRented){
//        self.title = ListListTitleForSold;
//        stateValue = @"6";
//    }else if (_soureCate == SourceShowAuction){
//        self.title = ListListTitleForSold;
//        stateValue = @"7";
//    }
//    else if (_soureCate == SourceShowAddNewlist){
    if([[DataManage instance] listFilterButtonIndex] == 0) {
        self.title = ListListTitleAll;
        self.sourceList = [DataManage instance].allListing;
    } else if([[DataManage instance] listFilterButtonIndex] == 1) {
        stateValue = @"1";
        self.title = ListListTitleForSale;
    } else if([[DataManage instance] listFilterButtonIndex] == 2) {
        self.title = ListListTitleForInContract;
        stateValue = @"2";
    } else if([[DataManage instance] listFilterButtonIndex] == 3) {
        self.title = ListListTitleForSold;
        stateValue = @"3";
    } else if([[DataManage instance] listFilterButtonIndex] == 4) {
        self.title = ListListTitleForSold;
        stateValue = @"4";
    } else if([[DataManage instance] listFilterButtonIndex] == 5) {
        self.title = ListListTitleForSold;
        stateValue = @"5";
    } else if([[DataManage instance] listFilterButtonIndex] == 6) {
        self.title = ListListTitleForSold;
        stateValue = @"6";
    } else if([[DataManage instance] listFilterButtonIndex] == 7) {
        self.title = ListListTitleForSold;
        stateValue = @"7";
    } else {
        self.title = ListListTitleAll;
        self.sourceList = [DataManage instance].allListing;
        
        [self soureSort];
        List * list_ = _sourceList[0];
        dispatch_async(dispatch_get_main_queue(), ^{
            SourceInfoViewController * vc = [[SourceInfoViewController alloc] initWithNibName:@"SourceInfoViewController" bundle:nil];
            vc.list  = list_;
            [self.navigationController pushViewController:vc animated:YES];
            self.soureCate =SourceShowAll;
        });
    }
    
    if (nil != stateValue) {
         self.sourceList = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"sellState=%@ and user_id='%@'",stateValue,[GlobalInstance instance].user_id]];
    }
    [self soureSort];
    [self tableRefresh];
    if (self.sourceList && self.sourceList.count != 0) {
        myTableView.hidden = NO;
        photoTableView.hidden = NO;
    }else{
        myTableView.hidden = YES;
        photoTableView.hidden = YES;
    }
//    [self hideLoadingView];
}

-(void)soureSort{
    self.sourceList = [self.sourceList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        List * list = (List *)obj1;
        List * list_ = (List *)obj2;
        
        NSDate *date1 = [Utility dateFromDateStr:list.creatDate withFormat:@"YYYY-MM-dd HH-mm-ss"];
        NSDate *date2 = [Utility dateFromDateStr:list_.creatDate withFormat:@"YYYY-MM-dd HH-mm-ss"];
        
        double timer1 = [Utility timeintervalForDate:date1];
        double timer2 = [Utility timeintervalForDate:date2];
        
        if ( timer1> timer2) {
            return NSOrderedAscending;
        }else{
            return NSOrderedDescending;
        }
    }];
}

-(void)tableRefresh{
        
    [photoTableView reloadData];
    [myTableView reloadData];
}

-(void)setNavItem{
    
    UIImage *itemBG = [[UIImage imageNamed: @"batItemBG"] stretchableImageWithLeftCapWidth: 10 topCapHeight:10];
    UIButton *gridB = [UIButton buttonWithType: UIButtonTypeCustom];
    [gridB setBackgroundImage: itemBG forState: UIControlStateNormal];
    gridB.frame = CGRectMake(0, 0, 40, 35);
    gridB.layer.borderColor = [UIColor lightGrayColor].CGColor;
    gridB.layer.borderWidth = 2;
    gridB.layer.cornerRadius = 2;
    [gridB setImage: [UIImage imageNamed: @"GridBarButtonImg"] forState: UIControlStateNormal];
    [gridB addTarget: self action: @selector(photoAppear) forControlEvents: UIControlEventTouchUpInside];
    photoAppearItem = [[UIBarButtonItem alloc] initWithCustomView: gridB];
    
    UIButton *listB = [UIButton buttonWithType: UIButtonTypeCustom];
    [listB setBackgroundImage: itemBG forState: UIControlStateNormal];
    listB.frame = CGRectMake(0, 0, 40, 35);
    listB.layer.borderColor = [UIColor lightGrayColor].CGColor;
    listB.layer.borderWidth = 2;
    listB.layer.cornerRadius = 2;
    [listB setImage: [UIImage imageNamed: @"ListBarButtonImg"] forState: UIControlStateNormal];
    [listB addTarget: self action: @selector(listAppear) forControlEvents: UIControlEventTouchUpInside];
    listAppearItem = [[UIBarButtonItem alloc] initWithCustomView: listB];
    
    if([GlobalInstance instance].listHomeAppearType == 1){
        self.navigationItem.leftBarButtonItem = photoAppearItem;
    }else{
        self.navigationItem.leftBarButtonItem = listAppearItem;
    }
}

-(void)listAppear{
    [GlobalInstance instance].listHomeAppearType = 1;
    [self.view bringSubviewToFront:myTableView];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view bringSubviewToFront:myTableView];
    } completion:^(BOOL finished) {
        
    }];
    self.navigationItem.leftBarButtonItem = photoAppearItem;
}

-(void)setSpecilListShow:(NSString *)specilListShow_{

    if (!specilListShow_) {
        return;
    }
    if (![_specilListShow isEqualToString:specilListShow_]) {

        _specilListShow = specilListShow_;
        NSArray * array = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"id_='%@'",_specilListShow]];
        if (array && array.count !=0) {

            NSArray * vcs = self.navigationController.viewControllers;
            
            if (vcs.count == 3) {
                UIViewController * vc = vcs[2];
                [vc.navigationController popViewControllerAnimated:NO];
            }
            SourceInfoViewController * vc = [[SourceInfoViewController alloc] initWithNibName:@"SourceInfoViewController" bundle:nil];
            vc.list  = array[0];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

-(void)photoAppear{
    
    [GlobalInstance instance].listHomeAppearType = 2;
    [self.view bringSubviewToFront:photoTableView];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view bringSubviewToFront:photoTableView];
    } completion:^(BOOL finished) {
        
    }];
    self.navigationItem.leftBarButtonItem = listAppearItem;
}


-(UIView *)photoItemView:(CGRect)fram tag:(int)tag{

    UIView * view = [[UIView alloc] initWithFrame:fram];
    view.backgroundColor = RGBCOLOR(219, 219, 219);
    view.tag = tag;
    view.hidden = YES;
    
    SyncImageView * imgV = [[SyncImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    imgV.backgroundColor = RGBCOLOR(219, 219, 219);
    imgV.clipsToBounds = YES;
    imgV.tag = photoItemViewImageViewTag;
    imgV.image = [UIImage imageNamed:@"no_image"];
    imgV.userInteractionEnabled = YES;
    [view addSubview:imgV];

    UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, fram.size.height-35, view.frame.size.width, 35)];
    lb.backgroundColor = RGBCOLOR(252, 147, 0);
    lb.alpha = 0.8;
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:15];
    lb.tag = photoItemViewAddressLbTag;
    lb.textAlignment = 1;
    [view addSubview:lb];
    
    UIButton * personBt = [UIButton buttonWithType:0];
    personBt.frame = CGRectMake(fram.size.width/2.0-50, lb.frame.origin.y-5-28, 60, 28);
    [personBt setBackgroundColor:RGBCOLOR(180, 180, 180)];
    [personBt setImage:[UIImage imageNamed:@"iconsleads.png"] forState:0];
    [personBt setTitleColor:[UIColor blackColor] forState:0];
    [personBt setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    personBt.titleLabel.font = [UIFont systemFontOfSize:17];
    personBt.layer.cornerRadius = 3;
    personBt.tag = photoItemViewPersonLbTag;
    personBt.alpha = 0.7;
    
    personBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [view addSubview:personBt];
    
    UIButton * photoBt = [UIButton buttonWithType:0];
    photoBt.frame = CGRectMake(fram.size.width/2.0+15, lb.frame.origin.y-5-28, 60, 28);
    photoBt.layer.cornerRadius = 3;
    [photoBt setTitleColor:[UIColor blackColor] forState:0];
    photoBt.backgroundColor = RGBCOLOR(180, 180, 180);
    [photoBt setImage:[UIImage imageNamed:@"iconscamera.png"] forState:0];
    [photoBt setImageEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 0)];
    photoBt.titleLabel.font = [UIFont systemFontOfSize:17];
    photoBt.tag = photoItemViewphotoNumLbTag;
    photoBt.alpha = 0.7;
    photoBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [view addSubview:photoBt];
    
    
    UIButton * bt_ = [UIButton buttonWithType:0];
    bt_.frame = view.bounds;
    [bt_ addTarget:self action:@selector(photoItemAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:bt_];
    
    return view;
}

-(void)photoItemAction:(UIButton *)bt{

    UIView * photoItemView = [bt superview];
    UITableViewCell * cell = (UITableViewCell *)[photoItemView superview];
    NSIndexPath * indexPath = [photoTableView indexPathForCell:cell];
    NSInteger listIndex = indexPath.row * 4 + ( photoItemView.tag - CellPhotoItemBaseTag);
    List * list = _sourceList[listIndex];
    
    SourceInfoViewController * vc = [[SourceInfoViewController alloc] initWithNibName:@"SourceInfoViewController" bundle:nil];
    vc.list  = list;
    [self.navigationController pushViewController:vc animated:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:listIndex forKey:@"tempoSave"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"setRow" object:nil];
}

#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (tableView == myTableView) {
        return _sourceList.count;
    }else{
        if (_sourceList.count % 4 == 0) {
            return _sourceList.count / 4;
        }else{
            return _sourceList.count / 4 + 1;
        }
    }
}

- (IBAction)onBtnFilter:(UIButton *)sender {
    ListFilterVC *vc = [[ListFilterVC alloc] init];
    vc.selectedIndex = self.selectedFilterButton;
    //    self.popContentVC.delegate = self;
    //    self.popContentVC.se = self.selectedFilterButton;
    //    self.popContentVC.selectedIndex = self.selectedFilterButton;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    
    self.popCtr = [[UIPopoverController alloc] initWithContentViewController:nav];
    self.popCtr.backgroundColor = RGBCOLOR(240, 240, 240);
    
    _popCtr.popoverContentSize = CGSizeMake(230, 335);
    
    [_popCtr presentPopoverFromRect:filterButton.bounds inView:filterButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    return;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == myTableView) {
        NSString * cellIdentify = @"cellIdentify";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        List * list = _sourceList[indexPath.row];
        
        cell.textLabel.text = list.address;
        return cell;
    }else{
        NSString * cellIdentify = @"cellIdentify_photoCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
       
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            float itemW = (self.view.frame.size.width-7.5) / 4.0;
            __block float cur_x = 1.5;
            
            for (int i=0; i<4; i++) {
                CGRect fram = CGRectMake(cur_x, 0, itemW, itemW-1.5);
                [cell addSubview:[self photoItemView:fram tag:CellPhotoItemBaseTag+i]];
                cur_x +=(itemW+1.5);
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSUInteger index = indexPath.row * 4;

        for (NSUInteger i =0; i<4; i++) {
            NSUInteger listIndex = i+index;
            if (listIndex <_sourceList.count) {
                List * list = _sourceList[listIndex];
                UIView * itemV = [cell viewWithTag:CellPhotoItemBaseTag+i];
                SyncImageView * imgV = (SyncImageView *)[itemV viewWithTag:photoItemViewImageViewTag];
                imgV.isBig = 0;
                imgV.placeHold = @"no_image";
                NSArray * array = [[list.picture allObjects] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    Picture * pic1 = (Picture *)obj1;
                    Picture * pic2 = (Picture *)obj2;
                    if ([pic1.order intValue] > [pic2.order intValue]) {
                        return NSOrderedDescending;
                    }else{
                        return NSOrderedAscending;
                    }
                }];
                if(array && array.count !=0){
                    Picture * pic = array[0];
                    imgV.pic = pic;
                }
                UIView * itemView = [cell viewWithTag:CellPhotoItemBaseTag+i];
                itemView.hidden = NO;
                UILabel * addressLb = (UILabel *)[itemView viewWithTag:photoItemViewAddressLbTag];
                addressLb.text = list.address;
                
                UIButton * personBt = (UIButton *)[itemView viewWithTag:photoItemViewPersonLbTag];
                NSString * person = [NSString stringWithFormat:@"%lu",(unsigned long)[list.lead allObjects].count];
                [personBt setTitle:person forState:0];
                
                UIButton * photoBt = (UIButton *)[itemView viewWithTag:photoItemViewphotoNumLbTag];
                
                NSString * photoNum;
                if (list.picture &&  [list.picture allObjects].count !=0) {
                    photoNum =[NSString stringWithFormat:@"%lu",(unsigned long)[list.picture allObjects].count];
                }else{
                    if (list.pictureNum) {
                        photoNum = list.pictureNum;
                    }else{
                        photoNum = @"0";
                    }
                }
                
                NSString * photo = photoNum;
                [photoBt setTitle:photo forState:0];
            }else{
                UIView * itemView = [cell viewWithTag:CellPhotoItemBaseTag+i];
                itemView.hidden = YES;
            }
        }
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == myTableView) {
        return 44;
    }else{
        float itemW = (self.view.frame.size.width-7.5) / 4.0;
        return itemW;
    }
}

#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == myTableView) {
        List * list = _sourceList[indexPath.row];
        
        SourceInfoViewController * vc = [[SourceInfoViewController alloc] initWithNibName:@"SourceInfoViewController" bundle:nil];
        vc.list  = list;
        [self.navigationController pushViewController:vc animated:YES];
        NSLog(@"pushed");
    }

}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}


@end
