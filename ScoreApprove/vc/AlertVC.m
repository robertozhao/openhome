//
//  EditAccountVC.m
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "AlertVC.h"

@interface AlertVC (){

    IBOutlet UILabel *lblTitle;
}

@end

@implementation AlertVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.preferredContentSize = CGSizeMake(444, 265);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)setTitle:(NSString *)title {
    [lblTitle setText:title];
}

- (IBAction)onContinueButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
