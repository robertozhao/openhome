//
//  ScheduleViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "List.h"

@protocol  ScheduelViewControllerDelegate <NSObject>
@optional
-(void)doneButtonPressed;

@end

@interface ScheduleViewController : BaseViewController


@property (nonatomic , strong) List * list;
@property (nonatomic, weak) id<ScheduelViewControllerDelegate>delegate;

@end
