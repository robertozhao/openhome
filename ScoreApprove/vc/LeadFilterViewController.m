//
//  LeadFilterViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LeadFilterViewController.h"

@interface LeadFilterViewController (){
    
    UIButton *btnShowSelected;
    UIButton *btnShowAll;
}

@property (nonatomic ,strong) NSIndexPath * curSelectIndex;

@end

@implementation LeadFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initContentView];
}

- (void)initContentView {
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 5, 70, 35)];
    headerLabel.text = @"FILTERS";
    headerLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
    headerLabel.textColor = RGBCOLOR(146, 146, 146);
    [self.view addSubview:headerLabel];
    
    UIView *headerUnderline = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 111, 3)];
    headerUnderline.backgroundColor = RGBCOLOR(246, 146, 30);
    [self.view addSubview:headerUnderline];
    
    btnShowSelected = [[UIButton alloc] initWithFrame:CGRectMake(0, 50, 230, 40)];
    [btnShowSelected setImage:[UIImage imageNamed:@"icon_radio"] forState:UIControlStateNormal];
    [btnShowSelected setImage:[UIImage imageNamed:@"icon_radio_selected"] forState:UIControlStateSelected];
    btnShowSelected.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [btnShowSelected setTitle:@"Show Selected" forState:UIControlStateNormal];
    [btnShowSelected setTitleColor:RGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
    [btnShowSelected setTitleColor:RGBCOLOR(60, 141, 254) forState:UIControlStateSelected];
    [self.view addSubview:btnShowSelected];
    
    btnShowAll = [[UIButton alloc] initWithFrame:CGRectMake(0, 90, 230, 40)];
    [btnShowAll setImage:[UIImage imageNamed:@"icon_radio"] forState:UIControlStateNormal];
    [btnShowAll setImage:[UIImage imageNamed:@"icon_radio_selected"] forState:UIControlStateSelected];
    btnShowAll.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [btnShowAll setTitle:@"Show All Leads" forState:UIControlStateNormal];
    [btnShowAll setTitleColor:RGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
    [btnShowAll setTitleColor:RGBCOLOR(60, 141, 254) forState:UIControlStateSelected];
    [self.view addSubview:btnShowAll];
    
    [btnShowSelected addTarget:self action:@selector(setFilter) forControlEvents:UIControlEventTouchUpInside];
    [btnShowAll addTarget:self action:@selector(cleanFilter) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_curList != nil) {
        [btnShowSelected setSelected:YES];
        [btnShowAll setSelected:NO];
    } else {
        [btnShowSelected setSelected:NO];
        [btnShowAll setSelected:YES];
    }
}

- (void)setFilter {
    [self.delegate listChoosed:self.menuList];
}

-(void)cleanFilter{
    [self.delegate listChoosed:nil];
}

@end
