//
//  AddListViewController.h
//  ScoreApprove
//
//  Created by Yang on 15/5/6.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@protocol AddListViewControllerDelegate <NSObject>
@optional
-(void)creatNewListing:(NSDictionary *)info;

@end


@interface AddListViewController : BaseViewController

@property (nonatomic , weak) id<AddListViewControllerDelegate>delegate;

@end
