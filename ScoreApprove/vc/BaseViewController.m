//
//  BaseViewController.m
//  vernon_admin
//
//  Created by Score Approve on 6/30/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "QuestionViewController.h"

@implementation UINavigationController(KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end


@interface BaseViewController ()<LoginViewControllerDelegate, UIGestureRecognizerDelegate>{
    UITapGestureRecognizer *recognizer;
    BOOL isModalOptional;
}

@end


@implementation BaseViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isNeedBack = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self showBackBt];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [self enableKeyboarderListener];
    isModalOptional = NO;

    if ([self isModal]) {
        // Dismiss modal
        isModalOptional = YES;
        recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBehind:)];
        
        [recognizer setNumberOfTapsRequired:1];
        recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view	
        recognizer.delegate = self;
        [self.view.window addGestureRecognizer:recognizer];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)handleTapBehind:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded && isModalOptional)
    {
        UIView *rootView = self.view.window.rootViewController.view;
        CGPoint location = [sender locationInView:rootView];
        if (![self.view pointInside:[self.view convertPoint:location fromView:rootView] withEvent:nil]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ModalViewDismissed" object:nil];
            [self dismissViewControllerAnimated:YES completion:^{
                [self.view.window removeGestureRecognizer:sender];
            }];
        }
    }
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}

-(void)showBackBt{
    if (isNeedBack) {
//        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        backBtn.backgroundColor = [UIColor clearColor];
//        [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -28, 0, 0)];
//        UIImage *backBtnImage = [UIImage imageNamed:@"back"];
//        [backBtn setImage:backBtnImage forState:UIControlStateNormal];
//        [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//        backBtn.frame = CGRectMake(0, 0, 54, 35);
//        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
//        self.navigationItem.leftBarButtonItem = backButton;

   

        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        self.navigationItem.leftBarButtonItem = backItem;
    }
}

- (void) viewDidDisappear:(BOOL)animated {
    isModalOptional = NO;
    
    [super viewDidDisappear: animated];
    [self disableKeyboarderListener];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) enableKeyboarderListener {
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleKeyboardWillShow:)
                                                 name: UIKeyboardWillShowNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleKeyboardWillHide:)
                                                 name: UIKeyboardWillHideNotification
                                               object: nil];
}

- (void) disableKeyboarderListener {
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIKeyboardWillShowNotification
                                                  object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: UIKeyboardWillHideNotification
                                                  object: nil];
}

- (void) handleKeyboardWillShow: (NSNotification *) notify {
    if (keyboardShown) {
        return;
    }
    keyboardHeight = [[notify userInfo][@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
    animationDuration = [[notify userInfo][@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
    keyboardShown = YES;
    //do some chagne here
    [self keyboardWillShow: notify];
}

- (void) keyboardWillShow: (NSNotification *) notify {
    //overwrite me
}

- (void) keyboardWillHide: (NSNotification *) notify {
    //overwrite me
}

- (void) handleKeyboardWillHide: (NSNotification *) notify {
    if (!keyboardShown) {
        return;
    }
    keyboardShown = NO;
    [self keyboardWillHide: notify];
}

- (void) simplePopUpWithTitle: (NSString *) title Msg: (NSString *) msg {
    [self simplePopUpWithTitle: title Msg: msg withTag: 0 cancelTitle: ButtonTitleDismiss confirmTitle: nil];
}

-(void)simplePopUpWithTitle : (NSString *)title Msg : (NSString *)msg withTag: (NSInteger) tag cancelTitle: (NSString *) cancelText confirmTitle: (NSString *) confirmText  {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: title
                                                    message: msg
                                                   delegate: (tag==0)?nil:self
                                          cancelButtonTitle: cancelText
                                          otherButtonTitles: confirmText, nil];
    alert.tag = tag;
    [alert show];
}

- (void) showLoadingViewWithText: (NSString *) text withDetailText: (NSString *) sText {
//    [self showLoadingViewWithText: text withDetailText: sText autoHide: 0];
}

- (void) showLoadingText: (NSString *) text {
    if (text != nil) {
        //        showFromDelegateCount++;
        [self showLoadingViewWithText: text];
    } else {
        //        showFromDelegateCount--;
        //        if (showFromDelegateCount == 0) {
        [self hideLoadingView];
        //        }
    }
}

- (void) showLoadingViewWithText: (NSString *) text withDetailText: (NSString *) sText autoHide: (NSTimeInterval) interval {
    if (nil != loadingView) {
        [loadingView hide: YES];
    }
    
    if (loadingParentView == nil) {
        loadingParentView = self.navigationController.view;
        if (loadingParentView == nil) {
            loadingParentView = self.view;
        }
    }
    loadingView = [[MBProgressHUD alloc] initWithView: loadingParentView];
    //	loadingView.delegate = self;
    //	loadingView.userInteractionEnabled = YES;
    
    // Add HUD to screen
    [loadingParentView addSubview: loadingView];
    
    // Regisete for HUD callbacks so we can remove it from the window at the right time
    //    loadingView.delegate = self;
    
    loadingView.labelText = text;
    if (sText != nil) {
        loadingView.detailsLabelText = sText;
    }
    [loadingView show: YES];
    if(interval > 0) {
        [loadingView hide: YES afterDelay: interval];
    }
}

- (void) showLoadingViewWithText: (NSString *) text {
    
    [self showLoadingViewWithText: text withDetailText: nil];
}

- (void) showCheckMarkPopView: (NSString *) text hideAfterDelay: (NSTimeInterval) interval{
    if (nil != loadingView) {
        [loadingView hide: YES];
    }
    
    if (loadingParentView == nil) {
        loadingParentView = self.navigationController.view;
        if (loadingParentView == nil) {
            loadingParentView = self.view;
        }
    }
    loadingView = [[MBProgressHUD alloc] initWithView: loadingParentView];
    //	loadingView.delegate = self;
    //	loadingView.userInteractionEnabled = YES;
    
    // Add HUD to screen
    [loadingParentView addSubview: loadingView];
    
    loadingView.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    
    loadingView.mode = MBProgressHUDModeCustomView;
    loadingView.labelText = text;
    
    [loadingView show: YES];
    [loadingView hide: YES afterDelay: interval];
}

- (void) hideLoadingView {
    
    loadingViewShowing = NO;
    if (loadingView != nil) {
        [loadingView hide: YES];
    }
}

- (void) showLoadingPopUp: (NSString *) text {
    if (loadingPopUpView == nil) {
        loadingPopUpView = [[UIAlertView alloc] initWithTitle: @"\n\n\n\n"
                                                      message: nil
                                                     delegate: nil
                                            cancelButtonTitle: nil
                                            otherButtonTitles: nil];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
        spinner.frame = CGRectMake(125, 60, 30, 30);
        [loadingPopUpView addSubview: spinner];
        [spinner startAnimating];
        
        UILabel *waitLabel = [[UILabel alloc] initWithFrame: CGRectMake(20, 100, 240, 30)];
        waitLabel.textAlignment = 1;
        waitLabel.backgroundColor = [UIColor clearColor];
        waitLabel.textColor = [UIColor whiteColor];
        waitLabel.text = (text==nil)?PopUpLoadingViewText:text;
        [loadingPopUpView addSubview: waitLabel];
    }
    [loadingPopUpView show];
}

- (void) hideLoadingPopUp {
    [loadingPopUpView dismissWithClickedButtonIndex: 0 animated: YES];
}

-(void)alertShow:(NSString *)alert_{

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:alert_ delegate:nil cancelButtonTitle:ButtonTitleDismiss otherButtonTitles:nil, nil];
    [alert show];
}

-(void)toEmail:(NSString *)email{
    
    NSURL * url = [NSURL   URLWithString:[NSString stringWithFormat:@"mailto://%@",email]];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication]openURL:url];
    }
}


-(void)toPhone:(NSString *)phone{
    
    
    NSString * phoneNum = @"0123456789";
    NSMutableString * newNum = [NSMutableString string];
    NSString *temp = nil;
    for(int i =0; i < [phone length]; i++)
    {
        temp = [phone substringWithRange:NSMakeRange(i, 1)];
        if([phoneNum rangeOfString:temp].location !=NSNotFound ){
            [newNum appendString:temp];
        }
    }
    
    NSURL * url = [NSURL   URLWithString:[NSString stringWithFormat:@"tel://%@",newNum]];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication]openURL:url];
    }
}


#pragma mark -- MBProgressHUDDelegate

-(void)hudWasHidden:(MBProgressHUD *)hud{
    // rewrite 
}


#pragma mark -- 

-(void)requeatFail:(NSString *)info{

    [self hideLoadingView];
    [self simplePopUpWithTitle:info Msg:nil];
}

-(void)needUserLogin:(NSString *)error_msg{

    [self hideLoadingView];
//    [self simplePopUpWithTitle:error_msg Msg:nil];

    LoginViewController * vc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    vc.delegate = self;
    [self.splitViewController presentViewController:vc animated:YES completion:nil];
}

-(void)systemError{
    [self hideLoadingView];
    [self simplePopUpWithTitle:NetWorkWrong Msg:nil];
}

#pragma mark -- LoginViewControllerDelegate
-(void)loginFinish{
    AppDelegate * appdelegate =  [UIApplication sharedApplication].delegate;
    appdelegate.window.rootViewController = appdelegate.splitViewController;
}
@end
