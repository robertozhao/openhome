//
//  DetailViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailBaseViewController.h"

@interface DetailViewController : DetailBaseViewController{

}

@property (nonatomic) int  detailType;

@property (nonatomic, strong) List * filterList;
@property (nonatomic, strong) List * selectedList;





@end

