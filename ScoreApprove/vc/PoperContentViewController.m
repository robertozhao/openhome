//
//  PoperContentViewController.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/7.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "PoperContentViewController.h"
#import "EmailEditViewController.h"

@interface PoperContentViewController ()<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{

    UITableView * myTableView;
    NSArray * source;
    
    
    UIPickerView * picker;
    NSArray * bedsArray;
    NSArray * bathsArray;
}

@property (nonatomic , strong) NSString * beds;
@property (nonatomic , strong) NSString * baths;

@end

@implementation PoperContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.tbType != TBtypeFitment) {
        if (!myTableView) {
            myTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
            myTableView.delegate = self;
            myTableView.dataSource = self;
            [self.view addSubview:myTableView];
        }
        [myTableView reloadData];
    }else{
        if (!picker) {
            self.beds = _list.beds;
            self.baths = _list.baths;
            [self bedsAndBathsPicker];
        }
        [picker reloadAllComponents];
    }
    
    if (_tbType == TBtypeSetting) {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

-(NSInteger)tbType:(int)type{
    self.tbType = type;
    switch (self.tbType) {
        case TBtypeAuction:{
            source = @[ForSale,InContract,Sold,ShortSale,Rental,Rented,Auction];
            break;
        }

        case TBtypeListType:{
            source = @[Home,Townhouse,Condo,Coop,SingleFamily Home,LotLand,TIC,Loft];
            break;
        }
        case TBtypePhotoChoose:{
            source = @[ActionTitleCamera,ActionTitlePhotoLibrary,EditPhoto];
            break;
        }
        case TBtypeMoneyUnit:{
            source = @[@"$",@"€",@"£"];
            break;
        }
        case TBtypeFitment:{
            bedsArray = @[@"Studio",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
            bathsArray = @[@"0",@"1",@"1.5",@"2",@"2.5",@"3",@"3.5",@"4",@"4.5",@"5",@"5.5",@"6",@"6.5",@"7",@"7.5",@"8",@"8.5",@"9"];
            break;
        }
        case TBtypeSetting:{
            self.title = SettingTilte;
            NSString * lastTime ;
            if ([GlobalInstance instance].lastSyncTime && ![[GlobalInstance instance].lastSyncTime isEqualToString:@""]) {
                lastTime =[NSString stringWithFormat:@"%@ (Last Updated %@)",SettingSync,[GlobalInstance instance].lastSyncTime];
            }else{
                lastTime = SettingSync;
            }
            source = @[@"Email & Lead Setting",SettingGetHelp,SettingManageQuestion,SettingEditAccount,lastTime,SettingLogout];
            break;
        }
        case TBtypeListShare:{
            self.title = @"Share Listing";
            source = @[@"Twitter",@"Email"];
            break;
        }
        case TBtypeQuestionAnswerType:{
            source = @[@"Select",@"Buttons",@"Text"];
            break;
        }
        case TBtypeBed:
            source = @[@"Studio",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
            break;
        case TBtypeBaths:{
            source = @[@"0",@"1",@"1.5",@"2",@"2.5",@"3",@"3.5",@"4",@"4.5",@"5",@"5.5",@"6",@"6.5",@"7",@"7.5",@"8",@"8.5",@"9"];
            break;
        }
        default:
            break;
    }
    if(type == TBtypeFitment){
        return 200;
    }else if (type == TBtypeQuestionAnswerType){
        if (source.count == 3) {
            return source.count * 44;
        }else{
            return 7.5*44;
        }
    }else{
        return source.count * 44;
    }
}

-(void)bedsAndBathsPicker{

    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 330, 350)];
    picker.dataSource = self;
    picker.delegate = self;
    [self.view addSubview:picker];
    
    __block NSUInteger index=-1;
    [bedsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * str = (NSString *)obj;
        if ([str intValue] == [_beds intValue]) {
            index = idx;
        }
    }];
    
    if (index != -1) {
        [picker selectRow:index inComponent:1 animated:YES];
    }
}

#pragma mark -- UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    if (component == 0) {
        return 2;
    }else{
        if ([pickerView selectedRowInComponent:0] == 0) {
            return bedsArray.count;
        }else{
            return bathsArray.count;
        }
    }
}

#pragma mark -- UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        if (row == 0) {
            return Beds;
        }else{
            return Baths;
        }
    }else{
        if ([pickerView selectedRowInComponent:0] == 0) {
            return bedsArray[row];
        }else{
            return bathsArray[row];
        }
    }
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    if (component == 0) {
        [picker reloadAllComponents];
        if (row == 0) {
            [pickerView selectRow:[bedsArray indexOfObject:_beds] inComponent:1 animated:YES];
        }else{
            [pickerView selectRow:[bathsArray indexOfObject:_baths] inComponent:1 animated:YES];
        }
    }else{
        if ([picker selectedRowInComponent:0] == 0) {
            if (row == 0) {
                self.beds = bedsArray[0];
            }else{
                self.beds = [NSString stringWithFormat:@"%@",bedsArray[row]];
            }
        }else{
            self.baths = [NSString stringWithFormat:@"%@",bathsArray[row]];
        }
    }
    [self.delegate choosedBeds:_beds baths:_baths];
}


#pragma mark --  UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return source.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
      NSString * cellIdentify = @"cellIdentify";

        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.textLabel.textColor = RGBCOLOR(0, 122, 255);
            cell.textLabel.textAlignment = 1;
            if (_tbType == TBtypeSetting) {
                cell.textLabel.textColor = [UIColor blackColor];
                cell.textLabel.textAlignment = 0;
            }
        }
    if (_tbType == TBtypeSetting) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    cell.textLabel.text = source[indexPath.row];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.minimumScaleFactor = 10;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
        return 44;
}
#pragma mark -- UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString * text = source[indexPath.row];
    
    if (self.tbType == TBtypeSetting && indexPath.row == 0) {
        EmailEditViewController * vc = [[EmailEditViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self.delegate tbItemChoosed:text];
    }
}



@end
