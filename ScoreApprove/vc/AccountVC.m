//
//  EditAccountVC.m
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "AccountVC.h"

@interface AccountVC (){

    IBOutlet UIView *syncView;
    IBOutlet UIActivityIndicatorView *sync;
    IBOutlet UILabel *syncLb;
    
    IBOutlet UIWebView *webView;
}

@end

@implementation AccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Load the request in the UIWebView.
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://agents.scoreapprove.com/leads/dashboard/profile"]]];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginToSync) name:@"BeginToSync" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishSync) name:@"FinishSync" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationItem.leftBarButtonItem = nil;
}

#pragma mark -- DataManageDelegate

-(void)beginToSync{
    syncView.hidden = NO;
    [sync startAnimating];
}

-(void)finishSync{
    [sync stopAnimating];
    syncView.hidden = YES;
}

@end
