//
//  LoginViewController.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "UUITextFile.h"

@protocol LoginViewControllerDelegate <NSObject>

-(void)loginFinish;

@end

@interface LoginViewController : BaseViewController

@property (nonatomic , weak) id<LoginViewControllerDelegate>delegate;

@property BOOL fromLogout;

@end
