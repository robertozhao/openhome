//
//  AppDelegate.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@property (nonatomic) BOOL isShowPortrait;
@property (nonatomic , strong) UISplitViewController *splitViewController;
@end

