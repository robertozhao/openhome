//
//  AppDelegate.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/1.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "AppDelegate.h"
#import "TextResource.h"
#import "AFAppDotNetAPIClient.h"
#import "CoredataManager.h"

#import "QuestionViewController.h"

@interface AppDelegate () <UISplitViewControllerDelegate,UIAlertViewDelegate,QuestionViewControllerDelegate>

@property (nonatomic,strong) NSString *curNotificationlistiId;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [AFAppDotNetAPIClient sharedClient];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showQuestionPageSheet) name:@"showQuestionPageSheet" object:nil];
    
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Question"];
    if (!array || array.count == 0) {  // appear lunch first time
    
        [[NSUserDefaults standardUserDefaults] setObject:@"yes" forKey:@"KeyForLoginAuto"];
    }
    
    self.splitViewController = (UISplitViewController *)self.window.rootViewController;
    [_splitViewController setValue:[NSNumber numberWithFloat:395] forKey:@"_masterColumnWidth"];
    UINavigationController *navigationController = [_splitViewController.viewControllers lastObject];
    navigationController.topViewController.navigationItem.leftBarButtonItem = _splitViewController.displayModeButtonItem;
    _splitViewController.delegate = self;
    
    self.isShowPortrait = NO;
  
    return YES;
}

-(void)showQuestionPageSheet{
    
    self.splitViewController = (UISplitViewController *)self.window.rootViewController;
    [_splitViewController setValue:[NSNumber numberWithFloat:395] forKey:@"_masterColumnWidth"];
    UINavigationController *navigationController = [_splitViewController.viewControllers lastObject];
    [navigationController dismissViewControllerAnimated:NO completion:^{
        nil;
    }];
    
    QuestionViewController * vc = [[QuestionViewController alloc] init];
    vc.delegate = self;
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [navigationController presentViewController:nav animated:YES completion:^{
        nil;
    }];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (_isShowPortrait) {
        return UIInterfaceOrientationMaskAll;
    }else{
        return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
 
    if ([[notification.userInfo objectForKey:@"key"] isEqualToString:@"NofiyForSchedule"]) {
//        判断应用程序当前的运行状态，如果是激活状态，则进行提醒，否则不提醒
    NSDictionary  * dic =  notification.userInfo;
        self.curNotificationlistiId = dic[@"id"];
        if (application.applicationState != UIApplicationStateActive) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NotificationTitlt message:[NSString stringWithFormat:NotificationMessage,dic[@"title"]] delegate:self cancelButtonTitle:ButtonTitleCancel otherButtonTitles:ButtonTitleYes, nil];
            [alert show];
        }
    }

}



#pragma mark -- UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationForList" object:_curNotificationlistiId];
    }
}

#pragma mark -- QuestionViewControllerDelegate

-(void)addQuestion:(NSString *)sortNum{
    self.splitViewController = (UISplitViewController *)self.window.rootViewController;
    
    [_splitViewController setValue:[NSNumber numberWithFloat:395] forKey:@"_masterColumnWidth"];
    
    UINavigationController *navigationController = [_splitViewController.viewControllers lastObject];
    
    [navigationController dismissViewControllerAnimated:NO completion:^{
        nil;
    }];

}

@end
