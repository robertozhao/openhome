//
//  QuestionCellVerify.h
//  ScoreApprove
//
//  Created by Yang on 15/5/1.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface QuestionCellVerify : NSObject


-(NSString *)questionVerify:(Question *)q;

@end
