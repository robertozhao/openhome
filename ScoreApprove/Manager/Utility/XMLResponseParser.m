//
//  XMLResponseParser.m
//  wlt
//
//  Created by Score Approve on 7/28/14.
//  Copyright (c) 2014 Score Approve. All rights reserved.
//

#import "XMLResponseParser.h"

@implementation XMLResponseParser

+ (XMLResponseParser *) parserWithXML: (NSString *) xmlStr
{
    XMLResponseParser *_parser = [[XMLResponseParser alloc] init];
    _parser.xmlRaw = xmlStr;
    return _parser;
}

//- (NSString *) valueForpath: (NSString *) xPathStr
//{
//    NSError *error_ = NULL;
//    NSArray *elements = [self.xmlDocument nodesForXPath: xPathStr error: &error_];
//    if (error_ != NULL) {
//        self.parseError = error_;
//    } else {
//        CXMLElement *element = [elements lastObject];
//        return [element stringValue];
//    }
//    return nil;
//}
//
//- (NSArray *) dataArrayForpath: (NSString *) xPathStr
//{
//    NSMutableArray *dataArray = [NSMutableArray array];
//    NSError *error_ = NULL;
//    NSArray *elements = [self.xmlDocument nodesForXPath: xPathStr error: &error_];
//    for (CXMLElement *element in elements) {
//        
//    }
//    return dataArray;
//}
//
//- (NSDictionary *) dataDictionaryForpath: (NSString *) xPathStr
//{
//    NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
//    return dataDic;
//}

/*
 possible return:
 1. string, including empty string;
 2. array
 3. dictionary
 */
//- (id) parseElement: (CXMLElement *) ele
//{
//    NSArray *children = [ele children];
//    if ([children count] > 0) {
//        NSMutableDictionary *responseDic = [NSMutableDictionary dictionary];
//        NSString *lastKeyName = nil;
//        NSMutableArray *processingArray = nil;
//        for (CXMLElement *ele in children) {
//            if (![ele isKindOfClass: [CXMLElement class]]
//                && [ele isKindOfClass: [CXMLNode class]]
//                && [children count] == 1) {
//                return [ele stringValue];
//            }
//            NSString *key = [ele name];
//            id eleVal = [self parseElement: ele];
//            if ([key isEqualToString: lastKeyName]) {
//                if (processingArray == nil) {
//                    processingArray = [NSMutableArray array];
//                    id lastObj = responseDic[lastKeyName];
//                    [responseDic removeObjectForKey: lastKeyName];
//                    [processingArray addObject: lastObj];
//                    responseDic[lastKeyName] = processingArray;
//                }
//                [processingArray addObject: eleVal];
//            } else {
//                if (processingArray != nil) {
//                    //end an array parse
//                    processingArray = nil;
//                }
//                responseDic[key] = eleVal;
//            }
//            lastKeyName = key;
//        }
//        return responseDic;
//    } else {
//        return [ele stringValue];
//    }
//    return nil;
//}
//
//- (NSDictionary *) convertToDictionary
//{
//    if (self.parseError != nil) {
//        return nil;
//    }
//    CXMLElement *rootEle = [[self.xmlDocument children] lastObject];
//    return [self parseElement: rootEle];
//}

- (NSDictionary *) convertToDictionary
{
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString: self.xmlRaw];
    return xmlDoc;
}
@end
