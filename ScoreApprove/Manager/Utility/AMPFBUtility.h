//
//  FBUtility.h
//  FBUtility
//
//  Created by  Score Approve on 3/7/15.
//  Copyright (c) 2015 AMP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

typedef enum {
    LoginFailedReasonUnknow,
    LoginFailedReasonUserCancel,
    LoginFailedReasonJustFailed
} LoginFailedReason;

typedef enum {
    PostResultFailed,
    PostResultComplete,
    PostResultPost,
    PostResultCancel
} PostResult;

@protocol AMPFBUtilityDelegate <NSObject>
@optional
- (void) finishLogin;
- (void) failedLogin: (LoginFailedReason) r;
- (void) gotUserProfile: (NSDictionary*) userInfoDic;
- (void) failedGetUserProfile: (NSString*) errorMsg;
- (void) finishPicturePost: (PostResult) result withInfo: (NSString *) info;
@end

@interface AMPFBUtility : NSObject {
    NSArray *permission;
    BOOL holdingOnFetchingProfile;
    BOOL holdingOnShareURL;
    NSDictionary *holdingOnPostDataToShare;
    BOOL sessionSetup;
}

@property id<AMPFBUtilityDelegate> delegate;
- (void) facebookLogin;
- (void) getUserProfile;
- (void) shareURL: (NSDictionary *) postData;
@end
