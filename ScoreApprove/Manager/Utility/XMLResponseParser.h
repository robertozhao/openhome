//
//  XMLResponseParser.h
//  wlt
//
//  Created by Score Approve on 7/28/14.
//  Copyright (c) 2014 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLDictionary.h"

@interface XMLResponseParser : NSObject

@property (nonatomic, strong) NSString *xmlRaw;
@property (nonatomic, strong) NSError *parseError;

+ (XMLResponseParser *) parserWithXML: (NSString *) xmlStr;
//- (NSString *) valueForpath: (NSString *) xPathStr;
//- (NSArray *) dataArrayForpath: (NSString *) xPathStr;
//- (NSDictionary *) dataDictionaryForpath: (NSString *) xPathStr;
- (NSDictionary *) convertToDictionary;
@end
