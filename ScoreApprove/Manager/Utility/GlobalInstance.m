//
//  GlobalInstance.m
//  vernon_customer
//
//  Created by Score Approve on 5/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "GlobalInstance.h"
#import "LocalFileManager.h"
#import "Utility.h"


#define KeyForLastSyncDate   @"KeyForLastSyncDate"

#define KeyForListingAppearType  @"KeyForListingAppearType"

static GlobalInstance *instance = NULL;
@implementation GlobalInstance {
    
    
    NSMutableDictionary *userDetailDic;
}

@synthesize lastSyncTime=_lastSyncTime;
//@synthesize curEmployee;

+ (GlobalInstance *) instance {
    if (instance == NULL) {
        instance=[[GlobalInstance alloc] init];
    }
    return instance;
}

- (id) init {
    if (instance == NULL) {
        self = [super init];
        instance = self;

        self.loginType =LoginTypeNone;
        
        NSString * str = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForListingAppearType];
        if (!str) {
            str= @"2";
            [[NSUserDefaults standardUserDefaults] setObject:str forKey:KeyForListingAppearType];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        self.listHomeAppearType = [str intValue];
        
        id obj = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLoginAuto];
        if (obj) {
            self.isLoginAuto = YES;
        }else{
            self.isLoginAuto = NO;
        }
        NSDictionary * info = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLastLoginInfo];
        if (!info) {  //  app intalled new
            self.isLoginAuto = YES;
        }
    }
    return instance;
}

-(void)setListHomeAppearType:(int)listHomeAppearType_{


    NSString * str = [NSString stringWithFormat:@"%d",listHomeAppearType_];
    _listHomeAppearType = listHomeAppearType_;
    
    
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:KeyForListingAppearType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString *)phoneNumWithOutFormat:(NSString *)text{
    
    NSString * s = [text stringByReplacingOccurrencesOfString:@"(" withString:@""];
    
    NSString * st = [s stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString * str = [st stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return str;
}


-(NSString *)phoneNumWithFormat:(NSString *)text{

    NSString * str =  [self phoneNumWithOutFormat:text];
    
    NSString * fir;
    NSString * sec;
    if (str.length>2) {

         fir = [str substringWithRange:NSMakeRange(0, 3)];
        
        if (str.length > 6) {
            sec =  [str substringWithRange:NSMakeRange(6, 1)];
        }
    }
    

    if (fir) {
      str=  [str stringByReplacingCharactersInRange:NSMakeRange(0, 3) withString:[NSString stringWithFormat:@"(%@)",fir]];
    }
    if (sec) {
        NSRange rang;
        if (fir) {
            rang = NSMakeRange(8, 1);
        }else{
            rang = NSMakeRange(4, 1);
        }
       str = [str stringByReplacingCharactersInRange:rang withString:[NSString stringWithFormat:@"-%@",sec]];
    }
    
    return str;
}

-(NSString *)lastSyncTime{

    if (!_lastSyncTime) {
        NSString * str = [[NSUserDefaults standardUserDefaults] objectForKey:KeyForLastSyncDate];
        return str;
    }else{
        return _lastSyncTime;
    }
}

-(void)setLastSyncTime:(NSString *)lastSyncTime_{

    _lastSyncTime = lastSyncTime_;
    [[NSUserDefaults standardUserDefaults] setObject:lastSyncTime_ forKey:KeyForLastSyncDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end