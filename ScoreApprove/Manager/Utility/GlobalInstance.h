//
//  GlobalInstance.h
//  vernon_customer
//
//  Created by Score Approve on 5/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextResource.h"
#define KeyForLoginAuto      @"KeyForLoginAuto"
#define KeyForLastLoginInfo  @"KeyForLastLoginInfo" 
#define KeyForEmailSignture  @"KeyForEmailSignture"


typedef enum {
    LoginTypeNone,
    LoginTypeLocally,
    LoginTypeServer
} LoginType;
@interface GlobalInstance : NSObject {


}


@property (nonatomic) BOOL isLoginAuto;
@property(nonatomic, strong) NSString *user_id;
@property(nonatomic, strong) NSString *username;
@property(nonatomic, strong) NSString *password;
@property  LoginType loginType;

@property (nonatomic) BOOL netWorkStatue;


@property (nonatomic) int listHomeAppearType;  // 1:list 2:photo;


@property (nonatomic , strong) NSString * lastSyncTime;

@property (nonatomic, strong) NSMutableDictionary *customerInfo;
+ (GlobalInstance *) instance;

//-(BOOL)netWorkState;


-(NSString *)phoneNumWithOutFormat:(NSString *)text;
-(NSString *)phoneNumWithFormat:(NSString *)text;



@end
