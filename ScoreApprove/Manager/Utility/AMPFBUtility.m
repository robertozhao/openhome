//
//  FBUtility.m
//  FBUtility
//
//  Created by  Score Approve on 3/7/15.
//  Copyright (c) 2015 AMP. All rights reserved.
//

#import "AMPFBUtility.h"

@implementation AMPFBUtility
- (void) facebookLogin
{
    if (nil == permission) {
        permission =  @[@"public_profile"];
    }
    if (sessionSetup) {
        //logged in
        if([self.delegate respondsToSelector: @selector(finishLogin)]) {
            [self.delegate finishLogin];
        }
    } else {
        //login first
        [[FBSession activeSession] closeAndClearTokenInformation];
        void (^completeHandler)(FBSession *session, FBSessionState status, NSError *error) =
        ^(FBSession *session, FBSessionState status, NSError *error) {
            if (FBSessionStateOpen == status) {
                //suceessful login
                sessionSetup = YES;
                if (holdingOnFetchingProfile) {
                    [self getUserProfile];
                } else if (holdingOnShareURL) {
                    [self shareURL: holdingOnPostDataToShare];
                } else {
                    if([self.delegate respondsToSelector: @selector(finishLogin)]) {
                        [self.delegate finishLogin];
                    }
                }
            } else {
                //faield login
                LoginFailedReason failedReason = LoginFailedReasonUnknow;
                NSString *errorReason = [[error userInfo] objectForKey:FBErrorLoginFailedReason];
                
                failedReason = LoginFailedReasonUnknow;
                if([errorReason isEqualToString:FBErrorLoginFailedReasonInlineCancelledValue]) {
                    //user cancel login
                    failedReason = LoginFailedReasonUserCancel;
                }
                if([self.delegate respondsToSelector: @selector(failedLogin:)]) {
                    [self.delegate failedLogin: failedReason];
                }
            }
        };
        
        if ([FBDialogs canPresentShareDialogWithPhotos]) {
            //user installed facebook app, jump to login
            if([FBSession openActiveSessionWithPublishPermissions: permission
                                                  defaultAudience: FBSessionDefaultAudienceEveryone
                                                     allowLoginUI: YES
                                                completionHandler: completeHandler] == YES) {
                [self getUserProfile];
            }
        } else {
            [[FBSession activeSession] openWithBehavior:FBSessionLoginBehaviorForcingWebView
                                      completionHandler: completeHandler];
        }
    }
}

- (void) getUserProfile {
    if(sessionSetup) {
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
            if (error) {
                if([self.delegate respondsToSelector: @selector(failedGetUserProfile:)]) {
                    [self.delegate failedGetUserProfile: [error localizedDescription]];
                }
            } else {
                if([self.delegate respondsToSelector: @selector(gotUserProfile:)]) {
                    [self.delegate gotUserProfile: user];
                }
            }
        }];
    } else {
        holdingOnFetchingProfile = YES;
        [self facebookLogin];
    }
}

/*
 params in postData
 @link: link on click of the post
 @name: name show in the post
 @caption:
 @picture: picture in the post
 @description:
 */

- (void) shareURL: (NSDictionary *) postData {
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString: postData[@"link"]];
    params.name = postData[@"name"];
    params.caption = postData[@"caption"];
    params.picture = [NSURL URLWithString: postData[@"picture"]];
    params.linkDescription = postData[@"description"];
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present the share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                         name:params.name
                                      caption:params.caption
                                  description:params.linkDescription
                                      picture:params.picture
                                  clientState:nil
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          PostResult result = PostResultFailed;
                                          NSString *msg = nil;
                                          if(error) {
                                              msg = error.description;
                                          } else {
                                              if([results[@"completionGesture"] isEqualToString: @"cancel" ]) {
                                                  result = PostResultCancel;
                                              } else if([results[@"completionGesture"] isEqualToString: @"post" ]) {
                                                  result = PostResultPost;
                                              } else {
                                                  result = PostResultComplete;
                                              }
                                          }
                                          if([self.delegate respondsToSelector: @selector(finishPicturePost:withInfo:)]) {
                                              [self.delegate finishPicturePost: result withInfo: msg];
                                          }
                                      }];
    } else {
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:postData
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      
                                                      PostResult postResult = PostResultFailed;
                                                      NSString *msg = nil;
                                                      if(error) {
                                                          msg = error.description;
                                                      } else {
                                                          if(result == FBWebDialogResultDialogNotCompleted) {
                                                              postResult = PostResultCancel;
                                                          } else {
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                                  postResult = PostResultCancel;
                                                              } else {
                                                                  // User clicked the Share button
                                                                  //                                                                  NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
                                                                  postResult = PostResultPost;
                                                              }
                                                          }
                                                      }
                                                      if([self.delegate respondsToSelector: @selector(finishPicturePost:withInfo:)]) {
                                                          [self.delegate finishPicturePost: PostResultPost withInfo: msg];
                                                      }
                                                  }];
    }
}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}
@end
