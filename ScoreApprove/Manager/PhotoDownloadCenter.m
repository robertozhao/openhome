//
//  PhotoDownloadCenter.m
//  ScoreApprove
//
//  Created by Yang on 15/5/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "PhotoDownloadCenter.h"


static  PhotoDownloadCenter * downCenter;

@implementation PhotoDownloadCenter

+(PhotoDownloadCenter *)instance{

    if (!downCenter) {
        downCenter = [[PhotoDownloadCenter alloc] init];
    }
    return downCenter;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        taskArray = [NSMutableArray array];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newTask:) name:@"PictureDownLoad" object:nil];
    }
    return self;
}


-(void)newTask:(NSNotification *)nofi{

    Picture * pic = [nofi object];
    
    __block BOOL isHad = NO;
    [taskArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * p = (Picture *)obj;
        if ([p.id_ intValue] == [pic.id_ intValue]) {
            isHad = YES;
            *stop = YES;
        }
    }];
    if (!isHad) {
        [taskArray addObject:pic];
        [self nextTask];
    }
}

-(void)nextTask{

    if (_cur_task || taskArray.count==0) {
        return;
    }
    self.cur_task = taskArray[0];
    if (!manage) {
        manage = [[SyncManage alloc] init];
        manage.delegate  = self;
    }
    if (!_cur_task.hash_code || _cur_task.hash_code.length == 0) {
        [taskArray removeObject:_cur_task];
        self.cur_task = nil;
        [self nextTask];
    }else{
        [manage syncPictureDataFromSV:_cur_task];
    }
}



#pragma  mark -- SyncManageDelegate

-(void)syncPictureDataFromSVFinish{
    
    NSString * name = [NSString stringWithFormat:@"%@_finish",_cur_task.localPath];

    [[NSNotificationCenter defaultCenter] postNotificationName:name object:nil];
    [taskArray removeObject:_cur_task];
    self.cur_task = nil;
    [self nextTask];
}


@end
