//
//  AppDelegate.h
//  Timelined
//
//  Created by Gordon Yang on 6/21/12.
//  Copyright (c) 2012 AMP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Manager.h"

@interface CoredataManager : Manager {
}

@property (nonatomic)  dispatch_queue_t coreDataQueue;

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(void)saveContext;
-(void)deleteObject : (NSManagedObject *)obj;

+(CoredataManager *)manager;
-(NSManagedObject *)newObjectFromEntity : (NSString *)entityName;

-(NSArray *)allNodeForEntity : (NSString *)entityName;
-(NSArray *)allNodeForEntity : (NSString *)entityName orderBy : (NSString *)oKey;
-(NSArray *)allNodeForEntity : (NSString *)entityName withPredicate : (id)predicate;
-(NSArray *)allNodeForEntity : (NSString *)entityName orderBy : (NSString *)oKey withPredicate : (id)predicate;

- (NSArray *) nodesFromEntity: (NSString *) entityName orderBy: (NSString *) oKey orderState:(NSString *)orderState withPredicate: (id) stringOrPredicate;

- (NSManagedObject *) getSingleFromEntity: (NSString *) entityName withPredicate: (NSPredicate*) predicate;

-(NSArray *)nodesFromEntity : (NSString *)entityName orderBy : (NSString *)oKey withPredicate : (id) stringOrPredicate withPageNum : (int)pageNum withPageSize : (int)pageSize;
-(void)turnBackToFault : (NSManagedObject *)obj;
-(void)deleteAllObjectsFromEntity : (NSString *)entity;

- (void) releaseMe;

@end
