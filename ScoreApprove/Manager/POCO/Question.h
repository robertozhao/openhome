//
//  Question.h
//  ScoreApprove
//
//  Created by Yang on 15/4/24.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>



@class Answer;


/*
 1.all phone num  same as  QUESTION_ANSER_TYPE_PHONENUMBER
 2.DOB:   brithday
 3. * and *_VERIFY same;
 4. * and *_JOIN  same
 */
//question answer type

//question answer type

#define QUESTION_ANSER_TYPE_SELECT  1
#define QUESTION_ANSER_TYPE_RADIO  2
#define QUESTION_ANSER_TYPE_CHECKBOX  3
#define QUESTION_ANSER_TYPE_BUTTON  4
#define QUESTION_ANSER_TYPE_INFO  5
#define QUESTION_ANSER_TYPE_TEXT  20
#define QUESTION_ANSER_TYPE_MONEY  21
#define QUESTION_ANSER_TYPE_PHONENUMBER  22
#define QUESTION_ANSER_TYPE_PHONENUMBER_VERIFY  23
#define QUESTION_ANSER_TYPE_EMAIL  24
#define QUESTION_ANSER_TYPE_EMAIL_VERIFY  25
//only one question but when show up, display 4 different question
#define QUESTION_ANSER_TYPE_ADDRESS  26
#define QUESTION_ANSER_TYPE_ADDRESS_VERIFY  27
#define QUESTION_ANSER_TYPE_DOB  28
#define QUESTION_ANSER_TYPE_SSN  29
//dispaly first name and last name in one line
#define QUESTION_ANSER_TYPE_FULL_NAME  30
//enter phonenumber in one text box
#define QUESTION_ANSER_TYPE_PHONENUMBER_JOIN  31
#define QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY  32
//enter dob in one text box with format: m-d-y
#define QUESTION_ANSER_TYPE_DOB_JOIN  33
//enter ssn in one text box
#define QUESTION_ANSER_TYPE_SSN_JOIN  34
#define QUESTION_ANSER_TYPE_PASSWORD  35
#define QUESTION_ANSER_TYPE_HIDDEN  36

@interface Question : NSManagedObject

@property (nonatomic, retain) NSString * is_active;
@property (nonatomic, retain) NSString * answer_type;
@property (nonatomic, retain) NSString * question_id;
@property (nonatomic, retain) NSString * is_required;
@property (nonatomic, retain) NSString * sortNum;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSString * config;
@property (nonatomic, retain) NSString * global_name;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * tool_tip;
@property (nonatomic, retain) NSString * is_saved_answer;
@property (nonatomic, retain) NSString * extra;   // default is nil,  if (extra > -1)  so edit or new
@property (nonatomic, retain) NSSet *answer;



@property (nonatomic , strong) NSArray * qCells;
@property (nonatomic , strong) NSString * answerType_temp;

@end

@interface Question (CoreDataGeneratedAccessors)

- (void)addAnswerObject:(Answer *)value;
- (void)removeAnswerObject:(Answer *)value;
- (void)addAnswer:(NSSet *)values;
- (void)removeAnswer:(NSSet *)values;

@end
