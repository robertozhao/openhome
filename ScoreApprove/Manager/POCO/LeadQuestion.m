//
//  LeadQuestion.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/23.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LeadQuestion.h"
#import "Lead.h"


@implementation LeadQuestion

@dynamic answer;
@dynamic answerId;
@dynamic answerValue;
@dynamic id_;
@dynamic questionId;
@dynamic title;
@dynamic type;
@dynamic sortNum;
@dynamic lead;

@end
