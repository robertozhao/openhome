//
//  MAnswer.h
//  ScoreApprove
//
//  Created by Yang on 15/5/9.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Question.h"

@interface MAnswer : NSObject

@property (nonatomic, strong) NSString * answer_id;
@property (nonatomic, strong) NSString * answer_option;
@property (nonatomic, strong) NSString * answer_value;
@property (nonatomic, strong) NSString * extra;
@property (nonatomic, strong) NSString * order;
@property (nonatomic, strong) NSString * question_id;
@property (nonatomic, strong) NSString * toDele;
@property (nonatomic, strong) NSString * needSync;

@property (nonatomic, strong) Question *question;



@end
