//
//  Lead_Note.h
//  ScoreApprove
//
//  Created by Yang on 15/5/26.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Lead;

@interface Lead_Note : NSManagedObject

@property (nonatomic, retain) NSString * note_id;
@property (nonatomic, retain) NSString * lead_id;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * creat;
@property (nonatomic, retain) NSString * extra;
@property (nonatomic, retain) Lead *lead;

@end
