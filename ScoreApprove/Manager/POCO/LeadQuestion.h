//
//  LeadQuestion.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/23.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Lead;

@interface LeadQuestion : NSManagedObject

@property (nonatomic, retain) NSString * answer;
@property (nonatomic, retain) NSString * answerId;
@property (nonatomic, retain) NSString * answerValue;
@property (nonatomic, retain) NSString * id_;
@property (nonatomic, retain) NSString * questionId;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;   //  1:    internal  2:external  （显示时区分）
@property (nonatomic, retain) NSString * sortNum;
@property (nonatomic, retain) Lead *lead;

@end
