//
//  Answer.m
//  ScoreApprove
//
//  Created by Yang on 15/5/12.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "Answer.h"
#import "Question.h"


@implementation Answer

@dynamic answer_id;
@dynamic answer_option;
@dynamic answer_value;
@dynamic extra;
@dynamic order;
@dynamic question_id;
@dynamic toDele;
@dynamic question;

@end
