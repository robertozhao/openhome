//
//  Picture.h
//  ScoreApprove
//
//  Created by Yang on 15/5/15.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class List;

@interface Picture : NSManagedObject

@property (nonatomic, retain) NSString * id_;
@property (nonatomic, retain) NSString * order;
@property (nonatomic, retain) NSString * localPath;
@property (nonatomic, retain) NSString * hash_code;
@property (nonatomic, retain) NSString * localPath_small;
@property (nonatomic, retain) NSString * extra;  // if -1,need sync to sv
@property (nonatomic, retain) NSString * isSyncing;  //  if 1,syncing to sv
@property (nonatomic, retain) NSString * toDele;  //  if 1, to dele form sv
@property (nonatomic, retain) List *list;

@end


