//
//  Answer.h
//  ScoreApprove
//
//  Created by Yang on 15/5/12.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question;

@interface Answer : NSManagedObject

@property (nonatomic, retain) NSString * answer_id;
@property (nonatomic, retain) NSString * answer_option;
@property (nonatomic, retain) NSString * answer_value;
@property (nonatomic, retain) NSString * extra;
@property (nonatomic, retain) NSString * order;
@property (nonatomic, retain) NSString * question_id;
@property (nonatomic, retain) NSString * toDele;
@property (nonatomic, retain) Question *question;

@end
