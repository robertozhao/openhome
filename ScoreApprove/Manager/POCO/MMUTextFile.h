//
//  MMUTextFile.h
//  ScoreApprove
//
//  Created by Yang on 15/4/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ENUM(int, TFState){
    TFStateNomal,
    TFStateWarming,
    TFStateAlert,
};

#define AlertLableOriginHeight  20

@protocol MMUTextFileDelegate <UITextFieldDelegate>
- (void)reSizeSupFram;
@end

@interface MMUTextFile : UITextField{
}

@property (nonatomic , strong)    UILabel * alertInfo;
@property (nonatomic) int tfState;

@property (nonatomic , weak) id<MMUTextFileDelegate>delegate;

- (void)setTfState:(int)state andText:(NSString *)text;
- (void)revertFram;

@end
