//
//  MMUTextFile.m
//  ScoreApprove
//
//  Created by Yang on 15/4/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "MMUTextFile.h"
#import "Macro.h"

@implementation MMUTextFile

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1;
        
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 15, frame.size.height)];
        self.leftView = lb;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        self.alertInfo = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width - 70, frame.size.height+frame.origin.y, frame.size.width, AlertLableOriginHeight)];
        self.alertInfo.textColor = [UIColor redColor];
        self.alertInfo.numberOfLines = 0;
        self.alertInfo.font = [UIFont systemFontOfSize:14];
        
    }
    return self;
}

-(void)didMoveToSuperview{
    if (self.superview) {
        [self.superview addSubview:self.alertInfo];
    }
}

-(void)setTfState:(int)tfState{

    _tfState = tfState;
    if (tfState == TFStateNomal) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.alertInfo.hidden = YES;
    }else if (tfState == TFStateWarming){
        self.layer.borderColor = RGBCOLOR(208, 201, 41).CGColor;
        self.alertInfo.hidden = NO;
        self.alertInfo.textColor = RGBCOLOR(208, 201, 41);
    }else{
        self.layer.borderColor = [UIColor redColor].CGColor;
        self.alertInfo.hidden = NO;
        self.alertInfo.textColor = [UIColor redColor];
    }
}
- (void)setTfState:(int)state andText:(NSString *)text{
    self.tfState = state;
    
    NSDictionary *attributes = @{NSFontAttributeName:self.alertInfo.font};
    CGRect rect_textLb = [text boundingRectWithSize:CGSizeMake(self.alertInfo.frame.size.width, MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
    if (rect_textLb.size.height > CGRectGetHeight(_alertInfo.frame)) {
        CGRect fram = _alertInfo.frame;
        fram.size.height = rect_textLb.size.height;
        _alertInfo.frame = fram;
        [self.delegate reSizeSupFram];
    }
    _alertInfo.text = text;
}
- (void)revertFram{
    CGRect fram = _alertInfo.frame;
    fram.size.height = 20;
    _alertInfo.frame = fram;
   
}
@end
