//
//  Lead.m
//  ScoreApprove
//
//  Created by Yang on 15/5/26.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "Lead.h"
#import "LeadQuestion.h"
#import "Lead_Note.h"
#import "List.h"


@implementation Lead

@dynamic address;
@dynamic agent_id;
@dynamic brithday;
@dynamic city;
@dynamic created;
@dynamic email;
@dynamic extra;
@dynamic first_name;
@dynamic hadChange;
@dynamic last_name;
@dynamic lead_id;
@dynamic phone;
@dynamic questions;
@dynamic state;
@dynamic user_id;
@dynamic zipCode;
@dynamic leadQ;
@dynamic list;
@dynamic notes;

@end
