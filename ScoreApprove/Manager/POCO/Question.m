//
//  Question.m
//  ScoreApprove
//
//  Created by Yang on 15/5/21.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "Question.h"
#import "Answer.h"


@implementation Question

@dynamic answer_type;
@dynamic config;
@dynamic extra;
@dynamic global_name;
@dynamic is_active;
@dynamic is_required;
@dynamic question;
@dynamic question_id;
@dynamic sortNum;
@dynamic tool_tip;
@dynamic user_id;
@dynamic is_saved_answer;
@dynamic answer;

@synthesize qCells;
@synthesize answerType_temp;

@end
