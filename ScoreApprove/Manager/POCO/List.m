//
//  List.m
//  ScoreApprove
//
//  Created by Yang on 15/5/21.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "List.h"
#import "Lead.h"
#import "Picture.h"


@implementation List

@dynamic address;
@dynamic baths;
@dynamic beds;
@dynamic creatDate;
@dynamic desc;
@dynamic extra;
@dynamic id_;
@dynamic listCate;
@dynamic needSync;
@dynamic personVisit;
@dynamic photoName;
@dynamic price;
@dynamic priceUnit;
@dynamic schedule;
@dynamic sellState;
@dynamic size;
@dynamic sizeUnit;
@dynamic user_id;
@dynamic zipCode;
@dynamic pictureNum;
@dynamic lead;
@dynamic picture;

@end
