//
//  Lead.h
//  ScoreApprove
//
//  Created by Yang on 15/5/26.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LeadQuestion, Lead_Note, List;

@interface Lead : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * agent_id;
@property (nonatomic, retain) NSString * brithday;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * created;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * extra;
@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * hadChange;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSString * lead_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * questions;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSSet *leadQ;
@property (nonatomic, retain) List *list;
@property (nonatomic, retain) NSSet *notes;
@end

@interface Lead (CoreDataGeneratedAccessors)

- (void)addLeadQObject:(LeadQuestion *)value;
- (void)removeLeadQObject:(LeadQuestion *)value;
- (void)addLeadQ:(NSSet *)values;
- (void)removeLeadQ:(NSSet *)values;

- (void)addNotesObject:(Lead_Note *)value;
- (void)removeNotesObject:(Lead_Note *)value;
- (void)addNotes:(NSSet *)values;
- (void)removeNotes:(NSSet *)values;

@end
