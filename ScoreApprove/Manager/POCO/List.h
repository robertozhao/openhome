//
//  List.h
//  ScoreApprove
//
//  Created by Yang on 15/4/8.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Lead;

@interface List : NSManagedObject

@property (nonatomic, retain) NSString * address;   //
@property (nonatomic, retain) NSString * baths;      //
@property (nonatomic, retain) NSString * beds;       //
@property (nonatomic, retain) NSString * desc;          //
@property (nonatomic, retain) NSString * listCate;       //
@property (nonatomic, retain) NSString * price;      //
@property (nonatomic, retain) NSString * priceUnit;   //

//    1:For Sale  2. In Contract 3. Sold 4. Short Sale 5.Rental   6.Rented   7.Auction

@property (nonatomic, retain) NSString * sellState;      //
@property (nonatomic, retain) NSString * size;        //
@property (nonatomic, retain) NSString * sizeUnit;      //
@property (nonatomic, retain) NSString * zipCode;    //
@property (nonatomic, retain) NSString * id_;           //  id
@property (nonatomic, retain) NSString * personVisit;  // person visitor num
@property (nonatomic, retain) NSString * photoName;  //  address(hash)+code+图片序号，
@property (nonatomic, retain) NSSet *lead;
@property (nonatomic, retain) NSDate * schedule;

@property (nonatomic, retain) NSString * user_id;

@property (nonatomic, retain) NSString * needSync;   // if list need to sync.when list edit and new
@property (nonatomic, retain) NSString * extra;    // temp id when list not sync to sv

@property (nonatomic, retain) NSString * creatDate;
@property (nonatomic, retain) NSString * pictureNum;
@property (nonatomic, retain) NSSet * picture;

@end

@interface List (CoreDataGeneratedAccessors)

- (void)addLeadObject:(Lead *)value;
- (void)removeLeadObject:(Lead *)value;
- (void)addLead:(NSSet *)values;
- (void)removeLead:(NSSet *)values;

- (void)addPictureObject:(NSManagedObject *)value;
- (void)removePictureObject:(NSManagedObject *)value;
- (void)addPicture:(NSSet *)values;
- (void)removePicture:(NSSet *)values;

@end
