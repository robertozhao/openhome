//
//  QuestionCell.h
//  ScoreApprove
//
//  Created by Yang on 15/4/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionCell : NSObject

@property (nonatomic)        int        questionType;
@property (nonatomic,strong) NSString * question_appear;
@property (nonatomic,strong) NSString * question_answer;
@property (nonatomic,strong) NSString * question_answerID;
@property (nonatomic,strong) NSString * key;



@property (nonatomic, strong) NSString * warmingText;

@end
