//
//  AFAppDotNetAPIClient.h
//  ScoreApprove
//
//  Created by Yang on 15/5/18.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface AFAppDotNetAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
