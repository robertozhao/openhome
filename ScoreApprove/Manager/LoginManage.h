//
//  LoginManage.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Manager.h"

@protocol LoginManageDelegate <ManagerDelegate>


-(void)loginFinish:(NSDictionary *)info errorMsg:(NSString *)str;
-(void)registerFinish:(NSString *)user_id errorMsg:(NSString *)str;

@end

@interface LoginManage : Manager

@property (nonatomic , weak) id<LoginManageDelegate>delegate;

-(void)login:(NSDictionary *)info;
-(void)toRegister:(NSDictionary *)info;
@end
