//
//  QuestionCellVerify.m
//  ScoreApprove
//
//  Created by Yang on 15/5/1.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "QuestionCellVerify.h"
#import "QuestionCell.h"
#import "Utility.h"
#import "TextResource.h"

@implementation QuestionCellVerify

-(NSString *)questionVerify:(Question *)q{


    
    __block BOOL isHasAnswer = NO;
    
    [q.qCells enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
       
        QuestionCell * cell = (QuestionCell *)obj;
        if (cell.question_answer && ![cell.question_answer isEqualToString:@""]) {
            isHasAnswer = YES;
            * stop = YES;
        }
    }];
    
    if (!isHasAnswer) {
        return nil;
    }


    //  if invalid, alertStr will be not nil
    NSString * alertStr;
    
    switch ([q.answer_type intValue]) {
        case QUESTION_ANSER_TYPE_SELECT:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_RADIO:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_CHECKBOX:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_BUTTON:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_PASSWORD:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_ADDRESS_VERIFY:
        case QUESTION_ANSER_TYPE_ADDRESS:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_TEXT:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_MONEY:{
            alertStr = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_PHONENUMBER:{
            alertStr  = nil;
            break;
        }
        case QUESTION_ANSER_TYPE_EMAIL:
        case QUESTION_ANSER_TYPE_EMAIL_VERIFY:{
            
            QuestionCell * cell = q.qCells[0];
            if ([Utility isLegalEmailAddress:cell.question_answer]) {
                alertStr = nil;
            }else{
                alertStr = EmailFormatWrong;
            }
            break;
        }
        case QUESTION_ANSER_TYPE_DOB_JOIN:
        case QUESTION_ANSER_TYPE_DOB:{
            
            break;
        }
        case QUESTION_ANSER_TYPE_SSN:
        case QUESTION_ANSER_TYPE_SSN_JOIN:{
        
            QuestionCell * qcell = q.qCells[0];
            if (qcell.question_answer.length != 9) {
                alertStr = @"The input format is not correct";
            }
            break;
        }
        case QUESTION_ANSER_TYPE_FULL_NAME:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN:
        case QUESTION_ANSER_TYPE_PHONENUMBER_JOIN_VERIFY:
        case QUESTION_ANSER_TYPE_HIDDEN:{
            
            break;
        }
        default:
            break;
    }


    return alertStr;

}

@end
