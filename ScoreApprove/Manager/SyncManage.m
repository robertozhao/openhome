//
//  SyncManage.m
//  ScoreApprove
//
//  Created by Yang on 15/5/7.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "SyncManage.h"
#import "CoredataManager.h"
#import "Question.h"
#import "Answer.h"
#import "List.h"
#import "GlobalInstance.h"

#import "LeadQuestion.h"
#import "Lead.h"
#import "DataManage.h"
#import "Picture.h"
#import "LocalFileManager.h"
#import "Lead_Note.h"

@implementation SyncManage
@dynamic delegate;

// about list
-(void)getPropertyForSV{

    NSMutableDictionary * para = [self paramsDic:nil];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        if ([responseDic allKeys].count ==2) {
            [self.delegate getListForSVFinish];
            return ;
        }
        
        if ([self checkResponse:responseDic]) {
            id obj = responseDic[@"data"][@"properties"][@"property"];
            NSArray * array;
            if ([obj isKindOfClass:[NSDictionary class]]) {
                array = @[obj];
            }else if(obj){
                array = obj;
            }
            NSDictionary * listSellDic = @{@"for sale":@"1",@"in contract":@"2",@"sold":@"3",@"short sale":@"4",@"rental":@"5",@"rented":@"6",@"auction":@"7"};
            
            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary * dic = (NSDictionary *)obj;
                
                NSArray * lists = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"id_='%@' and user_id='%@'",dic[@"property_id"],[GlobalInstance instance].user_id]];
                if (!lists  || lists.count ==0) {
                    
                    List * list = (List *)[[CoredataManager manager] newObjectFromEntity:@"List"];
                    list.address = dic[@"address"];
//                    list.agent_id = dic[@"agent_id"];
                    list.baths = dic[@"bath"];
                    list.beds = dic[@"bed"];
                    list.desc = dic[@"description"]?dic[@"description"]:@"";
                    list.creatDate = dic[@"created"];
                    list.price = dic[@"price"];

                    list.listCate = dic[@"type"];
                    list.zipCode = dic[@"zip"];
                    
                    list.size = dic[@"size"];
                    list.sizeUnit = dic[@"size_unit"];
                    
                    list.id_ = dic[@"property_id"];
                    list.user_id = [GlobalInstance instance].user_id;
                    NSString * str = [dic[@"status"] lowercaseString];
                    NSString * state = listSellDic[str];
                    list.sellState = state;
                }
            }];
            [[CoredataManager manager] saveContext];

            id obj_ = responseDic[@"data"][@"pictures"][@"picture"];
            
            if (obj) {
                NSArray * array_;
                if ([obj_ isKindOfClass:[NSDictionary class]]) {
                    array_ = @[obj_];
                }else{
                    array_ = obj_;
                }
                
                [array_ enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    
                    NSDictionary * info = (NSDictionary *)obj;
                    
                      NSArray * pices = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"id_='%@'",info[@"picture_id"]]];
                    if (!pices || pices.count ==0) {
                        Picture * pic = (Picture *)[[CoredataManager manager] newObjectFromEntity:@"Picture"];
                        pic.id_ = info[@"picture_id"];
                        pic.hash_code = info[@"hash_code"];
                        pic.order = info[@"order"];
                        pic.localPath = [[[LocalFileManager manager] photoFold] stringByAppendingPathComponent:pic.id_];
                        pic.localPath_small = [[[LocalFileManager manager] photoFoldSmall] stringByAppendingPathComponent:pic.id_];
                        NSArray * listArray = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"id_='%@'",info[@"property_id"]]];
                        if (listArray && listArray.count != 0) {
                            List * list = listArray[0];
                            [list addPictureObject:pic];
                            pic.list = list;
                        }
                    }
                }];
            }
            [self.delegate getListForSVFinish];
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_list"];
}



-(void)syncPictureDataFromSV:(Picture *)pic{
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSData *data = [[NSData alloc]initWithBase64EncodedString:operation.responseString options:0];
        UIImage * image = [UIImage imageWithData:data];
        if (image) {
            
            // origin pic
            UIImage * img;
            if (img.size.width > 1000) {
                float ratio = 1000.0 / image.size.width;
                img = [Utility imageWithImage:image scaledToSize:CGSizeMake(image.size.width * ratio, image.size.height*ratio)];
            }else{
                img = image;
            }
            
            NSData * data = UIImageJPEGRepresentation(img, 0.5);
            
            [[NSFileManager defaultManager] createFileAtPath:pic.localPath contents:data attributes:nil];
            
            
            // small pic save

            UIImage * img_small;
            
            if (img.size.width > 200) {
                float ratio = 200.0 / image.size.width;
                img_small = [Utility imageWithImage:image scaledToSize:CGSizeMake(image.size.width*ratio, image.size.height*ratio)];
            }else{
                img_small = img;
            }
            
            NSData * data_small = UIImageJPEGRepresentation(img_small, 1);
            [[NSFileManager defaultManager] createFileAtPath:pic.localPath_small contents:data_small attributes:nil];
        }
        [self.delegate syncPictureDataFromSVFinish];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    NSDictionary * dic = @{@"hash_code":pic.hash_code};
    NSMutableDictionary * para = [self paramsDic:dic];
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_picture_get"];
}



-(void)updataProperty:(NSArray *)lists{

    NSMutableArray * temp  = [NSMutableArray array];
    
    NSDictionary * listSellDic = @{@"1":@"for sale",@"2":@"in contract",@"3":@"sold",@"4":@"short sale",@"5":@"rental",@"6":@"rented",@"7":@"auction"};
    
    [lists enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        List * list = (List *)obj;
        NSDictionary  * dic;
        if ([list.id_ intValue] < ListBaseId+1) {
            
            // add
             dic=@{
                   @"size":list.size == nil ? @"" : list.size,
                   @"size_unit":list.sizeUnit == nil ? @"" : list.sizeUnit,
                   @"address":list.address == nil ? @"" : list.address,
                   @"zip":list.zipCode == nil ? @"" : list.zipCode,
                   @"status":listSellDic[list.sellState] == nil ? @"" : listSellDic[list.sellState],
                   @"price":list.price == nil ? @"" : list.price,
                   @"type":list.listCate == nil ? @"" : list.listCate,
                   @"bed":list.beds == nil ? @"" : list.beds,
                   @"bath":list.baths == nil ? @"" : list.baths,
                   @"description":list.desc == nil ? @"" : list.desc,
                   @"extra":list.extra == nil ? @"" : list.extra,
                   };
        }else{
            // updata
            dic=@{
                  @"size":list.size == nil ? @"" : list.size,
                  @"size_unit":list.sizeUnit == nil ? @"" : list.sizeUnit,
                  @"address":list.address == nil ? @"" : list.address,
                  @"zip":list.zipCode == nil ? @"" : list.zipCode,
                  @"status":listSellDic[list.sellState] == nil ? @"" : listSellDic[list.sellState],
                  @"price":list.price == nil ? @"" : list.price,
                  @"type":list.listCate == nil ? @"" : list.listCate,
                  @"bed":list.beds == nil ? @"" : list.beds,
                  @"bath":list.baths == nil ? @"" : list.baths,
                  @"description":list.desc == nil ? @"" : list.desc,
                  @"property_id":list.id_ == nil ? @"" : list.id_,
                  };

        }
        [temp addObject:dic];
    }];
    
    NSString * str_ =  [self JsonFromId:temp];
    NSDictionary * para = [self paramsDic:@{@"properties":str_}];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        if ([responseDic allKeys].count ==2) {
            [self.delegate updataPropertyFinsih];
            return ;
        }
        if ([self checkResponse:responseDic]) {
            id obj = responseDic[@"properties"][@"property"];
            NSArray * array;
            if ([obj isKindOfClass:[NSDictionary class]]) {
                array = @[obj];
            }else if(obj){
                array = obj;
            }
            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary * dic = (NSDictionary *)obj;
                if (dic[@"extra"]) {
                    // new
                    NSArray * lists = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"extra='%@'",dic[@"extra"]]];
                    if (lists && lists.count!=0) {
                        List * list = lists[0];
                        list.needSync = nil;
                        list.id_ = dic[@"property_id"];
                        list.creatDate = dic[@"created"];
                        list.extra = nil;
                    }
                }else{
                    // updata
                    NSArray * lists = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"id_='%@'",dic[@"property_id"]]];
                    if (lists && lists.count!=0) {
                        List * list = lists[0];
                        list.needSync = nil;
                    }
                }
            }];
            [[CoredataManager manager] saveContext];
            [self.delegate updataPropertyFinsih];
        }
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_update"];
}

// about lead

- (void) syncLeadToServer:(NSArray *)leads{
    
    NSString *infoJsonStr = [self JsonFromId: leads];

    NSDictionary * info = @{@"leads": infoJsonStr};
    NSMutableDictionary * para = [self paramsDic:info];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        if ([self checkResponse:responseDic]) {
            
            id obj = responseDic[@"leads"][@"lead"];
            NSArray * array;
            if ([obj isKindOfClass:[NSDictionary class]]) {
                array = @[obj];
            }else{
                array = obj;
            }
            [self.delegate syncLeadToServerFinish:array];
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_leads_add"];
}

- (void) syncLeadFromServer {
    NSMutableDictionary *param = [self paramsDic:nil];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        EFLog(@"%@", responseDic);
        
        if ([responseDic allKeys].count ==2) {
            [self.delegate syncLeadFromSVFinish];
            return ;
        }
        NSArray * leads;
        id obj = responseDic[@"leads"][@"leads"][@"lead"];
        if ([obj isKindOfClass:[NSArray class]]) {
            leads = obj;
        }else if(obj){
            leads = @[obj];
        }
        
        NSArray * questions;
        id obj_  = responseDic[@"leads"][@"questions"][@"question"];
        if ([obj_ isKindOfClass:[NSArray class]]) {
            questions = obj_;
        }else if(obj_){
            questions = @[obj_];
        }
        
        NSString * path = [[NSBundle mainBundle] pathForResource:@"AddressState" ofType:@"plist"];
        NSArray * stateArray = [NSArray arrayWithContentsOfFile:path];
        
        [leads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary * dic = (NSDictionary *)obj;
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"lead_id='%@'",dic[@"lead_id"]]];
            if (!array || array.count ==0) {
                Lead * lead = (Lead *)[[CoredataManager manager] newObjectFromEntity:@"Lead"];
                
                lead.created = dic[@"created"];
                lead.email = dic[@"email"];
                lead.first_name = dic[@"first_name"];
                lead.last_name = dic[@"last_name"];
                lead.lead_id = dic[@"lead_id"];
                lead.user_id = dic[@"user_id"];
                lead.phone = dic[@"phone"];
                lead.address = dic[@"street"];
                lead.zipCode = dic[@"zip"];
                lead.phone = dic[@"phone"];
                lead.brithday = dic[@"dob"];
                lead.city = dic[@"city"];
                
                
                NSString * state = dic[@"state"];
                if (state) {
                    [stateArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSDictionary * dic = (NSDictionary *)obj;
                        if ([dic[@"value"] isEqualToString:state]) {
                            lead.state = dic[@"appear"];
                            * stop = YES;
                        }
                    }];
                }
                
                lead.agent_id = [GlobalInstance instance].user_id;
                
                NSDictionary * noteDic  = dic[@"notes"];
                id  notes_obj = noteDic[@"note"];
                
                NSArray * notes;
                if ([notes_obj isKindOfClass:[NSArray class]]) {
                    notes = notes_obj;
                }else if (notes_obj){
                    notes = @[notes_obj];
                }
                
                [notes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary * dic_ = (NSDictionary *)obj;
                    
                    Lead_Note * note = (Lead_Note *)[[CoredataManager manager] newObjectFromEntity:@"Lead_Note"];
                    NSString * creatTime = dic_[@"created"];
                    double intervalFrom1970 = [Utility timeintervalForDate:[Utility dateFromDateStr:creatTime withFormat:@"yyyy-MM-dd HH:mm:ss"]];
                    note.creat = [NSString stringWithFormat:@"%f",intervalFrom1970];
                    note.lead_id = dic_[@"crm_id"];
                    note.extra = nil;
                    note.note_id = dic_[@"note_id"];
                    note.note = dic_[@"notes"];
                    
                    
                    [lead addNotesObject:note];
                    [[CoredataManager manager] saveContext];
                }];
                
                
                NSArray * lists = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"id_='%@'",dic[@"property_id"]]];
                if (lists && lists.count !=0) {
                    lead.list = lists[0];
                }
            }
        }];
        
        [[CoredataManager manager] saveContext];
        
        
        NSMutableDictionary * qDic = [NSMutableDictionary dictionary];
        
        
        [questions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary * dic = (NSDictionary *)obj;
            
             NSArray * qs = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"question_id='%@'",dic[@"question_id"]]];
            if (!qs || qs.count == 0) {
                return ;
            }
            NSArray * lqs = [[CoredataManager manager] allNodeForEntity:@"LeadQuestion" withPredicate:[NSString stringWithFormat:@"questionId='%@' and lead.lead_id='%@'",dic[@"question_id"],dic[@"lead_id"]]];
            if (!lqs || lqs.count == 0) {
                LeadQuestion * lq = (LeadQuestion *)[[CoredataManager manager] newObjectFromEntity:@"LeadQuestion"];
                
                if ([dic[@"answer_id"] intValue] == 0) {
                    lq.answerId =0;
                    lq.answer = dic[@"answer_value"];
                    lq.answerValue = dic[@"answer_value"];

                }else{
                    NSArray * answers = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",dic[@"answer_id"]]];
                    if (answers && answers.count !=0) {
                        Answer * a = answers[0];
                        lq.answerId =a.answer_id;
                        lq.answer = a.answer_option;
                        lq.answerValue = a.answer_value;
                    }
                }
                Question * q = qs[0];
                lq.title = q.question;
                lq.questionId = dic[@"question_id"];
                
                NSString * leadID = dic[@"lead_id"];
                NSMutableArray * temp = qDic[leadID];
                if (!temp) {
                    temp = [NSMutableArray array];
                    [temp addObject:lq];
                    [qDic setObject:temp forKey:leadID];
                }else{
                    NSMutableArray * array = [NSMutableArray arrayWithArray:temp];
                    [array addObject:lq];
                    [qDic setObject:array forKey:leadID];
                }
            }
        }];
        
        [[qDic allKeys] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * leadId = (NSString *)obj;

            NSArray * leads = [[CoredataManager manager] allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"lead_id='%@'",leadId]];
            if (leads && leads.count !=0) {
                Lead * lead = leads[0];
                NSArray * lq = qDic[leadId];
                [lead addLeadQ:[NSSet setWithArray:lq]];
            }
        }];
        
        [[CoredataManager manager] saveContext];
        
        [self.delegate syncLeadFromSVFinish];

    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: param
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_leads_list"];
}

-(void) updataLeadToServer:(NSArray *)leads{
    NSString *infoJsonStr = [self JsonFromId: leads];
    NSDictionary * info = @{@"leads": infoJsonStr};
    NSMutableDictionary * para = [self paramsDic:info];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        [self.delegate updataLeadToServerFinish:responseDic[@"status_code"]];
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_leads_update"];
}

-(void)addLeadNote:(NSArray *)notes{

    NSString * infoJson = [self JsonFromId:notes];
    NSDictionary * para = [self paramsDic:@{@"notes":infoJson}];
    
    //    NSLog(@"%@",para);
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        NSLog(@"%@",responseDic);
        if ([responseDic allKeys].count ==2) {
            if ([self.delegate respondsToSelector: @selector(getQuestionFinish)]) {
                [self.delegate getQuestionFinish];
            }
            return ;
        }
        if ([responseDic isKindOfClass: [NSDictionary class]]) {
            if ([self checkResponse:responseDic]) {
            
                NSArray * notes;
                id obj = responseDic[@"notes"][@"note"];
                if ([obj isKindOfClass:[NSArray class]]) {
                    notes = obj;
                }else if(obj){
                    notes = @[obj];
                }
                [notes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary * dic = (NSDictionary *)obj;
                    NSString * extra = dic[@"extra"];
                    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Lead_Note" withPredicate:[NSString stringWithFormat:@"extra='%@'",extra]];
                    if (array && array.count !=0) {
                        Lead_Note * note = array[0];
                        note.creat  = dic[@"created"];
                        note.note_id = dic[@"note_id"];
                        note.extra = nil;
                        [[CoredataManager manager] saveContext];
                    }
                }];
            }
           
        }
        [self.delegate syncNoteToSeverFinish];
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        DFLog(@"response: %@", operation.responseString);
        if ([self.delegate respondsToSelector: @selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"lead_note_creat"];
}

//  about question

-(void)getQuestonFromSV:(NSDictionary *)info{
    
    NSDictionary * para = [self paramsDic:info];
    
    //    NSLog(@"%@",para);
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        
        
        if ([responseDic allKeys].count ==2) {
            if ([self.delegate respondsToSelector: @selector(getQuestionFinish)]) {
                [self.delegate getQuestionFinish];
            }
            return ;
        }
        
        if ([responseDic isKindOfClass: [NSDictionary class]]) {
            if ([self checkResponse:responseDic]) {
                NSDictionary * dic = responseDic[@"data"];
                
                NSArray * answers_obj = dic[@"answers"][@"answer"];
                NSArray * questions_obj = dic[@"questions"][@"question"];
                
                NSArray * answers;
                if ([answers_obj isKindOfClass:[NSArray class]]) {
                    answers = answers_obj;
                }else if (answers_obj){
                    answers = @[answers_obj];
                }
                
                NSMutableDictionary * answerDic= [NSMutableDictionary dictionary];
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary * dic = (NSDictionary *)obj;
                    Answer * a = (Answer *)[[CoredataManager manager] newObjectFromEntity:@"Answer"];
                    a.answer_id = dic[@"answer_id"];
                    a.question_id = dic[@"question_id"];
                    a.answer_option = dic[@"answer_option"];
                    a.answer_value = dic[@"answer_value"];
                    a.order = dic[@"order"];
                    
                    NSMutableArray * array = answerDic[a.question_id];
                    if (!array) {
                        array = [NSMutableArray array];
                    }
                    [array addObject:a];
                    [answerDic setObject:array forKey:a.question_id];
                }]; 
                
                
                NSArray * questions;
                if ([questions_obj isKindOfClass:[NSArray class]]) {
                    questions = questions_obj;
                }else if (questions_obj){
                    questions = @[questions_obj];
                }
                
                __block int sortNum=3;
                [questions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    Question * q = (Question *)[[CoredataManager manager] newObjectFromEntity:@"Question"];
                    NSDictionary * dic = (NSDictionary *)obj;
                    q.answer_type =  dic[@"answer_type"];
                    q.config = dic[@"config"];
                    q.global_name = dic[@"global_name"];
                    q.is_saved_answer = dic[@"is_saved_answer"];
                    q.question = dic[@"question"];
                    q.question_id = dic[@"question_id"];
                    q.user_id = dic[@"user_id"];

                    q.tool_tip = dic[@"tool_tip"]?dic[@"tool_tip"]:@"";
                    q.extra = nil;
                    // sort num
                    
                    if ([q.question isEqualToString:@"First Name"]) {
                        q.sortNum = @"0";
                        q.is_required = @"1";
                        q.is_active = @"1";
                    }else if ([q.question isEqualToString:@"Last Name"]){
                        q.sortNum = @"1";
                        q.is_required = @"1";
                        q.is_active = @"1";
                    }else if ([q.question isEqualToString:@"Email"]){
                        q.sortNum = @"2";
                        q.is_required = @"1";
                        q.is_active = @"1";
                    }else{
                        q.sortNum = [NSString stringWithFormat:@"%d",sortNum];
                        sortNum ++;
                        q.is_required = @"0";
                        q.is_active = @"0";
                    }
                    
                    if(![q.user_id isEqualToString:@"0"]){
                    
                        if ([dic[@"is_active"] isEqualToString:@"1"]) {
                            q.is_active = @"1";
                        }
                    }
                    
                    NSArray * array = answerDic[q.question_id];
                    [q addAnswer:[NSSet setWithArray:array]];
                }];
                
                [[CoredataManager manager] saveContext];
                
                if ([self.delegate respondsToSelector: @selector(getQuestionFinish)]) {
                    [self.delegate getQuestionFinish];
                }
            }else{
                if ([self.delegate respondsToSelector: @selector(getQuestionFinish)]) {
                    [self.delegate getQuestionFinish];
                }
            }
        }
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        DFLog(@"response: %@", operation.responseString);
        if ([self.delegate respondsToSelector: @selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    [self newRequestWithParameters:para
         constructingBodyWithBlock:nil success:succeedBlock failure:failureBlock apiName:@"agent_questions"];
}

-(void)submitQuestionToSV:(NSArray *)questons{
    
    NSMutableArray * temp = [NSMutableArray array];
    
    [questons enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        Question * q = (Question *)obj;
        
        NSArray * answers = [q.answer allObjects];
        NSMutableArray * temp_anser = [NSMutableArray array];
        [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Answer * a = (Answer *)obj;
            NSDictionary * dic;
            if(a.extra ){
                //  add
                dic  = @{@"answer_option":a.answer_option,
                         @"answer_value":a.answer_option,
                         @"extra":a.extra,
                         @"order":a.order};
            }else{
                // updata
                dic = @{@"answer_id":a.answer_id,
                        @"question_id":a.question_id,
                        @"answer_option":a.answer_option,
                        @"answer_value":a.answer_option,
                        @"order":a.order};
                        }
            [temp_anser addObject:dic];
        }];
        
        
        NSDictionary * dic;
        if ([q.question_id intValue] > QuestionBaseId) {
            //  updata
            if(temp_anser.count == 0){
                dic = @{
                        @"question_id":q.question_id,
//                        @"user_id":[GlobalInstance instance].user_id,
                        @"question":q.question,
                        @"answer_type":q.answer_type,
                        @"global_name":@"",
                        @"tool_tip":q.tool_tip,
                        @"config":@"",
                        @"is_active":q.is_active,
                        };
            }else{
                dic = @{
                        @"question_id":q.question_id,
//                        @"user_id":[GlobalInstance instance].user_id,
                        @"question":q.question,
                        @"answer_type":q.answer_type,
                        @"global_name":@"",
                        @"tool_tip":q.tool_tip,
                        @"config":@"",
                        @"is_active":q.is_active,
                        @"answers":temp_anser,
                        };
            }
        }else{
            // add
            if(temp_anser.count == 0){
                dic = @{
//                        @"user_id":[GlobalInstance instance].user_id,
                        @"question":q.question,
                        @"answer_type":q.answer_type,
                        @"global_name":@"",
                        @"tool_tip":@"",
                        @"config":@"",
                        @"extra":q.extra,
                        @"is_active":q.is_active,
                        };
            }else{
                dic = @{
//                        @"user_id":[GlobalInstance instance].user_id,
                        @"question":q.question,
                        @"answer_type":q.answer_type,
                        @"global_name":@"",
                        @"tool_tip":@"",
                        @"config":@"",
                        @"extra":q.extra,
                        @"is_active":q.is_active,
                        @"answers":temp_anser,
                        };
            }
        }
        [temp addObject:dic];
    }];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        id obj = responseDic[@"questions"][@"question"];
        NSArray * qs;
        if ([obj isKindOfClass:[NSArray class]]) {
            qs = obj;
        }else if(obj){
            qs = @[obj];
        }
        
        [qs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary * dic = (NSDictionary *)obj;
            NSArray * qs;
            
            if (dic[@"extra"]) {
                // new
                qs = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"extra='%@'",dic[@"extra"]]];
            }else{
                //updata
                qs = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"question_id='%@'",dic[@"question_id"]]];
            }
            if (qs && qs.count != 0) {
                Question * q = qs[0];
                q.question_id =  [NSString stringWithFormat:@"%d",[dic[@"question_id"] intValue]];
                q.extra = @"-1";
                
                id aobj = dic[@"answers"][@"answer"];
                NSArray * answers ;
                if ([aobj isKindOfClass:[NSArray class]]) {
                    answers = aobj;
                }else if (aobj){
                    answers = @[aobj];
                }
                
                [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSDictionary * dic_a = (NSDictionary *)obj;
                    if (dic_a[@"extra"]) {
                        NSArray * as = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"extra='%@'",dic_a[@"extra"]]];
                        if (as && as.count != 0) {
                            Answer * a = as[0];
                            a.answer_id = dic_a[@"answer_id"];
                            a.extra = nil;
                            a.order = dic_a[@"order"];
                            a.question_id = dic_a[@"question_id"];

                        }
                    }else{
                        NSArray * as = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",dic_a[@"answer_id"]]];
                        if (as && as.count != 0) {

                            // nothing to do
                        }
                    }
                }];
                [[CoredataManager manager] saveContext];
            }
        }];

        [self.delegate submitQuestionToSVFinish];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    NSString * str_ =  [self JsonFromId:temp];
    NSDictionary * para = [self paramsDic:@{@"questions":str_}];
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_question_update"];
}

-(void)deleteAnswer:(NSArray *)answers{
    
    NSMutableArray * array = [NSMutableArray array];
    [answers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Answer * a = (Answer *)obj;
        [array addObject:a.answer_id];
    }];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        id obj = responseDic[@"answerIDs"][@"answerID"];
        NSArray * as;
        if ([obj isKindOfClass:[NSArray class]]) {
            as = obj;
        }else if(obj){
            as = @[obj];
        }
        [as enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * str = (NSString *)obj;
           
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"answer_id='%@'",str]];
            if (array && array.count != 0) {
                Answer * a = array[0];
                [[CoredataManager manager] deleteObject:a];
            }
        }];
        [self.delegate deleAnswerFinish];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    NSString * str_ =  [self JsonFromId:array];
    NSDictionary * para = [self paramsDic:@{@"answers":str_}];
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_answers_delete"];

}


-(void)deletePicture:(NSArray *)pictures{

    NSMutableArray * temp = [NSMutableArray array];
    [pictures enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * pic = (Picture *)obj;
        [temp addObject:pic.id_];
    }];
    
    NSString * str = [self JsonFromId:temp];
    NSDictionary * para = [self paramsDic:@{@"pictures":str}];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        id obj = responseDic[@"pictures"][@"picture"];
        
        NSArray * pics;
        if ([obj isKindOfClass:[NSArray class]]) {
            pics = obj;
        }else if(obj){
            pics = @[obj];
        }
        [pics enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
           
            NSString * picId = (NSString *)obj;
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"id_='%@'",picId]];
            if (array && array.count !=0) {
                Picture * pic = array[0];
                [[CoredataManager manager] deleteObject:pic];
            }
        }];
        [[CoredataManager manager] saveContext];
        [self.delegate deletePictureFinish];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_picture_delete"];
}


-(void)creatPicture:(NSArray *)picture{
    

    _picCreatArray = picture;
    
    if (!picManage) {
        picManage = [[PictureManage alloc] init];
        picManage.delegate = self;
        indexPic = 0;
    }
    if (indexPic < _picCreatArray.count) {
        Picture  * pic = _picCreatArray[indexPic];
        indexPic ++;
        if ([pic.isSyncing intValue] == 1) {
            [self creatSignPictureFinish];
        }else{
            pic.isSyncing = @"1";
            [[CoredataManager manager] saveContext];
            [picManage creatPicture:pic];
        }
    }else{
        indexPic = 0;
        [self.delegate creatPictureFinish];
    }
}

-(void)updataPicture:(NSArray *)picture{

    NSMutableArray * temp = [NSMutableArray array];
    [picture enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Picture * pic = (Picture *)obj;
        NSDictionary * dic = @{@"picture_id":pic.id_,@"order":pic.order};
        [temp addObject:dic];
    }];
    NSString * info = [self JsonFromId:temp];
    
    NSDictionary * para = [self paramsDic:@{@"pictures":info}];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        
        [self.delegate updataPictureFinish];
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_picture_update"];
}

-(void)verifyEmail:(NSString *)email{

    NSDictionary * para = [self paramsDic:@{@"email":email}];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        NSString * str = responseDic[@"is_available"];
        if ([str intValue] == 1) {
            [self.delegate verifyEmailFinsih:YES msg:nil];
        }else{
            NSString * msg = responseDic[@"msg"];
            [self.delegate verifyEmailFinsih:NO msg:msg];
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_lead_email_verify"];

}

-(void)leadEmialSendSetting:(NSDictionary *)info{

    NSString * str = [self JsonFromId:info];
    NSDictionary * para = [self paramsDic:@{@"preference":str}];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        
        if ([responseDic[@"status_code"] intValue] == 1) {
            [self.delegate leadEmialSendSettingFinish:responseDic[@"preference"]];
        }else{
            [self.delegate systemError];
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"update_agent_preference"];
}

-(void)agentSignatureInfo{

    NSDictionary * para = [self paramsDic:nil];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        if ([responseDic[@"status_code"] intValue] == 1) {
            NSDictionary * dic = responseDic[@"preference"];
            
            NSMutableDictionary * temp = [NSMutableDictionary dictionary];
            NSDictionary * info = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
            if (info) {
                [temp addEntriesFromDictionary:info];
            }
            
            if (dic[@"thankyou_email"]) {
                [temp setObject:dic[@"thankyou_email"] forKey:@"isAutoSend"];
            }
            
            if (dic[@"signature"]) {
                [temp setObject:dic[@"signature"] forKey:@"signature"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:temp forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self.delegate agentSignatureInfoFinish];
        }else{
            
            [self.delegate systemError];
        }
    };
    
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: nil
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_preference_get"];
}


#pragma mark --   PictureManageDeleaget


-(void)creatSignPictureFinish{

    [self creatPicture:_picCreatArray];
}

@end
