//
//  LocalFileManager.h
//
//  Created by Michael on 11/30/11.
//  Copyright AMP 2011. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kLoginDefault   @"LoginDefault"

#define KHasSubmitAPNToken @"hasSubmitAPNToken"
#define KAPNToken       @"apn_token"

@interface LocalFileManager : NSObject {
//    NSString *docPath_;
    NSString *libPath_;
    NSString *libCachePath_;
    NSString *libPrivatePath_;
    NSString *tempPath_;
    NSString *imgFolder_;
    NSFileManager *fileA_;
    NSUserDefaults *ud;
}

+(LocalFileManager *)manager;
-(void)setUp;

-(NSString *)DBPath;



-(NSString *)photoFold;
-(NSString *)photoFoldSmall;
-(NSString *)userPlistPath;  //  user contact file

-(NSString *)leadCSVPath;

@end
