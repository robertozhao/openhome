//
//  Manager.m
//  Alumina
//
//  Created by AMP on 2/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "Manager.h"

@implementation Manager


//- (id) init {
//    if (self = [super init]) {
//        return self;
//    }
//    return nil;
//}

- (void) smash {
    for (AFHTTPRequestOperation *request in requests) {
        [request cancel];
    }
    self.delegate = nil;
}

- (AFHTTPRequestOperation *) newRequestWithParameters: (id)parameters
                            constructingBodyWithBlock: (void (^)(id <AFMultipartFormData> formData))block
                                              success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
                                              failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
                                              apiName: (NSString*) apiName
{
    NSString * Server_Address;
#ifdef Production
    Server_Address = @"https://agents.scoreapprove.com/api/score_approve/";
#else
    Server_Address =  @"http://agents.scoreapprove.com/api/score_approve/";
#endif
    
    NSString *urlStr = [NSString stringWithFormat: @"%@%@", Server_Address, apiName];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    AFHTTPRequestOperation *operation = [manager POST: urlStr
                                           parameters: parameters
                            constructingBodyWithBlock: block
                                              success: successBlock
                                              failure: failureBlock];
    
    
    
    if (requests == nil) {
        requests
        = [[NSMutableArray alloc] init];
    }
    [requests addObject: operation];
    return operation;
}

- (NSMutableDictionary *) paramsDic:(NSDictionary *)info
{
    NSMutableDictionary *paramDic;
    if (info) {
        paramDic     = [NSMutableDictionary dictionaryWithDictionary:info];
    }else{
        paramDic     = [NSMutableDictionary dictionary];
    }
    

    paramDic[@"X-API-KEY"] = X_API_KEY;
    if(LoginTypeNone != [[GlobalInstance instance] loginType]) {
        paramDic[@"username"] = [[GlobalInstance instance] username];
        paramDic[@"password"] = [[GlobalInstance instance] password];
    }
    return paramDic;
}

- (BOOL) checkResponse: (NSDictionary *) responseDic
{
    if ([responseDic isKindOfClass: [NSDictionary class]])
    {
        if ([responseDic[@"status_code"] intValue] == 1)
        {
            return YES;
            
           
        }else if([responseDic[@"status_code"] intValue] == 0){
            //invalid user auth info, prompt user login
            [self.delegate needUserLogin:responseDic[@"error_msg"]];
            return NO;
        }else{
            return NO;
        }
    }
    return NO;
}

- (NSDictionary*) parseResponse: (NSString*) responseStr
{
    XMLResponseParser *parse = [XMLResponseParser parserWithXML: responseStr];
    NSDictionary *responseDic = [parse convertToDictionary];
    return responseDic;
}

- (NSString *) legalString: (id) obj
{
    return [Utility legalString: obj];
}

- (void) showErrorMsg: (NSString *) msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: msg
                                                    message: nil
                                                   delegate: nil
                                          cancelButtonTitle: ButtonTitleDismiss
                                          otherButtonTitles: nil];
    [alert show];
}

- (void) requestFailed {
    if ([self.delegate respondsToSelector: @selector(systemError)]) {
        [self.delegate systemError];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Unable to connect. Please try again later, or report issue to support.", @"timeout alert info")
                                                    message: nil
                                                   delegate: nil
                                          cancelButtonTitle: NSLocalizedString(@"Dimiss", @"dismiss text")
                                          otherButtonTitles: nil];
    [alert show];
}

- (NSString*) JsonFromId: (id) obj {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject: obj
                                                       options: NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        EFLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding: NSUTF8StringEncoding];
        return jsonString;
    }
    return nil;
}

- (id) objFromJson: (NSString*) jsonStr {
    
    NSData * jsonData = [jsonStr dataUsingEncoding: NSUTF8StringEncoding];
    NSError * error=nil;
    NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    return parsedData;
}

@end