//
//  PictureManage.m
//  ScoreApprove
//
//  Created by Yang on 15/5/17.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "PictureManage.h"
#import "CoredataManager.h"
#import "List.h"
#import "DataManage.h"

@implementation PictureManage
@dynamic delegate;

-(void)creatPicture:(Picture *)pic{

    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        if ([self.delegate respondsToSelector:@selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        DFLog(@"%@", responseDic);
        
        NSDictionary * dic = responseDic[@"picture"];
        if (nil ==  dic || ![dic isKindOfClass:[NSDictionary class]]) {
            [self.delegate creatSignPictureFinish];
            return ;
        }
        pic.extra = nil;
        pic.id_ =dic[@"picture_id"];
        pic.order = dic[@"order"];
        pic.isSyncing = nil;
        pic.hash_code = dic[@"hash_code"];
        [[CoredataManager manager] saveContext];
        
        [self.delegate creatSignPictureFinish];
    };

    NSDictionary * info = @{@"property_id":pic.list.id_,@"order":pic.order};
    NSString * info_str = [self JsonFromId:info];
    NSDictionary * para = [self paramsDic:@{@"picture_info":info_str}];
    NSData * imageData = UIImagePNGRepresentation([UIImage imageWithContentsOfFile:pic.localPath]);
    
    if(!imageData){
        [self.delegate creatSignPictureFinish];
        return;
    }
    
    [self newRequestWithParameters: para
         constructingBodyWithBlock: ^(id<AFMultipartFormData> formData) {
             [formData appendPartWithFileData:imageData name:@"picture" fileName:@"picture" mimeType: @"image/jpeg"];
         }
                           success: succeedBlock
                           failure: failureBlock
                           apiName: @"agent_property_picture_create"];
}






-(void)newPictureToSync:(Picture *)pic{

    NSDictionary * info = @{@"picID":pic.id_};
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(syncPic:) userInfo:info repeats:NO];
}


-(void)syncPic:(NSTimer *)timer{

    NSDictionary * dic = timer.userInfo;
    NSString * picID = dic[@"picID"];
    
    NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"id_='%@'",picID]];
    if (array && array.count != 0) {
        Picture * pic = array[0];
        if ([pic.id_ intValue]< 0) {
            [[DataManage instance] creatPicture:@[pic]];
        }else if ([pic.extra intValue] == -1){
            [[DataManage instance] updataPicture:@[pic]];
        }
    }
}

@end
