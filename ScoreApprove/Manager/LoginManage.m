//
//  LoginManage.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/20.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "LoginManage.h"

@implementation LoginManage
@dynamic delegate;

-(void)login:(NSDictionary *)info{
 
    NSDictionary * para = [self paramsDic:info];
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        if ([responseDic isKindOfClass: [NSDictionary class]]) {
            if ([self checkResponse:responseDic]) {

                if ([self.delegate respondsToSelector: @selector(loginFinish:errorMsg:)]) {
                    
                    NSMutableDictionary * temp = [NSMutableDictionary dictionary];
                    if (responseDic[@"user_data"]) {
                        [temp setObject:responseDic[@"user_data"] forKey:@"userInfo"];
                    }

                    if (responseDic[@"preference"]) {
                        [temp setObject:responseDic[@"preference"] forKey:@"preference"];
                    }
                    
                    [self.delegate loginFinish:temp errorMsg:nil];
                }
            }else{
                if ([self.delegate respondsToSelector: @selector(loginFinish:errorMsg:)]) {
                    [self.delegate loginFinish:nil errorMsg:responseDic[@"error_msg"]];
                }
            }
        }else{
            [self.delegate loginFinish:nil errorMsg:@"Request Faile"];
        }
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        DFLog(@"response: %@", operation.responseString);
        if ([self.delegate respondsToSelector: @selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    [self newRequestWithParameters:para
         constructingBodyWithBlock:nil success:succeedBlock failure:failureBlock apiName:@"agent_login"];
}


-(void)toRegister:(NSDictionary *)dic{
    
    NSString *infoJsonStr = [self JsonFromId: dic];
    NSDictionary * info = @{@"user_info": infoJsonStr};
    NSDictionary * para = [self paramsDic:info];
    
    void (^succeedBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSDictionary *responseDic = [self parseResponse: operation.responseString];
        if ([responseDic isKindOfClass: [NSDictionary class]]) {
            if ([responseDic[@"status_code"] integerValue] == 1) {
                if ([self.delegate respondsToSelector: @selector(registerFinish:errorMsg:)]) {
                    [self.delegate registerFinish:responseDic[@"user_id"] errorMsg:nil];
                }
            }else{
                if ([self.delegate respondsToSelector: @selector(registerFinish:errorMsg:)]) {
                    [self.delegate registerFinish:nil errorMsg:responseDic[@"error"]];
                }
            }
        }
    };
    void (^failureBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        EFLog(@"Error: %@", error);
        EFLog(@"response: %@", operation.responseString);
        if ([self.delegate respondsToSelector: @selector(systemError)]) {
            [self.delegate systemError];
        }
    };
    
    
    
    [self newRequestWithParameters:para
         constructingBodyWithBlock:nil success:succeedBlock failure:failureBlock apiName:@"agent_sign_up"];
}


@end
