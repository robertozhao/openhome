//
//  PhotoDownloadCenter.h
//  ScoreApprove
//
//  Created by Yang on 15/5/29.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Picture.h"
#import "SyncManage.h"

@interface PhotoDownloadCenter : NSObject<SyncManageDelegate>{

    SyncManage * manage;
    NSMutableArray * taskArray;
}
@property (nonatomic , strong) Picture * cur_task;

+(PhotoDownloadCenter *)instance;


@end
