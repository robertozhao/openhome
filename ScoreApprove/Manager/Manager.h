//
//  Manager.h
//  Alumina
//
//  Created by Score Approve on 2/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "constants.h"
#import "TextResource.h"
#import "Macro.h"
#import "LocalFileManager.h"
#import "Utility.h"
#import "AFHTTPRequestOperationManager.h"
#import "XMLResponseParser.h"
#import "GlobalInstance.h"

@protocol ManagerDelegate <NSObject>
@optional
- (void) needUserLogin:(NSString *)msg;
- (void) systemError;
@end


@interface Manager : NSObject {
    NSMutableArray *requests;

}

@property (nonatomic, weak) id<ManagerDelegate>delegate;

- (NSString*) JsonFromId: (id) obj;
- (id) objFromJson: (NSString*) jsonStr;
- (AFHTTPRequestOperation *) newRequestWithParameters: (id)parameters
                            constructingBodyWithBlock: (void (^)(id <AFMultipartFormData> formData))block
                                              success: (void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock
                                              failure: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock
                                              apiName: (NSString*) apiName;

- (BOOL) checkResponse: (NSDictionary *) responseDic;
- (NSDictionary*) parseResponse: (NSString*) responseStr;
- (NSMutableDictionary *) paramsDic:(NSDictionary *)info;
//- (NSDictionary*) testerProfile;
- (void) smash;
- (NSString *) legalString: (id) obj;
- (void) showErrorMsg: (NSString *) msg;



@end
