//
//  DataManage.h
//  ScoreApprove
//
//  Created by Score Approve on 15/3/3.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "List.h"

#import "LeadQuestion.h"
#import "TextResource.h"

#import "constants.h"
#import "SyncManage.h"



@protocol DataManageDelegate <NSObject>

-(void)beginToSync:(NSString *)text;
-(void)finishSync;


@end
@interface DataManage : NSObject<SyncManageDelegate>{
    
    SyncManage * syncManage;

    // about sync
    int postNum;

}

@property      dispatch_queue_t dataQueue;
@property (nonatomic, weak) id<DataManageDelegate>delegate;

@property (nonatomic, strong) Lead* curSyncLead;
@property (nonatomic, strong) NSArray * curUpdataLead;


@property  BOOL islogined;
@property int listFilterButtonIndex;
@property (nonatomic, strong)   NSMutableArray * allListing;

@property (nonatomic , strong) NSArray * picNeedDownLoad;// source from get list from

@property (nonatomic) BOOL netWorkIsOK;

+(instancetype)instance;
-(void)addNewList:(NSDictionary *)info;

// picture
-(Picture *)creatNewPicObj:(List *)list image:(UIImage *)image;

-(void)addLocalNofi:(NSDate *)date withList:(List *)list;
-(void)removeLocalNofi:(List *)list;

-(void)addNewUserToLocal:(NSDictionary *)info;
-(BOOL)localLogin:(NSDictionary *)info;

-(void)syncLeadToService:(NSArray *)lead;  //  use when leave open home
-(void)updataLeadToService:(NSArray *)leads; // use when lead info edit
-(void)dataSync;  // use in masterVC

//-(void)syncLeadToAddressList:(NSArray *)leads;
-(void)syncLeadToAddressList:(Lead *)lead;
-(NSString *)pathForLeadCSVFile:(List *)list;

-(void) :(NSArray *)array;

-(void)submitQuestionToSV:(NSArray *)array;
-(void)deleAnswer:(NSArray *)array;

-(void)updataPicture:(NSArray *)array;
-(void)creatPicture:(NSArray *)array;

-(void)syncNoteToSever:(NSArray *)note;
-(void)syncEmailSignToSV;

-(void)submitListToSv:(NSArray *)array_;
@end
