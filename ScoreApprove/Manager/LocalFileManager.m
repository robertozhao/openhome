//
//  LocalFileManager.m
//
//  Created by Michael on 11/30/11.
//  Copyright AMP 2011. All rights reserved.
//

#import "LocalFileManager.h"
#import "Macro.h"

#define kFirstTime   @"firstTime"

@interface LocalFileManager (private)
- (void) touchFolder: (NSString *) path;
- (void) preferenceSetup;
@end

static LocalFileManager * fileMan = NULL;
@implementation LocalFileManager

+ (LocalFileManager *) manager {
    if (fileMan == NULL) {
        fileMan = [[LocalFileManager alloc] init];
    }
    return fileMan;
}

- (id) init {
    if (fileMan == NULL) {
        fileMan = [super init];
        [fileMan setUp];
    }
    return fileMan;
}

- (void) setUp {
    fileA_ = [[NSFileManager alloc] init];

//    docPath_ = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"] retain];
    libPath_ = [NSHomeDirectory() stringByAppendingPathComponent: @"Library"];
    libCachePath_ = [libPath_ stringByAppendingPathComponent: @"Caches"];
    libPrivatePath_ = [libPath_ stringByAppendingPathComponent: @"Private Document"];
    [self touchFolder: libPrivatePath_];

    tempPath_ = [NSHomeDirectory() stringByAppendingPathComponent: @"tmp"];

    imgFolder_ = [libCachePath_ stringByAppendingPathComponent: @"img"];
    [self touchFolder: imgFolder_];
}

-(NSString *)DBPath{
    return @"";
}

#pragma mark - private methods

- (void) touchFolder: (NSString *) path {
    if (![fileA_ fileExistsAtPath: path]) {
        [fileA_ createDirectoryAtPath: path withIntermediateDirectories: YES attributes: nil error: nil];
    }
}

#pragma mark - public methods

-(NSString *)photoFold{

    NSString * photoFold = [libPrivatePath_ stringByAppendingPathComponent:@"photo"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:photoFold]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:photoFold withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return photoFold;
}

-(NSString *)photoFoldSmall{
    
    NSString * photoFold = [libPrivatePath_ stringByAppendingPathComponent:@"photo_small"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:photoFold]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:photoFold withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return photoFold;
}

-(NSString *)userPlistPath{
    NSString * photoFold = [libPrivatePath_ stringByAppendingPathComponent:@"user.plst"];
    return photoFold;
}

-(NSString *)leadCSVPath{
    NSString * filePath = [tempPath_ stringByAppendingPathComponent:@"leads.csv"];
    return filePath;

}

@end