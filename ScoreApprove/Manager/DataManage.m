//
//  DataManage.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/3.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//


#import "DataManage.h"
#import "CoredataManager.h"
#import "LocalFileManager.h"
#import "List.h"
#import "Question.h"
#import "Answer.h"
#import "Lead.h"
#import <AddressBook/AddressBook.h>
#import "AppDelegate.h"
#import "Picture.h"
#import "Lead_Note.h"
#import "PhotoDownloadCenter.h"
#import "AFAppDotNetAPIClient.h"

static DataManage * dataManage;


@implementation DataManage


+(instancetype)instance{

    if (!dataManage) {
        
        dataManage = [[DataManage alloc] init];
    }
    return dataManage;
}


- (instancetype)init
{
    self = [super init];
    if (self) {

        id objc = [[NSUserDefaults standardUserDefaults] objectForKey:SyncLeadOnAddressList];
        if ([objc isEqualToString:@"YES"]) {
//            self.isSyncLeadToAddressList = YES;
        }
        _dataQueue =  dispatch_queue_create("dataQueue", NULL);
        
        [PhotoDownloadCenter instance];
        
        // notification for network connect
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataSync) name:@"NetWorkConnect" object:nil];
    }
    return self;
}


-(NSMutableArray *)allListing{
    
    NSMutableArray * array = [NSMutableArray array];
    NSArray * selfList = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"user_id='%@'",[GlobalInstance instance].user_id]];

    
    [array addObjectsFromArray: selfList];

    
    _allListing = [NSMutableArray arrayWithArray:array];
    return _allListing;
}

//-(void)setIsSyncLeadToAddressList:(BOOL)isSyncLeadToAddressList_{
//
//    _isSyncLeadToAddressList = isSyncLeadToAddressList_;
//    
//    if (isSyncLeadToAddressList_) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:SyncLeadOnAddressList];
//    }else{
//        [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:SyncLeadOnAddressList];
//    }
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}


-(BOOL)netWorkIsOK{
    
    if ([AFAppDotNetAPIClient sharedClient].reachabilityManager.networkReachabilityStatus == AFNetworkReachabilityStatusUnknown ||
        [AFAppDotNetAPIClient sharedClient].reachabilityManager.networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable){
        
        return NO;
    }else{
        return YES;
    }
}

-(void)addNewUserToLocal:(NSDictionary *)info{

    self.islogined = YES;
//     NSDictionary *dic = @{@"user":emailTf.text,@"pwd":passwordTf.text};
    NSArray * array = [[NSUserDefaults standardUserDefaults] objectForKey:UserInLocalKey];
    NSMutableArray * newArray;
    
    if (array) {
        newArray = [NSMutableArray arrayWithArray:array];
    }else{
        newArray = [NSMutableArray array];
    }
    
    [newArray addObject:info];
    [[NSUserDefaults standardUserDefaults] setObject:newArray forKey:UserInLocalKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)localLogin:(NSDictionary *)info{

    NSArray * array = [[NSUserDefaults standardUserDefaults] objectForKey:UserInLocalKey];
    if (!array) {
        return NO;
    }else{
    //     NSDictionary *dic = @{@"user":emailTf.text,@"pwd":passwordTf.text};
        
       __block BOOL isValid = NO;
        [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSDictionary * dic = (NSDictionary *)obj;
            if ([dic[@"user"] isEqualToString:info[@"user"]] && [dic[@"pwd"] isEqualToString:info[@"pwd"]]) {
                [GlobalInstance instance].user_id = dic[@"user_id"];
                isValid = YES;
                * stop = YES;
            }
        }];
        return isValid;
    }
}

//  list

-(void)addNewList:(NSDictionary *)info{

    List * list = (List *)[[CoredataManager manager] newObjectFromEntity:@"List"];
 
    NSUserDefaults * udf = [NSUserDefaults standardUserDefaults];
    NSString * num = [udf objectForKey:ListID];
    if (!num) {
        num = [NSString stringWithFormat:@"%d",ListBaseId];
    }
    int n = (int)[num integerValue];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",(n-1)] forKey:ListID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    list.id_ = num;
    list.extra = num;

    list.user_id = [GlobalInstance instance].user_id;
    list.address = info[@"address"];
    list.zipCode = info[@"zipcode"];
    list.sellState = @"1";
    list.listCate = @"Home";
    list.sizeUnit = @"sqft";
    list.beds = @"Studio";
    list.baths  = @"0";
    list.price = @"0";
    list.desc = @"";
    list.size = @"0";
    list.sizeUnit = @"sqft";
    list.creatDate = [Utility timeStringFromFormat:@"YYYY-MM-dd HH-mm-ss" withDate:[NSDate date]];
    list.needSync = @"1";
//    [self.allListing addObject:list];
    [[CoredataManager manager] saveContext];
    [self submitListToSv:@[list]];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"addNewlistFinish" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationForList" object:num];
    
}

-(Picture *)creatNewPicObj:(List *)list image:(UIImage *)image{

    Picture * pic = (Picture*)[[CoredataManager manager] newObjectFromEntity:@"Picture"];
    
    NSString * num = [[NSUserDefaults standardUserDefaults] objectForKey:PictureID];
    if (!num) {
        num = [NSString stringWithFormat:@"%d",PictureBaseId];
    }
    
    pic.id_ = num;
    int n = (int)[num integerValue];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",(n-1)] forKey:PictureID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    pic.order = [NSString stringWithFormat:@"%lu",(unsigned long)[list.picture allObjects].count];
    pic.extra = [NSString stringWithFormat:@"%d",-1];
    pic.localPath = [[[LocalFileManager manager] photoFold] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[NSString stringWithFormat:@"image_%d",(int)[pic.id_ hash]]]];
    pic.localPath_small = [[[LocalFileManager manager] photoFoldSmall] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[NSString stringWithFormat:@"image_%d",(int)[pic.id_ hash]]]];
    
    UIImage * img;
    if (image.size.width > 1000) {
        float ratio = 1000.0 / image.size.width;
        img = [Utility imageWithImage:image scaledToSize:CGSizeMake(image.size.width * ratio, image.size.height*ratio)];
    }else{
        img = image;
    }
    NSData * data = UIImageJPEGRepresentation(img, 0.5);
    if ([[NSFileManager defaultManager] createFileAtPath:pic.localPath contents:data attributes:nil]) {
    }
    img = nil;
    data = nil;
    
    // small pic save

    UIImage * img_small;
    
    if (image.size.width > 200) {
        float ratio = 200.0 / image.size.width;
        img_small = [Utility imageWithImage:image scaledToSize:CGSizeMake(image.size.width*ratio, image.size.height*ratio)];
    }else{
        img_small = img;
    }
    NSData * data_small = UIImageJPEGRepresentation(img_small, 0.5);
    [[NSFileManager defaultManager] createFileAtPath:pic.localPath_small contents:data_small attributes:nil];
    img_small = nil;
    data_small = nil;
    
    [list addPictureObject:pic];
    [[CoredataManager manager] saveContext];
    
    return pic;
}


//  private method

-(NSString *)listPhotoFoldName:(List *)list{
    return [NSString stringWithFormat:@"%lu",(unsigned long)[list.id_ hash]];
}
-(NSString *)listImgName:(List *)list{
    return [NSString stringWithFormat:@"%lu.jpg",(unsigned long)[list.photoName hash]];
}


-(void)addLocalNofi:(NSDate *)date withList:(List *)list{
    
    [self removeLocalNofi:list];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    if (localNotification == nil) {
        return;
    }
    localNotification.fireDate = date;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = list.address;
    localNotification.repeatInterval = 0;
    localNotification.alertAction = list.address;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    NSDictionary *infoDic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",list.id_],@"id",list.address,@"title",@"NofiyForSchedule",@"key",nil];
    localNotification.userInfo = infoDic;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


-(void)removeLocalNofi:(List *)list{

    NSArray *notificaitons = [[UIApplication sharedApplication] scheduledLocalNotifications];
    if (!notificaitons || notificaitons.count <= 0) {
        return;
    }
    NSString *str = [NSString stringWithFormat: @"%@",list.id_];
    for (UILocalNotification *notify in notificaitons) {
        if ([[notify.userInfo objectForKey:@"id"] isEqualToString:str]) {
            [[UIApplication sharedApplication] cancelLocalNotification:notify];
            break;
        }
    }
}



-(void)syncLeadToAddressList:(Lead *)lead{
    
            [self addPersion:lead];
}

-(void)addPersion:(Lead *)lead{
    
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);

    ABRecordRef newPerson = ABPersonCreate();
    
    CFStringRef cfsname = CFStringCreateWithCString( kCFAllocatorDefault, [lead.first_name UTF8String], kCFStringEncodingUTF8);
    
    ABRecordSetValue(newPerson, kABPersonFirstNameProperty, cfsname, &error);
  
    //phone number
    
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    
    if (lead.phone && ![lead.phone isEqualToString:@""]) {
        //号码
        
        CFStringRef cfsPhone = CFStringCreateWithCString( kCFAllocatorDefault, [lead.phone UTF8String], kCFStringEncodingUTF8);
        ABMultiValueAddValueAndLabel(multiPhone,cfsPhone, kABPersonPhoneMobileLabel, NULL);//添加移动号码0
        
        ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone, &error);//写入全部号码    进联系人
    }
    CFRelease(multiPhone);
    
    //email
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    CFStringRef cfsEmail = CFStringCreateWithCString( kCFAllocatorDefault, [lead.email UTF8String], kCFStringEncodingUTF8);
    ABMultiValueAddValueAndLabel(multiEmail, cfsEmail, kABWorkLabel, NULL);
    ABRecordSetValue(newPerson, kABPersonEmailProperty, multiEmail, &error);

    CFRelease(multiEmail);
    ABAddressBookAddRecord(addressBook, newPerson, &error);
    ABAddressBookSave(addressBook, &error);
    
    CFRelease(newPerson);
    CFRelease(addressBook);
}


-(NSString *)pathForLeadCSVFile:(List *)list{

    NSArray * leads;
    if (list) {
        leads = [list.lead allObjects];
    }else{
        leads = [[CoredataManager manager] allNodeForEntity:@"Lead" orderBy:@"created"];
    }
    

    NSArray * properties = @[@"first_name",@"last_name",@"email",@"phone",@"notes",@"created",@"Property visited"];

    NSArray * titles = @[@"Name",@"Email",@"Phone number",@"Notes",@"Add time",@"Property visited"];
    
    NSMutableString * tempStr = [NSMutableString string];
    [titles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString * str = (NSString *)obj;
        [tempStr appendString:str];
        
        if (idx != titles.count -1) {
            [tempStr appendString:@","];
        }else{
            [tempStr appendString:@"\n"];
        }
    }];
    [leads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Lead * lead = (Lead *)obj;
        [properties enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString * title = (NSString *)obj;
            
            if (idx == 0) {
                
            }else if (idx == 1) {
                SEL sel_firName = NSSelectorFromString(properties[0]);
                SEL sel_lastName = NSSelectorFromString(properties[1]);
                NSString * firName = [lead performSelector:sel_firName withObject:nil];
                NSString * lastName = [lead performSelector:sel_lastName withObject:nil];
                [tempStr appendFormat:@"%@ %@,",firName,lastName?lastName:@""];
            }else if (idx>1 &&idx<5){
                NSString * value = [lead performSelector:NSSelectorFromString(title) withObject:nil];
                [tempStr appendString:[NSString stringWithFormat:@"%@,",value?value:@""]];
            }else{
            
                if ([title isEqualToString:@"created"]) {
                    NSString * value = [lead performSelector:NSSelectorFromString(title) withObject:nil];
                    NSString * time = [Utility timeStringFromFormat:@"YYYY-MM-dd" withTI:[value doubleValue]];
                    [tempStr appendString:[NSString stringWithFormat:@"%@,",time]];
                }else if ([title isEqualToString:@"Property visited"]){
                    NSString * address = lead.list.address;
                    [tempStr appendString:[NSString stringWithFormat:@"%@\n",address?address:@""]];
                }
            }
        }];
    }];
    DLog(tempStr);
    
    NSString * path = [[LocalFileManager manager] leadCSVPath];
    NSError * error;
    [tempStr writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        DLog(error);
        return nil;
    }else{
        return path;
    }
}





//=========================================================sync==================================================================

-(void)dataSync{

    /*
     1.获取list
     2.提交list
     3.获取问题
     4.提交问题
     5.删除问题答案
     6.获取lead
     7.提交lead
     8.提交图片
     9.更新图片
     10.删除图片
     11.下载图片
     */
    
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate beginToSync:@"Syncing..."];
        });
        if (!syncManage) {
            syncManage = [[SyncManage alloc] init];
            syncManage.delegate = self;
        }
        NSLog(@"-------------获取list---------------");
        [syncManage getPropertyForSV];
    });
}

-(void)submitListToSv:(NSArray *)array_{

            NSLog(@"-------------提交list---------------");
    if (!self.netWorkIsOK){
        return;
    }
    __block NSArray * array = array_;
    dispatch_async(_dataQueue, ^{
        if (!array) {
            array = [[CoredataManager manager] allNodeForEntity:@"List" withPredicate:[NSString stringWithFormat:@"needSync=1 and user_id=%@",[GlobalInstance instance].user_id]];
        }
        if (array && array.count !=0) {
            [syncManage updataProperty:array];
        }else{
            [self updataPropertyFinsih];
        }
    });
}


-(void)getQuestionFromSV{
                NSLog(@"-------------获取question·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:@"user_id==0"];
        if (!array || array.count == 0) {
            
            [syncManage getQuestonFromSV:nil];
            
        }else{
            [self getQuestionFinish];
        }
    });
}

-(void)submitQuestionToSV:(NSArray *)array_{
    NSLog(@"-------------提交question·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    __block NSArray * array = array_;
    dispatch_async(_dataQueue, ^{
        if (!array) {
            array = [[CoredataManager manager] allNodeForEntity:@"Question" withPredicate:[NSString stringWithFormat:@"extra > -1 and user_id='%@'",[GlobalInstance instance].user_id]];
            
            if (array && array.count !=0) {
                [syncManage submitQuestionToSV:array];
            }else{
                [self submitQuestionToSVFinish];
            }
        }else{
            [syncManage submitQuestionToSV:array];
        }
    });
}

-(void)deleAnswer:(NSArray *)array{
    NSLog(@"-------------删除answer·---------------");
     if (!self.netWorkIsOK){
        return;
    }
    
    dispatch_async(_dataQueue, ^{
        if (array && array.count != 0) {
            [syncManage deleteAnswer:array];
        }else{
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Answer" withPredicate:[NSString stringWithFormat:@"toDele=1"]];
            if (array && array.count !=0) {
                [syncManage deleteAnswer:array];
            }else{
                //  about lead api
                [self deleAnswerFinish];
            }
        }
    });
}

-(void)getLeadFromSV{   //  获取 lead
NSLog(@"-------------获取lead·---------------");
    if (!self.netWorkIsOK){
            return;
    }
    
    dispatch_async(_dataQueue, ^{
            [syncManage syncLeadFromServer];
    });
    
}

-(void)syncLeadToService:(NSArray *)leads{  // sync lead to sv   提交lead
NSLog(@"-------------提交lead·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        //  lead  sync to service
        
        NSArray * leadsArray = leads;
        if (!leadsArray) {
            leadsArray = [[CoredataManager manager] allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"extra>-1 and agent_id='%@'",[GlobalInstance instance].user_id]];
        }
        if (leadsArray && leadsArray.count !=0) {
            NSMutableArray * temp = [NSMutableArray arrayWithCapacity:leadsArray.count];
            [leadsArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Lead * lead = (Lead *)obj;
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setObject:lead.first_name forKey:@"first_name"];
                [dic setObject:lead.last_name forKey:@"last_name"];
                [dic setObject:lead.email forKey:@"email"];
                [dic setObject:lead.phone?lead.phone:@"" forKey:@"phone"];
                [dic setObject:lead.list.id_ forKey:@"property_id"];
                [dic setObject:lead.extra forKey:@"extra"];
                [dic setObject:lead.address?lead.address:@"" forKey:@"street"];
                NSString * state;
                if (lead.state && lead.state.length >2) {
                    state = [lead.state substringToIndex:2];
                }else{
                    state = @"";
                }
                [dic setObject:state forKey:@"state"];
                [dic setObject:lead.zipCode?lead.zipCode:@"" forKey:@"zip"];
                [dic setObject:lead.city?lead.city:@"" forKey:@"city"];
                [dic setObject:lead.brithday?lead.brithday:@"" forKey:@"dob"];
                
                NSMutableArray * qArray = [NSMutableArray array];
                NSArray * qs = [lead.leadQ allObjects];
                [qs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    LeadQuestion * lq = (LeadQuestion *)obj;
                    if ([lq.type isEqualToString:@"2"]) {
                        NSDictionary * dic = @{@"question_id":lq.questionId,@"question":lq.title,@"answer_id":lq.answerId?lq.answerId:@"",@"answer_value":lq.answerValue};
                        [qArray addObject:dic];
                    }
                }];
                [dic setObject:qArray forKey:@"questions"];
                [temp addObject:dic];
            }];
            [syncManage syncLeadToServer:temp];
        }else{
            [self syncLeadToServerFinish:nil];
        }
    });
}


-(void)updataLeadToService:(NSArray *)leads{ // updata lead to sv
    NSLog(@"-------------更新lead·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    
    dispatch_async(_dataQueue, ^{
        self.curUpdataLead = leads;
        
        if (_curUpdataLead) {
            self.curUpdataLead =  [[CoredataManager manager] allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"hadChange='yes' and agent_id='%@'",[GlobalInstance instance].user_id]];
        }
        //  lead  updata to service
        if(self.curUpdataLead && self.curUpdataLead.count != 0){
            
            NSMutableArray * leadsInfo = [NSMutableArray arrayWithCapacity:leads.count];
            [leads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Lead * lead = (Lead *)obj;
                NSMutableDictionary * dic = [NSMutableDictionary dictionary];
                [dic setObject:lead.lead_id forKey:@"lead_id"];
                [dic setObject:lead.notes?lead.notes:@"" forKey:@"notes"];
                [leadsInfo addObject: dic];
            }];
            [syncManage updataLeadToServer:leadsInfo];
        }else{
            [self updataLeadToServerFinish:nil];
        }
    });
}


-(void)syncNoteToSever:(NSArray *)note{
NSLog(@"-------------提交note·----- ----------");
    dispatch_async(_dataQueue, ^{
    if (!note || note.count !=0) {

        NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Lead_Note" withPredicate:@"extra<1"];
        if (array && array.count !=0) {
         
            NSMutableArray * temp = [NSMutableArray array];
            
            [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Lead_Note * note = (Lead_Note *)obj;
                if (note.extra) {
                    NSDictionary * dic = @{@"note":note.note?note.note:@"",
                                           @"extra":note.extra,
                                           @"created":note.creat,
                                           @"lead_id":note.lead_id};
                    [temp addObject:dic];
                }
            }];
            [syncManage addLeadNote:temp];
        }else{
            [self syncNoteToSeverFinish];
        }
    }else{
        [self syncNoteToSeverFinish];
    }
    });
}


-(void)creatPicture:(NSArray *)array{
 NSLog(@"-------------creatPicture·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        if (array && array.count != 0) {
            [syncManage creatPicture:array];
        }else{
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"extra=-1 and id_<0"]];
            
            if (array && array.count != 0) {
                [syncManage creatPicture:array];
            }else{
                [self creatPictureFinish];
            }
        }
    });
}

-(void)updataPicture:(NSArray *)array{
     NSLog(@"-------------updatePicture·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        if (array && array.count !=0) {
            [syncManage updataPicture:array];
        }else{
            NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"extra=-1 and id_>-1"]];
            if (array && array.count != 0) {
                [syncManage updataPicture:array];
            }else{
                [self updataPictureFinish];
            }
        }
    });
}

-(void)deletePicture{
       NSLog(@"-------------删除Picture·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        NSArray * array = [[CoredataManager manager] allNodeForEntity:@"Picture" withPredicate:[NSString stringWithFormat:@"toDele=1"]];
        if (array && array.count != 0) {
            [syncManage deletePicture:array];
        }else{
            [self deletePictureFinish];
        }
    });
}

-(void)syncEmailSignToSV{
        NSLog(@"-------------上传签名·---------------");
    if (!self.netWorkIsOK){
        return;
    }
    dispatch_async(_dataQueue, ^{
        NSDictionary * dic = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
        NSMutableDictionary * temp = [NSMutableDictionary dictionary];
        
        if (dic[@"isHadNew"] &&  [dic[@"isHadNew"] intValue]==1) {
            if(dic[@"signature"]){
                [temp setObject:dic[@"signature"] forKey:@"signature"];
            }
            [temp setObject:dic[@"isAutoSend"] forKey:@"auto_send_email"];
            [syncManage leadEmialSendSetting:temp];
        }else{
            [self leadEmialSendSettingFinish:nil];
        }

    });
}

-(void)syncAgentSignatureInfo{
     NSLog(@"-------------上传签名再次·---------------");
    dispatch_async(_dataQueue, ^{
    
        [syncManage agentSignatureInfo];
    });
}

#pragma mark -- SyncManageDelegate

-(void)getListForSVFinish{
    
    [self submitListToSv:nil];
}

-(void)updataPropertyFinsih{

    [self getQuestionFromSV];
}

-(void)getQuestionFinish{
    
    [self submitQuestionToSV:nil];
}

-(void)submitQuestionToSVFinish{

    [self deleAnswer:nil];
}

-(void)deleAnswerFinish{
    //  about lead api
    [self getLeadFromSV];
}

-(void)syncLeadFromSVFinish{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"syncListFinish" object:nil];
    dispatch_async(_dataQueue, ^{
        [self syncLeadToService:nil];
    });
}


-(void)syncLeadToServerFinish:(NSArray *)leads{
    
    dispatch_async(_dataQueue, ^{
        if (leads) {
            [leads enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSDictionary * dic = (NSDictionary *)obj;
                NSArray * array = [[CoredataManager manager]allNodeForEntity:@"Lead" withPredicate:[NSString stringWithFormat:@"extra='%@'",dic[@"extra"]]];
                if (dic[@"duplicate_lead"]) {
                    //  if email had has in server ,so delete this lead in local
                    //todo
                    if (array && array.count !=0) {
                        Lead * lead = array[0];
                        [[CoredataManager manager] deleteObject:lead];
                    }
                }else{
                    if (array && array.count !=0) {
                        Lead * lead = array[0];
                        lead.lead_id = dic[@"lead_id"];
                        lead.extra = nil;
                        NSArray * notes = [lead.notes allObjects];
                        [notes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                            Lead_Note * note = (Lead_Note *)obj;
                            note.lead_id = lead.lead_id;
                        }];
                        [[CoredataManager manager] saveContext];
                    }
                }
            }];
            [[CoredataManager manager] saveContext];
           [self updataLeadToService:nil];
        }else{
           [self updataLeadToService:nil];
        }
    });
}



-(void)updataLeadToServerFinish:(NSString *)status{
    
    dispatch_async(_dataQueue, ^{
        if ([status isEqualToString:@"1"]) {
            [self.curUpdataLead enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                Lead * lead = (Lead *)obj;
                lead.hadChange = @"no";
            }];
            [[CoredataManager manager] saveContext];
        }
        self.curUpdataLead = nil;
        
        [self syncNoteToSever:nil];
    });
}

-(void)syncNoteToSeverFinish{

    [self creatPicture:nil];
}

-(void)creatPictureFinish{

    [self updataPicture:nil];
}

-(void)updataPictureFinish{

    [self deletePicture];
}

-(void)deletePictureFinish{

    [self syncEmailSignToSV];
}


-(void)leadEmialSendSettingFinish:(NSDictionary *)info{

    if (info) {
        NSMutableDictionary * temp = [NSMutableDictionary dictionary];
        if (info[@"auto_send_email"]) {
            [temp setObject:info[@"auto_send_email"] forKey:@"isAutoSend"];
        }
        if (info[@"signature"]) {
            [temp setObject:info[@"signature"] forKey:@"signature"];
        }
        [temp setObject:@0 forKey:@"isHadNew"];

        [[NSUserDefaults standardUserDefaults] setObject:temp forKey:[NSString stringWithFormat:@"%@_%@",KeyForEmailSignture,[GlobalInstance instance].user_id]];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self syncAgentSignatureInfo];
}

-(void)agentSignatureInfoFinish{

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate finishSync];
    });
}

-(void)systemError{

}

-(void)needUserLogin:(NSString *)msg{

}


@end
