//
//  SyncManage.h
//  ScoreApprove
//
//  Created by Yang on 15/5/7.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Manager.h"
#import "constants.h"
#import "PictureManage.h"

@protocol SyncManageDelegate <ManagerDelegate>
@optional
-(void)syncLeadToServerFinish:(NSArray *)leads;
-(void)updataLeadToServerFinish:(NSString *)status;
-(void)syncLeadFromSVFinish;


-(void)getQuestionFinish;
-(void)submitQuestionToSVFinish;
-(void)deleAnswerFinish;

-(void)getListForSVFinish;
-(void)updataPropertyFinsih;


-(void)creatPictureFinish;
-(void)deletePictureFinish;
-(void)updataPictureFinish;

-(void)syncNoteToSeverFinish;

-(void)verifyEmailFinsih:(BOOL)isVerify msg:(NSString *)msg;

-(void)leadEmialSendSettingFinish:(NSDictionary *)info;

-(void)agentSignatureInfoFinish;
-(void)syncPictureDataFromSVFinish;

@end


@interface SyncManage : Manager<PictureManageDeleaget>{

    PictureManage * picManage;
    int indexPic;
}

@property (nonatomic , weak) id<SyncManageDelegate>delegate;

@property (nonatomic , strong) NSArray * picCreatArray;
@property (nonatomic , strong) NSArray * picArrayToGet;

-(void)getPropertyForSV;
-(void)updataProperty:(NSArray *)lists;

- (void) syncLeadToServer:(NSArray *)leads;
- (void) syncLeadFromServer;
-(void) updataLeadToServer:(NSArray *)leads;


-(void)getQuestonFromSV:(NSDictionary *)info;

-(void)submitQuestionToSV:(NSArray *)questons;
-(void)deleteAnswer:(NSArray *)answers;

-(void)creatPicture:(NSArray *)info;
-(void)deletePicture:(NSArray *)pictures;
-(void)updataPicture:(NSArray *)picture;
-(void)addLeadNote:(NSArray *)notes;


-(void)syncPictureDataFromSV:(Picture *)pic;
//  verify emai an question na

-(void)verifyEmail:(NSString *)email;

-(void)leadEmialSendSetting:(NSDictionary *)info;
-(void)agentSignatureInfo;
@end
