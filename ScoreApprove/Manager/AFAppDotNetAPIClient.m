//
//  AFAppDotNetAPIClient.m
//  ScoreApprove
//
//  Created by Yang on 15/5/18.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import "AFAppDotNetAPIClient.h"

#import "GlobalInstance.h"
#import "DataManage.h"

static NSString * const AFAppDotNetAPIBaseURLString = @"https://api.app.net/";

@implementation AFAppDotNetAPIClient


+ (instancetype)sharedClient {
    
    static AFAppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AFAppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        [_sharedClient.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                    
                case AFNetworkReachabilityStatusReachableViaWWAN:
                    [GlobalInstance instance].netWorkStatue = YES;
                    
                    if ([DataManage instance].islogined) {
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"NetWorkConnect" object:nil];
                    }
                    NSLog(@"-------AFNetworkReachabilityStatusWWAN------");
                    break;
                case AFNetworkReachabilityStatusReachableViaWiFi:
                    [GlobalInstance instance].netWorkStatue = YES;
                    
                    if ([DataManage instance].islogined) {
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"NetWorkConnect" object:nil];
                    }
                    NSLog(@"-------AFNetworkReachabilityStatusWiFi------");
                    break;
                case AFNetworkReachabilityStatusNotReachable:
                    
                    [GlobalInstance instance].netWorkStatue = NO;
                    
                    NSLog(@"-------AFNetworkReachabilityStatusNotReachable------");
                    break;
                default:
                    break;
                    
            }
        }];
        [_sharedClient.reachabilityManager startMonitoring];
    });
    
    return _sharedClient;
}


@end
