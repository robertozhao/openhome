//
//  AppDelegate.h
//  Timelined
//
//  Created by Gordon Yang on 6/21/12.
//  Copyright (c) 2012 AMP. All rights reserved.
//

#import "CoredataManager.h"


static CoredataManager *_manager = nil;

@implementation CoredataManager

@synthesize managedObjectContext = __managedObjectContext;

@synthesize managedObjectModel = __managedObjectModel;

@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize coreDataQueue;

+ (CoredataManager *) manager {
    if (_manager == nil) {
        _manager = [[CoredataManager alloc] init];
    }
    return _manager;
}

- (id) init {
    if (_manager == nil) {
        self = [super init];
        _manager = self;
        self.coreDataQueue = dispatch_queue_create("coreDataQueue", NULL);
    }
    return _manager;
}

//- (void)dealloc
//{
//    dispatch_release(coreDataQueue);
//    [super dealloc];
//}

- (void) saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save: &error]) {
            EFLog(@"%@",error);
            abort();
        }
    }
}

- (void) deleteObject: (NSManagedObject *) obj {
    [self.managedObjectContext deleteObject: obj];
    [[CoredataManager manager] saveContext];
}

#pragma mark - Core Data stack

/**
   Returns the managed object context for the application.
   If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return __managedObjectContext;
}

/**
   Returns the managed object model for the application.
   If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *) managedObjectModel {
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource: @"Model" withExtension: @"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL: modelURL];
    return __managedObjectModel;
}


- (NSPersistentStoreCoordinator *) persistentStoreCoordinator {
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * sqliteFilePath = [documentsDirectory stringByAppendingPathComponent:@"Model.db"];

    NSURL *storeURL = [NSURL fileURLWithPath: sqliteFilePath isDirectory: NO];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType: NSSQLiteStoreType configuration: nil URL: storeURL options: nil error: &error]) {
        abort();

    }
    return __persistentStoreCoordinator;
}

- (NSManagedObject *) newObjectFromEntity: (NSString *) entityName {
    return  [NSEntityDescription insertNewObjectForEntityForName: entityName inManagedObjectContext: self.managedObjectContext];
}

- (NSArray *) allNodeForEntity: (NSString *) entityName {
    return [self allNodeForEntity: entityName orderBy: nil withPredicate: nil];
}

- (NSArray *) allNodeForEntity: (NSString *) entityName orderBy: (NSString *) oKey {
    return [self allNodeForEntity: entityName orderBy: oKey withPredicate: nil];
}

- (NSArray *) allNodeForEntity: (NSString *) entityName withPredicate: (id) predicate {
    return [self allNodeForEntity: entityName orderBy: nil withPredicate: predicate];
}

- (NSArray *) allNodeForEntity: (NSString *) entityName orderBy: (NSString *) oKey withPredicate: (id) stringOrPredicate {
    return [self nodesFromEntity: entityName orderBy: oKey withPredicate: stringOrPredicate withPageNum: 0 withPageSize: 0];
}

- (NSManagedObject *) getSingleFromEntity: (NSString *) entityName withPredicate: (NSPredicate*) predicate {
    NSArray *objs = [self nodesFromEntity: entityName orderBy: nil withPredicate: predicate withPageNum:0 withPageSize: 0];
    if ([objs count] > 0) {
        return [objs objectAtIndex: 0];
    }
    return nil;
}


- (NSArray *) nodesFromEntity: (NSString *) entityName orderBy: (NSString *) oKey withPredicate: (id) stringOrPredicate withPageNum: (int) pageNum withPageSize: (int) pageSize {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName: entityName
                                   inManagedObjectContext: self.managedObjectContext];
    [fetchRequest setEntity: entity];
    /*if just need the first one
       fetchRequest.fetchLimit = 1;
     */

    /*
       if need sort by some property
     */
    if (oKey != nil) {
        [fetchRequest setSortDescriptors: @[[NSSortDescriptor sortDescriptorWithKey: oKey ascending: NO]]];
    }

    /*
       if need filter by some condition

       NSPredicate *pred = [NSPredicate predicateWithFormat:@"(isChannel = %@)", NumFromBOOL(isChannel)];
       [fetchRequest setPredicate: pred];

       NSPredicate *pred = [NSPredicate predicateWithFormat:@"pid in %@", ids];
     */
    //    if (predicate != nil) {
    //        NSPredicate *pred = [NSPredicate predicateWithFormat: predicate, nil];
    //        [fetchRequest setPredicate: pred];
    //    }
    if (stringOrPredicate) {
        NSPredicate *predicate;
        if ([stringOrPredicate isKindOfClass:[NSString class]]) {
            //            va_list variadicArguments;
            //            va_start(variadicArguments, stringOrPredicate);
            //            predicate = [NSPredicate predicateWithFormat:stringOrPredicate arguments:variadicArguments];
            //            va_end(variadicArguments);

            predicate = [NSPredicate predicateWithFormat: stringOrPredicate, nil];
        } else {
            //            NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
            //                      @"Second parameter passed to %s is of unexpected class %@",
            //                      sel_getName(_cmd), [stringOrPredicate className]);
            predicate = (NSPredicate *)stringOrPredicate;
        }
        [fetchRequest setPredicate: predicate];
    }
    /*
       if you need just fetch some particular properties
       NSArray *propertyToFetch = [NSArray arrayWithObjects: @"name", @"title",nil];

       [fetchRequest setPropertiesToFetch: propertyToFetch];
     */
    if (pageSize > 0) {
        [fetchRequest setFetchLimit: pageSize];
        [fetchRequest setFetchOffset: pageSize * pageNum];
    }

    NSError *error;
    NSArray *items = [self.managedObjectContext executeFetchRequest: fetchRequest error: &error];
    return items;
}

- (void) turnBackToFault: (NSManagedObject *) obj {
    [self.managedObjectContext refreshObject: obj mergeChanges: NO];
}

- (void) deleteAllObjectsFromEntity: (NSString *) entity {
    NSArray *objects = [self allNodeForEntity: entity];
    for (NSManagedObject *obj in objects) {
        [self.managedObjectContext deleteObject: obj];
    }
    [self saveContext];
}

- (void) releaseMe {
    _manager = nil;
    [CoredataManager manager];
}

//  special method


- (NSArray *) nodesFromEntity: (NSString *) entityName orderBy: (NSString *) oKey orderState:(NSString *)orderState withPredicate: (id) stringOrPredicate{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName: entityName
                                              inManagedObjectContext: self.managedObjectContext];
    [fetchRequest setEntity: entity];
    /*if just need the first one
     fetchRequest.fetchLimit = 1;
     */
    
    /*
     if need sort by some property
     */
    if (oKey != nil) {
        [fetchRequest setSortDescriptors: @[[NSSortDescriptor sortDescriptorWithKey: oKey ascending: [orderState isEqualToString:@"ascend"]?YES:NO]]];
    }
    
    /*
     if need filter by some condition
     
     NSPredicate *pred = [NSPredicate predicateWithFormat:@"(isChannel = %@)", NumFromBOOL(isChannel)];
     [fetchRequest setPredicate: pred];
     
     NSPredicate *pred = [NSPredicate predicateWithFormat:@"pid in %@", ids];
     */
    //    if (predicate != nil) {
    //        NSPredicate *pred = [NSPredicate predicateWithFormat: predicate, nil];
    //        [fetchRequest setPredicate: pred];
    //    }
    if (stringOrPredicate) {
        NSPredicate *predicate;
        if ([stringOrPredicate isKindOfClass:[NSString class]]) {
            //            va_list variadicArguments;
            //            va_start(variadicArguments, stringOrPredicate);
            //            predicate = [NSPredicate predicateWithFormat:stringOrPredicate arguments:variadicArguments];
            //            va_end(variadicArguments);
            
            predicate = [NSPredicate predicateWithFormat: stringOrPredicate, nil];
        } else {
            //            NSAssert2([stringOrPredicate isKindOfClass:[NSPredicate class]],
            //                      @"Second parameter passed to %s is of unexpected class %@",
            //                      sel_getName(_cmd), [stringOrPredicate className]);
            predicate = (NSPredicate *)stringOrPredicate;
        }
        [fetchRequest setPredicate: predicate];
    }
    /*
     if you need just fetch some particular properties
     NSArray *propertyToFetch = [NSArray arrayWithObjects: @"name", @"title",nil];
     
     [fetchRequest setPropertiesToFetch: propertyToFetch];
     */
//    if (pageSize > 0) {
//        [fetchRequest setFetchLimit: pageSize];
//        [fetchRequest setFetchOffset: pageSize * pageNum];
//    }
    
    NSError *error;
    NSArray *items = [self.managedObjectContext executeFetchRequest: fetchRequest error: &error];
    return items;
}
@end
