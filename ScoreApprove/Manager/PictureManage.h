//
//  PictureManage.h
//  ScoreApprove
//
//  Created by Yang on 15/5/17.
//  Copyright (c) 2015年 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Picture.h"
#import "Manager.h"

@protocol  PictureManageDeleaget <ManagerDelegate>

@optional
-(void)creatSignPictureFinish;
-(void)syncPicture:(Picture *)pic;

@end

@interface PictureManage : Manager{


    
}

@property (nonatomic , weak)id<PictureManageDeleaget>delegate;

-(void)creatPicture:(Picture *)para;

-(void)newPictureToSync:(Picture *)pic;


@end
