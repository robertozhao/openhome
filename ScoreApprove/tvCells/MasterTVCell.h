//
//  MasterTVCell.h
//  ScoreApprove
//
//  Created by Felix Page on 2/14/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *mIcon;
@property (strong, nonatomic) IBOutlet UILabel *mTitle;

@property (strong, nonatomic) NSString * mSelectedImage;
@property (strong, nonatomic) NSString * mImage;

@end
