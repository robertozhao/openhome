//
//  SubMenuTVCell.h
//  ScoreApprove
//
//  Created by Felix Page on 2/14/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "M13BadgeView.h"
#import "SyncImageView.h"

@interface SubMenuTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SyncImageView *mImg;
@property (strong, nonatomic) IBOutlet UILabel *mTitle;
@property (strong, nonatomic) IBOutlet UILabel *mPrice;

@property (strong, nonatomic) IBOutlet UIView *badgeContainer;
@property (strong, nonatomic) IBOutlet M13BadgeView *mBadge;
@property (strong, nonatomic) IBOutlet UILabel *mStates;
@property (strong, nonatomic) IBOutlet UIImageView *sub_cell_bg;

@end
