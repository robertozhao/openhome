//
//  MenuItem.h
//  ScoreApprove
//
//  Created by Felix Page on 2/14/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (strong, nonatomic) NSString *image;
@property (strong, nonatomic) NSString *selected_image;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) int item_id;

@end
