//
//  MasterTVCell.m
//  ScoreApprove
//
//  Created by Felix Page on 2/14/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import "MasterTVCell.h"

@implementation MasterTVCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        self.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"nav-select"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        [self.mTitle setTextColor:[UIColor colorWithRed:104.0/255.0f green:165.0/255.0f blue:252.0/255.0f alpha:1.0]];
        [self.mIcon setImage:[UIImage imageNamed:self.mSelectedImage]];
    } else {
        self.backgroundView = nil;
        [self.mTitle setTextColor:[UIColor colorWithRed:146.0/255.0f green:146.0/255.0f blue:146.0/255.0f alpha:1.0]];
        [self.mIcon setImage:[UIImage imageNamed:self.mImage]];

    }
}


@end
