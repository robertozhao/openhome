//
//  SubMenuTVCell.m
//  ScoreApprove
//
//  Created by Felix Page on 2/14/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import "SubMenuTVCell.h"

@implementation SubMenuTVCell

- (void)awakeFromNib {
    // Initialization code
    self.mBadge.hidesWhenZero = YES;
    self.mBadge.textColor = [UIColor whiteColor];
    self.mBadge.badgeBackgroundColor = [UIColor colorWithRed:246.0/255.0f green:146.0/255.0f blue:30.0/255.0f alpha:1.0f];
    self.mBadge.tag = 555;
    self.mBadge.verticalAlignment = M13BadgeViewVerticalAlignmentMiddle;
    self.mBadge.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter;
    
    UIView *myBackView = [[UIView alloc] initWithFrame:self.frame];
    myBackView.backgroundColor = [UIColor clearColor];
    self.selectedBackgroundView = myBackView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
