//
//  EmailTVCell.h
//  ScoreApprove
//
//  Created by Felix Page on 2/19/16.
//  Copyright © 2016 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailTVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UISwitch *cellSwitch;
@property (strong, nonatomic) IBOutlet UIImageView *imgArrow;
@property (strong, nonatomic) IBOutlet UIImageView *cellBg;

@end
