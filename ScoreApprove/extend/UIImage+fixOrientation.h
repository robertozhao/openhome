//
//  UIImage+fixOrientation.h
//  ScoreApprove
//
//  Created by YanYan on 5/21/15.
//  Copyright (c) 2015 Yang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end