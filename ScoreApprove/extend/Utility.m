//
//  Utility.m
//
//  Created by Michael on 9/9/11.
//  Copyright 2011 AMP. All rights reserved.
//

#import "Utility.h"
#import <CommonCrypto/CommonDigest.h>
#import "Macro.h"

@implementation Utility



+ (NSString *) hashIDForString : (NSString *) str {
    return [NSString stringWithFormat: @"%lu", (unsigned long)[str hash]];
}


//+ (NSString *) stringFromMD5: (NSString *) str {
//    if (self == nil ||[str length] == 0)
//        return nil;
//
//    const char *value = [str UTF8String];
//
//    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
//    CC_MD5(value, strlen(value), outputBuffer);
//
//    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity: CC_MD5_DIGEST_LENGTH * 2];
//    for (NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++) {
//        [outputString appendFormat: @"%02x", outputBuffer[count]];
//    }
//
//    return outputString;
//}

+ (NSString *) urlEncode: (NSString *) string {
    NSString *newString = CFBridgingRelease( CFBridgingRetain( (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( kCFAllocatorDefault, (CFStringRef)string, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding) )) ) );
    if (newString) {
        return newString;
    }
    return @"";
}

+ (NSString *) legalString: (id) str {
    if (str == nil || [str isKindOfClass: [NSNull class]]) {
        return @"";
    }
    
    if ([str isKindOfClass: [NSString class]]) {
        if ([[str lowercaseString] rangeOfString: @"null"].location != NSNotFound) {
            return @"";
        }
        str = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        str = [str description];
    }
    return str;
}

//+ (UIColor *) colorFromR: (CGFloat) rv g: (CGFloat) gv b: (CGFloat) bv {
//    return [UIColor colorWithRed: rv / 255 green: gv / 255 blue: bv / 255 alpha: 1.0];
//}

+ (NSArray *) runRegEx: (NSString *) regStr ForString: (NSString *) string {
    if (string == nil) {
        return nil;
    }
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern: regStr
                                  options: NSRegularExpressionCaseInsensitive
                                  error: &error];

    NSArray *matches = [regex matchesInString: string
                        options: 0
                        range: NSMakeRange(0, [string length])];

    return matches;
}

+ (BOOL) verifyString: (NSString *) str withRegex: (NSString *) regex {
    NSArray *result = [Utility runRegEx: regex ForString: str];
    if ([result count] == 1) {
        NSRange range = [result[0] range];
        if (range.length ==[str length]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL) isPortrait {
    UIInterfaceOrientation ori = [[UIApplication sharedApplication] statusBarOrientation];
    return ( (ori == UIInterfaceOrientationPortrait) || (ori == UIInterfaceOrientationPortraitUpsideDown));
}

+(BOOL)isIOS7 {
    NSString *osVersionStr = [[UIDevice currentDevice] systemVersion];
    if ([osVersionStr rangeOfString: @"7"].location != NSNotFound) {
        return YES;
    }
    return NO;
}


+(BOOL)isIOS7Later{
    NSString *osVersionStr = [[UIDevice currentDevice] systemVersion];
    if ([osVersionStr floatValue] > 7.0 || [osVersionStr floatValue] == 7.0) {
        return YES;
    }else{
        return NO;
    }
}

+(BOOL)isIOS8Later{
    NSString *osVersionStr = [[UIDevice currentDevice] systemVersion];
    if ([osVersionStr floatValue] > 8.0 || [osVersionStr floatValue] == 8.0) {
        return YES;
    }else{
        return NO;
    }
}

+ (BOOL) isLegalURL: (NSString *) urlStr {
    if ([urlStr rangeOfString: @"http" options: NSCaseInsensitiveSearch].location == NSNotFound) {
        return NO;
    }
    return YES;
}

+ (BOOL) isLegalEmailAddress: (NSString *) mailAddr {
    if ( ( ([mailAddr rangeOfString: @"@"].location == NSNotFound) ||
          ([mailAddr rangeOfString: @"."].location == NSNotFound) ) ) {
        return NO;
    }
    return YES;
}

+ (BOOL) isLegalPassword: (NSString *) pwd {
    if ([[Utility legalString: pwd] length] < 5) {
        return NO;
    }
    return YES;
}

+ (BOOL) isLegalString: (NSString *) str {
    if ( (str == nil) || !([str length] > 0) ) {
        return NO;
    }
    return YES;
}

+ (NSString *) showPrice: (float) price {
    return [NSNumberFormatter localizedStringFromNumber: @ (price)numberStyle: NSNumberFormatterCurrencyStyle];
}


+ (BOOL) is4Screen {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.height > 480) {
        return YES;
    }
    return NO;
}

+ (NSString *) curLanguage {
    NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    return preferredLang;
}

+ (NSString*) bundleVersion {
     NSString *versionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    return versionString;
}

+ (BOOL)isNumeric:(NSString *)code{
    NSCharacterSet *_NumericOnly = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *myStringSet = [NSCharacterSet characterSetWithCharactersInString: code];
    
    if ([_NumericOnly isSupersetOfSet: myStringSet])
    {
        return YES;
    }
    return NO;
}

+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}




@end

@implementation Utility (Date)

+ (NSString *)timeStringFromFormat : (NSString *)format withTI : (double)ti {
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat: format];
    NSDate *now = [NSDate dateWithTimeIntervalSince1970: ti];
    NSString *timeStr = [formater stringFromDate: now];
    return timeStr;
}



+ (NSDate *) dateFromDateStr: (NSString *) str withFormat: (NSString *) format {
    if (str == nil || format == nil) {
        return nil;
    }

    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat: format];
//    [formater setTimeZone:DefaultTimeZone];
    NSDate *date = [formater dateFromString: str];
    return date;
}

+ (NSString *) timeStringFromFormat: (NSString *) format withDate: (NSDate *) date {
    if (date == nil || format == nil) {
        return nil;
    }
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat: format];
    NSString *timeStr = [formater stringFromDate: date];
    return timeStr;
}

+ (NSString *) literalStringTI: (double) ti {
    NSDate *now = [NSDate date];
    NSDate *aDate = [Utility dateFromTI: ti];
    NSString *nowString = [[now description] substringToIndex: 10];
    NSString *aDateString = [[aDate description] substringToIndex: 10];
    if ([nowString isEqualToString: aDateString]) {
        return NSLocalizedString(@"today", "today text");
    } else {
        return [Utility timeStringFromFormat: @"MM-dd" withTI: ti];
    }
    return nil;
}

+ (NSTimeInterval) curTimeinterval {
    NSDate *date = [NSDate date];
    NSTimeInterval ti = [date timeIntervalSince1970];
    return ti;
}

+ (NSString *) curTimeintervalStr {
    return FormatStr(@"%.0f", [Utility curTimeinterval]);
}

+ (NSDate *) dateFromTI: (NSTimeInterval) ti {
    return [NSDate dateWithTimeIntervalSince1970: ti];
}

+ (double) timeintervalSinceTI: (NSTimeInterval) ti {
    NSDate *date = [NSDate date];
    NSTimeInterval nowti = [date timeIntervalSince1970];
    return nowti - ti;
}

+ (BOOL) isValidURLString: (NSString *) urlStr {
    if ([urlStr rangeOfString: @"http" options: NSCaseInsensitiveSearch].location != NSNotFound) {
        return YES;
    }
    return NO;
}

+ (double) timeintervalForDate: (NSDate *) date {
    return [date timeIntervalSince1970];
}

+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

@end
