//
//  UUITextFile.m
//  ScoreApprove
//
//  Created by Score Approve on 15/3/21.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

#import "UUITextFile.h"

@implementation UUITextFile


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 15, self.frame.size.height)];
        self.leftView = lb;
        self.leftViewMode = UITextFieldViewModeAlways;

    }
    return self;
}



-(void)didMoveToSuperview{

    if (self.superview) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 15, self.frame.size.height)];
        self.leftView = lb;
        self.leftViewMode = UITextFieldViewModeAlways;
    }
}
@end
