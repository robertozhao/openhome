//
//  Utility.h
//
//  Created by Michael on 9/9/12.
//  Copyright 2011 AMP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject
    + (NSString *)hashIDForString : (NSString *)str;
//+(BOOL)isIpad;
//+(NSString *)stringFromMD5 : (NSString *)str;
+(NSString *)urlEncode : (NSString *)string;
//+(UIColor *)colorFromR : (CGFloat) rv g : (CGFloat) gv b : (CGFloat)bv;
+ (NSString *) legalString: (id) str;
+(BOOL)verifyString : (NSString *)str withRegex : (NSString *)regex;
+(BOOL)isPortrait;
+(BOOL)isIOS7;
+(BOOL)isIOS7Later;
+(BOOL)isIOS8Later;
//+(BOOL)isIOS6;
//+(BOOL)isIOS5;
+(BOOL)isLegalURL : (NSString *)urlStr;
+(BOOL)isLegalString : (NSString *)str;
+(BOOL)isLegalEmailAddress : (NSString *)emailStr;
+(BOOL)isLegalPassword : (NSString *)pwd;
+(NSString *)showPrice : (float)price;
+ (BOOL) is4Screen;
+ (NSString *) curLanguage;
+ (NSString*) bundleVersion;
+ (BOOL)isNumeric:(NSString *)code;

+ (BOOL)isMobileNumber:(NSString *)mobileNum;

@end


@interface Utility (Date)
+ (NSString *)timeStringFromFormat : (NSString *)format withTI : (double)ti;
+(NSString *)timeStringFromFormat : (NSString *)format withDate : (NSDate *)date;
+(NSString *)literalStringTI : (double)ti;

+(double)curTimeinterval;
+(NSString *)curTimeintervalStr;
+(NSDate *)dateFromTI : (NSTimeInterval)ti;
+(double)timeintervalSinceTI : (NSTimeInterval)ti;
+(BOOL)isValidURLString : (NSString *)urlStr;
+(double)timeintervalForDate : (NSDate *)date;
+ (NSDate *) dateFromDateStr: (NSString *) str withFormat: (NSString *) format;

+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
