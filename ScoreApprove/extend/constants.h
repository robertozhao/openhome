//
//  constants.h
//  Score Approve
//
//  Created by Score Approve on 1/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//


//TODO: update to https1


#define Production @"product"



#define SplashPageDuration 2

#define X_API_KEY   @"scoreapprove"

#define DefaultTimeZone [NSTimeZone timeZoneWithName: @"America/New_York"]

#define ExtraID  @"extraID"

#define QuestionBaseId  -1
#define QuestionID   @"QuestionID"

#define AnswerBaseId  -1
#define AnswerID   @"AnswerID"


#define ListBaseId  -1
#define ListID   @"ListID"


#define LeadBaseId  -1
#define LeadID   @"LeadID"

#define PictureBaseId  -1
#define PictureID   @"PictureID"

#define LeadNoteBaseId  -1
#define LeadNoteID   @"LeadID"

#define  UserInLocalKey  @"UserInLocalKey"
#define SyncLeadOnAddressList  @"SyncLeadOnAddressList"


