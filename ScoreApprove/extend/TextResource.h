//
//  TextResource.h
//  Score Approve
//
//  Created by Score Approve on 1/25/13.
//  Copyright (c) 2015 Score Approve. All rights reserved.
//

//#define test

#define NotificationTitlt       @"Notification"
#define NotificationMessage     @"Open House schedule for %@"

#define NetWorkWrong            @"Server error encounter, please try again later"
#define AlertForInputEmail      @"Please input Email"
#define AlertForInputPwd        @"Please input password"
#define AlertForInputFirstName  @"Please input first Name"
#define AlertForInputLastName   @"Please input last Name"
#define AlertForInputPhone      @"Please input phone number"
#define AlertForInputUserName   @"Please input username"
#define AlertForInputRePwd      @"Please input retype password"

#define AlertForNetWorkOff      @"Network is not connected"

#define AlertForInputTwoPwdWrong @"Password not match"
#define OffLineLoginInFail       @"Wrong username and password combination"

#define ListListTitleAll        @"All Listings"
#define ListListTitleForSale    @"Listings For Sale"
#define ListListTitleForInContract @"Listings In Contract"
#define ListListTitleForSold    @"Listings Sold"
#define LeadListTitleAll        @"All Leads"
#define LeadListTitleRecent     @"Recent Leads"
#define LeadListTitleHot        @"Hot Leads"

#define LeadFilterFlag          @"Leads for %@"

#define MasterTbListSectionTitle    @"Listings"
#define MasterTbLeadSectionTitle    @"Leads"
#define MasterTbListAll             @"All"
#define MasterTbListForSale         @"For Sale"
#define MasterTbListForInContract   @"In Contract"
#define MasterTbListForSold         @"Sold"

#define MasterTBList        @"Listings"
#define MasterTBLeads       @"Leads"
#define MasterTBQuestions   @"Questions"
#define MasterTBAccount     @"Account"
#define MasterTBEmail       @"Email"
#define MasterTBSync        @"Sync"
#define MasterTBHelp        @"Help"
#define MasterTBLogout      @"Logout"

#define MasterTbList        @"Listings"
#define MasterTbLead        @"Leads"
#define MasterTbQuestions   @"Questions"
#define MasterTbAccount     @"Account"
#define MasterTbEmail       @"Email"
#define MasterTbSync        @"Sync"
#define MasterTbHelp        @"Help"
#define MasterTbLogout      @"Logout"

#define MasterTbLeadAll @"All"
#define PhotoEditAdd    @"+"

#define ListFilterClean     @"Clear Filter"

#define EmailCanNotUse          @"No Email Account"
#define EmailCanNotUseMsg       @"There is no email account configured.You can add an email account in Setting"
#define  CSVFileCreatFial       @"File creat failure"
#define LeadExportSubject       @"Leads export from Score Approve"
#define CameraCanNotUse         @"Camera can not use"

#define ButtonTitleDismiss      @"Dismiss"
#define ButtonTitleNo           @"No"
#define ButtonTitleYes          @"Yes"
#define ButtonTitleCancel       @"Cancel"
#define ButtonTitleDone         @"Done"
#define ButtonTitleContinue     @"Continue"
#define PopUpLoadingViewText    @"Loading..."
#define ButtonTitleSubmit       @"Submit..."
#define ButtonTitlePrice        @"Price"
#define ButtonTitleSave         @"Save"
#define ActionTitleCamera            @"Take Photo"
#define ActionTitlePhotoLibrary      @"Photo Library"

#define VerfiyEmail             @"Verfiy..."

#define AlertTitleNotice        @"Notice"
#define AlertMsgForLeavingApp   @"You are now leaving the app and being redirected to your browser to complete this action"

#define AlertMsgForPwdDigits    @"Invalid password"
#define AlertMsgForPhoneDigit   @"Please type valid phone number"

#define ManageQuestion  @"Manage Question"
#define Note            @"Note"
#define Add             @"Add"
#define Address         @"Address"
#define NewList         @"New Listing"
#define Description     @"Description"

#define Exit        @"Exit"
#define Back        @"Back"
#define Size        @"Size"
#define Phone       @"Phone"
#define Name        @"Name"
#define Email       @"Email"
#define EmailFormatWrong        @"Please type valid email"
#define AddListTfPlaceHold      @"Address of listing..."

#define AddListCodePlaceHold        @"Postal or Zip Code..."
#define ButtonTitleAddList          @"Create Listing"

#define AddQuestionTitle                @"What question will you ask visitors?"
#define AddQuestionAnserTypeChoose      @"How would you like your question answered?"
#define AddQuestionAnserTypePlainText   @"Plain Text"
#define AddQuestionAnserTypeYesOrNo     @"Yes or No"
#define AddQuestionAnserTypeMultipleChoice @"Multiple Choice"
#define AddQuestionAnserChooseTitle     @"Please add at least 2 answer options"
#define AddQuestionAnserChooseBtton     @"Add Option"

#define Active          @"Active"
#define InActive        @"Inactive"

#define AddQuestionQuestionTfPlaceHold  @"Question"

#define  AlertMsgForNotCodeForAddlist   @"Street and Zip Code cannot be empty"

#define AlertMsgForNoAddress            @"Please enter a listing address to continue"

#define AddLeadForSignIn                    @"Please Sign In"
#define AddLeadSuccessAlert                 @"Thanks for your visitor !"
#define PhoneNumPlaceHoldeInLeadDetail      @"phone number"
#define LeadAddNotePlaceHodeInLeaddetal     @"Add visitor notes"
#define LeadNoAnswerPlaceHodeInLeaddetal    @"No Answers Collected."

#define ScheduleScoreApproce    @"Schedule Open House"

#define ListDesctiptionPlaceHold @"Tap to edit description"

#define QuestionScanTitle       @"Question %d of %lu"

#define LeadListFilterBtTitlte @"Filter Leads"
#define LeadListExportBtTitlte @"Export Leads"

#define ListListViewForList     @"List View"
#define ListListViewForPhoto    @"Photo View"

#define ListInfoViewBeginOpenHouse  @"Begin open House"

#define LeadExportCSVFileName   @"leads.csv"

//  lead Sync
#define  NoUpdata               @"No Update"
#define  NunLeadDownded         @"%d leads downloaded"
#define  NunLeadPost            @"%d leads posted"
//  contact us
#define ExplanWhyToContactUs    @"Have question about ScoreApprove or your account? We have live representatives available to assist you 7 days a week."
#define CallUs @"Call Us"
#define ContactUsPhone          @"(323) 400-4170"
#define EmailUsMail             @"support@eopenhome.com"
#define ContactUsTime           @"Everyday: 5AM to 9PM PST"
#define MailUs                  @"Mail Us"
#define AppName                 @"ScroeApprove"
#define AddressForUs            @"Walnut,CA 91789"
#define AddressNumForUs         @"340 S Lemon St #8111"
#define ContactUsTitle          @"Contact Us"
#define Beds                    @"Beds"
#define Baths                   @"Baths"

// list statues
#define   ForSale               @"For Sale"
#define InContract              @"In Contract"
#define Sold                    @"Sold"
#define ShortSale               @"Short Sale"
#define Rental                  @"Rental"
#define Rented                  @"Rented"
#define Auction                 @"Auction"

// list cate
#define Home                    @"Home"
#define Townhouse               @"Townhouse"
#define Condo                   @"Condo"
#define Coop                    @"Coop"
#define SingleFamily            @"Single-Family"
#define LotLand                 @"Lot/Land"
#define TIC                     @"TIC"
#define Loft                    @"Loft"
// area unit
#define Sqmters                 @"sqmters"
#define Sqft                    @"sqft"


// photo
#define PhotoLibrary            @"Add Photo From Camera Library"
#define Camera                  @"Add Photo From Camera"
#define EditPhoto               @"Edit Photo"
// setting items
#define SettingTilte            @"Your Settings"
#define SettingGetHelp          @"Get Help & Tutorials"
#define SettingManageQuestion   @"Manage Your Questions"
#define SettingEditAccount      @"Edit Account Settings"
#define SettingSync             @"Sync"
#define SettingLogout           @"Log Out"

#define EndVisitorBtTitle       @"End Open House"
#define AddVisitorNoteBtTitle   @"Add Visitor Notes"
#define TextPhotoEditTitle      @"Photos"
#define TextInputFormatNotCorrect @"The input format is not correct"
#define CustomerIncomeNotEnoughForCertificate    @"Based on the income information you\'ve provided, we\'re unable to provide you a home financing certificate"
#define TextDebtsError          @"This amount is high. Your debt can't be higher than your income. Make sure you didn't include your current rent or mortgage, estimated payments toward the new mortgage you are seeking, or credit card balances that you pay off in full each month"

#define TextDebtsWarm  @"This amount is high. Make sure you didn't include your current rent or mortgage, estimated payments toward the new mortgage you are seeking, or credit card balances that you pay off in full each month"

#define LeadCreatSuccess  @"Thank You For Registering"

#define  RequireAlert @"This is required"
